const initialize = ({ models }) => ({
  async execute(manuscriptId: string) {
    const { File } = models;
    const reviewFiles = await File.findAllPublicReviewFilesByManuscript({
      manuscriptId,
    });
    const figureFiles = await File.findAllByManuscriptAndType({
      manuscriptId,
      type: File.Types.figure,
    });
    const supplementalFiles = await File.findAllByManuscriptAndType({
      manuscriptId,
      type: File.Types.supplementary,
    });
    const manuscriptFiles = await File.findAllByManuscriptAndType({
      manuscriptId,
      type: File.Types.manuscript,
    });
    const coverLetterFiles = await File.findAllByManuscriptAndType({
      manuscriptId,
      type: File.Types.coverLetter,
    });

    return {
      reviewFiles,
      figureFiles,
      manuscriptFiles,
      coverLetterFiles,
      supplementalFiles,
    };
  },
});

export { initialize };
