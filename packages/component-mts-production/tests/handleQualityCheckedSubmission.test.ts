import {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  generateSubmission,
  getManuscriptStatuses,
} from 'component-generators'

const useCases = require('../src/useCases')

const { handleQualityCheckedSubmissionUseCase } = useCases

interface ManuscriptType {
  find(): Promise<object>
  Statuses: object
  findLastManuscriptBySubmissionId(): Promise<object>
  getFinalRevisionDate(): Promise<object>
  getPeerReviewPassedDate(): Promise<object>
}

const Manuscript: ManuscriptType = {
  find: jest.fn(),
  findLastManuscriptBySubmissionId: jest.fn(),
  Statuses: getManuscriptStatuses(),
  getFinalRevisionDate: jest.fn(),
  getPeerReviewPassedDate: jest.fn(),
}

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript,
}

const applicationEventBus = {
  publishMessage: jest.fn(),
}
const mockedUseCases = {
  getDataForSubmittedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getDataForPeerReviewedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getFilesForFtpUploadUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
}

jest.mock('../src/eventMapper', () => ({
  createPpEventData: jest.fn(),
  createMTSAcceptedEventData: jest.fn(),
}))

describe('handle Submission Quality Check Passed event use case', () => {
  beforeEach(() => {})

  it('sends the package to Production if the manuscript is the latest version', async () => {
    const journal = generateJournal()
    const submission = generateSubmission({
      noOfVersions: 2,
      generateManuscript,
      statuses: getManuscriptStatuses(),
      props: { journal },
    })

    const manuscript = submission[submission.length - 1]

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)

    await handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        applicationEventBus,
      })
      .execute({
        submissionId: manuscript.submissionId,
        manuscripts: [manuscript],
        qc: {},
        qcLeaders: [],
        es: {},
        esLeaders: [],
      })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(2)
  })
  it('throws an error if manuscript is not found', async () => {
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(undefined)

    const result = handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        applicationEventBus: jest.fn(),
      })
      .execute({
        manuscripts: [{ id: 'some-id' }],
        qc: {},
        qcLeaders: [],
        es: {},
        esLeaders: [],
        submissionId: 'some-id',
      })

    await expect(result).rejects.toThrowError(
      'Manuscript with id some-id not found',
    )
  })
})
