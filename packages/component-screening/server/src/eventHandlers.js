const useCases = require('./useCases')
const models = require('@pubsweet/models')

const config = require('config')
const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const urlService = require('./urlService/urlService')
const events = require('component-events')
const { useCases: mtsProductionUseCases } = require('component-mts-production')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const approveEQSNotifications = require('./notifications/approveEQS')
const getEmailCopy = require('./notifications/getEmailCopy')
const getProps = require('./notifications/getProps')
const { repos } = require('component-model')
const { transaction } = require('objection')

const {
  useCases: {
    getUserWithWorkloadUseCase,
    getUserWithWorkloadForEditorialConflictsUseCase,
  },
} = require('component-model')

module.exports = {
  async TriggeredPeerReview(data) {
    const eventsService = events.initialize({ models })
    const getEmailCopyService = getEmailCopy.initialize()
    const getPropsService = getProps.initialize({
      baseUrl,
      urlService,
      footerText,
      unsubscribeSlug,
      getModifiedText,
    })
    const notificationService = approveEQSNotifications.initialize({
      Email,
      getEmailCopyService,
      getPropsService,
    })
    await useCases.setPeerReviewEditorialMappingUseCase
      .initialize({
        repos,
        models,
        transaction,
      })
      .execute({
        submissionId: data.submissionId,
        peerReviewEditorialModel: data.editorialModel,
      })

    // #endregion
    return useCases.startPeerReviewUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        notificationService,
        getUserWithWorkloadUseCase,
        getUserWithWorkloadForEditorialConflictsUseCase,
      })
      .execute({ data })
  },
  async SubmissionScreeningRTCd(data) {
    await useCases.declineEQSByEventUseCase
      .initialize({ Manuscript: models.Manuscript, logEvent })
      .execute(data.submissionId)

    await mtsProductionUseCases.handleRejectedSubmissionUseCase
      .initialize({
        models,
        useCases: mtsProductionUseCases,
        applicationEventBus,
      })
      .execute(data)
  },
  async SubmissionScreeningReturnedToDraft(data) {
    return useCases.returnToDraftUseCase
      .initialize({ models, logEvent })
      .execute({ data })
  },
  async SubmissionScreeningVoid(data) {
    return useCases.voidManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({ data })
  },
}
