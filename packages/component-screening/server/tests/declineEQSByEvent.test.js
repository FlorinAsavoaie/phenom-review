const Chance = require('chance')

const { models, fixtures } = require('fixture-service')
const { declineEQSByEventUseCase } = require('../src/useCases')

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})

logEvent.actions = { eqs_declined: 'eqs_declined' }
logEvent.objectType = { manuscript: 'manuscript' }
describe('Decline EQS use case', () => {
  it('sets hasPassedEQS to false and sets "refusedToConsider" status', async () => {
    const { Manuscript } = models
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: { technicalCheckToken: token, hasPassedEqs: null },
      Manuscript,
    })

    await declineEQSByEventUseCase
      .initialize({ Manuscript: models.Manuscript, logEvent })
      .execute(manuscript.submissionId)
    expect(manuscript.hasPassedEqs).toBeFalsy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.refusedToConsider)
  })
})
