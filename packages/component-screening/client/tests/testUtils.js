import React from 'react'
import { theme } from '@hindawi/ui'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender } from '@testing-library/react'

export const render = ui => {
  const Component = () => <ThemeProvider theme={theme}>{ui}</ThemeProvider>

  return rtlRender(<Component />)
}
