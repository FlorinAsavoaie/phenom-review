import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import merge from 'lodash/merge'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = merge({}, defaultValues, {
  secretNames: ['automation/review/app'],
  serviceProps: {
    image: {
      repository: '916437579680.dkr.ecr.eu-west-1.amazonaws.com/review-app',
      tag: 'latest',
    },
    ingressOptions: {
      rules: [
        {
          host: 'review.automation.phenom.pub',
        },
      ],
    },
  },
})
