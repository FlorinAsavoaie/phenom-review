import { WithAwsSecretsServiceProps, ServiceType } from '@hindawi/phenom-charts'

const appoloProbePath = '/.well-known/apollo/server-health'

const defaultValues: WithAwsSecretsServiceProps = {
  secretNames: [],
  serviceProps: {
    image: {
      repository: '',
      tag: 'latest',
    },
    replicaCount: 1,
    service: {
      port: 80,
      type: ServiceType.NODE_PORT,
    },
    containerPort: 3000,
    labels: {
      owner: 'Rocket',
    },
    livenessProbe: {
      path: appoloProbePath,
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    readinessProbe: {
      path: appoloProbePath,
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    startupProbe: {
      path: appoloProbePath,
      failureThreshold: 30,
      periodSeconds: 10,
    },
    resources: {
      requests: {
        memory: '1400Mi',
      },
    },
    ingressOptions: {
      annotations: {
        'nginx.ingress.kubernetes.io/proxy-body-size': '0',
      },
      rules: [],
    },
  },
}

export { defaultValues }
