/* eslint-disable no-await-in-loop */
const Chance = require('chance')

const chance = new Chance()
const uuid = require('uuid')
const Promise = require('bluebird')

module.exports = {
  createSubmission: async ({
    knex,
    journalId,
    articleTypeId,
    latestVersionStatus,
  }) => {
    const submissionId = chance.guid()

    return knex('manuscript')
      .insert([
        {
          submissionId,
          status: latestVersionStatus,
          version: '2',
          title: chance.sentence(),
          abstract: chance.paragraph(),
          journalId,
          agreeTc: true,
          articleTypeId,
        },
        {
          submissionId,
          status: 'olderVersion',
          version: '1',
          title: chance.sentence(),
          abstract: chance.paragraph(),
          journalId,
          agreeTc: true,
          articleTypeId,
        },
      ])
      .returning('*')
      .then(submission => submission)
  },
  createUser: async ({ knex }) => {
    const userId = uuid.v4()
    const user = await knex('user')
      .insert([
        {
          id: userId,
          agreeTc: true,
          isActive: true,
          isSubscribedToEmails: true,
          unsubscribeToken: uuid.v4(),
          defaultIdentityType: 'local',
        },
      ])
      .returning('*')
      .then(user => user[0])

    const identity = await knex('identity')
      .insert([
        {
          userId,
          type: 'local',
          isConfirmed: true,
          email: chance.email(),
          givenNames: chance.first(),
          surname: chance.last(),
          title: 'dr',
          aff: chance.company(),
          country: chance.country(),
        },
      ])
      .returning('*')
      .then(identity => identity[0])
    return { user, identity }
  },
  createTeam: async ({
    knex,
    role,
    journalId,
    manuscriptId,
    specialIssueId,
  }) => {
    const teamId = uuid.v4()
    return knex('team')
      .insert([
        {
          id: teamId,
          role,
          journalId,
          manuscriptId,
          specialIssueId,
        },
      ])
      .returning('*')
      .then(team => team[0])
  },
  createTeamMember: async ({ knex, teamId, userId, status }) => {
    const identities = await knex.select().table('identity')
    const userIdentity = identities.find(identity => identity.userId === userId)

    const alias = {
      given_names: userIdentity.givenNames,
      surname: userIdentity.surname,
      title: userIdentity.title,
      aff: userIdentity.aff,
      country: userIdentity.country,
      email: userIdentity.email,
    }
    return knex('team_member')
      .insert([
        {
          alias,
          teamId,
          userId,
          status,
        },
      ])
      .returning('*')
      .then(tm => tm[0])
  },
  createUsers: async ({ knex, numberOfUsers }) => {
    const users = []
    for (let i = 0; i < numberOfUsers; i += 1) {
      const userId = uuid.v4()
      const userInsertion = await knex('user')
        .insert([
          {
            id: userId,
            defaultIdentityType: 'local',
            unsubscribeToken: uuid.v4(),
            agreeTc: true,
            isActive: true,
            isSubscribedToEmails: true,
          },
        ])
        .returning('*')
      const user = userInsertion[0]

      const identityInsertion = await knex('identity')
        .insert([
          {
            userId,
            type: 'local',
            isConfirmed: true,
            email: chance.email(),
            givenNames: chance.first(),
            surname: chance.last(),
            title: 'dr',
            aff: chance.company(),
            country: chance.country(),
          },
        ])
        .returning('*')
      const identity = identityInsertion[0]
      user.identity = identity

      users.push(user)
    }

    return users
  },
  createTeams: async ({ knex, journal }) => {
    const roles = ['academicEditor', 'triageEditor', 'editorialAssistant']
    await Promise.each(roles, role => {
      const teamId = uuid.v4()
      return knex('team').insert([
        {
          id: teamId,
          role,
          journalId: journal.id,
        },
      ])
    })
  },
  createTeamMembers: async ({ knex, createUsers }) => {
    const teams = await knex
      .select()
      .table('team')
      .whereNotIn('team.role', ['admin', 'author'])

    const users = await createUsers({ knex, numberOfUsers: 8 })

    let i = 0
    await Promise.each(teams, async team => {
      const alias = {
        given_names: users[i].identity.givenNames,
        surname: users[i].identity.surname,
        title: users[i].identity.title,
        aff: users[i].identity.aff,
        country: users[i].identity.country,
        email: users[i].identity.email,
      }
      await knex('team_member').insert([
        {
          alias,
          teamId: team.id,
          userId: users[i].id,
          status: 'pending',
        },
      ])
      i += 1
    })
  },
  createReview: async ({
    knex,
    teamMemberId,
    manuscriptId,
    recommendation,
  }) => {
    const teamId = uuid.v4()
    return knex('review')
      .insert([
        {
          id: teamId,
          teamMemberId,
          manuscriptId,
          recommendation,
          submitted: new Date().toISOString(),
        },
      ])
      .returning('*')
      .then(review => review[0])
  },
  createSubmissions: async ({
    knex,
    journalId,
    articleTypeId,
    createSubmission,
    numberOfSubmissions,
  }) => {
    const submissions = []
    for (let i = 0; i < numberOfSubmissions; i += 1) {
      const submission = await createSubmission({
        knex,
        journalId,
        articleTypeId,
        latestVersionStatus: 'submitted',
      })
      submissions.push(submission)
    }

    return submissions
  },
}
