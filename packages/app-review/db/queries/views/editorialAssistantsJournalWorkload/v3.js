module.exports.up = `
DROP VIEW IF EXISTS "editorial_assistants_journal_workload";
CREATE OR REPLACE VIEW "editorial_assistants_journal_workload" AS (
  SELECT
TMs."workload",
tm."alias"::json -> 'email' AS EA_email,
j."name" as journal_name,
j."id" as journal_id
FROM
"team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
JOIN "journal" j ON t."journal_id" = j."id"
LEFT JOIN (
    SELECT
        count(tm1."id") AS workload,
        tm1."user_id",
        m."journal_id"
    FROM
        "team_member" tm1
        JOIN "team" t1 ON t1."id" = tm1."team_id"
        JOIN "manuscript" m ON m.id = t1."manuscript_id"
    WHERE
        tm1."status" = 'active'
        AND m."status" in('submitted', 'technicalChecks', 'underReview', 'makeDecision', 'reviewCompleted', 'pendingApproval', 'reviewersInvited', 'revisionRequested', 'academicEditorInvited', 'academicEditorAssigned', 'qualityChecksRequested', 'qualityChecksSubmitted', 'inQA', 'draft')
        AND( m."version"='1'  OR m."status" <> 'draft')
        AND m."submitted_date" IS NOT NULL
        AND t1."manuscript_id" IS NOT NULL
        AND t1."role" = 'editorialAssistant'
    GROUP BY
        tm1."user_id",
        m."journal_id") AS TMs ON TMs."user_id" = tm."user_id"
AND j."id" = TMs."journal_id"
WHERE
t."role" = 'editorialAssistant'
AND j."is_active" = TRUE
GROUP BY
j."id",
j."name",
tm."id",
TMs."workload",
TMs."journal_id"
ORDER BY
j."name"
);
`
module.exports.down = 'DROP VIEW "editorial_assistants_journal_workload";'
