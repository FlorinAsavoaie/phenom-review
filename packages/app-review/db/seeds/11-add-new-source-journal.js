const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'gsw') {
    await knex('source_journal').insert([
      {
        name: 'The Geological Society Special Publications',
        pissn: '2041-479X',
      },
    ])
  }
}
