const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    await knex('source_journal').insert([
      {
        name: 'Complexity',
        eissn: '1099-0526',
        publisher: 'Hindawi',
      },
    ])
  }
}
