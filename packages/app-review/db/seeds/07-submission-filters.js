exports.seed = async knex => {
  // Deletes ALL existing entries
  await knex('submission_filter').del()
  await knex('submission_filter').insert([
    { name: 'needsAttention' },
    { name: 'inProgress' },
    { name: 'archived' },
  ])
}
