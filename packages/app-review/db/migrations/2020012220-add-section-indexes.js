const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('section', table => {
      table.index('journal_id', null, 'hash')
      table.index('name', null, 'hash')
    })
    .then(() => logger.info('section indexes added successfully'))
