const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .createTable('source_journal', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table.string('name').notNullable()
      table.string('issn')
    })
    .then(_ => logger.info('source_journal table created successfully'))
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .dropTable('source_journal')
    .then(_ => logger.info('source_journal table dropped successfully'))
    .catch(err => logger.error(err))
