const TABLE_NAME = 'file'
const COLUMN_NAME = 'scanning_start_time'

module.exports = {
  up: async knex => {
    await knex.schema.table(TABLE_NAME, s => {
      s.timestamp(COLUMN_NAME).defaultTo(null)
    })
  },
  down: async knex =>
    knex.schema.table(TABLE_NAME, s => {
      s.dropColumn(COLUMN_NAME)
    }),
}
