const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .createTable('available_custom_ids', table => {
      table.string('custom_id').notNullable()
    })
    .then(_ => logger.info('available_custom_ids table created successfully'))
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .dropTable('available_custom_ids')
    .then(_ => logger.info('available_custom_ids table dropped successfully'))
    .catch(err => logger.error(err))
