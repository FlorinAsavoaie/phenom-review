DELETE from special_issue;

ALTER TABLE special_issue
  ADD COLUMN call_for_papers text NOT NULL;