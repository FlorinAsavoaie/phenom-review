exports.up = async knex => {
  try {
    await knex.schema.table('peer_review_model', table => {
      table.renameColumn(
        'reviewer_assignement_tool',
        'reviewer_assignment_tool',
      )
    })
  } catch (e) {
    throw new Error(e)
  }
}

exports.down = knex =>
  knex.schema.table('peer_review_model', table => {
    table.renameColumn('reviewer_assignment_tool', 'reviewer_assignement_tool')
  })
