ALTER TABLE manuscript
  ADD COLUMN is_post_acceptance boolean DEFAULT false;