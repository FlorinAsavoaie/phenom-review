const TABLE_NAME = 'file'
const COLUMN_NAME = 'scan_status'

module.exports = {
  up: async knex => {
    await knex.schema.table(TABLE_NAME, s => {
      s.string(COLUMN_NAME).defaultTo('skipped')
    })
  },
  down: async knex =>
    knex.schema.table(TABLE_NAME, s => {
      s.dropColumn(COLUMN_NAME)
    }),
}
