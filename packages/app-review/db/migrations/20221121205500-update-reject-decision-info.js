const TABLE_NAME = 'reject_decision_info'
const COLUMN_NAME = 'lack_of_novelty'

module.exports = {
  up: async knex => {
    await knex.schema.table(TABLE_NAME, s => {
      s.boolean(COLUMN_NAME)
    })
  },
  down: async knex =>
    knex.schema.table(TABLE_NAME, s => {
      s.dropColumn(COLUMN_NAME)
    }),
}
