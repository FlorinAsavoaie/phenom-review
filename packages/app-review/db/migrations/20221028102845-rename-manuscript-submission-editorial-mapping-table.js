const { logger } = require('component-logger')

exports.up = function(knex) {
  return knex
    .raw(
      `
      ALTER TABLE manuscript_submission_editorial_mapping RENAME TO submission_editorial_mapping;
    `,
    )
    .then(_ =>
      logger.info(
        'manuscript_submission_editorial_mapping table renamed to submission_editorial_mapping successfully',
      ),
    )
    .catch(err => logger.error(err))
}

exports.down = function(knex) {
  return knex
    .raw(
      `
      ALTER TABLE submission_editorial_mapping RENAME TO manuscript_submission_editorial_mapping;
    `,
    )
    .then(_ =>
      logger.info(
        'submission_editorial_mapping table renamed to manuscript_submission_editorial_mapping successfully',
      ),
    )
    .catch(err => logger.error(err))
}
