const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('journal_article_type', table => {
      table.index('journal_id', null, 'hash')
      table.index('article_type_id', null, 'hash')
    })
    .then(() => logger.info('journal_article_type indexes added successfully'))
