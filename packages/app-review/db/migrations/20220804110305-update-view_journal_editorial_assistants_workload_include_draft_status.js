const path = require('path')

const v2 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistantsJournalWorkload/v2.js',
))

// included draft status and checked for version=1
//  this will exclude draft manuscripts resulted from revisionRequested (including those manuscripts, would generate a bigger workload )
// and include draft manuscripts that are returned to draft
const v3 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistantsJournalWorkload/v3.js',
))

module.exports.up = async knex => {
  await knex.raw(v2.down)
  await knex.raw(v3.up)
}

module.exports.down = async knex => {
  await knex.raw(v3.down)
}
