module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', t => {
      t.timestamp('peerReviewPassedDate')
    }),
  down: async knex =>
    knex.schema.table('manuscript', t => {
      t.dropColumn('peerReviewPassedDate')
    }),
}
