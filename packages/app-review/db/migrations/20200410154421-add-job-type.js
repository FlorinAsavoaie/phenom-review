const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('job', t => {
      t.string('job_type')
    })
    .then(_ => logger.info('job_type column successfully added on job table'))
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .table('job', table => {
      table.dropColumn('job_type')
    })
    .then(_ =>
      logger.info('job_type column successfully dropped from job table'),
    )
    .catch(err => logger.error(err))
