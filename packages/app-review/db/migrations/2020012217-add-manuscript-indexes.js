const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('manuscript', table => {
      table.index('journal_id', null, 'hash')
      table.index('article_type_id', null, 'hash')
      table.index('section_id', null, 'hash')
      table.index('special_issue_id', null, 'hash')
      table.index(
        'submission_id',
        'manuscript_submission_id_hash_index',
        'hash',
      )
      table.index('status', null, 'hash')
      table.index('updated', null)
      table.index('submission_id', null)
    })
    .then(() => logger.info('manuscript indexes added successfully'))
