module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', m => {
      m.uuid('source_journal_id')
      m.foreign('source_journal_id').references('source_journal.id')
    }),
  down: async knex =>
    knex.schema.table('manuscript', m => {
      m.dropColumn('source_journal_id')
    }),
}
