module.exports = {
  up: async knex =>
    knex.schema.table('identity', m => {
      m.string('aff_ror_id')
    }),
  down: async knex =>
    knex.schema.table('identity', m => {
      m.dropColumn('aff_ror_id')
    }),
}
