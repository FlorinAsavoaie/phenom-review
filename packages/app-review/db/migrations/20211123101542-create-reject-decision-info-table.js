async function up(knex) {
  await knex.schema.createTable('reject_decision_info', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())

    table.boolean('out_of_scope').notNullable()
    table.boolean('technical_or_scientific_flaws').notNullable()
    table.boolean('publication_ethics_concerns').notNullable()
    table.text('otherReasons')
    table.boolean('transfer_to_another_journal').notNullable()
    table.text('transfer_suggestions')
  })

  await knex.schema.table('review', table => {
    table.uuid('reject_decision_info_id')
    table
      .foreign('reject_decision_info_id')
      .references('reject_decision_info.id')
  })
}

async function down(knex) {
  await knex.schema.table('review', table => {
    table.dropColumn('reject_decision_info_id')
  })
  await knex.schema.dropTable('reject_decision_info')
}

// TODO: should be removed when down method will be supported by pubsweet:migrate
async function upWithRevert(knex) {
  return new Promise(async (resolve, reject) => {
    try {
      await up(knex)
      resolve()
    } catch (err) {
      await down(knex)
      reject(err)
    }
  })
}

module.exports = {
  up: upWithRevert,
  down,
}
