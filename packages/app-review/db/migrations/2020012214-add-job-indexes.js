const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('job', table => {
      table.index('team_member_id', null, 'hash')
      table.index('manuscript_id', null, 'hash')
      table.index('special_issue_id', null, 'hash')
    })
    .then(() => logger.info('job indexes added successfully'))
