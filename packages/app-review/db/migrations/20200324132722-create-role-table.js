const { logger } = require('component-logger')

exports.up = knex =>
  knex.schema
    .createTable('role', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .string('name')
        .unique()
        .notNullable()
    })
    .then(_ => logger.info('role table created successfully'))
    .catch(err => logger.error(err))

exports.down = knex =>
  knex.schema
    .dropTable('role')
    .then(_ => logger.info('role table dropped successfully'))
    .catch(err => logger.error(err))
