const Chance = require('chance')

const chance = new Chance()
const {
  createTeam,
  createUser,
  createTeamMember,
  createSubmission,
} = require('../seedUtils')

exports.seed = async knex => {
  const journals = await knex.select().table('journal')
  if (journals.length === 0) {
    throw new Error('No journals have been found')
  }
  const journalId = journals[0].id
  const secondJournalId = journals[1].id

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  //  Temporary insert
  await knex('manuscript').insert([
    {
      submissionId: chance.guid(),
      status: 'reviewersInvited',
      version: '1',
      title: chance.sentence(),
      abstract: chance.paragraph(),
      journalId: secondJournalId,
      agreeTc: true,
      articleTypeId,
    },
  ])

  const draftManuscript = await knex('manuscript')
    .insert([
      {
        submissionId: chance.guid(),
        status: 'draft',
        version: '1',
        title: chance.sentence(),
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
      },
    ])
    .returning('*')
    .then(manuscript => manuscript[0])

  const userAndIdentity = await createUser({ knex })

  const authorTeam = await createTeam({
    knex,
    manuscriptId: draftManuscript.id,
    role: 'author',
  })

  await createTeamMember({
    knex,
    teamId: authorTeam.id,
    userId: userAndIdentity.user.id,
    status: 'pending',
  })

  const underReviewSubmission = await createSubmission({
    knex,
    journalId,
    articleTypeId,
    latestVersionStatus: 'underReview',
  })
  const lastVersion = underReviewSubmission.find(
    manuscript => manuscript.status === 'underReview',
  )
  const authorUser = await createUser({ knex })
  const academicEditorUser = await createUser({ knex })
  const triageEditorUser = await createUser({ knex })
  const reviewerUser = await createUser({ knex })
  const reviewer2User = await createUser({ knex })
  const editorialAssistantUser = await await createUser({ knex })

  const authTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'author',
  })
  const academicEditorTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'academicEditor',
  })
  const triageEditorTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'triageEditor',
  })
  const reviewerTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'reviewer',
  })
  const journalEditorialAssistantTeam = await createTeam({
    knex,
    journalId,
    role: 'editorialAssistant',
  })
  const manuscriptEditorialAssistantTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'editorialAssistant',
  })

  await createTeamMember({
    knex,
    teamId: authTeam.id,
    userId: authorUser.user.id,
    status: 'pending',
  })
  await createTeamMember({
    knex,
    teamId: triageEditorTeam.id,
    userId: triageEditorUser.user.id,
    status: 'active',
  })
  await createTeamMember({
    knex,
    teamId: academicEditorTeam.id,
    userId: academicEditorUser.user.id,
    status: 'accepted',
  })
  await createTeamMember({
    knex,
    teamId: reviewerTeam.id,
    userId: reviewerUser.user.id,
    status: 'declined',
  })
  await createTeamMember({
    knex,
    teamId: reviewerTeam.id,
    userId: reviewer2User.user.id,
    status: 'accepted',
  })
  await createTeamMember({
    knex,
    teamId: journalEditorialAssistantTeam.id,
    userId: editorialAssistantUser.user.id,
    status: 'pending',
  })
  await createTeamMember({
    knex,
    teamId: manuscriptEditorialAssistantTeam.id,
    userId: editorialAssistantUser.user.id,
    status: 'active',
  })

  await createSubmission({
    knex,
    journalId,
    articleTypeId,
    latestVersionStatus: 'draft',
  })
}
