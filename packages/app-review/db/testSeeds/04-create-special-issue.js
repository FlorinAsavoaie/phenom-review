const Chance = require('chance')

const chance = new Chance()

exports.seed = async knex => {
  const journals = await knex.select().table('journal')
  if (journals.length === 0) {
    throw new Error('No journals have been found')
  }
  const peerReviewModels = await knex.select().table('peer_review_model')
  if (peerReviewModels.length === 0) {
    throw new Error('No peerReviewModels have been found')
  }

  const peerReviewModelId = peerReviewModels[0].id
  const journalId = journals[0].id

  await knex('special_issue').insert([
    {
      name: chance.company(),
      journalId,
      customId: '111222',
      isActive: true,
      isCancelled: false,
      peerReviewModelId,
      startDate: new Date().toISOString(),
      endDate: new Date().toISOString(),
      callForPapers: chance.sentence(),
    },
  ])
}
