const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const include = require('./babel-includes')

module.exports = [
  {
    oneOf: [
      // ES6 JS
      {
        test: /\.(tsx?|jsx?)$/,
        include,
        loader: 'babel-loader',
        options: {
          presets: [
            require('@babel/preset-env'),
            require('@babel/preset-react'),
            require('@babel/preset-typescript'),
          ],
        },
      },

      // CSS Modules
      {
        test: /\.local\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              modules: true,
              localIdentName: '[name]_[local]-[hash:base64:8]',
            },
          },
          'css-loader',
        ],
      },

      // global CSS
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
        ],
      },

      // files
      {
        exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ],
  },
]
