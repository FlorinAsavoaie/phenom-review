process.env.NODE_ENV = 'development'
process.env.BABEL_ENV = 'development'

const path = require('path')
const config = require('config')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

const rules = require('./rules.development')
const resolve = require('./common-resolve')

const htmlWbpackPlugin = new HtmlWebpackPlugin({
  title: config.get('publisherConfig.pageTitle'),
  template: path.join(__dirname, '..', 'app/index.html'),
})
const namedModulesPlugin = new webpack.NamedModulesPlugin()
const hmrReplacementPlugin = new webpack.HotModuleReplacementPlugin()
const noEmitOnErrorsPlugin = new webpack.NoEmitOnErrorsPlugin()
const keycloakPlugin = new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
  'process.env.KEYCLOAK': JSON.stringify(config.get('keycloak.public')),
  'process.env.PUBLISHER_NAME':
    JSON.stringify(process.env.PUBLISHER_NAME) || 'hindawi',
  'process.env.GATEWAY_URI': JSON.stringify(process.env.GATEWAY_URI),
})
const phenomCommunications = new webpack.DefinePlugin({
  'process.env.PHENOM_COMMUNICATIONS_API_URL': JSON.stringify(
    process.env.PHENOM_COMMUNICATIONS_API_URL,
  ),
  'process.env.PHENOM_COMMUNICATIONS_EMAIL_SENDER': JSON.stringify(
    process.env.PHENOM_COMMUNICATIONS_EMAIL_SENDER,
  ),
})

const transferManuscript = new webpack.DefinePlugin({
  'process.env.TRANSFER_MANUSCRIPT_URL': JSON.stringify(
    process.env.TRANSFER_MANUSCRIPT_URL,
  ),
})

const specialIssuePPBK4962 = new webpack.DefinePlugin({
  'process.env.FEATURE_PPBK4962_SPECIAL_ISSUE': JSON.stringify(
    process.env.FEATURE_PPBK4962_SPECIAL_ISSUE,
  ),
})

const editorExpertise = new webpack.DefinePlugin({
  'process.env.EDITOR_EXPERTISE': JSON.stringify(
    config.get('features.EDITOR_EXPERTISE'),
  ),
  'process.env.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY': JSON.parse(
    config.get('features.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY'),
  ),
})

const contextReplacementPlugin = new webpack.ContextReplacementPlugin(
  /./,
  __dirname,
  {
    [config.authsome.mode]: config.authsome.mode,
  },
)

const copyWebpackPlugin = new CopyWebpackPlugin([
  { from: `../static/${process.env.PUBLISHER_NAME}` },
  {
    from: '../app/silent-check-sso.html',
    to: './silent-check-sso.html',
  },
])

const bundleAnalyzerPlugin = new BundleAnalyzerPlugin({ openAnalyzer: false })

const smp = new SpeedMeasurePlugin({
  disable: true, // enable this if you want to see some loading time analysis
})

const wbpckCfg = {
  name: 'app',
  watch: true,
  target: 'web',
  mode: 'development',
  stats: 'none',
  context: path.join(__dirname, '..', 'app'),
  entry: {
    app: ['webpack-hot-middleware/client?reload=true', './app'],
  },
  output: {
    path: path.join(__dirname, '..', '_build', 'assets'),
    filename: '[name].js',
    publicPath: '/assets/',
  },
  devtool: 'eval-source-map', // 'cheap-module-source-map',
  module: {
    rules,
  },
  resolve,
  plugins: [
    htmlWbpackPlugin,
    namedModulesPlugin,
    hmrReplacementPlugin,
    noEmitOnErrorsPlugin,
    keycloakPlugin,
    contextReplacementPlugin,
    copyWebpackPlugin,
    bundleAnalyzerPlugin,
    phenomCommunications,
    editorExpertise,
    transferManuscript,
    specialIssuePPBK4962,
  ],
  node: {
    fs: 'empty',
    __dirname: true,
  },
}

module.exports = [smp.wrap(wbpckCfg)]
