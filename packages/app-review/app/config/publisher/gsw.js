const AUTHOR_INSTRUCTIONS_LINK =
  'https://pubs.geoscienceworld.org/lithosphere/pages/instructions-for-authors'

const REVIEWERS_GUIDELINES_LINK =
  'https://pubs.geoscienceworld.org/lithosphere/pages/reviewer-guidelines'

module.exports = {
  logo: '/assets/logo.png',
  name: 'GeoScienceWorld',
  supportEmail: 'editorial@geoscienceworld.org',
  pageTitle: 'Lithosphere Review',
  links: {
    transferManuscriptLink:
      'https://pubs.geoscienceworld.com/transfer-manuscript',
    websiteLink: 'https://pubs.geoscienceworld.org/',
    privacyLink: 'https://pubs.geoscienceworld.org/pages/privacy-policy',
    termsLink: 'https://pubs.geoscienceworld.org/pages/terms-and-conditions',
    ethicsLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/code-of-publishing-ethics',
    coiLink: AUTHOR_INSTRUCTIONS_LINK,
    dataAvailabilityLink: AUTHOR_INSTRUCTIONS_LINK,
    apcLink:
      'https://pubs.geoscienceworld.org/lithosphere/pages/about-lithosphere',
    authorsGuidelinesLink: AUTHOR_INSTRUCTIONS_LINK,
    journalAuthorsGuidelinesLink: AUTHOR_INSTRUCTIONS_LINK,
    reviewLink: 'https://review.lithosphere.geoscienceworld.org',
    invoicingLink: 'https://invoicing.lithosphere.geoscienceworld.org/',
    authorServiceLink: AUTHOR_INSTRUCTIONS_LINK,
    newGSWSubmissionSystemLink:
      'https://lithosphere.kriyadocs.com/submission/gsw/lithosphere',
  },
  emailData: {
    logo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-lithosphere.png',
    ctaColor: '#63a945',
    logoLink: 'https://pubs.geoscienceworld.org/',
    shortReviewLink: 'review.lithosphere.geoscienceworld.org',
    privacy: `GeoScienceWorld respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://pubs.geoscienceworld.org/pages/privacy-policy">privacy policy</a> for information on how we store, process, and safeguard your data.
      Hindawi respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data.`,
    address:
      'GeoScienceWorld, a nonprofit corporation conducting business in the Commonwealth of Virginia at 1750 Tysons Boulevard, Suite 1500, McLean, Virginia 22102.',
    publisher: 'GeoScienceWorld',
    footerText: {
      unregisteredUsers:
        'This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited.',
      registeredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited.`,
    },
    communicationServiceMailLogo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-lithosphere-round.png',
  },
  recommendationScreenInfoBox: {
    title: 'A reminder that Lithosphere looks for:',
    description: `
      <ul>
        <li>Scientifically rigorous contributions, discussed in the context of existing literature;</li>
        <li>Methods, analyses and statistics appropriate for the aims of the research;</li>
        <li>Conclusions supported by data;</li>
        <li>Adherence to applicable ethics standards;</li>
        <li>Clear written English, so the study cannot be misunderstood.</li>
      </ul>
      <p>Please consider what is relevant to the current study, rather than additional work that would be considered the next step in the study.</p>
      <p>For more information, please read the <a href=${REVIEWERS_GUIDELINES_LINK} target="_blank" rel="noreferrer">Lithosphere Reviewer Guidelines</a>.</p>
      <p>Thank you for your review.</p>`,
  },
  editorFinalDecisionInfoBox: {
    title: '',
    description: '',
  },
  rejectDecisionReasonsTranslations: {
    outOfScope: 'Out of scope',
    technicalOrScientificFlaws: 'Technical/scientific flaws',
    publicationEthicsConcerns: 'Publication ethics concerns',
    lackOfNovelty: 'Lack of novelty or perceived impact',
    otherReasons: 'Other reason(s)',
  },
}
