require('dotenv').config()
const path = require('path')
const { get } = require('lodash')
const components = require('./components.json')
const publishersConfig = require('../app/config/publisher')
const activityLogEvents = require('./activityLog/events')
const activityLogTypes = require('./activityLog/logTypes')
const { logger } = require('component-logger')

Object.assign(global, require('../server/src/helpers/errors'))

const publisherName = process.env.PUBLISHER_NAME || 'hindawi'

const getDbConfig = () => {
  if (process.env.DATABASE) {
    // mode info about these settings right here https://github.com/knex/knex/issues/2820#issuecomment-481710112
    return {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE,
      host: process.env.DB_HOST,
      port: 5432,
      ssl: {
        rejectUnauthorized: false,
      },
      deleteAfterDays: 1, // When jobs in the archive table become eligible for deletion. Default: 7
      newJobCheckIntervalSeconds: 60, // How often subscriptions will poll the queue table for jobs. Available in the constructor as a default or per subscription in subscribe() and onComplete(). Default: 2
      archiveCompletedAfterSeconds: 60 * 60 * 6, // Specifies how long in seconds completed jobs get archived. Note: a warning will be emitted if set to lower than 60s and cron processing will be disabled. Default: 12
      oncomplete: false,
    }
  }
  return {}
}

module.exports = {
  authsome: {
    mode: path.resolve(__dirname, '../server', 'dist', 'authsome-mode.js'),
    teams: {
      academicEditor: {
        name: 'Academic Editors',
      },
      reviewer: {
        name: 'Reviewer',
      },
    },
  },
  pubsweet: {
    components,
  },
  dbManager: {
    migrationsPath: path.join(process.cwd(), 'db/migrations'),
  },

  'pubsweet-server': {
    app: path.join(__dirname, '../server/dist/app-configurator/index.js'),
    db: getDbConfig(),
    pool: {
      // https://github.com/timgit/pg-boss/blob/master/docs/configuration.md
      // https://github.com/vincit/tarn.js/
      min: 0,
      max: parseInt(process.env.MAX_CONNECTION_POOL, 10) || 10,
      createTimeoutMillis: 3000,
      acquireTimeoutMillis: 30000,
      idleTimeoutMillis: 30000,
      reapIntervalMillis: 1000,
      createRetryIntervalMillis: 100,
      // https://stackoverflow.com/questions/40435315/knex-timeout-acquiring-a-connection-the-pool-is-probably-full-are-you-missing
      propagateCreateError: false,
    },
    port: 3000,
    logger,
    uploads: 'uploads',
    secret: 'SECRET',
    enableExperimentalGraphql: true,
    graphiql: true,
    morganLogFormat:
      ':date[iso] trace review_access_logs [referrer=:referrer  method=:method url=:url correlationId=:correlationId graphql-operation=:graphql[operationName] graphql-payload=:graphql[variables] status=:status response-time=:response-time ms] :method request on :url from :referrer with :status response status code',
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    baseUrl: process.env.CLIENT_BASE_URL || 'http://localhost:3000',
    'login-redirect': '/',
  },
  orcid: {
    clientID: process.env.ORCID_CLIENT_ID,
    clientSecret: process.env.ORCID_CLIENT_SECRET,
    callbackPath: '/api/users/orcid/callback',
    successPath: '/profile',
    orcidSandboxEnabled: process.env.ORCID_SANDBOX_ENABLED || false,
    orcidBaseUrl: process.env.ORCID_SANDBOX_ENABLED
      ? 'https://sandbox.orcid.org'
      : 'https://orcid.org',
  },
  keycloak: {
    certs: process.env.KEYCLOAK_CERTIFICATES || 'http://localhost:3000/certs',
    admin: {
      username: process.env.KEYCLOAK_USERNAME,
      password: process.env.KEYCLOAK_PASSWORD,
    },
    public: {
      realm: process.env.KEYCLOAK_REALM,
      clientID: process.env.KEYCLOAK_CLIENTID,
      authServerURL: process.env.KEYCLOAK_SERVER_URL,
    },
  },
  'mail-transport': {
    sendmail: true,
  },
  'password-reset': {
    url:
      process.env.PUBSWEET_PASSWORD_RESET_URL ||
      'http://localhost:3000/profile/change-password',
    sender: process.env.PUBSWEET_PASSWORD_RESET_SENDER || 'dev@example.com',
    hoursUntilNextRequest: 24,
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    quarantineBucket: process.env.AWS_S3_QUARANTINE_BUCKET,
    eventStorage: process.env.AWS_S3_EVENT_STORAGE_BUCKET,
  },
  'pubsweet-component-aws-sqs': {
    secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
    endpoint: process.env.AWS_SQS_ENDPOINT,
    region: process.env.AWS_S3_REGION,
    queueName: process.env.AWS_SQS_QUEUE_NAME,
  },
  'invite-reset-password': {
    url: process.env.PUBSWEET_INVITE_PASSWORD_RESET_URL || '/invite',
  },
  'forgot-password': {
    url: process.env.PUBSWEET_FORGOT_PASSWORD_URL || '/forgot-password',
  },
  'accept-academic-editor': {
    url:
      process.env.PUBSWEET_ACCEPT_ACADEMIC_EDITOR ||
      '/emails/accept-academic-editor',
  },
  'decline-academic-editor': {
    url:
      process.env.PUBSWEET_DECLINE_ACADEMIC_EDITOR ||
      '/emails/decline-academic-editor',
  },
  'invite-reviewer': {
    url: process.env.PUBSWEET_INVITE_REVIEWER_URL || '/invite-reviewer',
  },
  'accept-review-new-user': {
    url:
      process.env.PUBSWEET_ACCEPT_NEW_REVIEWER ||
      '/emails/accept-review-new-user',
  },
  'accept-review': {
    url: process.env.PUBSWEET_ACCEPT_REVIEWER || '/emails/accept-review',
  },
  'decline-review': {
    url: process.env.PUBSWEET_DECLINE_REVIEWER || '/emails/decline-review',
  },
  'confirm-signup': {
    url: process.env.PUBSWEET_CONFIRM_SIGNUP_URL || '/confirm-signup',
  },
  'eqs-decision': {
    url: process.env.PUBSWEET_EQS_DECISION || '/eqs-decision',
  },
  'eqa-decision': {
    url: process.env.PUBSWEET_EQA_DECISION || '/eqa-decision',
  },
  unsubscribe: {
    url: process.env.PUBSWEET_UNSUBSCRIBE_URL || '/unsubscribe',
  },
  mailer: `${__dirname}/${
    JSON.parse(get(process, 'env.EMAIL_SENDING', true))
      ? 'mailer'
      : 'mailer-mock'
  }`,
  publicKeys: ['pubsweet-client', 'authsome', 'orcid.orcidBaseUrl'],
  publons: {
    key: process.env.PUBLONS_KEY || '',
    reviewersUrl: 'https://api.clarivate.com/api/wosrl/',
  },
  publisherName,
  staffEmail: process.env.EMAIL_SENDER || 'help@hindawi.com',
  accountsEmail: 'accounts@hindawi.com',
  maintenanceEmailsDestinations: [
    { email: 'team.rocket@hindawi.com', name: 'Team Rocket' },
    { email: 'css@hindawi.com', name: 'CSS Team' },
    { email: 'iasi.po@hindawi.com', name: 'Iasi PO Team' },
  ],
  publisherConfig: publishersConfig[publisherName],
  journal: { ...publishersConfig[publisherName].emailData },
  emailFooterText: publishersConfig[publisherName].emailData.footerText,
  features: {
    mts: JSON.parse(get(process, 'env.FEATURE_MTS', true)),
    EDITOR_EXPERTISE: !!process.env.KEYCLOAK_CERTIFICATES,
    SHOW_EDITOR_EXPERTISE_FUNCTIONALITY:
      process.env.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY || false,
    FEATURE_PPBK4962_SPECIAL_ISSUE:
      process.env.FEATURE_PPBK4962_SPECIAL_ISSUE || false,
  },
  recommendations: {
    minor: 'minor',
    major: 'major',
    reject: 'reject',
    publish: 'publish',
    type: {
      review: 'review',
      editor: 'editorRecommendation',
    },
  },
  minimumNumberOfInvitedReviewers: 5,
  minimumNumberOfSubmittedReports: 2,
  passwordStrengthRegex: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&,.?;\'*><)([}{}":`~+=_-\\|/])(?=.{6,128})',
  ),
  activityLogEvents,
  activityLogTypes,
  reminders: {
    businessDays: JSON.parse(get(process, 'env.BUSINESS_DAYS', true)),
    academicEditor: {
      inviteReviewers: {
        days: {
          first: process.env.REMINDER_REVIEWER_INVITATION_FIRST || 5,
          second: process.env.REMINDER_REVIEWER_INVITATION_SECOND || 8,
          third: process.env.REMINDER_REVIEWER_INVITATION_THIRD || 11,
        },
        timeUnit: process.env.REMINDER_REVIEWER_INVITATION_TIME_UNIT || 'days',
      },
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_ACADEMIC_EDITOR_FIRST || 3,
          second: process.env.REMINDER_ACADEMIC_EDITOR_SECOND || 6,
        },
        remove: process.env.REMINDER_REMOVE_ACADEMIC_EDITOR || 8,
        timeUnit: process.env.REMINDER_ACADEMIC_EDITOR_TIME_UNIT || 'days',
      },
    },
    reviewer: {
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_REVIEWER_FIRST || 4,
          second: process.env.REMINDER_REVIEWER_SECOND || 7,
          third: process.env.REMINDER_REVIEWER_THIRD || 14,
        },
        remove: process.env.REMINDER_REMOVE_REVIEWER || 21,
        timeUnit: process.env.REMINDER_REVIEWER_TIME_UNIT || 'days',
      },
      submitReport: {
        days: {
          first: process.env.REMINDER_ACCEPTED_REVIEWER_FIRST || 7,
          second: process.env.REMINDER_ACCEPTED_REVIEWER_SECOND || 13,
          third: process.env.REMINDER_ACCEPTED_REVIEWER_THIRD || 18,
        },
        remove: process.env.REMINDER_REMOVE_ACCEPTED_REVIEWER || 24,
        timeUnit: process.env.REMINDER_ACCEPTED_REVIEWER_TIME_UNIT || 'days',
      },
    },
  },
  roleLabels: {
    triageEditor: 'Chief Editor',
    academicEditor: 'Academic Editor',
    editorialAssistant: 'Editorial Assistant',
    researchIntegrityPublishingEditor: 'Research Integrity Publishing Editor',
  },
  specialIssueExpirationInterval: {
    value: 1,
    timeUnit: 'year',
  },
  newJobCheckIntervalSeconds:
    process.env.NEXT_JOB_CHECK_INTERVAL_SECONDS || 300,
  adminEmail: process.env.ADMIN_EMAIL || 'admin@example.com',
  jobScheduling: process.env.JOB_SCHEDULING || false,
  bccAddress: process.env.BCC_ADDRESS || 'bcc@example.com',
  remainingCustomIdsAlertThreshold:
    process.env.REMAINING_CUSTOM_IDS_ALERT_THRESHOLD || 20000,
  numberOfAcademicEditorsToShow:
    process.env.NUMBER_OF_ACADEMIC_EDITORS_TO_SHOW || 50,
  reviewersCampaignIsEnabled: JSON.parse(
    get(process, 'env.REVIEWERS_CAMPAIGN_IS_ENABLED', false),
  ),
  sendgridApiKey: process.env.SENDGRID_API_KEY || 'sendgridApiKey',
  femApiEndpoint:
    process.env.FEM_API_ENDPOINT ||
    'https://flexible-editorial-models-service.qa.phenom.pub',
  communications: {
    apiUrl:
      process.env.PHENOM_COMMUNICATIONS_API_URL ||
      'http://localhost:3333/graphql',
    emailSender:
      process.env.PHENOM_COMMUNICATIONS_EMAIL_SENDER || 'no-reply@hindawi.com',
    jwtSecret: process.env.JWT_SECRET || 'secret',
    disablePreviewEmailRequest:
      process.env.DISABLE_PREVIEW_EMAIL_REQUEST || false,
  },
  serviceSchemaUrl:
    process.env.SERVICE_SCHEMA_URL ||
    'http://host.docker.internal:3000/graphql',
  antiMalwareTimeout: process.env.ANTIMALWARE_TIMEOUT || 60,
  journalIdsForOptionalRejectReason:
    process.env.JOURNAL_IDS_FOR_OPTIONAL_REJECT_REASONS || null,
}
