const models = {
  Team: { Role: {} },
  Manuscript: {
    findManuscriptsBySubmissionId: jest.fn(),
  },
  TeamMember: {
    findOneBySubmissionAndUser: jest.fn(),
  },
}
const { Manuscript, TeamMember } = models
const authsomeMode = require('../src/authsome-mode')

describe('has access to submission policy', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns true when the user has access to the submission', async () => {
    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValueOnce([{}])
    jest
      .spyOn(TeamMember, 'findOneBySubmissionAndUser')
      .mockResolvedValueOnce({})

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToSubmission'] },
      { submissionId: 'submission.id' },
      {
        models,
      },
    )

    expect(TeamMember.findOneBySubmissionAndUser).toHaveBeenCalledTimes(1)
    expect(result).toEqual(true)
  })

  it('returns false when the user does not have access to the submission', async () => {
    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValueOnce([{}])
    jest
      .spyOn(TeamMember, 'findOneBySubmissionAndUser')
      .mockResolvedValueOnce(undefined)

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToSubmission'] },
      { submissionId: 'submission.id' },

      {
        models,
      },
    )

    expect(TeamMember.findOneBySubmissionAndUser).toHaveBeenCalledTimes(1)
    expect(result).toEqual(false)
  })
  it('returns false when the submission id is not sent to the policy', async () => {
    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToSubmission'] },
      {},
      {
        models,
      },
    )

    expect(Manuscript.findManuscriptsBySubmissionId).toHaveBeenCalledTimes(0)
    expect(result).toEqual(false)
  })

  it('returns false when no manuscripts are found with the given submission id', async () => {
    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValueOnce([])

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToSubmission'] },
      { submissionId: 'submission.id' },
      {
        models,
      },
    )

    expect(Manuscript.findManuscriptsBySubmissionId).toHaveBeenCalledTimes(1)
    expect(TeamMember.findOneBySubmissionAndUser).toHaveBeenCalledTimes(0)
    expect(result).toEqual(false)
  })
})
