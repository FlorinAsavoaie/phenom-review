const models = {
  Manuscript: {
    find: jest.fn(),
  },
  TeamMember: {
    find: jest.fn(),
    findOneByManuscriptAndUser: jest.fn(),
  },
}
const { Manuscript, TeamMember } = models
const authsomeMode = require('../src/authsome-mode')

describe('has access to manuscript policy', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns true when the user has access to the manuscript', async () => {
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValueOnce({})
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToManuscript'] },
      { manuscriptId: 'manuscript.id' },
      {
        models,
      },
    )

    expect(TeamMember.findOneByManuscriptAndUser).toHaveBeenCalledTimes(1)
    expect(result).toEqual(true)
  })

  it('returns false when the user does not have access to the manuscript', async () => {
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockReturnValue()
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({})

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToManuscript'] },
      { manuscriptId: 'manuscript.id' },
      {
        models,
      },
    )

    expect(TeamMember.findOneByManuscriptAndUser).toHaveBeenCalledTimes(1)
    expect(result).toEqual(false)
  })
  it('returns false when the manuscript is not sent to the policy', async () => {
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockReturnValue()
    jest.spyOn(Manuscript, 'find').mockReturnValue()

    const result = await authsomeMode(
      'userId',
      { policies: ['hasAccessToManuscript'] },
      {},
      {
        models,
      },
    )

    expect(TeamMember.findOneByManuscriptAndUser).toHaveBeenCalledTimes(0)
    expect(result).toEqual(false)
  })
})
