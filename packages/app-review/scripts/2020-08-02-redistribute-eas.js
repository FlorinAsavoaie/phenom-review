const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const Promise = require('bluebird')
const events = require('../../component-events')

const {
  getUpdatedEditorialAssistantsUseCase,
  redistributeEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
} = require('component-model/src/useCases/manuscript')

const { Journal } = models

const execute = async () => {
  const eventsService = events.initialize({ models })
  const redistributeEditorialAssistants = await redistributeEditorialAssistantsUseCase.initialize(
    {
      models,
      logger,
      Promise,
      eventsService,
      useCases: {
        getUpdatedEditorialAssistantsUseCase,
        getSubmissionsWithUpdatedEditorialAssistantsUseCase,
      },
    },
  )
  const journals = await Journal.findAllWithMultipleEAs()
  await Promise.each(journals, async journal => {
    await redistributeEditorialAssistants.execute(journal.id)
    logger.info(`The manuscripts for journal ${journal.id} were redistributed.`)
  })

  process.exit()
}
execute()
