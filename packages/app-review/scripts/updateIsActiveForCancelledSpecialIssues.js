const models = require('@pubsweet/models')
const { Promise } = require('bluebird')
const events = require('../../component-events')
const { logger } = require('component-logger')

const eventsService = events.initialize({ models })
const { SpecialIssue, Section } = models
const { createQueueService } = require('@hindawi/queue-service')

const execute = async () => {
  createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })
  const cancelledSpecialIssues = await SpecialIssue.findAllCancelledSpecialIssues()

  if (!cancelledSpecialIssues.length)
    return logger.info(`There are no cancelled Special Issues left to update`)

  await Promise.each(
    cancelledSpecialIssues,
    async cancelledSpecialIssue => {
      cancelledSpecialIssue.updateProperties({ isActive: false })
      await cancelledSpecialIssue.save()

      const { sectionId } = cancelledSpecialIssue
      let { journalId } = cancelledSpecialIssue
      let eventName = 'JournalSpecialIssueUpdated'

      if (sectionId) {
        ;({ journalId } = await Section.find(sectionId))
        eventName = 'JournalSectionSpecialIssueUpdated'
      }

      eventsService.publishJournalEvent({
        journalId,
        eventName,
      })
      eventsService.publishSpecialIssueEvent({
        specialIssueId,
        eventName: 'SpecialIssueUpdated',
      })
    },
    { concurrency: 10 },
  )
}
execute()
