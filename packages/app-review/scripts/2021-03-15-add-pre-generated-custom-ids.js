const { db: knex } = require('@pubsweet/db-manager')
const fs = require('fs')

const execute = async () => {
  const csvPath = process.argv[2]
  const ids = await getArrayFromCSV(csvPath)

  const rows = ids.map(record => ({
    custom_id: record,
  }))

  for (let i = 0; i < rows.length; i += 1000) {
    const limit = Math.min(i + 1000, rows.length)
    const sliceOfRows = rows.slice(i, limit)

    await knex('available_custom_ids').insert(sliceOfRows)

    console.log(`Ran ${limit} rows until now`)
  }
  process.exit()
}

const getArrayFromCSV = async path => {
  const rawData = fs.readFileSync(path, 'utf8', err => {
    if (err) console.log(err)
  })

  const records = rawData.split('\n').slice(1)
  return records.filter(record => !!record)
}

execute()
