function displayLoader(entriesCompleted, totalNumberOfEntries) {
  const base = 2
  const totalBlocks = 100 / base
  const percentageDone = Math.min(
    100,
    Math.floor((entriesCompleted * 100) / totalNumberOfEntries),
  )

  const numberOfBblocksDone = Math.floor(
    (entriesCompleted * 100) / totalNumberOfEntries / base,
  )
  const numberOfBlocksRemaining = Math.max(totalBlocks - numberOfBblocksDone, 0)

  const blocksDone = '▓'.repeat(numberOfBblocksDone)
  const blocksLeft = '░'.repeat(numberOfBlocksRemaining)

  const output = `${blocksDone}${blocksLeft} ${percentageDone}%`

  const clearLastLine = () => {
    process.stdout.moveCursor(0, -1) // up one line
    process.stdout.clearLine(1) // from cursor to end
  }

  clearLastLine()
  process.stdout.write('\n' + output)
}

module.exports = {
  displayLoader,
}
