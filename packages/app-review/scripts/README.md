## Table of Contents

- [Custom Ids](#Custom-Ids)
  - [2021-03-10-split-available-custom-ids](#2021-03-10-split-available-custom-ids.js)
  - [2021-03-15-add-pre-generated-custom-ids](#2021-03-15-add-pre-generated-custom-ids.js)

## Custom Ids

### 2021-03-10-split-available-custom-ids.js

This script calculates and shuffles available customIds from 1000000 to 9999999 excluding the ones provided in the csv files and splits them between hindawi(99%) and gsw(1%)

Example:

```
node 2012-03-10-split-available-custom-ids.js gsw.csv mts.csv hindawi.csv
```

from `./packages/app-review/scripts`
where:

- gsw.csv contains used customIds in GSW
- mts.csv contains used and reserved customIds for MTS
- hindawi.csv contains used customIds in Phenom Review

All csv files should have the following structure:

```
custom_id
1234567
```

This script will generate two csv files in the scripts folder:

- `hindawiCustomIds.csv`
- `gswCustomIds.csv`

### 2021-03-15-add-pre-generated-custom-ids.js

This script will insert the customIds provided by the csv file in the `available_custom_ids` table. This will be considered as the pool of customIds that will be used in submissions.

**All of the database connection information is taken from the environment variables, so be sure to have them properly set up (DB_HOST, DATABASE, DB_USER, DB_PASS)
Keep in mind that some variables should be set to false: (e.g JOB_SCHEDULING, EMAIL_SENDING)**

Example:

```
node 2021-03-15-add-pre-generated-custom-ids.js hindawiCustomIds.csv
```
