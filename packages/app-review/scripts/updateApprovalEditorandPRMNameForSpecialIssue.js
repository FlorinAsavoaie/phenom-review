const models = require('@pubsweet/models')
const { logger } = require('component-logger')

const { PeerReviewModel } = models
const execute = async () => {
  const specialIssuePeerReviewModel = await PeerReviewModel.findOneBy({
    queryObject: {
      name: 'Special Issue Chief Minus',
    },
  })

  specialIssuePeerReviewModel.updateProperties({
    approvalEditors: ['academicEditor'],
    name: 'Special Issue Associate Editor',
  })

  await specialIssuePeerReviewModel.save()

  logger.info(`Updated PRM with id ${specialIssuePeerReviewModel.id}`)

  process.exit()
}
execute()
