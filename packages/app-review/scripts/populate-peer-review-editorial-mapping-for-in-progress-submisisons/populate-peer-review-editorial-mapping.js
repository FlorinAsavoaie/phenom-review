/**
 *
 * run this locally
 *
 * prereq:
 * - start fem service locally
 * - add env var connection details to review database you want to populate
 *
 * implementation:
 * 1. get first batch of in progress submissions
 * 2. for each submission in batch
 *  - make a request to the local fem service with the submissionId
 *    ( this will return the editorial model for the submissionId )
 *  - extract the peerReviewEditorialModel and save it to the peer_review_editorial_mapping table
 * 3. when all promises resolve, get the next batch and repeat until batch size is 0
 */

const fs = require('fs')

const {
  QAConfig,
  QAGammaConfig,
  QAGSWConfig,
  DemoConfig,
  DemoGSWConfig,
  DemoSalesConfig,
  ProdConfig,
  ProdGSWConfig,
  LocalConfig,
  peerReviewStatuses,
} = require('../populate-editorial-mapping-shared-utils/config')

const {
  getConnectionToReviewDB,
  count,
  batch,
  getInProgressSubmissionsWithoutPeerReviewMapping,
  requestEditorialMappingBySubmissionId,
  extractPeerReviewEditorialModel,
  savePeerReviewEditorialMappingForSubmissionInReview,
  getPeerReviewEditorialMappingBySubmissionId,
} = require('../populate-editorial-mapping-shared-utils/lib')

const {
  displayLoader,
} = require('../populate-editorial-mapping-shared-utils/helpers')

async function populate(config, peerReviewStatuses) {
  console.log(
    `▓ running populate Peer Review Editorial Mapping script on ${config.ENV} environment.`,
  )
  const reviewDBConnection = getConnectionToReviewDB(config)
  const batchSize = 10
  let submissionsPopulated = 0
  let offset = 0

  const totalNumberOfSubmissions = await count(
    reviewDBConnection,
    getInProgressSubmissionsWithoutPeerReviewMapping(
      reviewDBConnection,
      peerReviewStatuses,
    ),
  )

  console.log(
    `▓ total number of submissions to be processed: ${totalNumberOfSubmissions.count} \n`,
  )

  displayLoader(0, totalNumberOfSubmissions.count)

  const logStream = fs.openSync(config.LOG_FILE, 'w')
  do {
    const remainingSubmissions = totalNumberOfSubmissions.count - offset
    const limit = Math.max(Math.min(batchSize, remainingSubmissions), 0)

    const manuscriptsBatch = await batch(
      getInProgressSubmissionsWithoutPeerReviewMapping(
        reviewDBConnection,
        peerReviewStatuses,
      ),
      offset,
      limit,
    )

    const localBatchSize = Math.min(batchSize, manuscriptsBatch.length)

    for (let i = 0; i < localBatchSize; i++) {
      let editorialMapping

      try {
        editorialMapping = await requestEditorialMappingBySubmissionId(
          config.FEM_SERVICE_URL,
          manuscriptsBatch[i].submission_id,
        )
      } catch (err) {
        offset++
        await fs.writeSync(
          logStream,
          `!!![EM_REQUEST_ERROR] there was a problem fetching the editorial model for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }

      if (!editorialMapping) {
        offset++
        await fs.writeSync(
          logStream,
          `!!![EM_NOT_FOUND_ERROR] there was no editorial mapping found for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }

      const peerReviewEditorialModel = extractPeerReviewEditorialModel(
        editorialMapping.editorialModel,
      )

      try {
        const existingEntryForPeerReviewEditorialMapping = await getPeerReviewEditorialMappingBySubmissionId(
          reviewDBConnection,
          manuscriptsBatch[i].submission_id,
        )

        if (existingEntryForPeerReviewEditorialMapping) {
          offset++
          await fs.writeSync(
            logStream,
            `■ peer review editorial mapping for submission: ${manuscriptsBatch[i].submission_id} already exists \n`,
          )
        } else {
          const idOfSavedEntry = await savePeerReviewEditorialMappingForSubmissionInReview(
            reviewDBConnection,
            manuscriptsBatch[i].submission_id,
            peerReviewEditorialModel,
          )
          await fs.writeSync(
            logStream,
            `■ peer review editorial mapping for submission: ${manuscriptsBatch[i].submission_id} has been inserted with id: ${idOfSavedEntry} \n`,
          )
        }
      } catch (err) {
        offset++
        await fs.writeSync(
          logStream,
          `!!![EM_HAS_NOT_BEEN_SAVED_ERROR] there was a problem saving the peer review editorial mapping for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }
    }

    submissionsPopulated = submissionsPopulated + batchSize

    displayLoader(submissionsPopulated, totalNumberOfSubmissions.count)
  } while (submissionsPopulated < totalNumberOfSubmissions.count)
  fs.closeSync(logStream)

  process.exit()
}

try {
  populate(LocalConfig, peerReviewStatuses)
} catch (err) {
  console.log(err)
}
