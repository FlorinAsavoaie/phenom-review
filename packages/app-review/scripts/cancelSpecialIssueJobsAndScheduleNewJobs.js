const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { Promise } = require('bluebird')

const SpecialIssueJobsService = require('../../component-admin/server/src/jobs/specialIssue/specialIssue')
const events = require('component-events')

const { SpecialIssue, Job, TeamMember, Team } = models

const execute = async () => {
  const specialIssueJobs = await Job.findAllWithSpecialIssue()
  await Promise.each(specialIssueJobs, async job => {
    await Job.cancel(job.id)
  })

  const scheduledActivationSpecialIssues = await SpecialIssue.findAllScheduledForActivation()
  const admin = await TeamMember.findOneByRole({ role: Team.Role.admin })
  const eventsService = events.initialize({ models })

  const jobsService = SpecialIssueJobsService.initialize({
    models,
    eventsService,
  })

  await Promise.each(scheduledActivationSpecialIssues, async specialIssue => {
    await jobsService.scheduleSpecialIssueActivation({
      specialIssue,
      teamMemberId: admin.id,
    })
    logger.info(
      `Scheduled activation for SI ${specialIssue.id} on ${specialIssue.startDate}`,
    )
  })

  const scheduledDeactivationSpecialIssues = await SpecialIssue.findAllScheduledForDeactivation()
  await Promise.each(scheduledDeactivationSpecialIssues, async specialIssue => {
    await jobsService.scheduleSpecialIssueDeactivation({
      specialIssue,
      teamMemberId: admin.id,
    })
    logger.info(
      `Scheduled deactivation for SI ${specialIssue.id} on ${specialIssue.endDate}`,
    )
  })

  process.exit()
}
execute()
