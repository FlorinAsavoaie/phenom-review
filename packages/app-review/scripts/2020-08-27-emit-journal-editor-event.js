const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')

const eventsService = events.initialize({ models })

const execute = async () => {
  const journalId = process.argv[2]
  const eventType = process.argv[3]

  if (!journalId) {
    logger.error('Please provide journalId as argument')
    process.exit(9)
  }

  if (!eventType) {
    logger.error('Please provide event type as argument')
    process.exit(9)
  }

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  if (eventType === 'remove') {
    await eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorRemoved',
    })
  } else {
    await eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorAssigned',
    })
  }

  process.exit()
}

execute()
