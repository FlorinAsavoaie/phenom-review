const { db, migrate } = require('@pubsweet/db-manager')

const deleteTables = async () => {
  // objection2.0
  const { rows } = await db.raw(`
  SELECT tablename
  FROM pg_tables
  WHERE schemaname = current_schema
`)
  const dropQuery = rows
    .map(row => `DROP TABLE IF EXISTS "${row.tablename}" CASCADE`)
    .join(';')
  await db.raw(dropQuery)
}

const cleanPgBoss = async () => {
  await db.raw(`
  DELETE FROM pgboss.archive;
  DELETE FROM pgboss.job;
`)
}

const execute = async () => {
  await deleteTables()
  await cleanPgBoss()
  await migrate()
}

execute()
