const { Promise } = require('bluebird')
const { transaction } = require('objection')

const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

const s3Service = require('../../component-files/server/src/services/s3Service')

const { Manuscript, Team, File, TeamMember } = models

// * We can launch the script with BATCH, TO_PROCESS and CONCURRENCY:
// *    BATCH=50 node ./scripts/2022-11-10-delete-draft-manuscripts.js OR
// *    BATCH=50 TO_PROCESS=100 node ./scripts/2022-11-10-delete-draft-manuscripts.js
// *    BATCH=50 TO_PROCESS=100 CONCURRENCY=10 node ./scripts/2022-11-10-delete-draft-manuscripts.js

// * default BATCH is 100, TO_PROCESS is ALL and CONCURRENCY is 10
let { BATCH = 100, TO_PROCESS = 'ALL', CONCURRENCY = 10 } = process.env
TO_PROCESS = Number.parseInt(TO_PROCESS)
let PROCESSED_COUNT = 0

// * ms no. 9375124 from PROD should not be processed
const FAULTY_MANUSCRIPTS = ['aa75e6a4-ff7c-4aa4-a7c3-94ef70945001']

const getManuscriptsIds = async () => {
  const sql = db
    .select('m.*')
    .from('manuscript as m')
    .where('m.status', Manuscript.Statuses.draft)
    .whereNull('m.submitted_date')
    .whereNotIn('m.id', FAULTY_MANUSCRIPTS)
    .andWhere('m.created', '<', db.raw("CURRENT_DATE - INTERVAL '3 months'"))
    .limit(BATCH)

  console.log('\x1b[36m%s\x1b[0m', `Query = ${sql}`)

  return await sql
}

const processBatch = async manuscriptIds => {
  await Promise.map(manuscriptIds, processManuscript, {
    concurrency: Number(CONCURRENCY),
  })
}

const processManuscript = async manuscriptId => {
  console.log('\x1b[35m%s\x1b[0m', `Process manuscript ${manuscriptId}:`)

  const manuscript = await Manuscript.find(manuscriptId)

  try {
    // * get team members
    const teamsIds = []
    const teamMembersIds = []

    const teamMembers = await TeamMember.findAllByManuscript({
      manuscriptId,
    })

    if (teamMembers.length) {
      await Promise.each(teamMembers, async teamMember => {
        const teamId = teamMember.teamId

        if (teamId) {
          const team = await Team.find(teamId)
          if (team) {
            teamsIds.push(teamId)
          }
        }
        teamMembersIds.push(teamMember.id)
      })
    }

    const trx = await transaction.start(Manuscript.knex())

    try {
      // * delete team members
      teamMembersIds.length &&
        (await TeamMember.query(trx)
          .whereIn('id', [...new Set(teamMembersIds)])
          .delete())

      // * delete team(s)
      teamsIds.length &&
        (await Team.query(trx)
          .whereIn('id', [...new Set(teamsIds)])
          .delete())

      // * delete file(s) of the manuscript
      const s3FilesToDelete = await deleteFiles(manuscriptId, trx)

      // * delete manuscript
      manuscript &&
        (await Manuscript.query(trx)
          .where({ id: manuscriptId })
          .delete())

      // * put back the customId
      manuscript.customId &&
        (await trx('available_custom_ids').insert({
          custom_id: manuscript.customId,
        }))

      await trx.commit()

      // * delete S3 file objects
      await Promise.each(s3FilesToDelete, async fileToDelete => {
        await s3Service.delete(fileToDelete.providerKey)
      })

      console.log('\x1b[32m%s\x1b[0m', `Deleted manuscript ${manuscriptId}.`)
    } catch (e) {
      logger.error(e)
      await trx.rollback()
      throw new Error('Something went wrong. No manuscript was deleted.')
    }
  } catch (err) {
    logger.error(err)
    // throw new Error('Something went wrong. No manuscript was deleted.')
    FAULTY_MANUSCRIPTS.push(manuscriptId)
  }
}

const deleteFiles = async (manuscriptId, trx) => {
  const manuscriptFiles = await File.findAllByManuscript({
    manuscriptId,
  })

  const s3filesToDelete = []

  await Promise.each(manuscriptFiles, async fileToDelete => {
    s3filesToDelete.push(fileToDelete)
    await File.query(trx)
      .delete()
      .where('id', fileToDelete.id)
  })

  return s3filesToDelete
}

const execute = async () => {
  try {
    const manuscriptIds = (await getManuscriptsIds()).map(item => item.id)
    logger.info(`Batch of ${manuscriptIds.length} manuscripts to process ::`)

    if (manuscriptIds.length) {
      if (Number.isNaN(TO_PROCESS)) {
        // TO_PROCESS = 'ALL'
        await processBatch(manuscriptIds)
        // * https://alanstorm.com/asyncawait-and-recursion/
        await execute()
      } else {
        if (PROCESSED_COUNT < TO_PROCESS) {
          await processBatch(manuscriptIds)
          PROCESSED_COUNT += manuscriptIds.length
          await execute()
        }
      }
    }
  } catch (err) {
    console.log(err)
  }

  console.timeEnd('deleteManuscripts')
  process.exit()
}

console.time('deleteManuscripts')
console.log('\x1b[32m%s\x1b[0m', '---> Start')
execute()
