var fs = require('fs')
const { db } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

const allButRevisionDrafts = db
  .select('*')
  .from('manuscript as m')
  .where(query => query.where('m.version', 1).orWhereNot('m.status', 'draft'))
  .as('abrd')

const getLatestVersions = async _ => {
  const res = await db
    .select(
      'dwmc.submission_id',
      'dwmc.created',
      'm.id',
      'm.custom_id',
      'm.status',
      'm.version',
    )
    // all but some statuses
    .from(
      // all distinct with max created
      db
        .select(db.raw(`abrd.submission_id, MAX(abrd.created) created`))
        .from(
          // all but revision drafts
          allButRevisionDrafts,
        )
        .groupBy('abrd.submissionId')
        .orderBy('created')
        .as('dwmc'),
    )
    .join('manuscript as m', function() {
      this.on('dwmc.submission_id', 'm.submission_id').andOn(
        'dwmc.created',
        'm.created',
      )
    })
    .whereNotIn('m.status', [
      'published',
      'rejected',
      'deleted',
      'void',
      'withdrawn',
      'accepted',
      'olderVersion',
      'refusedToConsider',
    ])
    .andWhereNot('m.isLatestVersion', true)

  return res
}

const updateColumnValue = async batch => {
  await db('manuscript')
    .whereIn('id', batch)
    .update({ isLatestVersion: true })
}

const writeResultsToFile = results => {
  var file = fs.createWriteStream('latestVersionsUnassigned.txt')
  file.on('error', function(err) {
    console.log('error at writing to file')
  })
  results.forEach(function(v) {
    file.write(JSON.stringify(v) + '\n')
  })
  file.end()
  console.log('results > latestVersionsUnassigned.txt')
}

const execute = async _ => {
  const entries = await getLatestVersions()
  const ids = entries.map(e => e.id)
  logger.info(`Found ${ids.length} manuscripts to update`)
  // writeResultsToFile(ids)

  const limit = 100
  let offset = 0
  while (offset * limit < ids.length) {
    const batch = ids.slice(offset * limit, offset * limit + limit)
    await updateColumnValue(batch)
    logger.info(`Updated isLatestVersion for BATCH ${offset + 1}`)
    ++offset
  }

  if (offset * limit >= ids.length) {
    console.log('finished updating all the flags')
  }
}

try {
  execute()
} catch (e) {
  throw e
}
