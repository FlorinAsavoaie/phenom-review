const { db } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

// some acaademic editors have `invited_date` on null as a result of a problem that our system used to have
// this script fixes the historic data in order to generate good reports in other apps

// in order to fix the issue
// we will populate `invited_date` field using the `created` field of the same AE found on the earliest manuscript version

const performUpdate = async batch => {
  const updateTeamMemberInvitedDate = () =>
    db('teamMember as tm')
      .update(
        'invitedDate',
        // searching for the AE to take it's created field
        db
          .select('tm1.created')
          .from('teamMember as tm1')
          .join('team as t1', 'tm1.teamId', 't1.id')
          .join('manuscript as m1', 't1.manuscriptId', 'm1.id')
          .where(
            'm1.id',
            // searching for the earliest manuscript version where the AE is invited
            db
              .select('m2.id')
              .from('manuscript as m2')
              .join('team as t2', 'm2.id', 't2.manuscriptId')
              .join('teamMember as tm2', 't2.id', 'tm2.teamId')
              .where(
                'm2.submissionId',
                // searching for the right submission, so that the above query
                // can find the earliest manuscript version of that specific submission
                db
                  .select('m3.submissionId')
                  .from('manuscript as m3')
                  .join('team as t3', 'm3.id', 't3.manuscriptId')
                  .join('teamMember as tm3', 't3.id', 'tm3.teamId')
                  .where('tm3.id', db.ref('tm.id')),
              )
              .andWhere('tm2.userId', db.ref('tm.userId'))
              .orderBy('m2.created', 'asc')
              .limit(1),
          )
          .andWhere('t1.role', 'academicEditor')
          .andWhere('tm1.userId', db.ref('tm.userId'))
          .limit(1),
      )
      .whereIn('tm.id', batch)

  const res = await updateTeamMemberInvitedDate()

  console.log(`Rows affected: ${res}`)
}

const getAeIds = () =>
  db
    .select('tm.id')
    .from('teamMember as tm')
    .join('team as t', 'tm.teamId', 't.id')
    .whereNotNull('t.manuscriptId') // we're not interested in the AEs assigned on journals
    .whereNull('tm.invitedDate') // only old data, the system generates correct invitedDates now
    .andWhere('t.role', 'academicEditor')

const execute = async () => {
  console.log('Update started...')
  try {
    console.time('updateAeInvitedDate')

    // find our targets that need updating
    // we need to know the ids prior because
    // we need to do these updates in batches
    // given the fact that there are almost 9 million entries in the DB
    const aeIds = await (await getAeIds()).map(item => item.id)

    logger.info(`Found ${aeIds.length} academic editors to update!`)

    const limit = 100
    let offset = 0
    while (offset * limit < aeIds.length) {
      console.time(`batch-${offset + 1}`)
      const batch = aeIds.slice(offset * limit, offset * limit + limit)

      await performUpdate(batch)

      logger.info(`Updated invitedDate for BATCH ${offset + 1}`)
      console.timeEnd(`batch-${offset + 1}`)
      ++offset
    }
  } catch (err) {
    console.log(err)
  }

  console.timeEnd('updateAeInvitedDate')
  process.exit()
}

execute()
