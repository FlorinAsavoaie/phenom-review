const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')
const Promise = require('bluebird')

const eventsService = events.initialize({ models })
const { Journal } = models

const execute = async () => {
  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const activeJournals = await Journal.findAllActiveJournals()
  await Promise.each(activeJournals, async journal => {
    await eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalEditorRemoved',
    })
  })

  process.exit()
}

execute()
