const models = require('@pubsweet/models')

const JobsService = require('../../component-admin/server/src/jobs/specialIssue/specialIssue')
const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const { SpecialIssue, Team, TeamMember } = models
const jobsService = JobsService.initialize({ models })

const execute = async () => {
  const activeSpecialIssues = await SpecialIssue.findAllActiveSpecialIssues()
  const pendingSpecialIssues = await SpecialIssue.findAllPendingActivationSpecialIssues()
  const specialIssues = [...activeSpecialIssues, ...pendingSpecialIssues]

  if (!specialIssues.length)
    return logger.info(`There are no active Special Issues left to update`)

  const admin = await TeamMember.findOneByRole({ role: Team.Role.admin })

  await Promise.each(
    specialIssues,
    async specialIssue => {
      jobsService.scheduleSpecialIssueDeactivation({
        specialIssue,
        teamMemberId: admin.id,
      })
    },
    { concurrency: 10 },
  )
}
execute()
