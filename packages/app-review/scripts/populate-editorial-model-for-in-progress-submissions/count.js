const {
  QAConfig,
  QAGSWConfig,
  QAGammaConfig,
  DemoConfig,
  DemoGSWConfig,
  ProdConfig,
  ProdGSWConfig,
  inProgressStatuses,
} = require('../populate-editorial-mapping-shared-utils/config')

const {
  getConnectionToReviewDB,
  count,
  getSubmissions,
  getCreatedAfterPopulate,
  getSubmissionsWithoutSubmissionEditorialMapping,
  getCreatedButNotPopulated,
} = require('../populate-editorial-mapping-shared-utils/lib')

async function countSubmissions(config, inProgressStatuses) {
  console.log(`▓ running count script on the ${config.ENV} environment.`)

  const reviewDBConnection = await getConnectionToReviewDB(config)

  const totalNumberOfSubmissions = await count(
    reviewDBConnection,
    getSubmissionsWithoutSubmissionEditorialMapping(
      reviewDBConnection,
      inProgressStatuses,
    ),
  )

  console.log(
    `▓ total number of submissions to process: ${totalNumberOfSubmissions.count}.`,
  )

  process.exit()
}

try {
  countSubmissions(DemoGSWConfig, inProgressStatuses)
} catch (err) {
  console.log(err)
}
