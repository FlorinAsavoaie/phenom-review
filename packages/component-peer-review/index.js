const fs = require('fs')
const path = require('path')

const resolvers = require('./server/dist/resolvers')
const eventHandlers = require('./server/dist/eventHandlers')
const useCases = require('./server/dist/useCases')
const jobsService = require('./server/dist/jobsService/jobsService')
const invitations = require('./server/dist/invitations/invitations')
const emailPropsService = require('./server/dist/emailPropsService/emailPropsService')
const emailPropsServiceWrapper = require('./server/dist/emailPropsService/emailPropsServiceWrapper')
const emailJobs = require('./server/dist/emailJobs')
const removalJobs = require('./server/dist/removalJobs')

module.exports = {
  resolvers,
  eventHandlers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
  useCases,
  dependencies: {
    emailJobs,
    invitations,
    jobsService,
    removalJobs,
    emailPropsService,
    emailPropsServiceWrapper,
  },
}
