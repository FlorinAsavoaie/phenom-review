import {
  getSubmissionJournalId as getSubmissionJournalIdQuery,
  getReasonsForRejection as getReasonsForRejectionQuery,
} from '../../../../../component-peer-review/client/graphql/queries'

export const getSubmissionJournalIdQueryMock = {
  request: {
    query: getSubmissionJournalIdQuery,
    variables: { submissionId: 'testid' },
  },
  result: {
    data: {
      getSubmission: [
        {
          journalId: '12345',
        },
      ],
    },
  },
}

export const getReasonsForRejectionQueryMock = {
  request: {
    query: getReasonsForRejectionQuery,
    variables: { journalId: '12345' },
  },
  result: {
    data: {
      getReasonsForRejection: [
        'outOfScope',
        'technicalOrScientificFlaws',
        'publicationEthicsConcerns',
        'lackOfNovelty',
        'otherReasons',
      ],
    },
  },
}
