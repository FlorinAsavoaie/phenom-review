import { check } from '../components/AllowedTo'
import rules from '../rbac-rules'

describe('allowedTo check function', () => {
  it('should return false for a TE if the AE accepted the invitation on a manuscript on a PRM with AE as approval editor', () => {
    const role = 'triageEditor'
    const action = 'manuscript:makeDecision'
    const data = {
      manuscriptStatus: 'academicEditorAssigned',
      isApprovalEditor: false,
      isTriageEditorWithCOI: false,
    }

    expect(check(rules, role, action, data)).toBeFalsy()
  })

  it('should return true for a TE if the AE was invited but did not yet accepted on a manuscript on a PRM with AE as approval editor', () => {
    const role = 'triageEditor'
    const action = 'manuscript:makeDecision'
    const data = {
      manuscriptStatus: 'academicEditorInvited',
      isApprovalEditor: false,
      isTriageEditorWithCOI: false,
    }

    expect(check(rules, role, action, data)).toBeTruthy()
  })
})
