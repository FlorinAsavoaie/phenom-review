import React from 'react'
import { cleanup, fireEvent } from '@testing-library/react'

import { mockMatchMedia } from '@hindawi/ui'

import { render } from '../testUtils'
import { InviteReviewersForm } from '../..'

mockMatchMedia()

jest.mock('react-apollo', () => ({
  ...jest.requireActual('react-apollo'),
  useApolloClient: () => ({
    query: jest
      .fn()
      .mockResolvedValue({ data: { getPreviewEmail: { html: 'html' } } }),
  }),
  useQuery: jest
    .fn()
    .mockReturnValueOnce({
      data: { isUserSubscribedToEmails: false },
      loading: false,
      error: null,
    })
    .mockReturnValueOnce({
      data: {
        getConflictingAuthors: [],
      },
      loading: false,
      error: null,
    }),
  useMutation: jest.fn(() => [jest.fn().mockResolvedValue()]),
}))

const reviewer = {
  email: 'pedro.ruiz@universidaddecolombia.co',
  givenNames: 'Pedro Rúiz',
  surname: 'Gonzales',
}

describe('Invite Reviewers Form', () => {
  afterEach(cleanup)

  it('should validate', done => {
    const { getByText, getAllByText } = render(<InviteReviewersForm />)
    fireEvent.click(getByText(/send/i))

    setTimeout(() => {
      const formErrors = getAllByText(/required/i)
      expect(formErrors).toHaveLength(3)

      done()
    }, 11)
  })

  it('should invite a reviewer', async () => {
    const { getByText, getByTestId, wait } = render(<InviteReviewersForm />)

    await wait(() => {
      fireEvent.change(getByTestId('reviewer-email'), {
        target: {
          value: reviewer.email,
        },
      })

      fireEvent.change(getByTestId('reviewer-givenNames'), {
        target: {
          value: reviewer.givenNames,
        },
      })

      fireEvent.change(getByTestId('reviewer-surname'), {
        target: {
          value: reviewer.surname,
        },
      })
    })

    fireEvent.click(getByText('SEND'))
    await wait(() => fireEvent.click(getByText('OK, SEND INVITATION')))
    await wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() =>
      expect(getByText('The Reviewer was invited successfully')).toBeTruthy(),
    )
  })
})
