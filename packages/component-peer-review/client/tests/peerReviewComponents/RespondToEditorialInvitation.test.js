import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { RespondToEditorialInvitation } from '../..'

const { generateTeamMember, getTeamRoles } = require('component-generators')

jest.mock('../../graphql/hooks/useGetCommunicationRequest', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    client: {
      query: jest.fn().mockResolvedValue({
        data: {
          template: { body: ['12', '34'], html: '<div>hello world!</div>' },
        },
      }),
      mutation: jest.fn(),
    },
    addCommentsProp: jest.fn(),
    getCommunicationPackage: jest.fn(),
    communicationPackageData: { mailInput: { manuscript: {} } },
  })),
  TemplateUsage: {
    ACCEPTED_ACADEMIC_INVITATION: 'ACCEPTED_ACADEMIC_INVITATION',
    DECLINED_ACADEMIC_INVITATION: 'DECLINED_ACADEMIC_INVITATION',
  },
}))

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  User: {
    find: jest.fn(),
  },
  TeamMember: { findOneByUserAndRole: jest.fn() },
}

const { Team } = models
const teamMember = generateTeamMember({
  team: Team.Role.admin,
})

describe('Check display error message (Same for Hindawi & GSW)', () => {
  let onSubmitMock, renderedComponent
  beforeEach(() => {
    onSubmitMock = jest.fn(() => Promise.resolve())

    renderedComponent = render(
      <RespondToEditorialInvitation
        onSubmit={onSubmitMock}
        publisher="Hindawi"
        teamMember={teamMember}
      />,
    )
  })

  afterEach(cleanup)

  it('Should appear require when an option is not selected', async done => {
    const { getByText, getAllByText } = renderedComponent
    fireEvent.click(getByText('Respond to Editorial Invitation'))
    fireEvent.click(getByText('Respond to Invitation'))
    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      done()
    }, 1000)
  })

  it('Should receive an error when decline is selected and no reason is selected', async done => {
    const { getByText, getAllByText } = renderedComponent
    fireEvent.click(getByText('Respond to Editorial Invitation'))
    fireEvent.click(getByText('Decline'))
    fireEvent.click(getByText('Respond to Invitation'))
    setTimeout(() => {
      expect(getAllByText(/Please select a reason/i)).toBeTruthy()
      done()
    }, 1000)
  })

  it('Should receive an error when the last option from decline is selected but there is no reason added', async done => {
    const { getByText, getAllByText } = renderedComponent
    fireEvent.click(getByText('Respond to Editorial Invitation'))
    fireEvent.click(getByText('Decline'))
    fireEvent.click(getByText('My reason is not in the list above'))
    fireEvent.click(getByText('Respond to Invitation'))
    setTimeout(() => {
      expect(getAllByText(/Please specify your reason/i)).toBeTruthy()
      done()
    }, 1000)
  })
})
