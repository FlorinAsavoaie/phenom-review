import React from 'react'
import { MockedProvider } from '@apollo/react-testing'
import { render } from '@testing-library/react'

import { Form } from '@hindawi/phenom-ui'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'

import { waitForGraphqlQueryDataResponse } from '../testUtils'
import ReasonCheckbox from '../../components/peerReviewComponents/EditorialDecision/DecisionToReject/ReasonForRejection/ReasonCheckbox'
import {
  journal as journalMockedData,
  publisher as publisherMockedData,
} from '../mocks/mockData'

import { INITIAL_FORM_VALUES } from '../../components/peerReviewComponents/EditorialDecision/validateEditorialDecisionForm'
import {
  getReasonsForRejectionQueryMock,
  getSubmissionJournalIdQueryMock,
} from '../mocks/graphql/queries'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    submissionId: 'testid',
  }),
}))

const getRenderedComponentWithMockedGQL = () => {
  const mocks = [
    getSubmissionJournalIdQueryMock,
    getReasonsForRejectionQueryMock,
  ]

  const Component = () => {
    const [form] = Form.useForm()
    return (
      <HindawiContextProvider
        journal={journalMockedData}
        publisher={publisherMockedData}
      >
        <MockedProvider
          addTypename={false}
          fetch-policy="no-cache"
          mocks={mocks}
        >
          <Form form={form} initialValues={INITIAL_FORM_VALUES}>
            <ReasonCheckbox form={form} />
          </Form>
        </MockedProvider>
      </HindawiContextProvider>
    )
  }

  return render(<Component />)
}

describe('ReasonCheckbox component unit tests', () => {
  it('should render skeleton UI elements while GQL query is loading', async () => {
    const { container } = getRenderedComponentWithMockedGQL()

    const skeletonContainers = container.getElementsByClassName(
      'ant-skeleton-paragraph',
    )
    expect(skeletonContainers.length).toBe(1)

    const skeletonParagraphs = skeletonContainers[0].getElementsByTagName('li')
    expect(skeletonParagraphs.length).toBe(5)
  })

  it('render rejection reasons options as received through GQL query', async () => {
    const { getByText } = getRenderedComponentWithMockedGQL()

    const {
      rejectDecisionReasonsTranslations: {
        outOfScope,
        technicalOrScientificFlaws,
        publicationEthicsConcerns,
        lackOfNovelty,
        otherReasons,
      },
    } = publisherMockedData

    await waitForGraphqlQueryDataResponse(1000)

    expect(getByText(outOfScope)).toBeVisible()
    expect(getByText(technicalOrScientificFlaws)).toBeVisible()
    expect(getByText(publicationEthicsConcerns)).toBeVisible()
    expect(getByText(lackOfNovelty)).toBeVisible()
    expect(getByText(otherReasons)).toBeVisible()
  })
})
