import React from 'react'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { EditorialRecommendation } from '../../components'

jest.mock('react-apollo', () => ({
  ...jest.requireActual('react-apollo'),
  useApolloClient: () => ({
    query: jest
      .fn()
      .mockResolvedValue({ data: { getPreviewEmail: { html: 'html' } } }),
  }),
}))

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})

const recommendationsMock = [
  {
    label: 'Publish',
    value: 'publish',
  },
  {
    label: 'Minor Revision',
    value: 'minor',
  },
  {
    label: 'Major Revision',
    value: 'major',
  },
  {
    label: 'Reject',
    value: 'reject',
  },
]

const matchMock = { params: { submissionId: 'testid' } }

describe('Academic Editor recommendation', () => {
  let onSubmitMock, renderedComponent
  beforeEach(() => {
    onSubmitMock = jest.fn(() => Promise.resolve())

    renderedComponent = render(
      <EditorialRecommendation
        hasPublishOption
        match={matchMock}
        onSubmit={onSubmitMock}
        options={recommendationsMock}
      />,
    )
  })

  afterEach(cleanup)

  it('should call onSubmit when manuscript is recommended for publish', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Recommendation'))

    fireEvent.click(getByTestId('recommendation-dropdown-filter'))
    fireEvent.click(getByText('Publish'))
    fireEvent.click(getByTestId('button-editorial-recommendation-submit'))

    wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() => expect(onSubmitMock).toHaveBeenCalledTimes(1))

    await wait(() =>
      expect(
        getByText('Manuscript has been recommended for publish'),
      ).toBeTruthy(),
    )
  })

  it('should call onSubmit when manuscript is recommended for minor revision', async () => {
    const { getByText, getByTestId, wait, container } = renderedComponent
    fireEvent.click(getByText('Your Editorial Recommendation'))

    fireEvent.click(getByTestId('recommendation-dropdown-filter'))
    fireEvent.click(getByText('Minor Revision'))

    fireEvent.change(
      container.querySelector(
        '[data-test-id=editorial-recommendation-message-for-author] textarea',
      ),
      {
        target: { value: 'test' },
      },
    )

    fireEvent.click(getByTestId('button-editorial-recommendation-submit'))

    wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() => expect(onSubmitMock).toHaveBeenCalledTimes(1))

    await wait(() =>
      expect(
        getByText('Manuscript has been recommended for revision(minor)'),
      ).toBeTruthy(),
    )
  })

  it('should call onSubmit when manuscript is recommended for major revision', async () => {
    const { getByText, getByTestId, wait, container } = renderedComponent
    fireEvent.click(getByText('Your Editorial Recommendation'))

    fireEvent.click(getByTestId('recommendation-dropdown-filter'))
    fireEvent.click(getByText('Major Revision'))

    fireEvent.change(
      container.querySelector(
        '[data-test-id=editorial-recommendation-message-for-author] textarea',
      ),
      {
        target: { value: 'test' },
      },
    )

    fireEvent.click(getByTestId('button-editorial-recommendation-submit'))

    wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() => expect(onSubmitMock).toHaveBeenCalledTimes(1))

    await wait(() =>
      expect(
        getByText('Manuscript has been recommended for revision(major)'),
      ).toBeTruthy(),
    )
  })

  it('should call onSubmit when manuscript is recommended for rejection', async () => {
    const { getByText, getByTestId, wait, container } = renderedComponent
    fireEvent.click(getByText('Your Editorial Recommendation'))

    fireEvent.click(getByTestId('recommendation-dropdown-filter'))
    fireEvent.click(getByText('Reject'))

    fireEvent.change(
      container.querySelector(
        '[data-test-id=editorial-recommendation-message-for-author] textarea',
      ),
      {
        target: { value: 'test' },
      },
    )

    fireEvent.click(getByTestId('button-editorial-recommendation-submit'))

    wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() => expect(onSubmitMock).toHaveBeenCalledTimes(1))

    await wait(() =>
      expect(
        getByText('Manuscript has been recommended for rejection'),
      ).toBeTruthy(),
    )
  })
})
