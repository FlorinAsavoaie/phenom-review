import { compose, lifecycle, withHandlers } from 'recompose'

import { parseError } from '../utils'
import { EmailResponseLayout } from '../components'
import { withReviewerGQL } from '../graphql/withGQL'

export default compose(
  withReviewerGQL,
  withHandlers({
    declineReviewerInvitation: ({
      setError,
      setFetching,
      declineReviewerInvitation,
      params: { invitationId, manuscriptId },
    }) => () => {
      setFetching(true)
      declineReviewerInvitation({
        variables: {
          teamMemberId: invitationId,
        },
      })
        .then(r => {
          setFetching(false)
        })
        .catch(e => {
          setError(parseError(e))
          setFetching(false)
        })
    },
  }),
  lifecycle({
    componentDidMount() {
      const { declineReviewerInvitation } = this.props
      declineReviewerInvitation()
    },
  }),
)(EmailResponseLayout)
