import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { EditorialReportCard, ContextualBox } from '@hindawi/ui'

const EditorialComments = ({
  decisions,
  editorialReviews,
  startExpanded,
  triageEditorLabel,
  academicEditorLabel,
}) =>
  editorialReviews.length ? (
    <ContextualBox
      data-test-id="contextual-box-editorial-comments"
      label="Editorial Comments"
      mt={4}
      startExpanded={startExpanded}
    >
      <Root>
        {editorialReviews.map(r => (
          <EditorialReportCard
            academicEditorLabel={academicEditorLabel}
            decisions={decisions}
            key={r.id}
            mb={2}
            privateLabel="Message for Editorial Team"
            publicLabel="Message for Author"
            report={r}
            triageEditorLabel={triageEditorLabel}
          />
        ))}
      </Root>
    </ContextualBox>
  ) : null

export default EditorialComments

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: 0;
`
