Manuscript Details top section

```js
const authors = [
  {
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
  },
  {
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
  },
  {
    email: 'barrack.obama@gmail1.com',
    firstName: 'Barrack 1',
    lastName: 'Obama',
  },
  {
    email: 'barrack.obama@gmail2.com',
    firstName: 'Barrack 2',
    lastName: 'Obama',
  },
]

const collection = {
  customId: '55113358',
  visibleStatus: 'Pending Approval',
  academicEditor: {
    id: 'academic-editor-1',
    name: 'Academicton Ignashevici',
  },
  invitations: [],
}

const fragment = {
  authors,
  created: Date.now(),
  submitted: Date.now(),
  metadata: {
    journal: 'Awesomeness',
    title: 'A very ok title with many authors',
    type: 'research',
  },
}
const history = {
  push: () => alert('go back'),
}

const currentUser = {
  isReviewer: true,
  token: 'abc-123',
  permissions: {
    canOverrideTechChecks: true,
    canEditManuscript: true,
  },
}

;<ManuscriptDetailsTop
  collection={collection}
  currentUser={currentUser}
  fragment={fragment}
  history={history}
/>
```
