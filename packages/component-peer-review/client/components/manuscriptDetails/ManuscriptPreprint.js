import React from 'react'
import PropTypes from 'prop-types'
import { Formik, Form } from 'formik'
import {
  Row,
  Text,
  AutoValidatedFormikField,
  validators,
  NewTheme,
} from '@hindawi/ui'
import { useMutation } from 'react-apollo'
import { debounce } from 'lodash'
import { mutations } from '../../graphql'

const { Accordion, accordionChildrenPropTypes } = NewTheme

export const ManuscriptPreprint = props => {
  const [updatePreprintValue] = useMutation(mutations.updatePreprintValue)

  const saveFieldValue = debounce(
    ({ manuscriptId, value: preprintValue }) =>
      updatePreprintValue({
        variables: {
          manuscriptId,
          preprintValue,
        },
      }),
    1000,
  )

  const handleSetFieldValue = setFieldValue => (name, value) => {
    setFieldValue(name, value)
    saveFieldValue({ manuscriptId: props.manuscriptId, value }).catch(
      console.error,
    )
  }

  const renderChild = ({
    errors,
    setFieldValue,
    values: { preprintValue },
    validateField,
  }) => (
    <ManuscriptPreprintChild
      {...props}
      errors={errors}
      preprintValue={preprintValue}
      setFieldValue={handleSetFieldValue(setFieldValue)}
      validateField={validateField}
    />
  )

  return (
    <Formik initialValues={{ preprintValue: props.initialPreprintValue }}>
      {renderChild}
    </Formik>
  )
}

const ManuscriptPreprintChild = ({
  preprintDescription,
  preprints,
  manuscriptIsEditable,
  index, // from Accordion Children
  expandedIndex, // from Accordion Children
  setExpandedIndex, // from Accordion Children
  preprintValue, // from Formik
  errors, // from Formik
  setFieldValue, // from Formik
  validateField, // from Formik
}) => {
  const formats = preprints.map(({ format }) => format)
  const validator = validators.oneOfTheFormats({
    formats,
    errorMessage: 'Incomplete ID',
  })
  const fieldName = 'preprintValue'

  const meta = validator(preprintValue)

  return (
    <Accordion.Item
      expandedIndex={expandedIndex}
      index={index}
      meta={meta}
      metaState="warning"
      setExpandedIndex={setExpandedIndex}
      title="Preprint ID"
    >
      <Text lineHeight="18px" whiteSpace="pre-wrap">
        <Form>
          <Text>
            <div dangerouslySetInnerHTML={{ __html: preprintDescription }} />
          </Text>
          <Row justify="flex-start" mb={0} width={50}>
            <AutoValidatedFormikField
              disabled={!manuscriptIsEditable}
              errorType="warning"
              icon="solidInfo"
              name={fieldName}
              placeholder="arXiv:YYMM.NNNNN[category]"
              setFieldValue={setFieldValue}
              validators={[validator]}
              value={preprintValue}
            />
          </Row>
        </Form>
      </Text>
    </Accordion.Item>
  )
}

const manuscriptPreprintPropTypes = {
  ...accordionChildrenPropTypes,
  manuscriptId: PropTypes.string.isRequired,
  manuscriptIsEditable: PropTypes.bool,
  initialPreprintValue: PropTypes.string,
  preprintDescription: PropTypes.string,
  preprints: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      format: PropTypes.string,
    }),
  ).isRequired,
}
const manuscriptPreprintDefaultProps = {
  preprintValue: '',
  preprintDescription: null,
  manuscriptIsEditable: false,
}

const formikProps = {
  errors: PropTypes.shape({ [PropTypes.string]: PropTypes.string }),
  setFieldValue: PropTypes.func,
  preprintValue: PropTypes.string.isRequired,
  validateField: PropTypes.func,
}

const formikDefaultProps = {
  setFieldValue: _ => null,
  validateField: _ => true,
}

ManuscriptPreprint.propTypes = {
  ...manuscriptPreprintPropTypes,
}

ManuscriptPreprint.defaultProps = {
  ...manuscriptPreprintDefaultProps,
}

ManuscriptPreprint.propTypes = {
  ...manuscriptPreprintPropTypes,
  ...formikProps,
}

ManuscriptPreprint.defaultProps = {
  ...manuscriptPreprintDefaultProps,
  ...formikDefaultProps,
}
