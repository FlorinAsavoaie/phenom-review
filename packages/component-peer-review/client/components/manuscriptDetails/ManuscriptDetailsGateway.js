// Temporary component for redirect

import React from 'react'
import { get } from 'lodash'
import { Row, Text, Loader } from '@hindawi/ui'
import { Redirect } from 'react-router'
import { useQuery } from 'react-apollo'

import { queries } from '../../graphql'

const ManuscriptDetailsGateway = ({ match }) => {
  const customId = get(match, 'params.customId')

  const { data, loading } = useQuery(queries.getSubmissionIds, {
    variables: { customId },
  })

  if (loading) {
    return (
      <Row mt={4}>
        <Loader />
      </Row>
    )
  }

  if (!get(data, 'getSubmissionIds')) {
    return (
      <Row mt={4}>
        <Text fontWeight={600}>
          Could not find any manuscript with this id.
        </Text>
      </Row>
    )
  }

  const { id: manuscriptId, submissionId } = data.getSubmissionIds
  return <Redirect to={`/details/${submissionId}/${manuscriptId}`} />
}

export default ManuscriptDetailsGateway
