import React from 'react'
import { Row, Text, NewTheme } from '@hindawi/ui'
import { ManuscriptFileList, ManuscriptPreprint } from './'

const { Accordion } = NewTheme

const NOT_DECLARED_LABEL = 'Not declared'

const ManuscriptMetadata = ({ manuscript, role }) => {
  const {
    id,
    files,
    articleType,
    preprintValue,
    isEditable,
    meta: { abstract, conflictOfInterest, dataAvailability, fundingStatement },
    journal: { preprints, preprintDescription },
  } = manuscript
  const hasAuthorDeclarations = ![
    'Erratum',
    'Corrigendum',
    'Retraction',
    'Expression of Concern',
  ].includes(articleType.name)
  // theoretically we should have used the rbac-rules and AllowedTo, but
  // by using the AllowedTo component, we are loosing the Accordion item
  // props we need to expand the preprintValue box
  const preprintIsVisible =
    !!preprints.length &&
    ['admin', 'editorialAssistant', 'author'].includes(role)
  return (
    <Accordion>
      <Accordion.Item title="Abstract">
        <Text lineHeight="18px" whiteSpace="pre-wrap">
          {abstract}
        </Text>
      </Accordion.Item>
      {hasAuthorDeclarations && (
        <Accordion.Item title="Author Declaration">
          <Row alignItems="center" justify="flex-start">
            <Text fontStyle={conflictOfInterest ? 'normal' : 'italic'}>
              <Text fontWeight={700}>Conflict of interest: &nbsp;</Text>
              {conflictOfInterest || NOT_DECLARED_LABEL}
            </Text>
          </Row>
          <Row alignItems="center" justify="flex-start" mt={2}>
            <Text fontStyle={dataAvailability ? 'normal' : 'italic'}>
              <Text fontWeight={700}>Data availability statement: &nbsp;</Text>
              {dataAvailability || NOT_DECLARED_LABEL}
            </Text>
          </Row>
          <Row alignItems="center" justify="flex-start" mt={2}>
            <Text fontStyle={fundingStatement ? 'normal' : 'italic'}>
              <Text fontWeight={700}>Funding statement: &nbsp;</Text>
              {fundingStatement || NOT_DECLARED_LABEL}
            </Text>
          </Row>
        </Accordion.Item>
      )}
      <Accordion.Item number={files.length} title="Files">
        <ManuscriptFileList files={files} />
      </Accordion.Item>
      {preprintIsVisible && (
        <ManuscriptPreprint
          initialPreprintValue={preprintValue}
          manuscriptId={id}
          manuscriptIsEditable={isEditable}
          preprintDescription={preprintDescription}
          preprints={preprints}
        />
      )}
    </Accordion>
  )
}

export default ManuscriptMetadata
