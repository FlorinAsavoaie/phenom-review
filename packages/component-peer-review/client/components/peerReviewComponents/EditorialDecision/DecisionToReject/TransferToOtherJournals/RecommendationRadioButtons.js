import React from 'react'
import styled from 'styled-components'
import { Col, Form, Radio, Row } from '@hindawi/phenom-ui'
import { ErrorMessage, hasError, InfoMessage } from '../Errors'

const label = 'Would you recommend transferring to another journal?'
const fieldName = ['transferToAnotherJournal', 'selectedOption']
const errorMessage =
  'Please specify if you would recommend transferring. Not visible to authors and reviewers'
const options = ['Yes', 'No']

function RecommendationRadioButtons({
  form,
  recommendation,
  setRecommendation,
}) {
  const onChange = e => {
    const { value } = e.target
    if (value === 'NO') {
      form.resetFields([['transferToAnotherJournal', 'transferSuggestions']])
    }
    setRecommendation(value)
  }

  return (
    <Row>
      <Col span={24}>
        <FormItem
          help={showErrorMessage({
            form,
            fieldName,
            recommendation,
            errorMessage,
          })}
          label={label}
          name={fieldName}
          rules={[{ required: true }]}
        >
          <Radio.Group onChange={onChange}>
            {options.map(option => (
              <StyledRadio key={option} value={option.toUpperCase()}>
                {option}
              </StyledRadio>
            ))}
          </Radio.Group>
        </FormItem>
      </Col>
    </Row>
  )
}

function showErrorMessage({ form, fieldName, recommendation, errorMessage }) {
  return recommendation === null && hasError({ form, fieldName }) ? (
    <ErrorMessage message={errorMessage} />
  ) : (
    <InfoMessage />
  )
}

export default RecommendationRadioButtons

const StyledRadio = styled(Radio)`
  align-items: center;
`

const FormItem = styled(Form.Item)`
  .ant-form-item-control-input-content {
    height: 22px;
  }
  .ant-form-item-control-input {
    min-height: 22px;
  }
`
