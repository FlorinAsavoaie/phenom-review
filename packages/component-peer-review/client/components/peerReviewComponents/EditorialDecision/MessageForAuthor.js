import React from 'react'
import styled from 'styled-components'
import { Form, Input } from '@hindawi/phenom-ui'
import { ErrorMessage } from './DecisionToReject/Errors'

const MessageForAuthor = ({ decision, academicEditorLabel, setMessage }) => {
  const label =
    decision === 'returnToAcademicEditor'
      ? `Comments for ${academicEditorLabel}`
      : 'Message to Author'

  const onChange = e => {
    const { value } = e.target
    setMessage(value)
  }

  return (
    <FormItem
      data-test-id="decision-dropdown"
      label={label}
      name="message"
      rules={[
        {
          required: true,
          whitespace: true,
          message: (
            <ErrorMessage
              icon="warning"
              message="Please specify your message to the author"
            />
          ),
        },
      ]}
    >
      <Input.TextArea
        data-test-id="triage-editor-decision-message"
        name="message"
        onChange={onChange}
        rows={4}
      />
    </FormItem>
  )
}

export default MessageForAuthor

const FormItem = styled(Form.Item)`
  margin-bottom: 16px;

  .ant-form-item-label {
    padding-bottom: 0;
  }
`
