import React from 'react'
import styled from 'styled-components'
import { Form, Select } from '@hindawi/phenom-ui'

const getUsage = decision => {
  switch (decision) {
    case 'reject':
      return 'EDITORIAL_DECISION_TO_REJECT'
    case 'returnToAcademicEditor':
      return 'EDITORIAL_DECISION_TO_RETURN'
    case 'minor':
      return 'EDITORIAL_DECISION_TO_REVISE' // maybe change this to minor revise? and add template in comms
    case 'major':
      return 'EDITORIAL_DECISION_TO_REVISE' // maybe change this to major revise? and add template in comms
    default:
      return null
  }
}

const getEditorDecisionsOptions = editorDecisions => {
  const decisionLabels = {
    publish: 'Publish',
    minor: 'Minor Revision',
    major: 'Major Revision',
    returnToAcademicEditor: 'Return to Academic Editor',
    reject: 'Reject',
  }
  return editorDecisions.map(decision => ({
    value: decision,
    label: decisionLabels[decision],
  }))
}

function DecisionSelector({
  form,
  submissionId,
  setDecision,
  isPublish,
  editorDecisions,
  getCommunicationPackage,
}) {
  const options = getEditorDecisionsOptions(editorDecisions)

  const handleDecisionChange = ({ form, decision, setDecision }) => {
    form.resetFields()
    form.setFieldsValue({
      decision,
    })
    setDecision(decision)

    if (!isPublish(decision)) {
      getCommunicationPackage({ submissionId, usage: getUsage(decision) })
    }
  }

  return (
    <FormItem label="Decision" name="decision" rules={[{ required: true }]}>
      <Select
        data-test-id="decision-dropdown"
        getPopupContainer={trigger => trigger.parentNode}
        name="decision"
        onChange={decision =>
          handleDecisionChange({ form, decision, setDecision })
        }
        options={options}
        placeholder="Choose in the list"
        style={{ width: 223, height: 31 }}
      />
    </FormItem>
  )
}

export default DecisionSelector

const FormItem = styled(Form.Item)`
  .ant-form-item-label {
    padding-bottom: 0;
  }
`
