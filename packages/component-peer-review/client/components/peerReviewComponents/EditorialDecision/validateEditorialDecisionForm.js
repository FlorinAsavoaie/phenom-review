const transferToAnotherJournal = {
  selectedOption: null,
  transferSuggestions: null,
}

const reasonsForRejection = {
  outOfScope: false,
  technicalOrScientificFlaws: false,
  publicationEthicsConcerns: false,
  lackOfNovelty: false,
  otherReasons: null,
}

export const INITIAL_FORM_VALUES = {
  decision: null,
  message: null,
  rejectDecisionInfo: { selectedReasons: null, otherReasons: null },
  transferToAnotherJournal,
}

export const rejectDecisionInfoDefault = {
  reasonsForRejection,
  transferToAnotherJournal,
}

export const rejectDecisionInfoToDTO = ({
  rejectDecisionInfo: { selectedReasons, otherReasons = null },
  transferToAnotherJournal,
}) => {
  const reasonForRejectionMapped = selectedReasons.reduce((obj, reason) => {
    obj[reason] = reason === 'otherReasons' ? otherReasons : true
    return obj
  }, {})

  return {
    ...rejectDecisionInfoDefault,
    reasonsForRejection: {
      ...reasonsForRejection,
      ...reasonForRejectionMapped,
    },
    transferToAnotherJournal,
  }
}
