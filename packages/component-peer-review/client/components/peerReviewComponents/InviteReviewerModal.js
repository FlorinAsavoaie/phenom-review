/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react'
import { useQuery } from 'react-apollo'
import { useHistory } from 'react-router-dom'
import { NewTheme } from '@hindawi/ui'
import { queries } from '../../graphql'

const { ConfirmationModal, LoadingModal, InformationModal } = NewTheme

const InviteReviewerModal = ({
  values,
  toggle,
  isShowing,
  handleRequestEmailPreview,
  loading,
}) => {
  if (!isShowing) return null

  const history = useHistory()
  const [, , , manuscriptId] = history
    ? history.location.pathname.split('/')
    : []

  const {
    data: queryData,
    loading: queryLoading,
    error: queryError,
  } = useQuery(queries.isUserSubscribedToEmails, {
    variables: { email: values?.email },
    fetchPolicy: 'network-only',
  })

  const isUnsubscribed = queryData?.isUserSubscribedToEmails === false
  const reviewerName = `${values?.givenNames} ${values?.surname}`

  const {
    data: conflictQueryData,
    loading: conflictQueryLoading,
    error: conflictQueryError,
  } = useQuery(queries.getConflictingAuthors, {
    variables: {
      manuscriptId,
      rorId: values?.affRorId,
      aff: values?.aff,
      country: values?.country,
    },
    fetchPolicy: 'network-only',
  })
  const conflictingAuthors = conflictQueryData?.getConflictingAuthors

  const onConfirm = () => {
    handleRequestEmailPreview()
    toggle()
  }

  const transformGQLError = gqlError =>
    gqlError?.message?.replace('GraphQL error: ', '')

  if (queryLoading || conflictQueryLoading)
    return <LoadingModal toggle={toggle} />
  if (queryError || conflictQueryError)
    return (
      <InformationModal
        error={transformGQLError(queryError)}
        icon="info"
        title="An error has occurred"
        toggle={toggle}
      />
    )

  if (conflictingAuthors.length) {
    return (
      <ConfirmationModal
        acceptButtonLabel="INVITE ANYWAY"
        authors={conflictingAuthors}
        cancelButtonLabel="CANCEL"
        icon="info"
        loading={loading}
        onConfirm={onConfirm}
        subtitle={`The reviewer ${reviewerName} has the same affiliation as the following authors:`}
        title="Potential conflict of interest identified!"
        toggle={toggle}
      />
    )
  }

  if (isUnsubscribed) {
    return (
      <ConfirmationModal
        acceptButtonLabel="OK, SEND INVITATION"
        icon="info"
        loading={loading}
        onConfirm={onConfirm}
        subtitle="This reviewer will not receive email invitation because he is unsubscribed. He can still respond to the invitation within the system."
        title="Send invitation to unsubscribed reviewer?"
        toggle={toggle}
      />
    )
  }

  handleRequestEmailPreview()
  toggle()
  return null
}

export default InviteReviewerModal
