import React, { Fragment } from 'react'
import { DateParser } from '@pubsweet/ui'

const ResponseDateTableCell = ({ reviewer }) => {
  const reviewerIsPending = reviewer.status === 'pending'
  const showReviewerResponseDate = !reviewerIsPending && reviewer.responded

  return (
    <td>
      <Fragment>
        {showReviewerResponseDate && (
          <DateParser timestamp={reviewer.responded}>
            {timestamp => timestamp}
          </DateParser>
        )}
      </Fragment>
    </td>
  )
}

export default ResponseDateTableCell
