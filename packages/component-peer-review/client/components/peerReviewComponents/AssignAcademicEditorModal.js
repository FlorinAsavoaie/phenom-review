import React from 'react'
import { ConfirmationModal } from '@hindawi/phenom-ui'
import { parseAffiliations } from '@hindawi/ui'

const AssignAcademicEditorModal = ({
  targetUser,
  loading,
  visible,
  onOk,
  onCancel,
  getModalContent,
}) => {
  const { authors, affiliations } = parseAffiliations(
    targetUser.conflictsOfInterest,
  )
  const content = getModalContent(
    targetUser,
    authors,
    affiliations,
    false,
    'Do you want to invite this editor to handle the manuscript anyway?',
  )
  const okText = 'INVITE ANYWAY'
  const title = 'Potential conflict of interest identified!'
  const icon = 'alert'

  return (
    <ConfirmationModal
      confirmLoading={loading}
      content={content}
      icon={icon}
      okText={okText}
      onCancel={onCancel}
      onOk={onOk}
      title={title}
      visible={visible}
    />
  )
}

export default AssignAcademicEditorModal
