/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useState } from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H3, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Label,
  NewTheme,
  validators,
  MenuCountry,
  ItemOverrideAlert,
  ValidatedFormField,
  trimFormStringValues,
  RorInfoState,
  renderRor,
} from '@hindawi/ui'
import { useMutation } from 'react-apollo'
import { useHistory } from 'react-router-dom'
import { MailTemplate } from '@hindawi/phenom-mail-template'

import { PreviewEmailModal } from './PreviewEmailModal'
import InviteReviewerModal from './InviteReviewerModal'
import { mutations, queries } from '../../graphql'
import useGetPreviewEmailRequest from '../../graphql/hooks/useGetPreviewEmailRequest'

const { useModal } = NewTheme

const transformGQLError = gqlError =>
  gqlError?.message?.replace('GraphQL error: ', '')

const rorInfo = new RorInfoState()

const errorMessage = `Please try sending the invitation again. If the issue persists, contact the Customer Support Team on help@hindawi.com`

const submitHandler = ({ setFieldValue, setTouched, submitForm }) => {
  const { aff, affRorId } = rorInfo.getData()

  // Ror component is quite custom so we need to manually trigger
  // updates for Formik fields
  setFieldValue('aff', aff)
  setFieldValue('affRorId', affRorId)

  // we need to do this manually since Formik doesn't seem to be doing it for some reason
  setTouched({ aff: true })

  // because we manually update aff field with `setFieldValue`, that triggers some update mechanism
  // inside Formik and prevents it from triggering `submitForm`
  // this is why we need to wait a bit for that action to clear up and then be able to run submit form action
  setTimeout(() => {
    submitForm()
  }, 10)
}

const InviteReviewersForm = () => {
  const { isShowing, toggle } = useModal()
  const [lastRerenderOnRoR, setlastRerenderOnRoR] = useState(new Date())
  const [reviewerAlias, setReviewerAlias] = useState()
  const [sendEmailLoading, setSendEmailLoading] = useState(false)

  const {
    emailContentChanges,
    showMailPreview,
    currentHtml,
    loading,
    handleSentEmail,
    handleError,
    handleRequestEmailPreview,
    handleOnChange,
    handleOnClose,
    handleOnRevert,
  } = useGetPreviewEmailRequest()

  const history = useHistory()
  const [, , submissionId, manuscriptId] = history
    ? history.location.pathname.split('/')
    : []

  const hardRefreshRorComponent = () => {
    setlastRerenderOnRoR(new Date())
  }

  const clearData = resetForm => () => {
    resetForm({})
    rorInfo.resetData()
    hardRefreshRorComponent()
  }

  const [inviteReviewerMutation] = useMutation(mutations.inviteReviewer, {
    refetchQueries: [
      {
        query: queries.getSubmission,
        variables: { submissionId },
      },
    ],
  })

  const handleMailSubmission = ({ values, resetForm }) => () => {
    setSendEmailLoading(true)
    inviteReviewerMutation({
      variables: {
        manuscriptId,
        input: values,
        emailContentChanges,
      },
    })
      .then(() => {
        clearData(resetForm)()
        handleSentEmail('The Reviewer was invited successfully')
      })
      .catch(e => {
        handleError(transformGQLError(e))
      })
    setSendEmailLoading(false)
  }

  return (
    <Formik onSubmit={toggle}>
      {({ resetForm, values, setFieldValue, setTouched, submitForm }) => (
        <Root>
          <Row justify="space-between" mb={2} mt={5}>
            <H3>Invite Reviewer</H3>

            <Item justify="flex-end">
              <Button
                data-type-id="button-invite-reviewer-clear"
                ml={4}
                onClick={clearData(resetForm)}
                secondary
                small
                width={32}
              >
                CLEAR
              </Button>

              <Button
                data-type-id="button-invite-reviewer-invite"
                ml={4}
                onClick={() => {
                  submitHandler({
                    setFieldValue,
                    setTouched,
                    submitForm,
                  })
                  const { surname, givenNames, email } = values
                  setReviewerAlias({ surname, givenNames, email })
                }}
                primary
                small
                width={32}
              >
                SEND
              </Button>

              <InviteReviewerModal
                handleRequestEmailPreview={handleRequestEmailPreview({
                  errorMessage,
                  input: {
                    submissionId,
                    usecase: 'InvitationToReviewAManuscript(Reviewer)',
                    targetUser: reviewerAlias,
                  },
                })}
                isShowing={isShowing}
                loading={loading}
                toggle={toggle}
                values={trimFormStringValues(values)}
              />
            </Item>
          </Row>
          <Row>
            <Item mr={4} vertical>
              <Label required>Email</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-email"
                inline
                name="email"
                validate={[validators.required, validators.emailValidator]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label required>First Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-givenNames"
                inline
                name="givenNames"
                validate={[validators.required]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label required>Last Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="reviewer-surname"
                inline
                name="surname"
                validate={[validators.required]}
              />
            </Item>

            <Item mr={4} vertical>
              <Label>Affiliation</Label>
              <ValidatedFormField
                component={renderRor({
                  initialValues: values,
                  rorInfo,
                })}
                data-test-id="reviewer-aff"
                inline
                name="aff"
                options={{ lastRerenderOnRoR }}
              />
            </Item>

            <ItemOverrideAlert vertical>
              <Label>Country</Label>
              <ValidatedFormField
                component={MenuCountry}
                data-test-id="reviewer-country"
                name="country"
              />
            </ItemOverrideAlert>
          </Row>

          <PreviewEmailModal
            handleMailSubmission={handleMailSubmission({ values, resetForm })}
            handleOnClose={handleOnClose}
            handleOnRevert={handleOnRevert}
            modalLoading={loading}
            successLoading={sendEmailLoading}
            title="Invite Reviewer"
            visible={showMailPreview}
          >
            <MailTemplate html={currentHtml} onChange={handleOnChange} />
          </PreviewEmailModal>
        </Root>
      )}
    </Formik>
  )
}

export default InviteReviewersForm

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue3')};
  padding: 0 calc(${th('gridUnit')} * 4);
  padding-bottom: calc(${th('gridUnit')} * 2);
`
// #endregion
