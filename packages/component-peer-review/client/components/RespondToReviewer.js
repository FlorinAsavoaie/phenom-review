import React from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { FileLayoutWithScanning } from 'component-files/client'
import { FilePicker, Spinner } from '@pubsweet/ui'
import { compose, setDisplayName, withProps, withHandlers } from 'recompose'
import {
  Row,
  Icon,
  Item,
  Label,
  Textarea,
  ActionLink,
  withFetching,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']
const fileObjPath = 'responseToReviewers.file'

const RespondToReviewer = ({
  onUpload,
  isFetching,
  responseFile,
  startExpanded,
  setFieldValue,
}) => (
  <ContextualBox
    label="Response to Reviewer Comments"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={2} pl={2}>
      <Row>
        <Item vertical>
          <Item alignItems="center" height={6}>
            <Label mr={2} required>
              Your Reply
            </Label>
            {isFetching ? (
              <Spinner />
            ) : (
              <FilePicker
                allowedFileExtensions={allowedFileExtensions}
                disabled={!!responseFile}
                onUpload={onUpload}
              >
                <ActionLink
                  data-test-id="add-file"
                  disabled={!!responseFile}
                  fontSize="12px"
                  fontWeight="bold"
                >
                  <Icon
                    bold
                    color="colorWarning"
                    fontSize="10px"
                    icon="expand"
                    mr={1}
                  />
                  UPLOAD FILE
                </ActionLink>
              </FilePicker>
            )}
          </Item>
          <ValidatedFormField
            component={Textarea}
            data-test-id="text-area"
            minHeight={22}
            name="responseToReviewers.content"
          />
        </Item>
      </Row>
      <Row justify="flex-start" mb={4}>
        {responseFile && (
          <FileStyle>
            <FileLayoutWithScanning
              file={responseFile}
              key={responseFile.id}
              removeItemFromForm={() => setFieldValue(fileObjPath, '')}
            />
          </FileStyle>
        )}
      </Row>
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withFetching,
  setDisplayName('RespondToReviewer'),
  withProps(({ formValues, revisionDraft }) => ({
    responseCommentId: get(revisionDraft, 'comment.id'),
    responseFile: get(formValues, fileObjPath),
  })),
  withHandlers({
    addFileToField: ({ setFieldValue }) => field => file =>
      setFieldValue(field, file),
  }),
  withHandlers({
    onUpload: ({
      handleUpload,
      setFetching,
      responseCommentId,
      addFileToField,
      ...rest
    }) => file => {
      handleUpload(file, {
        type: 'responseToReviewers',
        entityId: responseCommentId,
        setFetching,
        setFileField: addFileToField(fileObjPath),
        ...rest,
      })
    },
  }),
)(RespondToReviewer)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${space};
`

const FileStyle = styled.div`
  width: 100%;
`
