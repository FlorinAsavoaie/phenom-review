import React, { Fragment } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'
import {
  Label,
  Icon,
  MultiAction,
  Text,
  CopyEmail,
  ActionLink,
} from '@hindawi/ui'

const PersonInvitation = ({
  role,
  label,
  invitation,
  toggleAssignAcademicEditor,
  withUnassigned,
  invitationName,
  invitationEmail,
  invitationStatus,
  revokeInvitation,
  manuscriptStatus,
  withName = true,
  withEmailCopyTooltip,
}) => (
  <Root>
    {label && <Label>{label}</Label>}
    {withUnassigned && !get(invitation, 'id') && <Text>Unassigned</Text>}
    {withName &&
      (withEmailCopyTooltip ? (
        <CopyEmail email={invitationEmail}>
          <ActionLink ml={label ? 1 : 0}>{invitationName}</ActionLink>
        </CopyEmail>
      ) : (
        <Text ml={label ? 1 : 0}>{invitationName}</Text>
      ))}
    {invitationStatus === 'pending' && manuscriptStatus !== 'rejected' ? (
      <Fragment>
        <Modal
          cancelText="BACK"
          component={MultiAction}
          confirmText="REVOKE"
          modalKey={`${invitation.id}-revoke`}
          onConfirm={revokeInvitation}
          subtitle="Are you sure you want to revoke the invitation?"
          title={invitationName}
        >
          {showModal => (
            <Icon
              data-test-id="revoke-icon"
              fontSize="14px"
              icon="remove"
              ml={2}
              onClick={showModal}
            />
          )}
        </Modal>
      </Fragment>
    ) : (
      ['admin', 'editorialAssistant'].includes(role) &&
      invitationStatus === 'accepted' && (
        <Fragment>
          <ActionLink
            fontWeight={600}
            onClick={toggleAssignAcademicEditor}
            secondary
            whenHover
          >
            <Icon icon="reassign" ml={1} mr={1} />
            Reassign
          </ActionLink>
        </Fragment>
      )
    )}
  </Root>
)

export default compose(
  withProps(({ invitation }) => ({
    invitationName: `${get(invitation, 'alias.name.givenNames', '')} ${get(
      invitation,
      'alias.name.surname',
      '',
    )}`,
    invitationEmail: get(invitation, 'alias.email'),
    invitationStatus: get(invitation, 'status'),
  })),
  withHandlers({
    revokeInvitation: ({ onRevoke, invitation }) => modalProps => {
      if (typeof onRevoke === 'function') {
        onRevoke(invitation, modalProps)
      }
    },
  }),
)(PersonInvitation)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${space};
`
// #endregion
