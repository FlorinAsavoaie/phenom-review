import React from 'react'
import { space } from 'styled-system'
import styled from 'styled-components'
import { compose, withHandlers, withProps } from 'recompose'

import { th } from '@pubsweet/ui-toolkit'
import { TextField } from '@pubsweet/ui'
import { WizardAuthors } from 'component-submission/client/components'

import {
  Row,
  Menu,
  Item,
  Label,
  Textarea,
  validators,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

function conditionallyRenderAbstract(submissionEditorialModel) {
  if (submissionEditorialModel && !submissionEditorialModel.hasAbstract) return
  return (
    <Row mb={6}>
      <Item data-test-id="submission-abstract" vertical>
        <Label required>Abstract</Label>
        <ValidatedFormField
          component={Textarea}
          minHeight={30}
          name="meta.abstract"
          validate={[validators.required]}
        />
      </Item>
    </Row>
  )
}

const DetailsAndAuthors = ({
  journalCode,
  formErrors,
  formValues,
  onSaveAuthor,
  onEditAuthor,
  onDeleteAuthor,
  startExpanded,
  setFieldValue,
  setWizardEditMode,
  manuscriptTypeOptions,
  submissionEditorialModel,
}) => (
  <ContextualBox
    label="Details & Authors"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={2} pl={2}>
      <Row mb={4}>
        <Item data-test-id="submission-title" flex={3} mr={4} vertical>
          <Label required>Manuscript Title</Label>
          <ValidatedFormField
            component={TextField}
            inline
            name="meta.title"
            validate={[validators.required]}
          />
        </Item>
        <Item data-test-id="submission-type" vertical>
          <Label disabled>Manuscript Type</Label>
          <ValidatedFormField
            component={Menu}
            disabled
            name="meta.articleTypeId"
            options={manuscriptTypeOptions}
            placeholder="Please select"
            validate={[validators.required]}
          />
        </Item>
      </Row>
      {conditionallyRenderAbstract(submissionEditorialModel)}
      <WizardAuthors
        formValues={formValues}
        journalCode={journalCode}
        onDeleteAuthor={onDeleteAuthor({ formValues, setFieldValue })}
        onEditAuthor={onEditAuthor({ formValues, setFieldValue })}
        onSaveAuthor={onSaveAuthor({ formValues, setFieldValue })}
        setWizardEditMode={setWizardEditMode}
        wizardErrors={formErrors}
      />
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withProps(({ manuscriptType }) => ({
    manuscriptTypeOptions: [
      { label: manuscriptType.name, value: manuscriptType.id },
    ],
  })),
  withHandlers({
    setWizardEditMode: ({ setFieldValue }) => value =>
      setFieldValue('isEditing', value),
    onDeleteAuthor: ({ revisionManuscriptId, removeAuthor }) => ({
      values,
      setFieldValue,
    }) => (
      { id: authorTeamMemberId },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      clearError()
      setFetching(true)
      removeAuthor({
        variables: { manuscriptId: revisionManuscriptId, authorTeamMemberId },
      })
        .then(r => {
          setFetching(false)
          setEditMode(false)
          setFieldValue('authors', r.data.removeAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    onEditAuthor: ({ revisionManuscriptId, editAuthor }) => ({
      values,
      setFieldValue,
    }) => (
      { id: authorTeamMemberId, ...authorInput },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      clearError()
      setFetching(true)
      editAuthor({
        variables: {
          manuscriptId: revisionManuscriptId,
          authorInput,
          authorTeamMemberId,
        },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.editAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    onSaveAuthor: ({ addAuthorToManuscript, revisionManuscriptId }) => ({
      values,
      setFieldValue,
    }) => (
      { id, ...authorInput },
      {
        index,
        formFns,
        setError,
        clearError,
        setEditMode,
        setFetching,
        setWizardEditMode,
      },
    ) => {
      clearError()
      setFetching(true)
      addAuthorToManuscript({
        variables: { manuscriptId: revisionManuscriptId, authorInput },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.addAuthorToManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(DetailsAndAuthors)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${space};
`
