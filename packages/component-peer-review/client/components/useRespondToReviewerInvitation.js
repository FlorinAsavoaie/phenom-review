import { useMutation } from 'react-apollo'
import { get } from 'lodash'
import { mutations } from '../graphql'
import { parseError } from '../utils'
import { refetchGetSubmission } from '../graphql/refetchQueries'

const useRespondToReviewerInvitation = ({
  match,
  history,
  manuscript,
  userId,
  totalVersions,
}) => {
  const [acceptReviewerInvitation] = useMutation(
    mutations.acceptReviewerInvitation,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )
  const [declineReviewerInvitation] = useMutation(
    mutations.declineReviewerInvitation,
    {
      refetchQueries: () => {
        if (!userId) return {}

        return totalVersions > 1 ? [refetchGetSubmission(match)] : []
      },
    },
  )

  const respondToReviewerInvitation = ({ reviewerDecision }, modalProps) => {
    const reviewerInvitation = manuscript.reviewers.find(
      r => r.user.id === userId,
    )
    const teamMemberId = get(reviewerInvitation, 'id', '')
    modalProps.setFetching(true)
    if (reviewerDecision === 'yes') {
      acceptReviewerInvitation({
        variables: {
          teamMemberId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    } else {
      declineReviewerInvitation({
        variables: {
          teamMemberId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
          history.replace('/')
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    }
  }

  return { respondToReviewerInvitation }
}

export default useRespondToReviewerInvitation
