export const checkIfTriageEditorHasCOI = ({
  manuscript,
  specialIssueAcademicEditors,
  specialIssueTriageEditors,
}) => {
  const { hasTriageEditorConflictOfInterest } = manuscript || {}

  return (
    hasTriageEditorConflictOfInterest ||
    triageEditorIsUnfit({
      manuscript,
      specialIssueAcademicEditors,
      specialIssueTriageEditors,
    })
  )
}

const triageEditorIsUnfit = ({
  manuscript,
  specialIssueAcademicEditors,
  specialIssueTriageEditors,
}) => {
  const { specialIssue, triageEditor } = manuscript || {}

  if (!triageEditor) {
    return false
  }

  return (
    !!specialIssue &&
    triageEditorIsLeadGuestEditor({
      triageEditor,
      specialIssueTriageEditors,
    }) &&
    leadGuestEditorIsUnfit({ manuscript, specialIssueAcademicEditors })
  )
}

const triageEditorIsLeadGuestEditor = ({
  triageEditor,
  specialIssueTriageEditors,
}) => {
  specialIssueTriageEditors = specialIssueTriageEditors || []
  const triageEditorUserId = triageEditor.user.id
  const specialIssueTriageEditorsUserIds = specialIssueTriageEditors.map(
    te => te.user.id,
  )

  return specialIssueTriageEditorsUserIds.includes(triageEditorUserId)
}

const leadGuestEditorIsUnfit = ({
  manuscript,
  specialIssueAcademicEditors,
}) => {
  specialIssueAcademicEditors = specialIssueAcademicEditors || []
  const manuscriptAcademicEditors = manuscript.academicEditors || []
  const specialIssueAcademicEditorsIds = specialIssueAcademicEditors.map(
    ae => ae.user.id,
  )

  const allManuscriptAEsAreFromSpecialIssue = manuscriptAcademicEditors.every(
    ae => specialIssueAcademicEditorsIds.includes(ae.user.id),
  )

  return !allManuscriptAEsAreFromSpecialIssue
}
