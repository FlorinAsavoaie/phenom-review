import gql from 'graphql-tag'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      affRorId
      name {
        surname
        givenNames
      }
    }
    conflictsOfInterest {
      id
      alias {
        aff
        country
        email
        name {
          surname
          givenNames
        }
      }
    }
  }
`

export const teamMemberWithIdentities = gql`
  fragment teamMemberWithIdentities on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
      identities {
        ... on External {
          identifier
        }
        ... on Local {
          name {
            surname
          }
        }
      }
    }
    status
    alias {
      aff
      email
      country
      affRorId
      name {
        surname
        givenNames
      }
    }
    conflictsOfInterest {
      id
      alias {
        aff
        country
        email
        name {
          surname
          givenNames
        }
      }
    }
  }
`

export const specialIssueFragment = gql`
  fragment specialIssue on SpecialIssue {
    id
    name
    isCancelled
    endDate
  }
`

export const sectionFragment = gql`
  fragment section on Section {
    id
    name
  }
`

export const fileFragment = gql`
  fragment manuscriptDetailsFile on File {
    id
    type
    size
    filename
    originalName
    mimeType
  }
`

export const articleTypeFragment = gql`
  fragment articleTypeDetails on ArticleType {
    id
    name
    hasPeerReview
  }
`

export const reviewDetails = gql`
  fragment reviewDetails on Review {
    id
    member {
      role
      user {
        id
      }
      alias {
        name {
          surname
          givenNames
        }
      }
      reviewerNumber
    }
    created
    comments {
      id
      type
      content
      files {
        ...manuscriptDetailsFile
      }
    }
    recommendation
    rejectDecisionInfo {
      reasonsForRejection {
        outOfScope
        technicalOrScientificFlaws
        publicationEthicsConcerns
        lackOfNovelty
        otherReasons
      }
      transferToAnotherJournal {
        selectedOption
        transferSuggestions
      }
    }
    open
    submitted
  }
  ${fileFragment}
`

export const manuscriptFragment = gql`
  fragment manuscriptDetails on Manuscript {
    id
    status
    submissionId
    preprintValue
    journalId
    submissionId
    created
    visibleStatus
    inProgress
    hasSpecialIssueEditorialConflictOfInterest
    hasTriageEditorConflictOfInterest
    isEditable
    peerReviewModel {
      name
      hasTriageEditor
      approvalEditors
      hasFigureheadEditor
      figureheadEditorLabel
      academicEditorLabel
      triageEditorLabel
    }
    journal {
      ... on Journal {
        id
        name
        code
        preprintDescription
        preprints {
          type
          format
        }
      }
    }
    triageEditor {
      ...teamMember
    }
    isApprovalEditor
    specialIssue {
      id
      section {
        name
      }
    }
    authors {
      id
      isSubmitting
      isCorresponding
      alias {
        aff
        name {
          surname
          givenNames
        }
        email
        country
      }
    }
    meta {
      title
      abstract
      dataAvailability
      fundingStatement
      conflictOfInterest
    }
    version
    technicalCheckToken
    role
    customId
    articleType {
      ...articleTypeDetails
    }
    files {
      ...manuscriptDetailsFile
    }
    specialIssue {
      ...specialIssue
    }
    section {
      ...section
    }
    reviews {
      ...reviewDetails
    }
    academicEditor {
      ...teamMember
    }
    academicEditors {
      ...teamMember
    }
    pendingAcademicEditor {
      ...teamMember
    }
    reviewers {
      ...teamMember
      reviewerStatusLabel
    }
    researchIntegrityPublishingEditor {
      ...teamMember
    }
    statusColor
    editorDecisions
  }
  ${sectionFragment}
  ${specialIssueFragment}
  ${teamMember}
  ${fileFragment}
  ${reviewDetails}
  ${articleTypeFragment}
`

export const userFragment = gql`
  fragment manuscriptDetailsUser on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        isConfirmed
        country
      }
    }
  }
`
