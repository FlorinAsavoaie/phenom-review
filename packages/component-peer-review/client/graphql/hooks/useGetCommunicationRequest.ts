import { cloneDeep } from 'lodash'
import { useEffect, useState } from 'react'
import { useLazyQuery } from 'react-apollo'
import { TemplateInputDto } from '@hindawi/phenom-communications-ui'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import 'cross-fetch/polyfill'
import { useJournal } from 'component-journal-info'

import { getCommunicationPackage as getCommunicationPackageQuery } from '../queries'
import { deepOmit } from '../../utils'

export interface CommunicationQueryResponse {
  getCommunicationPackage?: TemplateInputDto
}

export enum TemplateUsage {
  EDITORIAL_DECISION = 'EDITORIAL_DECISION',
  INVITE_ACADEMIC_EDITOR = 'INVITE_ACADEMIC_EDITOR',
  ACCEPTED_ACADEMIC_INVITATION = 'ACCEPTED_ACADEMIC_INVITATION',
  DECLINED_ACADEMIC_INVITATION = 'DECLINED_ACADEMIC_INVITATION',
}
export class GQLClient {
  private static instance: GQLClient
  public static getInstance(): GQLClient {
    if (!GQLClient.instance) {
      GQLClient.instance = new ApolloClient({
        link: new HttpLink({
          // * is set using the DefinePlugin in webpack configuration
          uri: process.env.PHENOM_COMMUNICATIONS_API_URL,
        }),
        cache: new InMemoryCache({
          addTypename: false,
        }),
      })
    }

    return GQLClient.instance
  }
}

export interface QueryResponse {
  getCommunicationPackage: TemplateInputDto
}

interface Input {
  usage: TemplateUsage
  submissionId?: string
  teamMemberId?: string
  userId?: string
  comments?: string
}

const getCuratedCommunicationPackageData = data => {
  if (data) {
    return deepOmit(data, '__typename')
  }
  return null
}

const useGetCommunicationRequest = () => {
  const [getCommunicationPackage, { data, loading }] = useLazyQuery<
    QueryResponse
  >(getCommunicationPackageQuery, { fetchPolicy: 'network-only' })

  const [packageData, setPackageData] = useState<TemplateInputDto>()

  const {
    emailData: { communicationServiceMailLogo },
  } = useJournal()

  useEffect(() => {
    const curatedData = getCuratedCommunicationPackageData(
      data?.getCommunicationPackage,
    )
    if (curatedData) {
      curatedData.mailInput.logoUrl = communicationServiceMailLogo
    }
    setPackageData(curatedData)
  }, [data])

  const addCommentsProp = comments => {
    const updatedPackageData = cloneDeep(packageData)
    updatedPackageData.mailInput.manuscript.comments = comments
    setPackageData(updatedPackageData)
  }

  const handleGetCommunicationRequest = ({
    submissionId,
    teamMemberId,
    userId,
    usage,
    comments,
  }: Input) => {
    getCommunicationPackage({
      variables: {
        submissionId,
        teamMemberId,
        userId,
        usage,
        comments,
      },
    })
  }
  return {
    client: GQLClient.getInstance(),
    addCommentsProp,
    getCommunicationPackage: handleGetCommunicationRequest,
    communicationPackageData: packageData,
    loading,
    getCuratedCommunicationPackageData,
  }
}

export default useGetCommunicationRequest
