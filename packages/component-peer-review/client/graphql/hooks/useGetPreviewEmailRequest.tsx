import { message } from '@hindawi/phenom-ui'
import * as React from 'react'
import { useApolloClient } from 'react-apollo'
import { useDebouncedCallback } from 'use-debounce'

import { getPreviewEmail as getPreviewEmailQuery } from '../queries'

const { useState } = React

const useGetPreviewEmailRequest = () => {
  const [showMailPreview, setShowMailPreview] = useState(false)
  const [emailContentChanges, setEmailContentChanges] = useState([])
  const [originalHtml, setOriginalHtml] = useState()
  const [currentHtml, setCurrentHtml] = useState()
  const [loading, setLoading] = useState(false)

  const client = useApolloClient()

  const handleRequestEmailPreview = ({ errorMessage, input }) => async () => {
    setLoading(true)
    setShowMailPreview(true)
    setEmailContentChanges([])
    try {
      const { data } = await client.query({
        query: getPreviewEmailQuery,
        variables: {
          input,
        },
        fetchPolicy: 'network-only',
      })

      const { html } = data.getPreviewEmail

      setCurrentHtml(html)
      setOriginalHtml(html)
      setLoading(false)
    } catch (queryCallError) {
      handleError(errorMessage)
      setLoading(false)
    }
  }

  const handleOnChange = useDebouncedCallback(
    (bodies, document) => {
      setCurrentHtml(document)
      setEmailContentChanges(bodies)
    },
    300,
    { maxWait: 2000 },
  )

  const handleSentEmail = successMessage => {
    setShowMailPreview(false)

    void message.success({
      content: successMessage,
      className: 'communication-notification',
    })
  }

  const handleOnClose = () => {
    setShowMailPreview(false)
  }

  const handleOnRevert = () => {
    setCurrentHtml(originalHtml)
    setEmailContentChanges([])
  }

  const handleError = errorMessage => {
    setShowMailPreview(false)

    void message.error({
      content: errorMessage,
      className: 'communication-notification',
    })
  }

  return {
    showMailPreview,
    currentHtml,
    emailContentChanges,
    loading,
    handleRequestEmailPreview,
    handleOnChange,
    handleOnClose,
    handleOnRevert,
    handleSentEmail,
    handleError,
  }
}

export default useGetPreviewEmailRequest
