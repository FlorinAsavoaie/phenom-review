import { useLazyQuery } from 'react-apollo'
import { getConflictingAuthors } from '../queries'

export const useConflictOfInterest = () => {
  const [doQuery, { data: conflictQueryData, loading, error }] = useLazyQuery<
    { getConflictingAuthors: any },
    {
      manuscriptId: string
      rorId: string
      aff: string
      country: string
    }
  >(getConflictingAuthors, {
    fetchPolicy: 'network-only',
  })

  return {
    requestConflictingAuthors: doQuery,
    conflictingAuthors: conflictQueryData?.getConflictingAuthors,
    conflictQueryLoading: loading,
    conflictQueryError: error,
  }
}
