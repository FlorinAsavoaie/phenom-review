import { queries as activityLogQueries } from 'component-activity-log/client'
import * as queries from './queries'

export const refetchGetSubmission = ({ params: { submissionId = '' } }) => ({
  query: queries.getSubmission,
  variables: { submissionId },
})

export const refetchGetAcademicEditors = ({
  params: { manuscriptId = '' },
}) => ({
  query: queries.getAcademicEditors,
  variables: { manuscriptId, searchValue: '' },
})

export const refetchGetAuditLogs = ({ params: { submissionId = '' } }) => ({
  query: activityLogQueries.getAuditLogs,
  variables: { submissionId },
})

export const refetchLoadReviewerSuggestions = ({
  params: { manuscriptId = '' },
}) => ({
  query: queries.loadReviewerSuggestions,
  variables: { manuscriptId },
})
export const refetchGetTriageEditors = ({ params: { manuscriptId = '' } }) => ({
  query: queries.getTriageEditors,
  variables: { manuscriptId },
})
