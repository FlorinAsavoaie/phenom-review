import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import {
  refetchGetSubmission,
  refetchLoadReviewerSuggestions,
} from './refetchQueries'

const withAcademicEditorGQL = compose(
  graphql(mutations.acceptAcademicEditorInvitation, {
    name: 'acceptAcademicEditorInvitation',
    options: ({ match }) => ({
      refetchQueries: match.params.submissionId
        ? [refetchGetSubmission(match)]
        : [],
    }),
  }),
  graphql(mutations.declineAcademicEditorInvitation, {
    name: 'declineAcademicEditorInvitation',
  }),
  graphql(mutations.inviteReviewer, {
    name: 'inviteReviewer',
    options: ({ match }) => ({
      refetchQueries: [
        refetchGetSubmission(match),
        refetchLoadReviewerSuggestions(match),
      ],
    }),
  }),
  graphql(mutations.cancelReviewerInvitation, {
    name: 'cancelReviewerInvitation',
    options: ({ match }) => ({
      refetchQueries: [refetchGetSubmission(match)],
    }),
  }),
)

export default withAcademicEditorGQL
