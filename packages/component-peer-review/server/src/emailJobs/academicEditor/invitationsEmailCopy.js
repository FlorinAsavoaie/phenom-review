const getInvitationsEmailCopy = ({
  emailType,
  titleText,
  expectedDate,
  journalName,
}) => {
  let upperContent, lowerContent
  const hasLink = true
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'academic-editor-resend-invitation-first-reminder':
      upperContent = `On ${expectedDate}, we sent you a request to handle ${titleText}, submitted for possible publication in ${journalName}.<br/><br/>
        We'd be grateful if you could submit a decision on whether or not you will be able to handle this manuscript using the link below:`
      lowerContent = `Thank you in advance for your help with the evaluation of this manuscript.<br/><br/>
        We look forward to hearing from you.`
      break
    case 'academic-editor-resend-invitation-second-reminder':
      upperContent = `We recently sent you a request to handle ${titleText}. However, we have not yet received a reply from you. 
      We would appreciate it if you could let us know whether or not you will be able to handle this manuscript using the link below:`
      lowerContent = `Thank you in advance for your help with the assessment of this manuscript.<br/>
      We look forward to hearing from you.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    hasIntro,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getInvitationsEmailCopy,
}
