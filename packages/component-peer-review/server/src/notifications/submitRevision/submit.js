const { get } = require('lodash')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAcademicEditor({
    draftManuscript,
    journal,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const titleText = get(draftManuscript, 'title', '')
    const targetUserName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'academic-editor',
      targetUserName,
    })

    const emailProps = getPropsService.getProps({
      manuscript: draftManuscript,
      subject: `${draftManuscript.customId}: Revision submitted`,
      toUser: academicEditor,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
      signatureName: editorialAssistant.getName(),
      journalName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyTriageEditor({
    draftManuscript,
    journal,
    triageEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText: get(draftManuscript, 'title', ''),
      emailType: 'triage-editor',
      targetUserName: submittingAuthor.getName(),
      journalName,
    })
    const emailProps = getPropsService.getProps({
      manuscript: draftManuscript,
      toUser: triageEditor,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
      subject: `${draftManuscript.customId}: Revision submitted`,
      signatureName: editorialAssistant.getName(),
      paragraph,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = { initialize }
