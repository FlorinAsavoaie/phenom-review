const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyTriageEditor({
    reason,
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()
    const approvalEditorName = academicEditor.getName()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      comments: reason,
      academicEditorLabel,
      targetUserName: approvalEditorName,
      emailType: 'academic-editor-declined',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })
    const emailProps = getPropsService.getProps({
      toUser: triageEditor,
      subject: `${customId}: Editor invitation declined`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyEditorialAssistant({
    reason,
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const approvalEditorName = academicEditor.getName()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      comments: reason,
      targetUserName: approvalEditorName,
      emailType: 'academic-editor-declined',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: editorialAssistant,
      subject: `${customId}: Editor invitation declined`,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
