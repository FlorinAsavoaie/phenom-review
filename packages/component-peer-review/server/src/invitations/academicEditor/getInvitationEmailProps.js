const config = require('config')
const { get, concat } = require('lodash')
const { getModifiedText } = require('component-transform-text')

const urlService = require('../../urlService/urlService')

const acceptUrl = config.get('accept-academic-editor.url')
const declineUrl = config.get('decline-academic-editor.url')
const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')
const bccAddress = config.get('bccAddress')

module.exports = {
  getInvitationEmailProps({
    journal,
    subject,
    manuscript,
    triageEditor,
    academicEditor,
    authorTeamMembers,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { title, abstract, submissionId, id: manuscriptId } = manuscript
    const editorialAssistantEmail = get(editorialAssistant, 'alias.email')

    const agreeLink = urlService.createUrl({
      baseUrl,
      slug: acceptUrl,
      queryParams: {
        teamMemberId: academicEditor.id,
        manuscriptId,
        submissionId,
      },
    })

    const declineLink = urlService.createUrl({
      baseUrl,
      slug: declineUrl,
      queryParams: {
        teamMemberId: academicEditor.id,
      },
    })

    const authorsNameList = authorTeamMembers
      .map((author, index) => `${author.getName()}<sup>${index + 1}</sup>`)
      .join(', ')

    const authorAffiliationsList = authorTeamMembers
      .map(
        (author, index) =>
          `<div><sup style="color:#242424">${index +
            1}</sup><span style="color: #586971;">${get(
            author,
            'alias.aff',
          )}.</span></div>`,
      )
      .join(' ')

    const authorsList = concat(authorsNameList, authorAffiliationsList)

    const sender = triageEditor
      ? triageEditor.getName()
      : editorialAssistant.getName()

    return {
      type: 'user',
      templateType: 'invitation',
      bcc: bccAddress,
      fromEmail: `${sender} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(academicEditor, 'alias.email'),
        name: get(academicEditor, 'alias.surname'),
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: sender,
        authorsList,
        subject,
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: academicEditor.userId,
            token: academicEditor.user.unsubscribeToken,
          },
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: academicEditor.alias.email,
        }),
      },
    }
  },
}
