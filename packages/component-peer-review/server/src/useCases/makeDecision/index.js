const makeDecisionToPublishUseCase = require('./publish')
const makeDecisionToRejectUseCase = require('./reject/reject')
const rejectWithPeerReviewUseCase = require('./reject/rejectWithPeerReview')
const rejectWithNoPeerReviewUseCase = require('./reject/rejectWithNoPeerReview')
const getReasonsForRejectionUseCase = require('./reject/getReasonsForRejection')
const makeDecisionToReturnUseCase = require('./return')

module.exports = {
  getReasonsForRejectionUseCase,
  makeDecisionToPublishUseCase,
  makeDecisionToRejectUseCase,
  rejectWithPeerReviewUseCase,
  rejectWithNoPeerReviewUseCase,
  makeDecisionToReturnUseCase,
}
