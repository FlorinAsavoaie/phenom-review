const config = require('config')
const { transaction } = require('objection')

const useCases = require('./index')
const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')
const rejectNotifications = require('../../notifications/makeDecision/reject')
const publishNotifications = require('../../notifications/makeDecision/publish')
const { notifyAcademicEditor } = require('./notifyAcademicEditor')

const JobsService = require('../../jobsService/jobsService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/makeDecision/getEmailCopy')
const events = require('component-events')

const resolver = {
  Query: {
    async getReasonsForRejection(_, { journalId }, ctx) {
      return useCases.getReasonsForRejectionUseCase
        .initialize({ models, config })
        .execute({ journalId })
    },
  },
  Mutation: {
    async makeDecisionToPublish(_, { manuscriptId }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = publishNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })

      return useCases.makeDecisionToPublishUseCase
        .initialize({
          models,
          logger,
          logEvent,
          transaction,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async makeDecisionToReject(
      _,
      { manuscriptId, content, rejectDecisionInfo },
      ctx,
    ) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = rejectNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })

      return useCases.makeDecisionToRejectUseCase
        .initialize({
          config,
          models,
          logger,
          logEvent,
          useCases,
          transaction,
          jobsService,
          eventsService,
          notificationService,
          notifyAcademicEditor,
        })
        .execute({
          manuscriptId,
          content,
          rejectDecisionInfo,
          userId: ctx.user,
        })
    },
    async makeDecisionToReturn(_, { manuscriptId, content }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.makeDecisionToReturnUseCase
        .initialize({
          models,
          logger,
          logEvent,
          transaction,
          eventsService,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
  },
}

module.exports = resolver
