const reassignTriageEditorUseCase = require('./reassignTriageEditor')
const getAllTriageEditorsUseCase = require('./getAllTriageEditors')
const getTriageEditorsUseCase = require('./getTriageEditors')

module.exports = {
  reassignTriageEditorUseCase,
  getAllTriageEditorsUseCase,
  getTriageEditorsUseCase,
}
