const makeRecommendationToPublishUseCase = require('./publish')
const makeRecommendationToRejectUseCase = require('./reject')

module.exports = {
  makeRecommendationToPublishUseCase,
  makeRecommendationToRejectUseCase,
}
