import getCommunicationPackageUseCase from './getCommunication'
import getPreviewEmailUseCase from './previewEmail'

const makeDecisionUseCases = require('./makeDecision')
const submitRevisionUseCases = require('./submitRevision')
const submitReviewUseCases = require('./submitReview')
const requestRevisionUseCases = require('./requestRevision')
const makeRecommendationUseCases = require('./makeRecommendation')
const inviteReviewersUseCases = require('./inviteReviewers')
const inviteAcademicEditorUseCases = require('./inviteAcademicEditor')
const reassignTriageEditorUseCase = require('./reassignTriageEditor')
const assignAcademicEditor = require('./assignAcademicEditor')
const updateManuscriptDetails = require('./updateManuscriptDetails')
const manuscriptTaEligibilityUpdated = require('./manuscriptTaEligibilityUpdatedHandler')

module.exports = {
  ...submitReviewUseCases,
  ...makeDecisionUseCases,
  ...assignAcademicEditor,
  ...submitRevisionUseCases,
  ...requestRevisionUseCases,
  ...inviteReviewersUseCases,
  ...makeRecommendationUseCases,
  ...reassignTriageEditorUseCase,
  ...inviteAcademicEditorUseCases,
  ...updateManuscriptDetails,
  ...manuscriptTaEligibilityUpdated,
  getCommunicationPackageUseCase,
  getPreviewEmailUseCase,
}
