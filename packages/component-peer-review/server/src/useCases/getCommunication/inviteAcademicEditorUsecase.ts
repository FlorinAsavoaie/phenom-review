import {
  TemplateInputDto,
  TemplateName,
} from '@hindawi/phenom-communications-types'
import { CommunicationInput } from './getCommunication'

import { buildLinks, getEditorialAssistant, getSignedToken } from './shared'

export const inviteAcademicEditorUseCase = async (
  data: CommunicationInput,
  models,
): Promise<TemplateInputDto> => {
  const { Manuscript, Team, TeamMember, Journal, User } = models
  const { submissionId, userId, sender } = data
  const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
    submissionId,
    order: 'ASC',
    orderByField: 'version',
  })

  const manuscript = manuscripts.find(m => m.isLatestVersion === true)
  if (!manuscript) {
    throw new Error('Could not find the latest manuscript version')
  }

  if (
    [
      Manuscript.Statuses.deleted,
      Manuscript.Statuses.withdrawn,
      Manuscript.Statuses.revisionRequested,
    ].includes(manuscript.status)
  ) {
    throw new Error(
      `Cannot invite an Academic Editor when the manuscript status is ${manuscript.status}`,
    )
  }
  const journal = await Journal.find(manuscript.journalId)
  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)

  const authors = await TeamMember.findAllByManuscriptAndRole({
    manuscriptId: manuscript.id,
    role: Team.Role.author,
  })

  const teamMember = await TeamMember.findOneByManuscriptAndUser({
    userId,
    manuscriptId: manuscript.id,
  })

  const invitedAcademicEditor = await User.find(userId, 'identities')
  const { defaultIdentity } = invitedAcademicEditor
  const links = buildLinks(manuscript, teamMember || invitedAcademicEditor)
  const editorialAssistant = await getEditorialAssistant(models, manuscript)

  const signedTokens = getSignedToken({
    sender,
    links,
    to: {
      email: defaultIdentity.email,
      name: `Dr. ${defaultIdentity.surname}`,
    },
    manuscript,
    authors,
    editorialAssistant,
  })

  return {
    templateName: TemplateName.invitationToHandleManuscript,
    mailInput: {
      manuscript: {
        manuscriptId: manuscript.customId,
        manuscriptTitle: manuscript.title,
        journalName: journal.name,
        submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
      },
    },
    signedInput: signedTokens,
  }
}
