import { TemplateInputDto } from '@hindawi/phenom-communications-types'
import models from '@pubsweet/models'
import { editorialDecisionToReturnUseCase } from './editorialDecisionToReturnUseCase'
import { editorialDecisionToReviseUseCase } from './editorialDecisionToReviseUseCase'
import { editorialDecisionToRejectUseCase } from './editorialDecisionToRejectUseCase'
import { inviteAcademicEditorUseCase } from './inviteAcademicEditorUsecase'
import { responseToAcademicEditorInvitationUseCase } from './responseToAcademicEditorInvitationUseCase'

export interface CommunicationInput {
  usage: TemplateUsage
  sender: any
  userId?: string
  teamMemberId?: string
  comments?: string
  submissionId?: string
}

export enum TemplateUsage {
  EDITORIAL_DECISION_TO_REJECT = 'EDITORIAL_DECISION_TO_REJECT',
  INVITE_ACADEMIC_EDITOR = 'INVITE_ACADEMIC_EDITOR',
  EDITORIAL_DECISION_TO_RETURN = 'EDITORIAL_DECISION_TO_RETURN',
  EDITORIAL_DECISION_TO_REVISE = 'EDITORIAL_DECISION_TO_REVISE',
  ACCEPTED_ACADEMIC_INVITATION = 'ACCEPTED_ACADEMIC_INVITATION',
  DECLINED_ACADEMIC_INVITATION = 'DECLINED_ACADEMIC_INVITATION',
}

export const getCommunicationPackage = (
  data: CommunicationInput,
): Promise<TemplateInputDto> => {
  switch (data.usage) {
    case TemplateUsage.INVITE_ACADEMIC_EDITOR:
      return inviteAcademicEditorUseCase(data, models)
    case TemplateUsage.EDITORIAL_DECISION_TO_REJECT:
      return editorialDecisionToRejectUseCase(data, models)
    case TemplateUsage.EDITORIAL_DECISION_TO_RETURN:
      return editorialDecisionToReturnUseCase(data, models)
    case TemplateUsage.EDITORIAL_DECISION_TO_REVISE:
      return editorialDecisionToReviseUseCase(data, models)
    case TemplateUsage.ACCEPTED_ACADEMIC_INVITATION:
    case TemplateUsage.DECLINED_ACADEMIC_INVITATION:
      return responseToAcademicEditorInvitationUseCase(data, models)
    default:
      return null
  }
}

const authsomePolicies = ['isAuthenticated']
export default { authsomePolicies }
