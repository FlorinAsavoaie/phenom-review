import {
  TemplateInputDto,
  TemplateName,
} from '@hindawi/phenom-communications-types'

import {
  buildLinks,
  getEditorialAssistant,
  getManuscriptData,
  getSignedToken,
} from './shared'
import { CommunicationInput, TemplateUsage } from './getCommunication'

const { isEmpty } = require('lodash')

const getTemplateName = async (models, manuscript): Promise<TemplateName> => {
  const { Team, TeamMember } = models

  const reviewers = await TeamMember.findAllByManuscriptAndRole({
    role: Team.Role.reviewer,
    manuscriptId: manuscript.id,
    eagerLoadRelations: 'user',
  })

  const submittingReviewers = reviewers.filter(
    r => r.status === TeamMember.Statuses.submitted,
  )

  let templateName = TemplateName.rejectNoPeerReviews
  if (!isEmpty(submittingReviewers)) {
    templateName = TemplateName.rejectWithPeerReviews
  }

  return templateName
}

export const editorialDecisionToRejectUseCase = async (
  data: CommunicationInput,
  models,
): Promise<TemplateInputDto> => {
  const { TeamMember } = models
  const { sender, usage } = data

  const { manuscript, journal, authors } = await getManuscriptData(models, data)

  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)
  const submittingAuthorName = `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`

  const links = buildLinks(manuscript, submittingAuthor)

  const editorialAssistant = await getEditorialAssistant(models, manuscript)
  const receiver = editorialAssistant

  const signedTokens = getSignedToken({
    links,
    to:
      usage === TemplateUsage.EDITORIAL_DECISION_TO_REJECT
        ? {
            name: `Dr. ${submittingAuthor.alias.surname}`,
            email: submittingAuthor.alias.email,
          }
        : {
            name: `Dr. ${receiver.alias.surname}`,
            email: receiver.alias.email,
          },
    sender,
    manuscript,
    authors,
    editorialAssistant,
  })

  const templateName =
    usage === TemplateUsage.EDITORIAL_DECISION_TO_REJECT
      ? await getTemplateName(models, manuscript)
      : TemplateName.recommendationToReject

  return {
    templateName,
    mailInput: {
      manuscript: {
        manuscriptId: manuscript.customId,
        manuscriptTitle: manuscript.title,
        journalName: journal.name,
        submittingAuthor: submittingAuthorName,
      },
    },
    signedInput: signedTokens,
  }
}
