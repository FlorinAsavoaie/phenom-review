interface inputType {
  email: string
}
const initialize = TeamMember => ({
  async execute(
    input: inputType,
    userId: string,
    teamId: string,
    manuscriptId: string,
    ReviewerRole: string,
    noOfReviewers: number,
  ) {
    let reviewer = await TeamMember.findOneByManuscriptAndRoleAndUser({
      userId,
      manuscriptId,
      role: ReviewerRole,
    })

    if (!reviewer) {
      reviewer = new TeamMember({
        userId,
        teamId,
        alias: input,
        position: noOfReviewers + 1,
      })

      return reviewer.save()
    }

    if (reviewer.status === TeamMember.Statuses.expired) {
      reviewer.updateProperties({ status: TeamMember.Statuses.pending })

      return reviewer.save()
    }

    throw new Error(`User ${input.email} is already invited as ${ReviewerRole}`)
  },
})

export { initialize }
