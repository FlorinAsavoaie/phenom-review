const { isEmpty } = require('lodash')

const initialize = ({
  models: { Job, Team, Journal, TeamMember, Manuscript, ReviewerSuggestion },
  logger,
  logEvent,
  transaction,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ manuscriptId, teamMemberId, userId }) {
    const reviewer = await TeamMember.find(teamMemberId, '[user, team]')

    const reviewerJobs = await Job.findAllByTeamMember(teamMemberId)
    jobsService.cancelJobs(reviewerJobs)

    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    if (!manuscript) {
      throw new NotFoundError('Manuscript does not exist')
    }
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    reviewer.status = TeamMember.Statuses.expired

    const reviewers = await TeamMember.findAllByStatuses({
      role: Team.Role.reviewer,
      statuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
        TeamMember.Statuses.submitted,
        TeamMember.Statuses.removed,
        TeamMember.Statuses.conflicting,
      ],
      manuscriptId,
    })
    if (isEmpty(reviewers)) {
      manuscript.status = Manuscript.Statuses.academicEditorAssigned
    }

    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: reviewer.alias.email,
        manuscriptId: manuscript.id,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = false
    }

    const trx = await transaction.start(Manuscript.knex())
    try {
      await manuscript.$query(trx).update(manuscript)
      await reviewer.$query(trx).update(reviewer)

      if (reviewerSuggestion)
        await reviewerSuggestion.$query(trx).update(reviewerSuggestion)

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback()
      throw new Error('Something went wrong. No data was updated.')
    }

    if (reviewer.user && reviewer.user.isSubscribedToEmails) {
      const submittingAuthor = await TeamMember.findSubmittingAuthor(
        manuscriptId,
      )
      const journal = await Journal.find(manuscript.journalId)
      const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId,
          role: Team.Role.editorialAssistant,
          status: TeamMember.Statuses.active,
        },
      )
      notificationService.notifyReviewer({
        journal,
        manuscript,
        user: reviewer,
        submittingAuthor,
        editorialAssistant,
      })
    }

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerCancelled',
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: reviewer.userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
