import { rorService } from 'component-ror'

const initialize = ({ TeamMember, Team }) => ({
  async execute(manuscriptId, rorId, aff, country) {
    if (!rorId && (!aff || !country)) {
      return []
    }

    if (!rorId) {
      const bestROR = await rorService.getBestROR(aff, country)
      if (!bestROR?.organization?.id) {
        return []
      }
      rorId = bestROR?.organization?.id
    }

    const authors = await TeamMember.findAllByManuscriptsAndRole({
      manuscriptIds: [manuscriptId],
      role: Team.Role.author,
    })
    const conflictingAuthors =
      authors.filter(author => author.alias.affRorId === rorId) || []
    return conflictingAuthors.map(author => author.toDTO())
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

export { initialize, authsomePolicies }
