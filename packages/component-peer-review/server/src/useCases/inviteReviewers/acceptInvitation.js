const { head } = require('lodash')
const config = require('config')

const removalDays = config.get('reminders.reviewer.submitReport.remove')
const timeUnit = config.get('reminders.reviewer.submitReport.timeUnit')

const initialize = ({
  models: {
    Job,
    User,
    Team,
    Review,
    Comment,
    Journal,
    Identity,
    Manuscript,
    TeamMember,
  },
  logEvent,
  jobsService,
  eventsService,
  emailJobsService,
  removalJobsService,
  notificationService,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(teamMemberId)
    const reviewerJobs = await Job.findAllByTeamMember(teamMemberId)
    jobsService.cancelJobs(reviewerJobs)

    if (teamMember.status === TeamMember.Statuses.expired) {
      throw new ValidationError('Your invitation has expired')
    }

    if (teamMember.status !== TeamMember.Statuses.pending) {
      throw new ValidationError('User already responded to the invitation.')
    }
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    if (
      ![
        Manuscript.Statuses.reviewersInvited,
        Manuscript.Statuses.underReview,
        Manuscript.Statuses.reviewCompleted,
        Manuscript.Statuses.makeDecision,
      ].includes(manuscript.status)
    ) {
      throw new ValidationError('Your invitation has expired')
    }

    if (teamMember.userId !== userId) {
      throw new AuthorizationError('Unauthorized')
    }

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const acceptedSubmittedReviewers = await TeamMember.findAllByStatuses({
      role: Team.Role.reviewer,
      statuses: [TeamMember.Statuses.accepted, TeamMember.Statuses.submitted],
      submissionId: manuscript.submissionId,
    })
    await setReviewerNumber({
      acceptedSubmittedReviewers,
      teamMember,
      userId,
      manuscript,
      Manuscript,
    })
    teamMember.updateProperties({ status: TeamMember.Statuses.accepted })
    await teamMember.save()

    const localIdentity = await Identity.findOneBy({
      queryObject: {
        type: 'local',
        userId: teamMember.userId,
      },
    })
    localIdentity.updateProperties({
      isConfirmed: true,
    })
    await localIdentity.save()

    const draftReview = new Review({
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
    })
    await draftReview.save()

    const publicComment = new Comment({
      reviewId: draftReview.id,
      type: Comment.Types.public,
    })
    await publicComment.save()

    const privateComment = new Comment({
      reviewId: draftReview.id,
      type: Comment.Types.private,
    })
    await privateComment.save()

    const journal = await Journal.find(manuscript.journalId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    academicEditor.user = await User.find(academicEditor.userId)
    teamMember.user = await User.find(teamMember.userId)

    await emailJobsService.scheduleEmailsWhenReviewerAcceptsInvitation({
      journal,
      manuscript,
      reviewer: teamMember,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistant,
    })

    await removalJobsService.scheduleRemovalJob({
      timeUnit,
      days: removalDays,
      invitationId: teamMemberId,
      manuscriptId: manuscript.id,
    })

    await notificationService.notifyReviewer({
      journal,
      manuscript,
      academicEditor,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    notificationService.notifyAcademicEditor({
      journal,
      manuscript,
      academicEditor,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerAccepted',
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
const setReviewerNumber = async ({
  acceptedSubmittedReviewers,
  teamMember,
  userId,
  manuscript,
  Manuscript,
}) => {
  const responded = new Date()

  const oldReviewer = acceptedSubmittedReviewers.filter(
    teamMember => teamMember.userId === userId,
  )
  const uniqueReviewers = [
    ...new Set(acceptedSubmittedReviewers.map(teamMember => teamMember.userId)),
  ]

  let reviewerNumber
  if (oldReviewer) {
    reviewerNumber = head(oldReviewer.map(nr => nr.reviewerNumber))
    teamMember.updateProperties({ reviewerNumber, responded })
  }

  if (!teamMember.reviewerNumber) {
    if (acceptedSubmittedReviewers.length === 0) {
      teamMember.updateProperties({ reviewerNumber: 1, responded })
      manuscript.updateStatus(Manuscript.Statuses.underReview)
      await manuscript.save()
    } else {
      reviewerNumber = uniqueReviewers.length + 1
      teamMember.updateProperties({ reviewerNumber, responded })
    }
  }

  return reviewerNumber
}
