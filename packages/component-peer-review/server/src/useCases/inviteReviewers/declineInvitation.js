const initialize = ({
  models: { TeamMember, Journal, Manuscript, User, Job, Team },
  logEvent,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ teamMemberId }) {
    const teamMember = await TeamMember.find(teamMemberId)
    const teamMemberJobs = await Job.findAllByTeamMember(teamMemberId)
    jobsService.cancelJobs(teamMemberJobs)

    if (teamMember.status !== TeamMember.Statuses.pending) {
      throw new ConflictError('User already responded to the invitation.')
    }
    const responded = new Date()
    teamMember.updateProperties({
      status: TeamMember.Statuses.declined,
      responded,
    })
    await teamMember.save()

    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const journal = await Journal.find(manuscript.journalId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    academicEditor.user = await User.find(academicEditor.userId)

    notificationService.notifyAcademicEditor({
      journal,
      manuscript,
      academicEditor,
      submittingAuthor,
      user: teamMember,
      editorialAssistant,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerDeclined',
    })

    logEvent({
      userId: teamMember.userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})
const authsomePolicies = ['public']
module.exports = {
  initialize,
  authsomePolicies,
}
