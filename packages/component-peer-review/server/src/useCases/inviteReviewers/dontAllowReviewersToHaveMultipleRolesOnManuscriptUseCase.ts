interface TeamMemberType {
  findOneByManuscriptAndUser({ userId, manuscriptId }): TeamMemberType
}

const initialize = (TeamMember: TeamMemberType) => ({
  async execute({ userId, manuscriptId }) {
    const manuscriptRole = await TeamMember.findOneByManuscriptAndUser({
      userId,
      manuscriptId,
    })

    if (manuscriptRole)
      throw new Error(`The reviewer already has a role on the manuscript`)
  },
})

export { initialize }
