const initialize = ({ TeamMember }) => ({
  async execute(reviewers) {
    reviewers.forEach(rev => {
      rev.status = TeamMember.Statuses.expired
    })
    return reviewers
  },
})

export { initialize }
