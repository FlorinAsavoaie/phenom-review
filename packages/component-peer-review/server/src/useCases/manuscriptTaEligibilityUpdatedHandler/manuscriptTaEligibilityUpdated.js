function initialize({ models: Manuscript }) {
  return { execute }

  async function execute({ submissionId, fundingStatementText }) {
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    // if no funding statement provided do nothing
    if (fundingStatementText === null) {
      return
    }

    await appendFunderText(lastManuscript, fundingStatementText)
  }

  async function appendFunderText(lastManuscript, fundingStatementText) {
    const existingStatement = lastManuscript.funding_statement || ''

    const appendedStatement = `${existingStatement} ${fundingStatementText}`
    lastManuscript.updateProperties({
      funding_statement: appendedStatement,
    })
    await lastManuscript.save()
  }
}

module.exports = { initialize }
