const manuscriptTaEligibilityUpdatedHandler = require('./manuscriptTaEligibilityUpdated')

module.exports = {
  manuscriptTaEligibilityUpdatedHandler,
}
