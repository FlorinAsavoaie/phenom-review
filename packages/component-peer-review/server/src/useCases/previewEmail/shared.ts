import axios from 'axios'

const { last } = require('lodash')
const { logger } = require('component-logger')

export enum EmailUseCases {
  EDITORIAL_RECOMMENDATION_TO_REVISE_AUTHOR = 'EditorialRecommendationToRevise(Author)',
  EDITORIAL_RECOMMENDATION_TO_REJECT_TE = 'EditorialRecommendationToReject(TE)',
  EDITORIAL_RECOMMENDATION_TO_PUBLISH_TE = 'EditorialRecommendationToPublish(TE)',
  INVITATION_TO_HANDLE_A_MANUSCRIPT_AE = 'InvitationToHandleAManuscript(AE)',
  INVITATION_TO_REVIEW_A_MANUSCRIPT_REVIEWER = 'InvitationToReviewAManuscript(Reviewer)',
}
export interface CommsPreviewEmailInput {
  usecase: string
  templatePlaceholders: Record<string, string>
}
export interface TargetUser {
  email: string
  givenNames: string
  surname: string
  id: string
}
export interface PreviewEmailInput {
  usecase: string
  submissionId: string
  additionalComments?: string
  targetUser?: TargetUser
  requestedJournalEditorialBoard?: boolean
}
export interface PreviewEmailResponse {
  html: string
  subject: string
}

const url: string = process.env.GATEWAY_URI

export const getPreviewEmail = async (
  { usecase, templatePlaceholders }: CommsPreviewEmailInput,
  ctx: any,
): Promise<PreviewEmailResponse> => {
  const res = await axios.post(
    url,
    {
      query: `
        query previewEmail($input: PreviewEmailInput!) {
          previewEmail(emailInput: $input) {
            html
            subject
          }
        }
        `,
      variables: {
        input: {
          usecase,
          templatePlaceholders,
        },
      },
    },
    { headers: { authorization: ctx.req.authInfo.token } },
  )

  const dataResponse = res.data.data

  if (dataResponse) {
    const { html, subject } = dataResponse.previewEmail
    return { html, subject }
  }
  const errorResponse = res.data.errors[0].message

  logger.error(usecase, errorResponse)
  throw new Error(errorResponse)
}

export const getManuscriptData = async (models, submissionId: string) => {
  try {
    const { Manuscript, Journal } = models
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      order: 'ASC',
      orderByField: 'version',
    })

    const manuscript = last(manuscripts)

    const journal = await Journal.find(manuscript.journalId)

    return { manuscript, journal }
  } catch (error) {
    logger.error(
      `Failed to get manuscript data for submissionId: ${submissionId}`,
      error,
    )
    throw new Error(
      `Failed to get manuscript data for submissionId: ${submissionId}`,
    )
  }
}

export const getEmailParticipants = ({
  usecase,
  submittingAuthor,
  triageEditor,
  academicEditor,
  invitedAcademicEditor,
  editorialAssistant,
  reviewer,
}): { toUser: any; fromUser: any } => {
  switch (usecase) {
    case EmailUseCases.EDITORIAL_RECOMMENDATION_TO_REVISE_AUTHOR:
      return {
        toUser: submittingAuthor,
        fromUser: academicEditor || triageEditor || editorialAssistant,
      }
    case EmailUseCases.EDITORIAL_RECOMMENDATION_TO_REJECT_TE:
    case EmailUseCases.EDITORIAL_RECOMMENDATION_TO_PUBLISH_TE:
      return {
        toUser: triageEditor,
        fromUser: academicEditor || editorialAssistant,
      }
    case EmailUseCases.INVITATION_TO_HANDLE_A_MANUSCRIPT_AE:
      return {
        toUser: invitedAcademicEditor,
        fromUser: triageEditor || editorialAssistant,
      }
    case EmailUseCases.INVITATION_TO_REVIEW_A_MANUSCRIPT_REVIEWER:
      return { toUser: reviewer, fromUser: academicEditor }
    default:
      throw new Error('UseCase not supported')
  }
}

export const getInvitedAcademicEditor = async ({
  manuscript,
  targetUser,
  journal,
  models,
  requestedJournalEditorialBoard,
}) => {
  const { TeamMember, Team } = models

  if (targetUser?.id) {
    if (manuscript.specialIssueId && !requestedJournalEditorialBoard) {
      return TeamMember.findOneBySpecialIssueAndUser({
        userId: targetUser.id,
        specialIssueId: manuscript.specialIssueId,
      })
    }
    return TeamMember.findOneByJournalAndUser({
      role: Team.Role.academicEditor,
      userId: targetUser.id,
      journalId: journal.id,
    })
  }
}

export const getInvitedReviewer = (targetUser, models) => {
  const { TeamMember } = models
  if (targetUser) {
    return new TeamMember({
      alias: { ...targetUser },
    })
  }
}
