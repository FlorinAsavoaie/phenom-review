import {
  getManuscriptData,
  getEmailParticipants,
  getPreviewEmail,
  PreviewEmailResponse,
  PreviewEmailInput,
  getInvitedAcademicEditor,
  getInvitedReviewer,
} from './shared'

const config = require('config')
const models = require('@pubsweet/models')
const getProps = require('../../emailPropsService/emailPropsService')
const urlService = require('../../urlService/urlService')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')

export const getPreviewEmailUseCase = async (
  data: PreviewEmailInput,
  ctx,
): Promise<PreviewEmailResponse> => {
  const { Team, TeamMember, ArticleType } = models
  const {
    usecase,
    submissionId,
    additionalComments,
    targetUser,
    requestedJournalEditorialBoard,
  } = data
  const { manuscript, journal } = await getManuscriptData(models, submissionId)

  const articleType = await ArticleType.findArticleTypeByManuscript(
    manuscript.id,
  )

  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
      manuscriptId: manuscript.id,
    },
  )

  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)

  const triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: manuscript.id,
    role: Team.Role.triageEditor,
    status: TeamMember.Statuses.active,
  })

  const invitedAcademicEditor = await getInvitedAcademicEditor({
    manuscript,
    targetUser,
    journal,
    models,
    requestedJournalEditorialBoard,
  })
  const reviewer = getInvitedReviewer(targetUser, models)

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    manuscriptId: manuscript.id,
  })

  const { toUser, fromUser } = getEmailParticipants({
    usecase,
    submittingAuthor,
    triageEditor,
    academicEditor,
    invitedAcademicEditor,
    editorialAssistant,
    reviewer,
  })

  const authors = await TeamMember.findAllByManuscriptAndRole({
    role: Team.Role.author,
    manuscriptId: manuscript.id,
  })

  const getPropsService = getProps.initialize({
    baseUrl,
    urlService,
    unsubscribeSlug,
  })

  const emailProps = {
    manuscript,
    journal,
    toUser,
    fromUser,
    articleTypeName: articleType.name,
    submittingAuthor,
    additionalComments,
    authors,
    editorialAssistant,
  }

  const { templatePlaceholders } = getPropsService.getEmailData(emailProps)
  return getPreviewEmail({ usecase, templatePlaceholders }, ctx)
}

const authsomePolicies = ['isAuthenticated']
export default { authsomePolicies }
