import { PreviewEmailInput, PreviewEmailResponse } from './shared'
import { getPreviewEmailUseCase } from './previewEmail'

const config = require('config')
const { logger } = require('component-logger')

const disablePreviewEmailRequest = config.get(
  'communications.disablePreviewEmailRequest',
)

const resolver = {
  Query: {
    async getPreviewEmail(
      _,
      { input }: Record<'input', PreviewEmailInput>,
      ctx,
    ): Promise<PreviewEmailResponse> {
      const { usecase } = input
      if (disablePreviewEmailRequest) {
        return {
          html: `<div style="color:red">
          Disabled get email content for local testing.
          <br />
          The RequestEmail event will be sent with this custom html, but if you don't have the Communications app opened, the email will not be sent.
          </div>`,
          subject: '',
        }
      }

      try {
        return await getPreviewEmailUseCase(input, ctx)
      } catch (error) {
        logger.error(usecase, error)
        throw new Error(error)
      }
    },
  },
}

module.exports = resolver
