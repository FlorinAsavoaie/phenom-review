const Promise = require('bluebird')

const initialize = ({
  models,
  useCases,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { Team, TeamMember, User } = models

    await useCases.cancelAllJobsUseCase
      .initialize({ models, jobsService })
      .execute({
        manuscript,
        academicEditor,
        editorialAssistant,
      })

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
      eagerLoadRelations: 'user',
    })
    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.accepted,
    )
    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.pending,
    )

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })

    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)

      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
        eventsService,
      })
    }

    await Promise.each(
      [...acceptedReviewers, ...pendingReviewers],
      async reviewer => (reviewer.user = await User.find(reviewer.userId)),
    )

    notificationService.notifyReviewers({
      journal,
      manuscript,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
      reviewers: [...acceptedReviewers, ...pendingReviewers],
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionRevisionRequested',
    })
  },
})

const authsomePolicies = ['isAcademicEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
