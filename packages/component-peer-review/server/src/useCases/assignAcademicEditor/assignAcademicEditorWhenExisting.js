const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const initialize = ({ models, jobsService, eventsService }) => ({
  async execute({ manuscripts, latestManuscript, user, academicEditors }) {
    const { Manuscript, Team, TeamMember, Job } = models
    const removedAcademicEditors = []
    await Promise.each(academicEditors, async academicEditor => {
      academicEditor.status = TeamMember.Statuses.removed
      const jobs = await Job.findAllByTeamMember(academicEditor.id)
      await jobsService.cancelJobs(jobs)
      removedAcademicEditors.push(academicEditor)
    })

    const newAcademicEditors = []
    await Promise.each(manuscripts, async manuscript => {
      const academicEditorTeam = await Team.findOneByManuscriptAndRole({
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      })
      const academicEditor = academicEditorTeam.addMember({
        user,
        options: {
          status: TeamMember.Statuses.accepted,
          responded: new Date(),
        },
      })
      newAcademicEditors.push(academicEditor)
    })

    if (latestManuscript.status === Manuscript.Statuses.academicEditorInvited) {
      latestManuscript.status = Manuscript.Statuses.academicEditorAssigned
    }
    try {
      await Manuscript.updateManuscriptAndTeamMembers({
        teamMembers: [...removedAcademicEditors, ...newAcademicEditors],
        manuscript: latestManuscript,
        TeamMember,
      })
    } catch (err) {
      logger.error('Something went wrong.', err)
      throw new Error(err)
    }

    eventsService.publishSubmissionEvent({
      submissionId: latestManuscript.submissionId,
      eventName: 'SubmissionAcademicEditorRemoved',
    })
  },
})

module.exports = { initialize }
