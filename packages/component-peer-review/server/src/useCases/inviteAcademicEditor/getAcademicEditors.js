const { pullAllBy, take } = require('lodash')

const initialize = ({ TeamMember, Team, Manuscript }, config) => {
  return {
    execute,
  }

  async function execute({
    manuscriptId,
    searchValue,
    requestedJournalEditorialBoard,
  }) {
    const numberOfAcademicEditorsToShow = config.get(
      'numberOfAcademicEditorsToShow',
    )
    const manuscript = await Manuscript.find(manuscriptId)

    let members
    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      { Team, TeamMember },
    )

    if (
      manuscript.specialIssueId &&
      !hasSpecialIssueEditorialConflictOfInterest &&
      !requestedJournalEditorialBoard
    ) {
      members = await TeamMember.findAllBySpecialIssueAndRoleAndSearchValue({
        specialIssueId: manuscript.specialIssueId,
        role: Team.Role.academicEditor,
        searchValue,
        eagerLoadRelations: 'user.identities',
      })
    } else {
      members = await TeamMember.findAllByJournalAndRoleAndSearchValue({
        role: Team.Role.academicEditor,
        journalId: manuscript.journalId,
        searchValue,
        eagerLoadRelations: 'user.identities',
      })
    }

    let orderedMembers = await TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail(
      {
        academicEditors: members,
        manuscript,
      },
    )

    orderedMembers = take(orderedMembers, numberOfAcademicEditorsToShow)

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })

    orderedMembers = orderedMembers.filter(
      member => !authors.find(author => member.userId === author.userId),
    )

    const declinedManuscriptAcademicEditorMembers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    const removedSubmissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.removed],
        submissionId: manuscript.submissionId,
      },
    )

    const academicEditors = pullAllBy(
      orderedMembers,
      [
        ...declinedManuscriptAcademicEditorMembers,
        ...removedSubmissionAcademicEditorMembers,
      ],
      'userId',
    ).map(tm => ({
      ...tm,
      invited: tm.created,
      alias: {
        ...tm.alias,
        name: {
          surname: tm.alias.surname,
          givenNames: tm.alias.givenNames,
          title: tm.alias.title,
        },
      },
    }))

    // * check the conflicts of interests with the authors
    academicEditors.forEach(academicEditor => {
      if (!academicEditor.alias.affRorId) return []
      authors.forEach(author => {
        if (author.alias.affRorId === academicEditor.alias.affRorId) {
          author.alias = {
            ...author.alias,
            aff: author.alias.aff,
            country: author.alias.country,
            email: author.alias.email,
            name: {
              surname: author.alias.surname,
              givenNames: author.alias.givenNames,
              title: author.alias.title,
            },
          }

          if (!Array.isArray(academicEditor.conflictsOfInterest)) {
            academicEditor.conflictsOfInterest = []
          }

          academicEditor.conflictsOfInterest.push(author)
        }
      })
    })

    if (academicEditors.length === 0) {
      return []
    }

    return academicEditors
  }
}

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
