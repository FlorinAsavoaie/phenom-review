import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateTeamMember,
} from 'component-generators'

import { dontAllowEditorsAsReviewersOnSpecialIssuesUseCase } from '../../src/useCases/inviteReviewers'

const mockModels = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findAllBySpecialIssueAndUserAndRoles: jest.fn(),
  },
}
const { Team, TeamMember } = mockModels

describe('Do not allow editors as reviewers on special issue use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('throws an error if the invited user has an editorial role on a special issue', async () => {
    const teamMember = generateTeamMember()

    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndUserAndRoles')
      .mockReturnValue([teamMember])

    expect(() => {
      dontAllowEditorsAsReviewersOnSpecialIssuesUseCase
        .initialize(TeamMember)
        .execute('user-id', 'si-id', Team.Role)
    }).toThrow(
      'This user cannot be invited because they are already part of the Special Issue team.',
    )
  })
})
