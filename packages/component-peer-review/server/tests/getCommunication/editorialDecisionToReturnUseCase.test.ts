import {
  generateJournal,
  generateManuscript,
  generateTeamMember,
  generateUser,
  getTeamRoles,
  getTeamMemberStatuses,
} from 'component-generators'

import { TemplateName } from '@hindawi/phenom-communications-types'

import {
  CommunicationInput,
  TemplateUsage,
} from '../../src/useCases/getCommunication/getCommunication'
import { editorialDecisionToReturnUseCase } from '../../src/useCases/getCommunication/editorialDecisionToReturnUseCase'

const shared = require('../../src/useCases/getCommunication/shared')

describe('editorialDecisionToReturnUseCase', () => {
  let models
  let data: CommunicationInput
  let responseMock

  const manuscript = generateManuscript({
    customId: 'asd',
    title: 'Manuscript Title',
  })
  const submittingAuthor = generateTeamMember()
  const journal = generateJournal()
  const signedToken = 'signedToken'

  shared.getSignedToken = () => signedToken

  beforeAll(() => {
    models = {
      Manuscript: {
        findManuscriptsBySubmissionId: jest.fn(() => [manuscript]),
      },
      TeamMember: {
        findOneByManuscriptAndRoleAndStatus: jest.fn(() =>
          generateTeamMember(),
        ),
        findOneByManuscriptAndRole: jest.fn(() =>
          generateTeamMember({ getName: () => 'John Doe' }),
        ),
        findSubmittingAuthor: jest.fn(() => submittingAuthor),
        findAllByManuscriptAndRole: jest.fn(() => [
          generateTeamMember({ getName: () => 'John Doe' }),
          generateTeamMember({ getName: () => 'John Doi' }),
        ]),
        Statuses: getTeamMemberStatuses(),
      },
      Team: { Role: getTeamRoles() },
      Journal: { find: jest.fn(() => journal) },
    }

    data = {
      submissionId: '1234',
      usage: TemplateUsage.EDITORIAL_DECISION_TO_RETURN,
      sender: generateUser({
        identities: [{ surname: 'test', givenNames: 'test' }],
      }),
    }

    responseMock = {
      templateName: TemplateName.editorialDecisionReturnedWithComments,
      mailInput: {
        manuscript: {
          manuscriptId: manuscript.customId,
          manuscriptTitle: manuscript.title,
          journalName: journal.name,
          submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
        },
      },
      signedInput: signedToken,
    }
  })
  it('should return proper communication data when manuscript is returned to academic editor', async () => {
    const response = await editorialDecisionToReturnUseCase(data, models)
    expect(response).toEqual(responseMock)
  })
})
