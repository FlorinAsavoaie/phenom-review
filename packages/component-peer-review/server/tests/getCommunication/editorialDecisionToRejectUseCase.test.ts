import {
  generateJournal,
  generateManuscript,
  generateTeamMember,
  generateUser,
  getTeamRoles,
  getTeamMemberStatuses,
} from 'component-generators'

import { TemplateName } from '@hindawi/phenom-communications-types'

import { editorialDecisionToRejectUseCase } from '../../src/useCases/getCommunication/editorialDecisionToRejectUseCase'
import {
  CommunicationInput,
  TemplateUsage,
} from '../../src/useCases/getCommunication/getCommunication'

const shared = require('../../src/useCases/getCommunication/shared')

describe('editorialDecisionToRejectUseCase', () => {
  let models
  let data: CommunicationInput
  let responseMock

  const manuscript = generateManuscript({
    customId: 'asd',
    title: 'Manuscript Title',
  })
  const submittingAuthor = generateTeamMember()
  const journal = generateJournal()
  const signedToken = 'signedToken'

  shared.getSignedToken = () => signedToken

  beforeAll(() => {
    models = {
      Manuscript: {
        findManuscriptsBySubmissionId: jest.fn(() => [manuscript]),
      },
      TeamMember: {
        findSubmittingAuthor: jest.fn(() => submittingAuthor),
        findAllByManuscriptAndRole: jest.fn(() => [
          generateTeamMember({ getName: () => 'John Doe' }),
          generateTeamMember({ getName: () => 'John Doi' }),
        ]),
        findOneByManuscriptAndRole: jest.fn(() =>
          generateTeamMember({ getName: () => 'John Doe' }),
        ),
        findTriageEditor: jest.fn(() => generateTeamMember()),
        Statuses: getTeamMemberStatuses(),
      },
      Team: { Role: getTeamRoles() },
      Journal: { find: jest.fn(() => journal) },
      User: { find: jest.fn(() => generateUser()) },
    }

    data = {
      submissionId: '1234',
      usage: TemplateUsage.EDITORIAL_DECISION_TO_REJECT,
      sender: generateUser({
        identities: [{ surname: 'test', givenNames: 'test' }],
      }),
    }

    responseMock = {
      mailInput: {
        manuscript: {
          manuscriptId: manuscript.customId,
          manuscriptTitle: manuscript.title,
          journalName: journal.name,
          submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
        },
      },
      signedInput: signedToken,
    }
  })

  it('should return proper communication data when manuscript with no peer review was rejected', async () => {
    const response = await editorialDecisionToRejectUseCase(data, models)
    responseMock.templateName = TemplateName.rejectNoPeerReviews
    expect(response).toEqual(responseMock)
  })
  it('should return proper communication data when manuscript was rejected with peer review', async () => {
    models.TeamMember.findAllByManuscriptAndRole = jest.fn(() => [
      generateTeamMember({ getName: () => 'John Doe', status: 'submitted' }),
      generateTeamMember({ getName: () => 'John Doi' }),
    ])
    const response = await editorialDecisionToRejectUseCase(data, models)
    responseMock.templateName = TemplateName.rejectWithPeerReviews
    expect(response).toEqual(responseMock)
  })
})
