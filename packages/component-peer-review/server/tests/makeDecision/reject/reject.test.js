const {
  generateManuscript,
  generateTeamMember,
  generateArticleType,
  getManuscriptStatuses,
  getManuscriptNonActionableStatuses,
} = require('component-generators')

const {
  makeDecisionToRejectUseCase,
} = require('../../../src/useCases/makeDecision')

const useCases = {
  rejectWithPeerReviewUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  rejectWithNoPeerReviewUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  getReasonsForRejectionUseCase: {
    initialize: jest.fn(() => ({
      execute: () => [
        'outOfScope',
        'technicalOrScientificFlaws',
        'publicationEthicsConcerns',
        'otherReasons',
      ],
    })),
  },
}

let models = {}
let Manuscript = {}
let TeamMember = {}
let RejectDecisionInfo = jest.fn()

const uploadToMTS = jest.fn()
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  manuscript_rejected: 'manuscript_rejected',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Make decision to reject', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Manuscript: {
        find: jest.fn(),
        Statuses: getManuscriptStatuses(),
        NonActionableStatuses: getManuscriptNonActionableStatuses(),
      },
      TeamMember: {
        findOneByManuscriptAndUser: jest.fn(),
      },
      RejectDecisionInfo,
    }
    ;({ Manuscript, TeamMember, RejectDecisionInfo } = models)
  })

  it('Throws an error if the manuscript is in a wrong status', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.published,
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const res = makeDecisionToRejectUseCase.initialize({ models }).execute({
      manuscriptId: manuscript.id,
      userId: editor.id,
      content: 'Manuscript rejected',
      rejectDecisionInfo: {
        reasonsForRejection: {
          outOfScope: true,
          technicalOrScientificFlaws: true,
          publicationEthicsConcerns: true,
          otherReasons: 'test2',
        },
        transferToAnotherJournal: {
          selectedOption: 'YES',
          transferSuggestions: 'My journal',
        },
      },
    })

    return expect(res).rejects.toThrow(
      'Cannot reject a manuscript in the current status.',
    )
  })

  it('Executes rejectWithPeerReviewUseCase if manuscript has peer review', async () => {
    const articleType = generateArticleType({
      hasPeerReview: true,
    })
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.makeDecision,
      articleType,
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValue(editor)

    await makeDecisionToRejectUseCase
      .initialize({ models, useCases, uploadToMTS, eventsService, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editor.id,
        content: 'Manuscript rejected',
        rejectDecisionInfo: {
          reasonsForRejection: {
            outOfScope: true,
            technicalOrScientificFlaws: true,
            publicationEthicsConcerns: true,
            otherReasons: 'test2',
          },
          transferToAnotherJournal: {
            selectedOption: 'YES',
            transferSuggestions: 'My journal',
          },
        },
      })

    expect(useCases.rejectWithPeerReviewUseCase.initialize).toBeCalledTimes(1)
  })

  it('Executes rejectWithNoPeerReviewUseCase if manuscript has peer review', async () => {
    const articleType = generateArticleType({
      hasPeerReview: false,
    })
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.makeDecision,
      articleType,
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValue(editor)

    await makeDecisionToRejectUseCase
      .initialize({ models, useCases, uploadToMTS, eventsService, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editor.id,
        content: 'Manuscript rejected',
        rejectDecisionInfo: {
          reasonsForRejection: {
            outOfScope: true,
            technicalOrScientificFlaws: true,
            publicationEthicsConcerns: true,
            otherReasons: 'test2',
          },
          transferToAnotherJournal: {
            selectedOption: 'YES',
            transferSuggestions: 'My journal',
          },
        },
      })

    expect(useCases.rejectWithNoPeerReviewUseCase.initialize).toBeCalledTimes(1)
  })
})
