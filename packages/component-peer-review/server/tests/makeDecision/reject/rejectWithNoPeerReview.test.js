const {
  getTeamRoles,
  getCommentTypes,
  generateManuscript,
  generateTeamMember,
  getRecommendations,
  getManuscriptStatuses,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  rejectWithNoPeerReviewUseCase,
} = require('../../../src/useCases/makeDecision')

let models = {}
let Manuscript = {}
let TeamMember = {}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const jobsService = {
  cancelJobs: jest.fn(),
}

const notificationService = {
  notifyAuthorsNoPeerReview: jest.fn(),
}

describe('Make decision to reject when article has no peer review', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        find: jest.fn(),
        knex: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        Statuses: getTeamMemberStatuses(),
      },
      Job: {
        findAllByTeamMembers: jest.fn(),
        findAllByTeamMemberAndManuscript: jest.fn(),
      },
      Review: jest.fn(), // mock constructor
      Comment: jest.fn(), // mock constructor
    }
    models.Review.Recommendations = getRecommendations()
    models.Comment.Types = getCommentTypes()
    ;({ Manuscript, TeamMember } = models)
  })
  it('Executes the main flow', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.published,
      $query: jest.fn(() => ({
        updateAndFetch: jest.fn(() => manuscript),
      })),
      $relatedQuery: jest.fn(() => ({
        insert: jest.fn(),
        insertAndFetch: jest.fn(() => manuscript),
      })),
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})

    await rejectWithNoPeerReviewUseCase
      .initialize({
        models,
        transaction,
        logger,
        jobsService,
        notificationService,
      })
      .execute({
        manuscript,
        rejectEditor: editor,
        content: 'Rejecting manuscript',
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)
  })
})
