const {
  getTeamRoles,
  getTeamMemberStatuses,
  generateTeamMember,
  generateManuscript,
  getManuscriptStatuses,
} = require('component-generators')

const {
  acceptAcademicEditorInvitationUseCase,
} = require('../../src/useCases/inviteAcademicEditor')

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript: {
    findManuscriptByTeamMember: jest.fn(),
    Statuses: getManuscriptStatuses(),
  },
  TeamMember: {
    find: jest.fn(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
}

const useCases = {
  acceptNormalAcademicEditorInvitationUseCase: {
    initialize: jest.fn(() => ({
      execute: async () => {},
    })),
  },
  acceptReassignmentAcademicEditorInvitationUseCase: {
    initialize: jest.fn(() => ({
      execute: async () => {},
    })),
  },
}

const initializeUseCase = overrides => {
  const params = {
    useCases,
    jobsService: {
      cancelJobs: jest.fn(),
    },
    eventsService: {
      publishSubmissionEvent: jest.fn(),
    },
    emailJobsService: {
      sendAcademicEditorRemindersToInviteReviewers: jest.fn(),
    },
    notificationService: {
      notifyTriageEditor: jest.fn(),
      notifyEditorialAssistant: jest.fn(),
      notifyPendingAcademicEditorAfterConflictOfInterests: jest.fn(),
      notifyPendingAcademicEditor: jest.fn(),
    },
    logEvent: jest.fn(),
    models,
    ...overrides,
  }

  return acceptAcademicEditorInvitationUseCase.initialize(params)
}
describe('Accept invitation', () => {
  beforeEach(() => jest.clearAllMocks())
  it('throws validation error if user already responded to the invitation', async () => {
    // generate ae with notPending status
    const pendingAcademicEditor = generateTeamMember({
      status: 'otherThanPending',
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue({ id: '1' })

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    const usecase = await initializeUseCase({})

    await expect(
      usecase.execute({ userId: pendingAcademicEditor.userId }),
    ).rejects.toThrowError('User already responded to the invitation.')
  })
  it('throws validation error if manuscript status is "rejected"', async () => {
    // generate ae with pending status
    const pendingAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    // generate rejected manuscript
    const rejectedManuscript = generateManuscript({
      status: models.Manuscript.Statuses.rejected,
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(rejectedManuscript)

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    const usecase = await initializeUseCase({})

    await expect(
      usecase.execute({ userId: pendingAcademicEditor.userId }),
    ).rejects.toThrowError('Cannot accept invitation in this status')
  })
  it('throws validation error if manuscript is "deleted"', async () => {
    // generate ae with pending status
    const pendingAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    // generate deleted manuscript
    const rejectedManuscript = generateManuscript({
      status: models.Manuscript.Statuses.deleted,
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(rejectedManuscript)

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    const usecase = await initializeUseCase({})

    await expect(
      usecase.execute({ userId: pendingAcademicEditor.userId }),
    ).rejects.toThrowError('Unauthorized')
  })

  it('throws validation error if manuscript is "widthdrawn"', async () => {
    // generate ae with pending status
    const pendingAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    // generate withdrawn manuscript
    const rejectedManuscript = generateManuscript({
      status: models.Manuscript.Statuses.withdrawn,
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(rejectedManuscript)

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    const usecase = await initializeUseCase({})

    await expect(
      usecase.execute({ userId: pendingAcademicEditor.userId }),
    ).rejects.toThrowError('Unauthorized')
  })

  it('follows the acceptReassignment usecase if there is an activeAcademicEditor and the current one is pending', async () => {
    // generate ae with pending status
    const pendingAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    // generate ae with accepted status
    const activeAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue({})

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(activeAcademicEditor)

    await initializeUseCase().execute({
      teamMemberId: '',
      userId: pendingAcademicEditor.userId,
      useCommunication: false,
    })

    expect(
      useCases.acceptReassignmentAcademicEditorInvitationUseCase.initialize,
    ).toBeCalled()
  })
  it('follows the acceptNormalInvitation usecase if there is no accepted(active) ae already', async () => {
    // generate ae with pending status
    const pendingAcademicEditor = generateTeamMember({
      status: models.TeamMember.Statuses.pending,
    })

    jest
      .spyOn(models.Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue({})

    jest
      .spyOn(models.TeamMember, 'find')
      .mockResolvedValue(pendingAcademicEditor)

    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(undefined)

    await initializeUseCase().execute({
      teamMemberId: '',
      userId: pendingAcademicEditor.userId,
      useCommunication: false,
    })

    expect(
      useCases.acceptNormalAcademicEditorInvitationUseCase.initialize,
    ).toBeCalled()
  })
})

// todo. each separate usecase from above should have their own tests files
