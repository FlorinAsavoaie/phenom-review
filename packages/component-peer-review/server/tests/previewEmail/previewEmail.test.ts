import axios from 'axios'

import {
  generateJournal,
  generateManuscript,
  generateTeamMember,
  getTeamRoles,
  generateArticleType,
} from 'component-generators'

import {
  CommsPreviewEmailInput,
  EmailUseCases,
  getManuscriptData,
  getPreviewEmail,
} from '../../src/useCases/previewEmail/shared'

const Chance = require('chance')
const getProps = require('../../src/emailPropsService/emailPropsService')

const chance = new Chance()

jest.mock('axios')

jest.mock('@pubsweet/logger', () => ({
  error: jest.fn(),
}))

const mockedAxios = axios as jest.Mocked<typeof axios>

describe('previewEmail', () => {
  const manuscript = generateManuscript({
    id: chance.guid(),
    customId: chance.guid(),
    title: chance.sentence(),
    abstract: chance.sentence(),
    submissionId: chance.guid(),
  })
  const submittingAuthor = generateTeamMember()
  const fromUser = generateTeamMember({
    getLastName: () => 'Doe',
    alias: { surname: 'Doe', givenNames: chance.name(), email: chance.email() },
  })
  const toUser = generateTeamMember()
  const journal = generateJournal({ name: chance.sentence() })
  const articleType = generateArticleType()
  const ctx = { req: { authInfo: { token: 'token' } } }
  const usecase = EmailUseCases.EDITORIAL_RECOMMENDATION_TO_REVISE_AUTHOR

  const postError = new Error('post() method crashed')
  const notFoundUseCaseError =
    'Could not find any entity of type "EmailTemplateEntity" matching: {"usecase": "RequestRevision(Author)"}'

  describe('getManuscriptData', () => {
    const submissionId = chance.guid()
    const baseModels = {
      TeamMember: {
        findOneByJournalAndUser: jest.fn(() => fromUser),
        findSubmittingAuthor: jest.fn(() => submittingAuthor),
      },
      Team: { Role: getTeamRoles() },
      Journal: { find: jest.fn(() => journal) },
      ArticleType: { findArticleTypeByManuscript: jest.fn(() => articleType) },
    }

    it('should get the manuscript and journal', async () => {
      const manuscriptModel = {
        Manuscript: {
          findManuscriptsBySubmissionId: jest.fn(() => [manuscript]),
        },
      }
      const models = { ...baseModels, ...manuscriptModel }
      const expectedResponse = {
        journal,
        manuscript,
      }
      const response = await getManuscriptData(models, submissionId)

      expect(response).toEqual(expectedResponse)
    })
    it('should throw an error when findManuscriptsBySubmissionId() method fails', async () => {
      const manuscriptModel = {
        Manuscript: {
          findManuscriptsBySubmissionId: jest.fn(() => {
            throw new Error('findManuscriptsBySubmissionId() method crashed')
          }),
        },
      }
      const models = { ...baseModels, ...manuscriptModel }
      await expect(getManuscriptData(models, submissionId)).rejects.toThrow(
        `Failed to get manuscript data for submissionId: ${submissionId}`,
      )
    })
  })

  describe('getPreviewEmail via Communications', () => {
    const templatePlaceholders = {
      MANUSCRIPT_TITLE: 'Manuscript Title',
      MANUSCRIPT_ID: 'Manuscript Id',
    }

    const emailInput: CommsPreviewEmailInput = {
      usecase,
      templatePlaceholders,
    }

    it('should get the html and placeholders', async () => {
      const data = {
        data: {
          data: {
            previewEmail: { html: 'html', subject: 'Reguest Revision' },
          },
        },
      }
      const expectedResponse = { html: 'html', subject: 'Reguest Revision' }

      mockedAxios.post.mockResolvedValueOnce(data)

      const response = await getPreviewEmail(emailInput, ctx)

      expect(response).toEqual(expectedResponse)
    })

    it('should throw an error when post() method fails', async () => {
      mockedAxios.post.mockRejectedValueOnce(postError)

      await expect(getPreviewEmail(emailInput, ctx)).rejects.toThrow(postError)
    })

    it('should throw an error when emailPlaceholders() has errors', async () => {
      const data = {
        data: {
          errors: [
            {
              message: notFoundUseCaseError,
            },
          ],
        },
      }

      mockedAxios.post.mockResolvedValueOnce(data)

      await expect(getPreviewEmail(emailInput, ctx)).rejects.toThrow(
        notFoundUseCaseError,
      )
    })
  })

  describe('getEmailData', () => {
    const bccAddress = 'bcc@example.com'
    const baseUrl = chance.url()
    const urlService = {
      createUrl: () => baseUrl,
    }
    const paragraph1 = chance.sentence()
    const paragraph2 = chance.sentence()
    const unsubscribeSlug = chance.sentence()
    const additionalComments = chance.sentence()
    const getPropsService = getProps.initialize({
      baseUrl,
      urlService,
      unsubscribeSlug,
    })
    const author = generateTeamMember({
      alias: {
        surname: 'surname',
        givenNames: 'givenNames',
        aff: 'affiliation',
      },
    })
    const journal = generateJournal({
      name: chance.sentence(),
      email: chance.email(),
    })

    const editorialAssistant = generateTeamMember()

    const emailProps = {
      manuscript,
      journal,
      toUser,
      fromUser,
      articleTypeName: articleType.name,
      submittingAuthor,
      additionalComments,
      editorialAssistant,
      authors: [author],
      emailContentChanges: [paragraph1, paragraph2],
    }

    it('should get templatePlaceholders and emailInput', async () => {
      const response = getPropsService.getEmailData(emailProps)

      const expectedResponse = {
        emailInput: {
          bcc: [
            {
              email: bccAddress,
            },
          ],
          recipient: {
            email: toUser.alias.email,
            name: `${toUser.alias.givenNames} ${toUser.alias.surname}`,
          },
          sender: {
            email: journal.email,
            name: journal.name,
          },
          replyTo: {
            email: editorialAssistant.alias.email,
            name: journal.name,
          },
          updatedHtmlBodies: [paragraph1, paragraph2],
        },
        templatePlaceholders: {
          ADDITIONAL_COMMENTS: additionalComments,
          JOURNAL_NAME: journal.name,
          MANUSCRIPT_DETAILS_LINK: baseUrl,
          MANUSCRIPT_ID: manuscript.customId,
          MANUSCRIPT_TITLE: manuscript.title,
          MANUSCRIPT_TYPE: articleType.name,
          MANUSCRIPT_ABSTRACT: manuscript.abstract,
          RECIPIENT_EMAIL: toUser.alias.email,
          RECIPIENT_GIVENNAME: toUser.alias.givenNames,
          RECIPIENT_SURNAME: toUser.alias.surname,
          RECIPIENT_TITLE: 'Dr.',
          REVIEWER_SURNAME: fromUser.alias.surname,
          REVIEWER_TITLE: 'Dr.',
          SENDER_EMAIL: fromUser.alias.email,
          SENDER_GIVENNAME: fromUser.alias.givenNames,
          SENDER_SURNAME: fromUser.alias.surname,
          SENDER_TITLE: 'Dr.',
          SUBMITTING_AUTHOR_EMAIL: submittingAuthor.alias.email,
          SUBMITTING_AUTHOR_GIVENNAME: submittingAuthor.alias.givenNames,
          SUBMITTING_AUTHOR_TITLE: 'Dr.',
          SUBMITTING_AUTHOR_SURNAME: submittingAuthor.alias.surname,
          UNSUBSCRIBE_LINK: baseUrl,
          AUTHORS_NAME_LIST: ['givenNames surname'],
          AUTHOR_AFFILIATIONS_LIST: ['affiliation'],
          EDITORIAL_ASSISTANT_GIVENNAME: editorialAssistant.alias.givenNames,
          EDITORIAL_ASSISTANT_SURNAME: editorialAssistant.alias.surname,
          REVIEWER_INVITATION_AGREE_LINK: baseUrl,
          REVIEWER_INVITATION_DECLINE_LINK: baseUrl,
        },
      }

      expect(response).toEqual(expectedResponse)
    })

    it('Should throw an error when getEmailData() has errors', () => {
      const wrongEmailProps = emailProps
      delete wrongEmailProps.manuscript

      expect(() => {
        getPropsService.getEmailData(wrongEmailProps)
      }).toThrow('Failed to get email data')
    })
  })
})
