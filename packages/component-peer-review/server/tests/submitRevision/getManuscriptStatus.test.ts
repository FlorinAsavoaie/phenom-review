import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  getRecommendations,
  getManuscriptStatuses,
  generateTeamMember,
} from 'component-generators'
import { getManuscriptStatusUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findAllByManuscriptAndRole: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
  },
  Review: {
    Recommendations: getRecommendations(),
  },
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
}
const { Review, Manuscript, TeamMember } = models

describe('Get manuscript status use cases', () => {
  beforeEach(() => {})
  it('return submitted if the triage editor requests minor/major revision and there is no academic editor assigned', async () => {
    const reviewRecommendation = Review.Recommendations.major
    const revisionManuscript = generateManuscript()

    const result = await getManuscriptStatusUseCase
      .initialize({ models })
      .execute(reviewRecommendation, revisionManuscript)

    expect(result).toEqual(Manuscript.Statuses.submitted)
  })
  it('return academicEditorAssigned if there is an academicEditor assigned but there are no reviewers', async () => {
    const reviewRecommendation = Review.Recommendations.major
    const revisionManuscript = generateManuscript()
    const academicEditor = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ academicEditor })
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    const result = await getManuscriptStatusUseCase
      .initialize({ models })
      .execute(reviewRecommendation, revisionManuscript)

    expect(result).toEqual(Manuscript.Statuses.academicEditorAssigned)
  })
  it('return reviewCompleted if there is an academicEditor assigned, there are reviewers assigned and a minor revision was requested', async () => {
    const reviewRecommendation = Review.Recommendations.minor
    const revisionManuscript = generateManuscript()
    const academicEditor = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ academicEditor })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([{ id: 'team-member-id' }])

    const result = await getManuscriptStatusUseCase
      .initialize({ models })
      .execute(revisionManuscript, reviewRecommendation)

    expect(result).toEqual(Manuscript.Statuses.reviewCompleted)
  })

  it('return underReview if there is an academicEditor assigned, there are reviewers assigned and a major revision was requested', async () => {
    const reviewRecommendation = Review.Recommendations.major
    const revisionManuscript = generateManuscript()
    const academicEditor = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ academicEditor })
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([{ id: 'team-member-id' }])

    const result = await getManuscriptStatusUseCase
      .initialize({ models })
      .execute(revisionManuscript, reviewRecommendation)

    expect(result).toEqual(Manuscript.Statuses.underReview)
  })
})
