import {
  getTeamRoles,
  generateTeam,
  generateManuscript,
  getTeamMemberStatuses,
} from 'component-generators'
import { generateTeamMember } from 'component-generators/teamMember'
import { copyReviewersToNewVersionUseCase } from '../../src/useCases/submitRevision'

class mockTeamMemberModel {
  constructor(args) {
    return {
      id: 'teamMemberId',
      save() {
        return this
      },
      ...args,
    }
  }
  static Statuses = getTeamMemberStatuses()
}

const models = {
  Team: {
    Role: getTeamRoles(),
    findOrCreate: jest.fn(),
  },
  TeamMember: mockTeamMemberModel,
}

const { TeamMember, Team } = models

describe('Copy Reviewers to new version Use Case', () => {
  beforeEach(() => {})
  it('Returns undefined if there is no reviewer team on the manuscript', async () => {
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(undefined)

    const result = await copyReviewersToNewVersionUseCase
      .initialize({
        Team,
        TeamMember,
      })
      .execute({ id: 'some-id' }, [])

    expect(result).toHaveLength(0)
  })
  it('Returns new reviewers if everything works out fine', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam()
    const teamMember = generateTeamMember({ teamId: team.id })
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(team)

    const result = await copyReviewersToNewVersionUseCase
      .initialize({
        Team,
        TeamMember,
      })
      .execute(manuscript, [teamMember])

    expect(result).toBeInstanceOf(Array)
    expect(result).toHaveLength(1)
    expect(result[0].teamId).toEqual(teamMember.teamId)
  })
})
