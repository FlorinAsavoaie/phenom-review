// const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  generateJournal,
  generateManuscript,
  generatePeerReviewModel,
  generateTeam,
  generateTeamMember,
  getManuscriptStatuses,
  getTeamRoles,
  getTeamMemberStatuses,
  getRecommendations,
  getCommentTypes,
} = require('component-generators')

const useCases = require('../../src/useCases')

const {
  useCases: {
    createMajorVersion,
    copyFilesBetweenManuscripts,
    copyAuthorsBetweenManuscripts,
    copyEAsBetweenManuscripts,
    copyTEsBetweenManuscripts,
    copyAEsBetweenManuscripts,
    copySubmittingStaffMemberBetweenManuscripts,
  },
} = require('component-model')
const { User } = require('fixture-service/models')

const { requestRevisionUseCase } = useCases

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'returned manuscript with comments to',
}
logEvent.objectType = { manuscript: 'manuscript' }

const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const chance = new Chance()

class mockReviewModel {
  constructor(args) {
    return {
      ...args,
      save() {
        return jest.fn(async () => args)
      },
    }
  }
  static Recommendations = getRecommendations()
}

class mockCommentModel {
  constructor(args) {
    return {
      ...args,
      save() {
        return jest.fn(async () => args)
      },
    }
  }
  static Types = getCommentTypes()
}

describe('Request revision use case', () => {
  let notificationService = {}
  beforeEach(() => {
    notificationService = {
      notifyReviewers: jest.fn(),
      notifyTriageEditor: jest.fn(),
      notifyAcademicEditor: jest.fn(),
      notifySubmittingAuthor: jest.fn(),
    }
  })
  it('creates a minor review when a minor revision is requested', async () => {
    const {
      Team,
      Review,
      Journal,
      Manuscript,
      TeamMember,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    const submissionId = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        submissionId,
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    manuscript.journal = journal

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.accepted },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.active },
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    await requestRevisionUseCase
      .initialize({
        models,
        useCases: {
          ...useCases,
          createMajorVersion,
          copyFilesBetweenManuscripts,
          copyAuthorsBetweenManuscripts,
          copyEAsBetweenManuscripts,
          copyTEsBetweenManuscripts,
          copyAEsBetweenManuscripts,
          copySubmittingStaffMemberBetweenManuscripts,
        },
        logEvent,
        services: {
          jobsService,
          eventsService,
          notificationService,
        },
      })
      .execute({
        comment: chance.sentence(),
        manuscriptId: manuscript.id,
        userId: academicEditor.userId,
        type: Review.Recommendations.minor,
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.revisionRequested)
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )

    const draftManuscript = manuscripts.find(
      m => m.version === '2' && m.status === Manuscript.Statuses.draft,
    )

    expect(manuscripts).toHaveLength(2)
    expect(draftManuscript).toBeTruthy()
    expect(
      draftManuscript.teams.find(
        team => team.role === Team.Role.editorialAssistant,
      ),
    ).toBeTruthy()
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(1)
  })

  it('createMajorVersion is called when major revision is requested', async () => {
    const { Team, Review, Manuscript, TeamMember } = models

    const peerReviewModel = generatePeerReviewModel({
      approvalEditors: [Team.Role.academicEditor],
    })

    const journal = generateJournal({
      peerReviewModelId: peerReviewModel.id,
    })

    const manuscript = generateManuscript({
      version: '1',
      journalId: journal.id,
      status: Manuscript.Statuses.makeDecision,
      journal,
    })

    const TETeam = generateTeam({
      role: Team.Role.triageEditor,
      manuscriptId: manuscript.id,
    })
    const AETeam = generateTeam({
      role: Team.Role.academicEditor,
      manuscriptId: manuscript.id,
    })

    const te = generateTeamMember({
      teamId: TETeam.id,
      status: TeamMember.Statuses.active,
    })
    const ae = generateTeamMember({
      teamId: AETeam.id,
      status: TeamMember.Statuses.accepted,
    })

    const mockedUseCases = {
      ...useCases,
      createMajorVersion: {
        initialize: jest.fn(() => ({
          execute: jest.fn(),
        })),
      },
    }

    await requestRevisionUseCase
      .initialize({
        models: {
          Manuscript: {
            Statuses: getManuscriptStatuses(),
            find: jest.fn(async () => manuscript),
            transaction: jest.fn(async trx => trx),
          },
          TeamMember: {
            Statuses: getTeamMemberStatuses(),
            findSubmittingAuthor: jest.fn(async () => ({ userId: 1 })),
            findOneByManuscriptAndRoleAndUser: jest.fn(async () => ae),
            findOneByManuscriptAndRoleAndStatus: jest.fn(async () => ({})),
            findAllByManuscriptAndRoleAndStatus: jest.fn(async () => []),
            findAllByManuscriptAndRole: jest.fn(async () => []),
            findTriageEditor: jest.fn(async () => te),
          },
          User: {
            find: jest.fn(async () => ({})),
          },
          Team: {
            Role: getTeamRoles(),
          },
          Job: {
            findAllByTeamMembers: jest.fn(async () => []),
          },
          Review: mockReviewModel,
          Comment: mockCommentModel,
        },
        useCases: mockedUseCases,
        logEvent,
        services: {
          jobsService,
          eventsService,
          notificationService,
        },
      })
      .execute({
        comment: chance.sentence(),
        manuscriptId: manuscript.id,
        userId: ae.userId,
        type: Review.Recommendations.major,
      })

    expect(manuscript.updateStatus).toHaveBeenCalledWith(
      Manuscript.Statuses.revisionRequested,
    )
    expect(mockedUseCases.createMajorVersion.initialize).toHaveBeenCalledTimes(
      1,
    )
  })
})
