const {
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
} = require('component-generators')

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findOneByRole: jest.fn(),
    findAllByManuscriptAndRole: jest.fn(),
  },
  Job: {
    findAllByTeamMembers: jest.fn(),
  },
}

const jobsService = {
  cancelStaffMemberJobs: jest.fn(),
  cancelJobs: jest.fn(),
}

const { TeamMember } = models
const { cancelAllJobsUseCase } = require('../../src/useCases/requestRevision')

describe('Cancel all jobs use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('Executes the main flow', async () => {
    const manuscript = generateManuscript()
    const academicEditor = generateTeamMember()
    const editorialAssistant = generateTeamMember()
    const reviewer = generateTeamMember()

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([reviewer])

    await cancelAllJobsUseCase.initialize({ models, jobsService }).execute({
      manuscript,
      academicEditor,
      editorialAssistant,
    })

    expect(jobsService.cancelStaffMemberJobs).toBeCalled()
    expect(jobsService.cancelJobs).toBeCalled()
  })
})
