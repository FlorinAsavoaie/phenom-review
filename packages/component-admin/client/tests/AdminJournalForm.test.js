import React from 'react'
import moment from 'moment'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent, waitForElement } from '@testing-library/react'
import Chance from 'chance'
import { render } from './testUtils'
import AdminJournalForm from '../components/AdminJournalForm'
import { articleTypes, users } from './mockData'

const chance = new Chance()

const journalInput = {
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: articleTypes[2],
  triageEditor: {
    id: users[1].id,
    name: `${users[1].identities[0].name.givenNames} ${users[1].identities[0].name.surname}`,
    email: users[1].identities[0].email,
  },
}
describe('Add journal', () => {
  let onConfirmMock
  let container
  beforeEach(() => {
    cleanup()
    onConfirmMock = jest.fn()
    container = render(<AdminJournalForm onConfirm={onConfirmMock} />)
  })

  it('Should throw error if "Journal Name" input is invalid', async done => {
    const { getByText, getByTestId, getAllByText } = container
    expect(getByText('Journal Name')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()
    fireEvent.click(getByTestId('modal-confirm'))
    setTimeout(() => {
      expect(getAllByText('Required')).toBeTruthy()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw error if "Journal Code" input is invalid', async done => {
    const { getByText, getByTestId, getAllByText } = container

    expect(getByText('Journal Code')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()
    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(getAllByText('Required')).toBeTruthy()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw error if "ISSN" input is invalid', async done => {
    const { getByText, getByTestId } = container

    expect(getByText('ISSN')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()
    fireEvent.change(getByTestId('issn-input'), {
      target: { value: chance.email() },
    })
    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(getByText('Invalid')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should click the active submission and see the current date', async done => {
    const { getByText, getByTestId, getByLabelText } = container
    const currentDate = moment().format('YYYY-MM-DD')

    expect(getByText('Active for submissions')).toBeInTheDocument()
    fireEvent.click(getByLabelText('Active for submissions'))

    setTimeout(() => {
      expect(getByTestId('calendar-button')).toBeInTheDocument()
      expect(getByTestId('date-field')).toBeInTheDocument()
      expect(getByText(currentDate)).toBeInTheDocument()
      done()
    })
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it.skip('Should click the calendar button and select a new date', async done => {
    const { getByText, getByTestId, container: ctr } = container
    const dueDay = moment()
      .add(1, 'day')
      .format('LL')
    const newDate = moment()
      .add(1, 'day')
      .format('YYYY-MM-DD')

    expect(getByText('Active for submissions')).toBeInTheDocument()
    const calendarCheckbox = getByText('Active for submissions')
    fireEvent.click(calendarCheckbox)

    const calendarButton = getByTestId('calendar-button')
    fireEvent.click(calendarButton)

    const dueDayLabelled = ctr.querySelector(`abbr[aria-label="${dueDay}"]`)
    fireEvent.click(dueDayLabelled)

    setTimeout(() => {
      expect(calendarButton).toBeInTheDocument()
      expect(getByTestId('date-field')).toBeInTheDocument()
      expect(getByText(newDate)).toBeInTheDocument()
      done()
    })
  })

  it('Should throw error if "Email" input is invalid', async done => {
    const { getAllByText, getByTestId } = container

    expect(getByTestId('email-input')).toBeInTheDocument()
    expect(getByTestId('modal-confirm')).toBeInTheDocument()
    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(getAllByText('Required')).toBeTruthy()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it.skip('Should call submit handler if form is valid ', async done => {
    const { getByTestId, findByLabelText } = container
    expect(getByTestId('modal-confirm')).toBeInTheDocument()

    fireEvent.change(getByTestId('journal-name-input'), {
      target: { value: journalInput.name },
    })
    fireEvent.change(getByTestId('code-input'), {
      target: { value: journalInput.code },
    })
    fireEvent.change(getByTestId('issn-input'), {
      target: { value: journalInput.issn },
    })
    fireEvent.change(getByTestId('apc-input'), {
      target: { value: journalInput.apc },
    })

    fireEvent.change(getByTestId('email-input'), {
      target: { value: journalInput.email },
    })
    const articleType = await waitForElement(() =>
      findByLabelText(journalInput.articleTypes.name),
    )
    fireEvent.click(articleType)

    fireEvent.click(getByTestId('modal-confirm'))

    setTimeout(() => {
      expect(onConfirmMock).toHaveBeenCalledWith(
        {
          name: journalInput.name,
          code: journalInput.code,
          issn: journalInput.issn,
          apc: journalInput.apc,
          email: journalInput.email,
          articleTypes: [journalInput.articleTypes.id],
          triageEditorId: journalInput.triageEditor.id,
        },
        expect.anything(),
      )
      done()
    })
  })
})
