import React, { Fragment } from 'react'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Spinner } from '@pubsweet/ui'
import { get, isEmpty } from 'lodash'
import { ScrollContainer, Icon, Text } from '@hindawi/ui'
import { Modal } from 'component-modal'
import AdminJournalSectionForm from './AdminJournalSectionForm'
import useEditSection from './useEditSection'

const SectionsTable = ({ sections, loading }) => {
  const { handleEditSection } = useEditSection()
  if (loading) return <Spinner />
  return (
    <Fragment>
      {sections.length !== 0 && (
        <TableHead>
          <thead>
            <Tr>
              <Th>Section Name</Th>
              <Th>Section Editor</Th>
            </Tr>
          </thead>
        </TableHead>
      )}
      {sections.length !== 0 && (
        <ScrollContainer>
          <Table>
            <tbody>
              {sections.map(section => (
                <SectionRow key={section.id}>
                  <Td>{section.name}</Td>
                  <EditorName>
                    {!isEmpty(section.sectionEditors) ? (
                      section.sectionEditors.map(sectionEditor => (
                        <Text key={sectionEditor.id} mb={3} mt={3} pl={3}>
                          {`${get(sectionEditor, 'alias.name.givenNames', '')}
                      ${get(sectionEditor, 'alias.name.surname', '')}${
                            section.sectionEditors.length > 1 ? ',' : ''
                          }`}
                        </Text>
                      ))
                    ) : (
                      <Text error mb={3} mt={3} pl={3}>
                        UNASSIGNED
                      </Text>
                    )}
                  </EditorName>

                  <HiddenCell>
                    <Modal
                      component={AdminJournalSectionForm}
                      confirmText="SAVE"
                      editMode
                      handleEditSection={handleEditSection}
                      modalKey={section.id}
                      section={section}
                      title="Edit Section"
                    >
                      {showModal => (
                        <Icon fontSize="16px" icon="edit" onClick={showModal} />
                      )}
                    </Modal>
                  </HiddenCell>
                </SectionRow>
              ))}
            </tbody>
          </Table>
        </ScrollContainer>
      )}
      {sections.length === 0 && (
        <Table>
          <tbody>
            <tr>
              <NoDataTd colSpan={3}>No Sections added yet</NoDataTd>
            </tr>
          </tbody>
        </Table>
      )}
    </Fragment>
  )
}

export default SectionsTable

const noWrap = props => {
  if (props.noWrap) {
    return css`
      white-space: nowrap;
    `
  }
}

const Table = styled.table`
  font-family: 'Nunito';
  border-collapse: collapse;
  min-width: 100%;
`

const TableHead = styled.table`
  font-family: 'Nunito';
  border-collapse: collapse;
  min-width: 100%;
  margin-top: calc(${th('gridUnit')} * 4);
`

const Th = styled.th`
  text-align: start;
`

const Tr = styled.tr`
  display: inline-grid;
  grid-template-columns: 45% 55%;
  border-bottom: 1px solid ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 10);
  width: 100%;
  padding-left: calc(${th('gridUnit')} * 4);
`

const Td = styled.td`
  border: none;
  margin-top: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 2);
  padding-left: calc(${th('gridUnit')} * 4);
  text-align: start;
  vertical-align: middle;

  ${noWrap}
`

const NoDataTd = styled.td`
  height: calc(${th('gridUnit')} * 24);
  background-color: ${th('white')};
  text-align: center;
  vertical-align: middle;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`
const HiddenCell = styled(Td)`
  display: flex;
  flex-direction: row;
  margin-right: calc(${th('gridUnit')} * 4);
  align-items: center;
  display: flex;
  justify-content: flex-end;
  opacity: 0;
`
const SectionRow = styled.tr`
  display: grid;
  grid-template-columns: 45% 50% 5%;
  border-bottom: 1px solid ${th('colorBorder')};
  font-family: 'Nunito';
  &:hover {
    background-color: ${th('labelColor')};
    ${HiddenCell} {
      opacity: 1;
    }
  }
`
const EditorName = styled.td`
  display: flex;
  flex-direction: row;
  vertical-aign: middle;
`
