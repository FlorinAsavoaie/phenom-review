import React, { Fragment, useMemo } from 'react'
import { useQuery } from 'react-apollo'
import { Modal } from 'component-modal'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { isEmpty, flatMap, chain } from 'lodash'
import { Spinner, Button, TextField } from '@pubsweet/ui'
import {
  Row,
  Text,
  Icon,
  Item,
  Label,
  Pagination,
  MultiAction,
  TextTooltip,
  useRoles,
  usePaginatedItems,
} from '@hindawi/ui'

import { queries } from '../graphql'
import useRemoveEditorialMember from './useRemoveEditorialMember'
import useAssignLeadEditorialAssistant from './useAssignLeadEditorialAssistant'

const getFullName = ({ givenNames = '', surname = '' }) =>
  `${givenNames} ${surname}`

const getRoleLabel = (role, roleList, specialIssueName) => {
  if (role === 'triageEditor' && specialIssueName) {
    return roleList.find(el => el.value === 'triageEditorSI').label
  }
  if (role === 'academicEditor' && specialIssueName) {
    return roleList.find(el => el.value === 'academicEditorSI').label
  }
  return roleList.find(el => el.value === role).label
}

const filterEditors = searchQuery => editor => {
  const whereToSearch = `${editor.fullName.surname} ${editor.fullName.givenNames} ${editor.email} ${editor.specialIssueName}`.toLowerCase()
  return whereToSearch.includes(searchQuery.trim().toLowerCase())
}

const EditorialBoardTable = ({
  sections,
  journalId,
  peerReviewModel,
  journalSpecialIssues,
}) => {
  const { roles } = useRoles(peerReviewModel)
  const { handleRemoveEditorialMember } = useRemoveEditorialMember(journalId)
  const {
    handleAssignLeadEditorialAssistant,
  } = useAssignLeadEditorialAssistant(journalId)

  const { data, loading } = useQuery(queries.getEditorialBoard, {
    variables: { journalId },
    fetchPolicy: 'network-only',
  })

  const editors = useMemo(
    () =>
      chain(data)
        .get('getEditorialBoard', [])
        .sortBy(i => getFullName(i.fullName))
        .value(),
    [data],
  )

  const {
    totalCount,
    searchQuery,
    clearSearch,
    handleSearch,
    paginatedItems,
    ...pagination
  } = usePaginatedItems({
    items: editors,
    filterFn: filterEditors,
  })

  const specialIssues = useMemo(
    () => [
      ...flatMap(sections, section => section.specialIssues),
      ...journalSpecialIssues,
    ],
    [journalSpecialIssues, sections],
  )

  if (loading) return <Spinner />

  return (
    <Fragment>
      <Label mb={2} mt={3}>
        Editorial Board
      </Label>
      <Row alignItems="start" justify="start" mt={1}>
        <Item maxWidth={90} mr={4}>
          <TextField
            onChange={handleSearch}
            placeholder="Type and search by Name, Email or Special Issue"
            value={searchQuery}
          />
        </Item>
        <Button invert onClick={clearSearch} secondary small>
          Clear
        </Button>
      </Row>
      <Table>
        <thead>
          <Tr>
            <Th />
            <Th>Role</Th>
            <Th>Full Name</Th>
            {!isEmpty(sections) && <Th>Section</Th>}
            {!isEmpty(specialIssues) && <Th>Special Issue</Th>}
            <Th>Email</Th>
            <Th />
          </Tr>
        </thead>
        <tbody>
          {editors.length ? (
            paginatedItems.map(
              ({
                id,
                role,
                email,
                fullName,
                sectionName,
                isCorresponding,
                specialIssueName,
              }) => (
                <Tr key={`board-table-${id}`}>
                  <Td>
                    {role === 'editorialAssistant' && (
                      <Modal
                        cancelText="CANCEL"
                        component={MultiAction}
                        confirmText="ASSIGN"
                        modalKey={`assign-lead-editorial-assistant-${id}`}
                        onConfirm={handleAssignLeadEditorialAssistant(id)}
                        subtitle={`${getFullName(fullName)} (${email})`}
                        title="Assign Lead Editorial Assistant?"
                      >
                        {showModal => (
                          <TextTooltip title="Assign Lead">
                            <Icon
                              data-test-id="assign-lead-editorial-assistant"
                              fontSize="16px"
                              icon={isCorresponding ? 'solidLead' : 'lead'}
                              onClick={showModal}
                            />
                          </TextTooltip>
                        )}
                      </Modal>
                    )}
                  </Td>
                  <Td>{getRoleLabel(role, roles, specialIssueName)}</Td>
                  <Td>{getFullName(fullName)}</Td>
                  {!isEmpty(sections) && (
                    <Td>
                      <WithEllipsis>{sectionName} </WithEllipsis>
                    </Td>
                  )}
                  {!isEmpty(specialIssues) && (
                    <Td>
                      <WithEllipsis>{specialIssueName} </WithEllipsis>
                    </Td>
                  )}
                  <Td>{email}</Td>
                  <HiddenCell>
                    <Modal
                      cancelText="CANCEL"
                      component={MultiAction}
                      confirmText="REMOVE"
                      modalKey={`remove-editor-${id}`}
                      onConfirm={handleRemoveEditorialMember(id)}
                      subtitle={`${getFullName(fullName)} (${email})`}
                      title="Are you sure you want to remove editor?"
                    >
                      {showModal => (
                        <Button invert onClick={showModal} secondary xs>
                          REMOVE
                        </Button>
                      )}
                    </Modal>
                  </HiddenCell>
                </Tr>
              ),
            )
          ) : (
            <Tr>
              <NoDataTd colSpan={4}>No User added yet</NoDataTd>
            </Tr>
          )}
        </tbody>
      </Table>

      <Row justify="flex-end" mt={4}>
        <Pagination totalCount={totalCount} {...pagination} />
      </Row>
    </Fragment>
  )
}

const noWrap = props => {
  if (props.noWrap) {
    return css`
      white-space: nowrap;
    `
  }
}

const Table = styled.table`
  font-family: 'Nunito';
  border-collapse: collapse;
  min-width: 100%;
`

const WithEllipsis = styled(Text)`
  max-width: calc(${th('gridUnit')} * 50);
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`

const Th = styled.th`
  border: none;
  height: calc(${th('gridUnit')} * 10);
  text-align: start;
  vertical-align: middle;
`
const Td = styled.td`
  border: none;
  height: calc(${th('gridUnit')} * 9);
  text-align: start;
  vertical-align: middle;

  ${noWrap}
`

const HiddenCell = styled(Td)`
  display: flex;
  flex-direction: row;
  margin-right: calc(${th('gridUnit')} * 4);
  align-items: center;
  display: flex;
  justify-content: flex-end;
  opacity: 0;
`
const Tr = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
  &:hover {
    background-color: ${th('labelColor')};
    ${HiddenCell} {
      opacity: 1;
    }
`

const NoDataTd = styled.td`
  height: calc(${th('gridUnit')} * 22);
  background-color: ${th('backgroundColor')};
  text-align: center;
  vertical-align: middle;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`

export default EditorialBoardTable
