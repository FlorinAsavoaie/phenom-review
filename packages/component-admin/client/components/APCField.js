import React from 'react'
import styled from 'styled-components'
import { th, override, validationColor } from '@pubsweet/ui-toolkit'
import { formatAPC } from './utils'

const APCField = ({ type = 'text', value = '', label, ...props }) => (
  <Root>
    {label && <Label htmlFor="apc-input">{label}</Label>}
    <InputWrapper>
      <Input id="apc-input" type={type} value={formatAPC(value)} {...props} />
    </InputWrapper>
  </Root>
)

export default APCField

const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
  ${override('ui.TextField')};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  display: block;
  ${override('ui.Label')};
  ${override('ui.TextField.Label')};
`
const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const Input = styled.input`
  position: relative;
  font-family: inherit;
  font-size: inherit;
  width: fill-available;
  height: calc(${th('gridUnit')} * 12);

  border: ${th('borderWidth')} ${th('borderStyle')} ${validationColor};
  border-radius: ${th('borderRadius')};
  padding-right: calc(${th('gridUnit')} * 2);
  padding-left: calc(${th('gridUnit')});

  &::placeholder {
    color: ${th('colorTextPlaceholder')};
  }
  ${override('ui.TextField.Input')};
`
