import moment from 'moment'

export const setInitialValues = (values = {}) => ({
  id: values.id || '',
  apc: values.apc,
  name: values.name || '',
  issn: values.issn || '',
  articleTypes: (values.articleTypes || []).map(at => at.id),
  email: values.email || '',
  code: values.code || '',
  activationDate: values.activationDate,
  isActive: values.isActive || false,
  peerReviewModelId: values.peerReviewModel.id || '',
})
export const setSectionInitialValues = (values = {}) => ({
  id: values.id || '',
  name: values.name || '',
})
export const setSpecialIssuesInitialValues = (values = {}) => ({
  ...values,
  id: values.id || '',
  name: values.name || '',
  sectionId: values.section ? values.section.id : null,
  callForPapers: values.callForPapers || '',
  startDate:
    values.startDate || new Date(moment().format('YYYY-MM-DD')).toISOString(),
  endDate: values.endDate || null,
  publicationDate: values.publicationDate || null,
  description: values.description || [],
  topics: values.topics || [],
  isAnnual: values.isAnnual || false,
  conflictOfInterest: values.conflictOfInterest || '',
  acronym: values.acronym || '',
})

export const parseError = e => e.message.replace('GraphQL error: ', '')

export const formatAPC = value => {
  if (typeof value !== 'number') return 'Not set'

  return '$'.concat(
    value
      .toString()
      .replace(/\s/g, '')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 '),
  )
}
