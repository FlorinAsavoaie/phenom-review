import React from 'react'
import styled from 'styled-components'
import { flatMap } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { ActionLink, Icon } from '@hindawi/ui'
import SpecialIssuesTable from './SpecialIssuesTable'
import SpecialIssueDrawer from './SpecialIssueDrawer'
import SpecialIssueContext from './SpecialIssueContext'
import useSpecialIssueDrawer from './useSpecialIssueDrawer'

const SpecialIssues = ({
  journalId,
  sections,
  loading,
  journalSpecialIssues,
}) => {
  const {
    isSpecialIssueDrawerVisible,
    specialIssueId: editedSpecialIssueId,
    journalId: editedJournalId,
    showDrawer,
    closeDrawer,
  } = useSpecialIssueDrawer()

  const showCreateSpecialIssueDrawer = () => showDrawer({ journalId })
  const sectionSpecialIssues = flatMap(
    sections,
    section => section.specialIssues,
  )
  const specialIssues = [...journalSpecialIssues, ...sectionSpecialIssues]

  return (
    <Root>
      <ActionLink
        fontWeight="bold"
        mb={2}
        mt={2}
        onClick={showCreateSpecialIssueDrawer}
      >
        <Icon fontSize="11px" icon="expand" mr={1} />
        Add special issue
      </ActionLink>
      <SpecialIssueContext.Provider value={showDrawer}>
        <SpecialIssuesTable
          journalId={journalId}
          loading={loading}
          sections={sections}
          specialIssues={specialIssues}
        />
      </SpecialIssueContext.Provider>
      <SpecialIssueDrawer
        isVisible={isSpecialIssueDrawerVisible}
        journalId={editedJournalId}
        onClose={closeDrawer}
        specialIssueId={editedSpecialIssueId}
      />
    </Root>
  )
}

const Root = styled.div`
  min-height: calc(${th('gridUnit')} * 35);
  padding: calc(${th('gridUnit')} * 3);
`

export default SpecialIssues
