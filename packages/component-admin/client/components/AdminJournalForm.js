import React, { Fragment } from 'react'
import { get } from 'lodash'
import {
  Row,
  Text,
  Item,
  Label,
  FormModal,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'
import { TextField } from '@pubsweet/ui'
import { setInitialValues } from './utils'
import APCField from './APCField'
import ArticleTypes from './ArticleTypes'
import JournalActivation from './JournalActivation'
import PeerReviewModel from './PeerReviewModel'

const validate = values => {
  const errors = {}

  if (get(values, 'articleTypes', []).length === 0) {
    errors.articleTypes = 'Required'
  }
  return errors
}

const AdminJournalForm = ({
  hideModal,
  isFetching,
  addJournal,
  editJournal,
  editMode,
  confirmText,
  journal,
  title,
}) => {
  const onSubmit = (values, props) => {
    editMode
      ? editJournal({ ...values }, props)
      : addJournal({ ...values }, props)
  }

  return (
    <FormModal
      cancelText="CANCEL"
      confirmText={confirmText}
      content={FormFields}
      hideModal={hideModal}
      initialValues={!!journal && setInitialValues(journal)}
      isFetching={isFetching}
      onSubmit={onSubmit}
      title={title}
      validate={validate}
    />
  )
}

const FormFields = ({ formProps }) => (
  <Fragment>
    <Row mt={6}>
      <Item justify="flex-start" mr={4} vertical>
        <Label pb={2} required>
          Journal Name
        </Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="journal-name-input"
          inline
          name="name"
          validate={[validators.required]}
        />
      </Item>
      <Item maxWidth={30} vertical>
        <Label pb={2} required>
          Journal Code
        </Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="code-input"
          inline
          name="code"
          validate={[validators.required, validators.alphaNumericValidator]}
        />
      </Item>
    </Row>
    <Row>
      <Item mr={8} vertical width={32}>
        <Label required>Email</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="email-input"
          inline
          name="email"
          validate={[validators.required, validators.emailValidator]}
        />
      </Item>

      <Item maxWidth={30} mr={4} vertical>
        <Label pb={2}>ISSN</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="issn-input"
          inline
          name="issn"
          validate={[validators.issnValidator]}
        />
      </Item>
      <Item maxWidth={30} vertical>
        <Label disabled pb={2}>
          APC
        </Label>
        <APCField disabled value={get(formProps, 'initialValues.apc')} />
      </Item>
    </Row>
    <Row>
      <Item mr={8} vertical>
        <Label disabled={formProps.values.isActive} pb={2} required>
          Peer Review Model
        </Label>
        <PeerReviewModel journalStatus={formProps.values.isActive} />
      </Item>
      <Item flex={1} vertical>
        <ValidatedFormField
          component={JournalActivation}
          data-test-id="activation-date-calendar"
          journalDate={formProps.values.activationDate}
          journalStatus={formProps.values.isActive}
          name="activationDate"
          setFieldValue={formProps.setFieldValue}
        />
      </Item>
    </Row>
    {formProps.values.activationDate &&
      !Number.isInteger(formProps.values.apc) && (
        <Item justify="flex-end" mt={-4}>
          <Text alert small>
            This journal can become active only once the price is set!
          </Text>
        </Item>
      )}
    <Row>
      <Item flex="auto" vertical>
        <Label required>Article Types</Label>
        <ArticleTypes {...formProps} />
      </Item>
      <Item />
    </Row>
  </Fragment>
)

export default AdminJournalForm
