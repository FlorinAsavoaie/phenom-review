const getEmailCopy = ({ emailType, specialIssueName }) => {
  let paragraph
  const hasIntro = true
  const hasLink = false
  const hasSignature = true

  if (!emailType) {
    throw new Error(`The ${emailType} email type is not defined.`)
  }

  if (emailType === 'special-issue-deactivated') {
    paragraph = `Special Issue ${specialIssueName} has been deactivated.<br/><br/>
          If you have any questions regarding this action, please let us know.<br/><br/>`
  }

  return { hasSignature, paragraph, hasIntro, hasLink }
}

module.exports = {
  getEmailCopy,
}
