const moment = require('moment')
const queueNames = require('./queueNames')

module.exports.initialize = ({ Job }) => ({
  async scheduleJournalActivation({ journal, teamMemberId }) {
    const params = {
      journalId: journal.id,
    }

    const executionDate = moment(journal.activationDate)
      .add(3, 'hours')
      .toISOString()

    return Job.schedule({
      params,
      teamMemberId,
      executionDate,
      journalId: journal.id,
      queueName: `${queueNames.JOURNAL_ACTIVATION_QUEUE}`,
    })
  },
})
