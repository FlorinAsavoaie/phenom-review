const getEmailCopyForUserAddedByAdmin = ({ isAdmin, isRIPE }) => {
  let paragraph
  let hasIntro = true
  const hasLink = true
  const hasSignature = true
  if (isAdmin) {
    hasIntro = false
    paragraph = `You have been invited to join Hindawi as an Administrator.
    Please confirm your account and set your account details by clicking on the link below.`
  } else if (isRIPE) {
    paragraph = `You have been invited to join Hindawi as a Research Integrity Publishing Editor.
    Please confirm your account and set your account details by clicking on the link below.`
  } else {
    paragraph = `You have been invited to join Hindawi as a User.
    Please confirm your account and set your account details by clicking on the link below.`
  }
  return { paragraph, hasIntro, hasLink, hasSignature }
}

module.exports = {
  getEmailCopyForUserAddedByAdmin,
}
