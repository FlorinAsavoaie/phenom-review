const { get } = require('lodash')
const config = require('config')
const { services } = require('helper-service')

const Email = require('component-sendgrid')

const { getEmailCopyForUserAddedByAdmin } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const unsubscribeSlug = config.get('unsubscribe.url')
const accountsEmail = config.get('accountsEmail')
const footerText = config.get('emailFooterText.registeredUsers')

module.exports = {
  async notifyUserAddedByAdmin({ user, identity, isAdmin, isRIPE }) {
    const resetPath = config.get('invite-reset-password.url')
    const baseUrl = config.get('pubsweet-client.baseUrl')
    const { paragraph, ...bodyProps } = getEmailCopyForUserAddedByAdmin({
      isAdmin,
      isRIPE,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `Hindawi Support <${accountsEmail}>`,
      toUser: {
        email: identity.email,
        name: identity.surname ? `${identity.surname}` : '',
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          givenNames: get(identity, 'givenNames', ''),
          surname: get(identity, 'surname', ''),
          title: get(identity, 'title', ''),
          country: get(identity, 'country', ''),
          aff: get(identity, 'aff', ''),
          email: get(identity, 'email', ''),
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: 'Hindawi Review System',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: identity.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
}
