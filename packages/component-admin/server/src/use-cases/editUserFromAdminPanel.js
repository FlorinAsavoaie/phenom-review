const { rorService } = require('component-ror')

const initialize = ({
  eventsService,
  models: { User, Team, TeamMember },
  updateUserRoleInSSO,
  removeUserRoleInSSO,
  logger,
}) => ({
  execute: async ({ input, id, userId }) => {
    const { isAdmin, isRIPE } = input
    if (isAdmin && isRIPE) {
      throw new ConflictError(
        'A user cannot be both admin and research integrity publishing editor.',
      )
    }

    const adminRole = Team.Role.admin
    const updatedUser = await User.find(id, 'identities')

    if (isAdmin === false) {
      if (id === userId)
        throw new ConflictError('Cannot remove own admin role.')

      await TeamMember.deleteStaffMemberByUserIdAndRole({
        role: adminRole,
        userId: id,
      })

      try {
        await removeUserRoleInSSO(updatedUser.defaultIdentity.email, adminRole)
      } catch (err) {
        logger.error('ERROR: There was a problem with SSO role removal:', err)
      }
    } else {
      const adminTeam = await Team.findOneBy({
        queryObject: { role: adminRole },
      })
      const adminMember = adminTeam.addMember({
        user: updatedUser,
      })
      await adminMember.save()

      try {
        await updateUserRoleInSSO(updatedUser.defaultIdentity.email, adminRole)
      } catch (err) {
        logger.error('ERROR: There was a problem with SSO role update:', err)
      }
    }

    const ripeRole = Team.Role.researchIntegrityPublishingEditor
    if (isRIPE === false) {
      await TeamMember.deleteStaffMemberByUserIdAndRole({
        role: ripeRole,
        userId: id,
      })
    } else {
      const ripeTeam = await Team.findOrCreate({
        queryObject: {
          role: ripeRole,
        },
        options: {
          role: ripeRole,
        },
      })

      const ripeMember = ripeTeam.addMember({
        user: updatedUser,
      })
      await ripeMember.save()
    }

    delete input.isAdmin
    delete input.isRIPE

    const localIdentity = updatedUser.defaultIdentity
    localIdentity.updateProperties(input)

    await appendRORInfoIfMissing(localIdentity)
    await localIdentity.save()

    eventsService.publishUserEvent({
      userId: updatedUser.id,
      eventName: 'UserUpdated',
    })

    return User.findOneWithDefaultIdentity(updatedUser.id)
  },
})

const appendRORInfoIfMissing = async identity => {
  if (identity.aff && !identity.affRorId) {
    const bestROR = await rorService.getBestROR(identity.aff, identity.country)
    if (bestROR) {
      identity.affRorId = bestROR.organization.id
    }
  }
}

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
