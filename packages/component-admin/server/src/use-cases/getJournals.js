const initialize = ({ Journal }) => ({
  execute: async () =>
    Journal.findAll({
      orderByField: 'created',
      order: 'desc',
      eagerLoadRelations: '[peerReviewModel]',
    }),
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
