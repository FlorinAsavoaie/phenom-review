const initialize = ({ models: { Section }, eventsService }) => ({
  execute: async ({ name, journalId }) => {
    let section

    section = await Section.findUniqueSection({ name })
    if (section) throw new ConflictError(`Section already exists.`)

    section = new Section({ name, journalId })
    await section.save()

    eventsService.publishJournalEvent({
      journalId: section.journalId,
      eventName: 'JournalSectionAdded',
    })

    return section.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
