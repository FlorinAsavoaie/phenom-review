const initialize = ({
  models: { Team, TeamMember },
  eventsService,
  logger,
  updateUserRoleInSSO,
}) => ({
  execute: async ({ user, journalId, role }) => {
    let team = await Team.findOneByJournalAndRole({ journalId, role })
    if (!team) team = new Team({ journalId, role })

    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    const userEmail = localIdentity.email

    try {
      await updateUserRoleInSSO(userEmail, role)
    } catch (err) {
      logger.error('ERROR: There was a problem with SSO role update:', err)
    }

    team.addMember({
      user,
      options: { status: TeamMember.Statuses.pending },
    })

    await team.saveGraph({
      noDelete: true,
      noUpdate: true,
      relate: true,
    })

    await eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorAssigned',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
