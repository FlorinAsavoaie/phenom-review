const initialize = ({
  logger,
  models,
  useCases,
  eventsService,
  updateUserRoleInSSO,
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ user, journalId, role }) => {
    const { Team, TeamMember } = models
    const existingUserJournalMember = await TeamMember.findOneByJournalAndUser({
      userId: user.id,
      journalId,
    })
    if (existingUserJournalMember)
      throw new Error(`User already has an editorial role on this journal.`)

    let useCase = 'assignEditorOnJournalUseCase'

    if (role === Team.Role.editorialAssistant) {
      useCase = 'assignEditorialAssistantOnJournalUseCase'
    }

    await useCases[useCase]
      .initialize({
        logger,
        models,
        eventsService,
        updateUserRoleInSSO,
        redistributeEditorialAssistants,
      })
      .execute({ user, journalId, role })
  },
})

module.exports = {
  initialize,
}
