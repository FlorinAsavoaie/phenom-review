const { logger } = require('component-logger')

const initialize = ({
  models: { Team, TeamMember },
  models,
  useCases,
  eventsService,
  removeUserRoleInSSO,
  getUpdatedEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
}) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    logger.info(
      `Removing Editorial Member: ${teamMemberId} with role: ${teamMember.team.role}`,
    )

    let useCase = 'removeOtherMemberUseCase'

    if (teamMember.team.role === Team.Role.editorialAssistant) {
      useCase = 'removeEditorialAssistantUseCase'
    }

    await useCases[useCase]
      .initialize({
        models,
        eventsService,
        removeUserRoleInSSO,
        getUpdatedEditorialAssistantsUseCase,
        getSubmissionsWithUpdatedEditorialAssistantsUseCase,
      })
      .execute(teamMemberId)

    logger.info(
      `Removed Editorial Member: ${teamMemberId} with role: ${teamMember.team.role}`,
    )
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
