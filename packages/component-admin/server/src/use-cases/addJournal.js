const initialize = ({
  models: { Journal, User, Team, TeamMember },
  isDateToday,
  jobsService,
  eventsService,
}) => ({
  execute: async ({ input, userId }) => {
    let journal = {}
    const {
      name,
      code,
      issn,
      email,
      articleTypes,
      activationDate,
      peerReviewModelId,
    } = input

    journal = new Journal({
      name,
      code,
      issn,
      email,
      activationDate,
      peerReviewModelId,
      isActive: false,
    })

    await journal.saveJournalAndJournalArticleTypes(articleTypes)

    // the journal will become active when the APC is set in Invoicing
    // that's why is redundant to schedule a job is the activation date is today
    if (activationDate && !isDateToday(activationDate)) {
      const admin = await TeamMember.findOneByUserAndRole({
        userId,
        role: Team.Role.admin,
      })
      jobsService.scheduleJournalActivation({ journal, teamMemberId: admin.id })
    }

    eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalAdded',
    })

    return journal
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
