const initialize = ({
  dateService,
  jobsService,
  eventsService,
  models: { SpecialIssue, Job, TeamMember, Section, Team },
}) => ({
  execute: async ({ input, id }) => {
    const {
      startDate,
      endDate,
      publicationDate,
      name,
      sectionId,
      callForPapers,
      isAnnual,
      acronym,
      conflictOfInterest,
      description,
      topics,
    } = input

    if (endDate <= startDate) {
      throw new Error(`End date must be after start date.`)
    }
    if (publicationDate && publicationDate <= startDate) {
      throw new Error(`Publication date must be after start date.`)
    }
    const specialIssue = await SpecialIssue.find(id)
    const oldStartDate = specialIssue.startDate
    const oldEndDate = specialIssue.endDate
    const wasInactive = !specialIssue.isActive || specialIssue.isCancelled

    if (specialIssue.name !== name) {
      const existingSpecialIssue = await SpecialIssue.findUnique(name)
      if (existingSpecialIssue) throw new Error(`Special issue already exists.`)
    }

    specialIssue.updateProperties({
      name,
      endDate,
      startDate,
      publicationDate,
      callForPapers,
      isAnnual,
      acronym,
      conflictOfInterest,
      description,
      topics,
      ...(sectionId ? { sectionId, journalId: null } : {}),
    })

    const admin = await TeamMember.findOneByRole({ role: Team.Role.admin })
    if (!specialIssue.isActive) {
      await specialIssue.handleActivation({
        oldEndDate,
        jobsService,
        dateService,
        oldStartDate,
        models: { Job },
        adminId: admin.id,
      })
    }
    await specialIssue.save()

    if (!dateService.areDatesEqual(oldEndDate, endDate)) {
      await specialIssue.cancelJobByType({
        Job,
        jobType: Job.Type.deactivation,
      })

      jobsService.scheduleSpecialIssueDeactivation({
        specialIssue,
        teamMemberId: admin.id,
      })
    }

    // events
    const eventType =
      specialIssue.isActive && wasInactive && endDate !== oldEndDate
        ? 'Extended'
        : 'Updated'
    let parentType = 'Journal'
    let { journalId } = specialIssue

    if (sectionId) {
      const section = await Section.find(sectionId)
      ;({ journalId } = section)
      parentType = 'JournalSection'
    }

    eventsService.publishJournalEvent({
      journalId,
      eventName: `${parentType}SpecialIssue${eventType}`,
    })
    eventsService.publishSpecialIssueEvent({
      specialIssueId: id,
      eventName: `SpecialIssueUpdated`,
    })

    return specialIssue.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
