const {
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
} = require('component-generators')

const { removeEditorialAssistantUseCase } = require('../../../src/use-cases')

const {
  getUpdatedEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
} = require('../../../../../component-model/server/dist/useCases/manuscript')

jest.mock('@pubsweet/logger', () => ({
  info: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
}))

const models = {
  Manuscript: {
    InProgressStatuses: getManuscriptInProgressStatuses(),
    Statuses: getManuscriptStatuses(),
    findAllByJournalAndUserAndRoleAndTeamMemberStatusesAndManuscriptStatuses: jest.fn(),
    countSubmissionsByJournalAndStatuses: jest.fn(),
    findAll: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: jest.fn(),
}
Object.assign(models.TeamMember, {
  Statuses: getTeamMemberStatuses(),
  find: jest.fn(),
  findAllByJournalWithWorkload: jest.fn(),
  findOneByUserAndRoleAndStatusOnManuscript: jest.fn(),
  knex: jest.fn(),
  query: jest.fn(() => ({
    upsertGraph: jest.fn(),
  })),
})
const { Team, TeamMember, Manuscript } = models

const eventsService = {
  publishJournalEvent: jest.fn(),
  publishSubmissionEvent: jest.fn(),
}

const MS_STATUSES_FOR_EA_DISTRIBUTION = [
  ...Manuscript.InProgressStatuses,
  Manuscript.Statuses.inQA,
  Manuscript.Statuses.qualityChecksRequested,
  Manuscript.Statuses.qualityChecksSubmitted,
  Manuscript.Statuses.technicalChecks,
  Manuscript.Statuses.draft,
]

describe('Remove Editorial Assistant use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should remove the EA from journal and add another one on manuscript', async () => {
    const manuscriptEditorialAssistant = generateTeamMember({
      id: 'some-manuscript-team-member-id',
      userId: 'some-user-id',
      teamId: 'some-manuscript-team-id',
      team: {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const journalEditorialAssistant = generateTeamMember({
      id: 'some-team-member-id',
      userId: 'some-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const otherJournalEditorialAssistant = generateTeamMember({
      id: 'some-other-team-member-id',
      userId: 'some-other-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    const lastManuscript = generateManuscript({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
      status: Manuscript.Statuses.technicalChecks,
    })

    jest
      .spyOn(TeamMember, 'find')
      .mockResolvedValueOnce(journalEditorialAssistant)

    jest
      .spyOn(TeamMember, 'findAllByJournalWithWorkload')
      .mockResolvedValueOnce([
        {
          userId: journalEditorialAssistant.userId,
          alias: journalEditorialAssistant.alias,
          workload: 2,
        },
        {
          userId: otherJournalEditorialAssistant.userId,
          workload: 1,
          alias: otherJournalEditorialAssistant.alias,
        },
      ])

    jest
      .spyOn(
        Manuscript,
        'findAllByJournalAndUserAndRoleAndTeamMemberStatusesAndManuscriptStatuses',
      )
      .mockResolvedValueOnce([lastManuscript])

    jest.spyOn(Manuscript, 'findAll').mockResolvedValueOnce([lastManuscript])

    jest
      .spyOn(Manuscript, 'countSubmissionsByJournalAndStatuses')
      .mockResolvedValueOnce({ count: 3 })

    jest
      .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
      .mockResolvedValueOnce(manuscriptEditorialAssistant)

    await removeEditorialAssistantUseCase
      .initialize({
        models,
        eventsService,
        getUpdatedEditorialAssistantsUseCase,
        getSubmissionsWithUpdatedEditorialAssistantsUseCase,
      })
      .execute('some-team-member-id')

    expect(TeamMember.find).toHaveBeenCalledTimes(1)
    expect(TeamMember.find).toHaveBeenCalledWith('some-team-member-id', 'team')

    expect(TeamMember.findAllByJournalWithWorkload).toHaveBeenCalledWith({
      role: Team.Role.editorialAssistant,
      journalId: 'some-journal-id',
      teamMemberStatuses: [TeamMember.Statuses.active],
      manuscriptStatuses: MS_STATUSES_FOR_EA_DISTRIBUTION,
    })

    expect(
      Manuscript.findAllByJournalAndUserAndRoleAndTeamMemberStatusesAndManuscriptStatuses,
    ).toHaveBeenCalledTimes(1)
  })

  it('should throw an error if trying to remove the only EA on the journal', async () => {
    const journalEditorialAssistant = generateTeamMember({
      id: 'some-team-member-id',
      userId: 'some-user-id',
      team: {
        journalId: 'some-journal-id',
        role: Team.Role.editorialAssistant,
      },
    })
    jest
      .spyOn(TeamMember, 'find')
      .mockResolvedValueOnce(journalEditorialAssistant)
    jest
      .spyOn(TeamMember, 'findAllByJournalWithWorkload')
      .mockResolvedValueOnce([journalEditorialAssistant])

    const res = removeEditorialAssistantUseCase
      .initialize({
        models,
        eventsService,
        getUpdatedEditorialAssistantsUseCase,
        getSubmissionsWithUpdatedEditorialAssistantsUseCase,
      })
      .execute('some-team-member-id')

    return expect(res).rejects.toThrowError(
      "You can't remove the only Editorial Assistant on the journal.",
    )
  })
})
