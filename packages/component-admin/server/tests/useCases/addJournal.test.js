const {
  generatePeerReviewModel,
  generateTeamMember,
  getTeamRoles,
} = require('component-generators')

let models = {}
let Team = {}
let TeamMember = {}

const Chance = require('chance')

const { addJournalUseCase } = require('../../src/use-cases')

const chance = new Chance()

const jobsService = {
  scheduleJournalActivation: jest.fn(),
}
const eventsService = {
  publishJournalEvent: jest.fn(),
}
const isDateToday = jest.fn()

const generateInput = () => ({
  name: chance.company(),
  code: chance.word({ length: 7 }).toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
  isActive: false,
})

describe('Add journal use case', () => {
  let input

  beforeEach(async () => {
    input = generateInput()
    jest.clearAllMocks()

    models = {
      Team: {
        Role: getTeamRoles(),
      },
      User: {
        find: jest.fn(),
      },
      TeamMember: { findOneByUserAndRole: jest.fn() },
      Journal: jest.fn(() => ({
        saveJournalAndJournalArticleTypes: jest.fn(),
        ...input,
      })),
    }
    ;({ Team, TeamMember } = models)
  })

  it('creates a new journal', async () => {
    generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })

    const { userId: adminId } = generateTeamMember({
      team: Team.Role.admin,
    })
    const journal = await addJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday })
      .execute({ input, userId: adminId })

    expect(journal.saveJournalAndJournalArticleTypes).toHaveBeenCalledTimes(1)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
  })

  it('returns an error if the apc is negative', async () => {
    generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })
    const { userId: adminId } = generateTeamMember({
      team: Team.Role.admin,
    })
    input.apc *= -1

    try {
      await addJournalUseCase
        .initialize({ models, eventsService, isDateToday })
        .execute({ input, adminId })
    } catch (e) {
      expect(e.message).toEqual(
        `Journal APC ${input.apc} cannot be a negative number.`,
      )
    }
  })
  it('creates an active journal if the activation date is set to today', async () => {
    generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })

    const admin = generateTeamMember({
      team: Team.Role.admin,
    })

    input.activationDate = new Date().toISOString()
    const isDateToday = jest.fn(() => true)
    input.isActive = isDateToday()

    const journal = await addJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday })
      .execute({ input, userId: admin.userId })

    expect(jobsService.scheduleJournalActivation).not.toHaveBeenCalled()
    expect(journal.isActive).toBe(true)
  })
  it('creates an inactive journal when the activation date is in the future', async () => {
    generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })

    const admin = generateTeamMember({
      team: Team.Role.admin,
    })
    jest
      .spyOn(TeamMember, 'findOneByUserAndRole')
      .mockResolvedValueOnce({ admin })

    input.activationDate = new Date(chance.date({ year: 2069, string: true }))

    const journal = await addJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday })
      .execute({ input, userId: admin.userId })

    expect(journal.activationDate).toEqual(input.activationDate)
    expect(jobsService.scheduleJournalActivation).toHaveBeenCalledTimes(1)
    expect(journal.isActive).toBe(false)
  })
})
