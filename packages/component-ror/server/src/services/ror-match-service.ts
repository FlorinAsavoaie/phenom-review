import { RORResult } from '../types'
import { convertCountryCode } from '../converters/country-converter'

const getChosenCandidate = (candidates: RORResult[]) =>
  candidates.find(c => c.chosen)

const getFirstCandidateWithMaxScore = (candidates: RORResult[]) =>
  candidates.find(c => c.score === 1)

const getBestMatchBasedOnCountry = (
  country: string,
  candidates: RORResult[],
): RORResult =>
  candidates.find(
    c => c?.organization?.country?.code === convertCountryCode(country),
  )

const getCandidatesWithCityInAffiliation = (
  affiliation: string,
  candidates: RORResult[],
): RORResult[] => {
  const aff = affiliation.toLowerCase()

  return candidates.filter(c => {
    const citiesFromAddresses = c.organization.addresses
      .map(a => a.city.toLowerCase())
      .filter(a => !!a)

    return citiesFromAddresses.some(city => aff.includes(city))
  })
}

const getBestMatchBasedOnCountryAndCity = (
  affiliation: string,
  country: string,
  candidates: RORResult[],
): RORResult => {
  const candidatesWithTheSameCity = getCandidatesWithCityInAffiliation(
    affiliation,
    candidates,
  )

  return getBestMatchBasedOnCountry(country, candidatesWithTheSameCity)
}

const getBestMatchBasedOnCity = (
  affiliation: string,
  candidates: RORResult[],
): RORResult => {
  const candidatesWithTheSameCity = getCandidatesWithCityInAffiliation(
    affiliation,
    candidates,
  )
  return candidatesWithTheSameCity[0]
}

export const getBestMatchROR = (
  affiliation: string,
  country: string,
  candidates: RORResult[],
): RORResult => {
  if (candidates.length === 0) {
    return undefined
  }

  if (candidates.length === 1) {
    return candidates[0]
  }

  const chosenCandidate = getChosenCandidate(candidates)
  if (chosenCandidate) {
    return chosenCandidate
  }

  const maxScoreCandidate = getFirstCandidateWithMaxScore(candidates)
  if (maxScoreCandidate) {
    return maxScoreCandidate
  }

  const bestMatchBasedOnCountryAndCity = getBestMatchBasedOnCountryAndCity(
    affiliation,
    country,
    candidates,
  )
  if (bestMatchBasedOnCountryAndCity) {
    return bestMatchBasedOnCountryAndCity
  }

  const bestMatchBasedOnCity = getBestMatchBasedOnCity(affiliation, candidates)
  if (bestMatchBasedOnCity) {
    return bestMatchBasedOnCity
  }
  const bestMatchOnCountry = getBestMatchBasedOnCountry(country, candidates)
  if (bestMatchOnCountry) {
    return bestMatchOnCountry
  }

  return candidates[0]
}
