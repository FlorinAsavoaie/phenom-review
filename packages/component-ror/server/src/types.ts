export type Address = {
  city: string
}

export type Country = {
  name: string
  code: string
}

export type Organization = {
  id: string
  name: string
  country: Country
  addresses: Address[]
}

export type RORResult = {
  score: number
  chosen: boolean
  organization: Organization
}

export interface RORService {
  getRORById(rorId: string): Promise<RORResult | null>
  getBestROR(
    affiliation: string,
    country: string,
  ): Promise<RORResult | undefined>
  getRORResults(affiliation: string): Promise<RORResult[]>
}
