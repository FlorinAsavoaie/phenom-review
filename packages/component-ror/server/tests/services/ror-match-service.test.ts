/* eslint-disable sonarjs/no-duplicate-string */
import { getBestMatchROR } from '../../src/services/ror-match-service'

describe('ror-match-service', () => {
  describe('getBestMatchROR', () => {
    it('returns undefined if candidates is empty', () => {
      const bestMatch = getBestMatchROR('aff', 'RO', [])
      expect(bestMatch).toEqual(undefined)
    })
    it('returns the only candidate', () => {
      const candidates = [
        {
          score: 0.5,
          chosen: false,
          organization: {
            id: 'some-id',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
      ]
      const bestMatch = getBestMatchROR('aff', 'RO', candidates)
      expect(bestMatch).toEqual(candidates[0])
    })
    it('returns the chosen candidate', () => {
      const candidates = [
        {
          score: 0.5,
          chosen: false,
          organization: {
            id: 'some-id',
            name: 'candidate-1',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 0.6,
          chosen: true,
          organization: {
            id: 'some-id',
            name: 'candidate-2',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 0.7,
          chosen: false,
          organization: {
            id: 'some-id',
            name: 'candidate-3',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
      ]
      const bestMatch = getBestMatchROR('aff', 'RO', candidates)
      expect(bestMatch).toEqual(candidates[1])
    })
    it('returns the first candidate if score equals 1', () => {
      const candidates = [
        {
          score: 1,
          chosen: false,
          organization: {
            id: 'candidate-1',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 0.5,
          chosen: false,
          organization: {
            id: 'candidate-2',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
      ]
      const bestMatch = getBestMatchROR('aff', 'RO', candidates)
      expect(bestMatch).toEqual(candidates[0])
    })
    it('returns the chosen candidate if multiple candidates have score 1', () => {
      const candidates = [
        {
          score: 1,
          chosen: false,
          organization: {
            id: 'candidate-1',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 1,
          chosen: true,
          organization: {
            id: 'candidate-2',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 0.5,
          chosen: false,
          organization: {
            id: 'candidate-3',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
      ]
      const bestMatch = getBestMatchROR('aff', 'RO', candidates)
      expect(bestMatch).toEqual(candidates[1])
    })
    it('returns the proper candidate based on country and city', () => {
      const candidates = [
        {
          score: 0.99,
          chosen: false,
          organization: {
            id: 'candidate-1',
            name: 'org-name',
            country: { name: 'Afghanistan', code: 'AFG' },
            addresses: [],
          },
        },
        {
          score: 0.9,
          chosen: false,
          organization: {
            id: 'candidate-2',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [],
          },
        },
        {
          score: 0.8,
          chosen: false,
          organization: {
            id: 'candidate-3',
            name: 'org-name',
            country: { name: 'United States Of America', code: 'USA' },
            addresses: [{ city: 'Iasi' }],
          },
        },
        {
          score: 0.7,
          chosen: false,
          organization: {
            id: 'candidate-4',
            name: 'org-name',
            country: { name: 'Romania', code: 'RO' },
            addresses: [{ city: 'Iasi' }],
          },
        },
      ]

      expect(getBestMatchROR('University Of iasi', 'RO', candidates)).toEqual(
        candidates[3],
      )
      expect(getBestMatchROR('University Of Iasi', 'UK', candidates)).toEqual(
        candidates[2],
      )
      expect(
        getBestMatchROR('University Of Bucharest', 'RO', candidates),
      ).toEqual(candidates[1])
      expect(getBestMatchROR('University Of London', 'UK', candidates)).toEqual(
        candidates[0],
      )
    })
  })
})
