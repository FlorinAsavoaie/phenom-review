// @ts-nocheck

const {
  getTeamRoles,
  generateAuditLog,
  generateTeamMember,
  generateManuscript,
  getManuscriptStatuses,
} = require('component-generators')

const { getAuditLogsUseCase } = require('../src/use-cases')

let models = {}
let Manuscript = {}
let TeamMember = {}
let AuditLog = {}
let Team = {}

describe('get all audit logs for all versions of a manuscript', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Manuscript: {
        compareVersion: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
      TeamMember: {
        findOneByManuscriptAndUser: jest.fn(),
      },
      AuditLog: {
        findAllBySubmissionId: jest.fn(),
        getTarget: jest.fn(),
        getTransformedLog: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
    }
    ;({ Manuscript, Team, TeamMember, AuditLog } = models)
  })
  it('should return the existing audit logs in the correct form', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.submitted,
      version: '1',
    })
    const author = generateTeamMember({
      team: { role: Team.Role.author },
    })

    const auditLog = generateAuditLog({
      userId: author.userId,
      manuscriptId: manuscript.id,
      action: 'manuscript_submitted',
      objectType: 'manuscript',
      objectId: manuscript.id,
      version: manuscript.version,
    })

    jest.spyOn(AuditLog, 'findAllBySubmissionId').mockResolvedValue([auditLog])

    const result = await getAuditLogsUseCase
      .initialize(models)
      .execute(manuscript.submissionId)

    expect(result.length).toEqual(1)
  })
  it('should return the target user data if the objectType is user', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.submitted,
      version: '1',
    })

    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const reviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
    })

    const auditLog = await generateAuditLog({
      userId: academicEditor.userId,
      manuscriptId: manuscript.id,
      action: 'reviewer_invited',
      objectType: 'user',
      objectId: reviewer.userId,
      version: manuscript.version,
    })

    jest.spyOn(AuditLog, 'findAllBySubmissionId').mockResolvedValue([auditLog])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValueOnce(academicEditor)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValueOnce(reviewer)

    const result = await getAuditLogsUseCase
      .initialize(models)
      .execute(manuscript.submissionId)

    expect(result.length).toEqual(1)
    expect(result[0].logs[0].user.email).toEqual(academicEditor.alias.email)
  })
  it('should correctly map logs to versions', async () => {
    const manuscriptV1 = generateManuscript({
      status: Manuscript.Statuses.olderVersion,
      version: '1',
    })
    const manuscriptV2 = generateManuscript({
      version: '2',
      status: Manuscript.Statuses.submitted,
      submissionId: manuscriptV1.submissionId,
    })
    const authorM1 = generateTeamMember({
      team: { role: Team.Role.author },
    })
    const admin = generateTeamMember({
      team: { role: Team.Role.admin },
    })
    const authorM2 = generateTeamMember({ team: { role: Team.Role.author } })

    const auditLogV1 = generateAuditLog({
      userId: authorM1.userId,
      manuscriptId: manuscriptV1.id,
      action: 'manuscript_submitted',
      objectType: 'manuscript',
      objectId: manuscriptV1.id,
      version: manuscriptV1.version,
    })

    const auditLogV2 = generateAuditLog({
      userId: admin.userId,
      manuscriptId: manuscriptV2.id,
      action: 'author_removed',
      objectType: 'user',
      objectId: authorM2.userId,
      version: manuscriptV2.version,
    })

    jest
      .spyOn(AuditLog, 'findAllBySubmissionId')
      .mockResolvedValue([auditLogV1, auditLogV2])

    const result = await getAuditLogsUseCase
      .initialize(models)
      .execute(manuscriptV1.submissionId)

    expect(result.length).toEqual(2)
    expect(result[0].version).toEqual('2')
    expect(result[0].logs).toBeDefined()
    expect(result[1].version).toEqual('1')
    expect(result[1].logs).toBeDefined()
  })
})
