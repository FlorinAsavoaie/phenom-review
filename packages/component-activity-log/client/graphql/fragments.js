import gql from 'graphql-tag'

export const auditLogsFragment = gql`
  fragment auditLogs on AuditOutput {
    logs {
      id
      created
      user {
        email
        role
        reviewerNumber
      }
      target {
        email
        role
        reviewerNumber
      }
      action
    }
    version
  }
`
