/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { get } from 'lodash'

import * as queries from './queries'

export default compose(
  graphql(queries.getAuditLogs, {
    options: props => ({
      variables: {
        submissionId: get(props, 'match.params.submissionId'),
      },
    }),
  }),
)
