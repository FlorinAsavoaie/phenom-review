# Component Logger

## 1. Why?

We were using pubsweet logger for logging until recently, to which we were passing a slightly customised Winston logger. We created this new component because while trying to format the logs according to the [Base Service Specs](https://confluence.wiley.com/pages/viewpage.action?spaceKey=PPT&title=Base+Service+Specs) we could not use either the latest version of Winston, or [Pino](https://www.npmjs.com/package/pino) and we wanted to have more control over how we log inside our application.

In the end we removed any third parties for logging anyway, but kept the logger component in order to have the control, as mentioned above.

Ofcourse, other components, like pubsweet-server of pubsweet-db and so on, still generate logs using the pubsweet-logger to which we pass our logger, as we did before, until we move the functionality from pubsweet into our own components.

## 2. How?

Since we only want a specificly formated string log, and not json, we've decided to use the global console and override its methods.

We can now generate logs like:

```javascript
/**
 * info/warn/debug level
**/
logger.info('message')
/**
 * output:
 * 2022-08-29T11:52:35.500Z info/warn/debug/trace review [] message
**/
logger.info('message', {contextKey: contextVal})
/**
 * output:
 * 2022-08-29T11:52:35.500Z info/warn/debug/trace review [contextKey=contextVal] message
**/


/**
 * error level
**/
const error = new Error('some error')
logger.error(error)
/**
 * output:
 * 2022-08-29T11:52:35.500Z error review [] Error: some error: {...}
**/
logger.error('message')
/**
 * output:
 * 2022-08-29T11:52:35.500Z error review [] message
**/
logger.error('message', error)
/**
 * output:
 * 2022-08-29T11:52:35.500Z error review [] Error: some error: {...} message
**/
logger.error('message', error, {contextKey: contextVal})
/**
 * output:
 * 2022-08-29T11:52:35.500Z error review [contextKey=contextVal] Error: some error: {...} message
**/
```

Access logs are generated with `trace` level and those are formated in the default config file under the `morganLogFormat` property like so:
```
':date[iso] trace review_access_logs [referrer=:referrer  method=:method url=:url graphql-operation=:graphql[operation] graphql-payload=:graphql[variables] status=:status response-time=:response-time ms] :method request on :url from :referrer with :status response status code'
```

## 3. Observations while developing:

### 1. logger.stream

`pubsweer-server` tries to stream morgan logs to logger.stream. The logger we pass to it does not have such a method, so it defaults to process.stdout, which is fine, because thats where we collect our logs anyway. 

```javascript
 app.use(
    morgan(config.get('pubsweet-server').morganLogFormat || 'combined', {
      stream: logger.stream,
    }),
  )
```

### 2. error logged 2 times?

On `development` or `test` envs, when an error is throwned, `pubsweet-server` outputs two error logs for some reason:

```javascript
if (app.get('env') === 'development' || app.get('env') === 'test') {
      logger.error(err)
      logger.error(err.stack)
    }
```

### 3. Custom errors

We use a series of customized errors from `@pubsweet/errors`:

```
module.exports.NotFoundError = require('./NotFoundError')
module.exports.ConflictError = require('./ConflictError')
module.exports.ValidationError = require('./ValidationError')
module.exports.AuthorizationError = require('./AuthorizationError')
```


we get these errors from the global object where we assign them in `default.js` config file, and let eslint know about that in `.tslintrc`

```javascript
Object.assign(global, require('@pubsweet/errors'))
```

```javascript
  "globals": {
    "ValidationError": true,
    "NotFoundError": true,
    "AuthorizationError": true,
    "ConflictError": true,
    "applicationEventBus": true
  }
```
### 4. Graphql errors logger

We log error from grapgql errors in `packages/app-review/server/src/app-configurator/graphql.ts` where we also ...format how we do that. 




