const config = require('config')
const axios = require('axios')
const { get, chain } = require('lodash')

const { logger } = require('component-logger')

const publonsKey = config.get('publons.key')
const publonsUrl = config.get('publons.reviewersUrl')

const getReviewerSuggestions = async ({
  id,
  title,
  abstract,
  journal,
  teams,
}) => {
  // if PUBLONS_KEY is not present in the .env file skip this code
  // this works like a feature flag
  if (!publonsKey) {
    return []
  }

  journal = {
    name: journal.name,
  }
  const authors = chain(teams)
    .find(t => t.role === 'author')
    .get('members', [])
    .map(m => ({
      email: get(m, 'alias.email', ''),
      firstName: get(m, 'alias.givenNames', ''),
      lastName: get(m, 'alias.surname', ''),
    }))
    .value()

  let response

  try {
    response = await axios({
      method: 'post',
      url: publonsUrl,
      headers: {
        'x-apikey': publonsKey,
      },
      data: {
        searchArticle: {
          title,
          abstract,
          journal,
          authors,
        },
      },
    })
  } catch (err) {
    logger.warn(err)
    throw new Error(
      `Could not fetch reviewer suggestions. Please check Publons provider integration`,
    )
  }

  return response.data.recommendedReviewers.map(rev => ({
    ...rev,
    type: 'publons',
    manuscriptId: id,
  }))
}

module.exports = {
  getReviewerSuggestions,
}
