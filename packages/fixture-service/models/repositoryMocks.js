const { filter, orderBy } = require('lodash')

module.exports = {
  findMock(id, type, fixtures) {
    const foundObj = Object.values(fixtures[type]).find(
      fixtureObj => fixtureObj.id === id,
    )
    if (!foundObj) {
      return
    }

    return Promise.resolve(foundObj)
  },
  allMock(type, fixtures) {
    return fixtures[type]
  },
  findAllMock(type, fixtures, orderByField, order, queryProps) {
    const filteredData = filter(fixtures[type], queryProps)
    return orderBy(filteredData, orderByField, order)
  },
  findOneByMock(values, type, fixtures) {
    delete values.eagerLoadRelations
    return filter(fixtures[type], values.queryObject)[0]
  },

  findByFieldMock(field, value, type, fixtures) {
    const foundObjs = Object.values(fixtures[type]).filter(
      fixtureObj => fixtureObj[field] === value,
    )

    return Promise.resolve(foundObjs)
  },
  findByMock(values, type, fixtures) {
    const foundObjs = filter(Object.values(fixtures[type]), values)
    return Promise.resolve(foundObjs)
  },
  findInMock(field, options, type, fixtures) {
    const foundObjs = Object.values(fixtures[type]).filter(fixtureObj =>
      options.includes(fixtureObj[field]),
    )

    return Promise.resolve(foundObjs)
  },
}
