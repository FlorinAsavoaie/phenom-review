const { assign } = require('lodash')
const Chance = require('chance')
const { Promise } = require('bluebird')

const chance = new Chance()

class Job {
  constructor(props) {
    this.id = chance.guid()
    this.jobType = props.jobType
    this.journalId = props.journalId
    this.teamMemberId = props.teamMemberId
    this.manuscriptId = props.manuscriptId
    this.specialIssueId = props.specialIssueId
  }

  static get Type() {
    return {
      activation: 'activation',
      deactivation: 'deactivation',
    }
  }

  static async findAllByTeamMembers(teamMemberIds) {
    return []
  }

  static async findAllByTeamMember(teamMemberId) {
    return []
  }

  static async findAllByTeamMemberAndManuscript({
    teamMemberId,
    manuscriptId,
  }) {
    return []
  }

  static async findOneBy() {
    return {
      delete: jest.fn(),
    }
  }

  static async cancel() {
    return jest.fn()
  }

  async save() {
    return Promise.resolve(this)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }
}

module.exports = Job
