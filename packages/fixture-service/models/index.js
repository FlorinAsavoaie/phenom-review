const Job = require('./job')
const User = require('./user')
const File = require('./file')
const Team = require('./team')
const Review = require('./review')
const Comment = require('./comment')
const Journal = require('./journal')
const Section = require('./section')
const Identity = require('./identity')
const AuditLog = require('./auditLog')
const Manuscript = require('./manuscript')
const TeamMember = require('./teamMember')
const ArticleType = require('./articleType')
const SpecialIssue = require('./specialIssue')
const PeerReviewModel = require('./peerReviewModel')
const JournalArticleType = require('./journalArticleType')
const ReviewerSuggestion = require('./reviewerSuggestion')

module.exports = {
  Job,
  User,
  Team,
  File,
  Review,
  Comment,
  Journal,
  Section,
  Identity,
  AuditLog,
  TeamMember,
  Manuscript,
  ArticleType,
  SpecialIssue,
  PeerReviewModel,
  ReviewerSuggestion,
  JournalArticleType,
}
