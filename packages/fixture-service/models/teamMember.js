const {
  get,
  pick,
  find,
  assign,
  filter,
  flatMap,
  countBy,
  forEach,
} = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

let { teamMembers } = fixtures
const { teams } = fixtures
const { findMock, findByMock, findByFieldMock } = require('./repositoryMocks')

const Manuscript = require('./manuscript')
const ArticleType = require('./articleType')

class TeamMember {
  constructor(props) {
    this.id = chance.guid()
    this.position = props.position || null
    this.isSubmitting = props.isSubmitting || false
    this.isCorresponding = props.isCorresponding || false
    this.status = props.status || 'pending'
    this.userId = props.userId || null
    this.teamId = props.teamId || null
    this.reviewerNumber = props.reviewerNumber || null
    this.jobs = props.jobs || []
  }

  static get Statuses() {
    return {
      pending: 'pending',
      accepted: 'accepted',
      declined: 'declined',
      submitted: 'submitted',
      expired: 'expired',
      removed: 'removed',
      active: 'active',
    }
  }

  static findByField = (field, value) =>
    findByFieldMock(field, value, 'teamMembers', fixtures)
  static findBy = values => findByMock(values, 'teamMembers', fixtures)
  static find = id => findMock(id, 'teamMembers', fixtures)
  static query = () => ({
    upsertGraph: obj =>
      Array.isArray(obj) ? obj.map(i => i.save()) : obj.save(),
  })

  static async findAllByStatuses({
    role,
    statuses,
    manuscriptId,
    submissionId,
  }) {
    let teamMembers = []
    let teams = []
    if (manuscriptId) {
      teams = filter(fixtures.teams, { role, manuscriptId })
    }

    if (submissionId) {
      const manuscripts = fixtures.manuscripts.filter(
        m => m.submissionId === submissionId,
      )
      const manuscriptTeams = flatMap(manuscripts, 'teams')
      const roleTeams = manuscriptTeams.filter(mT => mT.role === role)
      let submissionMembers = flatMap(roleTeams, 'members')
      if (statuses) {
        submissionMembers = submissionMembers.filter(sM =>
          statuses.includes(sM.status),
        )
      }

      return submissionMembers
    }

    if (!teams[0]) return []

    if (statuses) {
      teamMembers = teams[0].members.filter(m => statuses.includes(m.status))
    } else {
      teamMembers = teams[0].members
    }

    return teamMembers
  }

  static async findAllByJournalAndRole({ journalId, role }) {
    const teams = filter(fixtures.teams, { journalId, role })

    return flatMap(teams, t => t.members)
  }

  static async findAllByManuscriptAndRole({ manuscriptId, role }) {
    const teams = filter(fixtures.teams, { manuscriptId, role })

    return flatMap(teams, t => t.members)
  }

  static async findAllByManuscriptAndRoleAndExcludedStatuses({
    manuscriptId,
    role,
    excludedStatuses,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => m.manuscriptId === manuscriptId,
    )
    const manuscriptTeams = flatMap(manuscripts, 'teams')
    const roleTeams = manuscriptTeams.filter(mT => mT.role === role)
    const members = flatMap(roleTeams, 'members')

    return members.filter(m => excludedStatuses.includes(m.status))
  }

  static async findAllBySubmissionAndRole({ submissionId, role }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    const manuscriptTeams = flatMap(manuscripts, 'teams')
    const roleTeams = manuscriptTeams.filter(mT => mT.role === role)

    return flatMap(roleTeams, 'members')
  }

  static async findAllBySectionAndRole({ sectionId, role }) {
    const teams = filter(fixtures.teams, { sectionId, role })

    return flatMap(teams, t => t.members)
  }

  static async findAllBySpecialIssueAndRole({ specialIssueId, role }) {
    if (specialIssueId) {
      const teams = filter(fixtures.teams, { specialIssueId, role })
      return flatMap(teams, t => t.members)
    }
    return []
  }

  static async findAllBySubmissionAndRoleAndUser({
    role,
    userId,
    submissionId,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    const manuscriptTeams = flatMap(manuscripts, 'teams')
    const roleTeams = manuscriptTeams.filter(mT => mT.role === role)
    const members = flatMap(roleTeams, 'members')

    return members.filter(m => m.userId === userId)
  }

  static async findAllBySubmissionAndRoleAndStatuses({
    role,
    statuses,
    submissionId,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    const manuscriptTeams = flatMap(manuscripts, 'teams')
    const roleTeams = manuscriptTeams.filter(mT => mT.role === role)
    const members = flatMap(roleTeams, 'members')

    return members.filter(m => statuses.includes(m.status))
  }

  static async findAllByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
  }) {
    const teams = filter(fixtures.teams, { manuscriptId, role })
    if (!teams[0]) return []
    const members = flatMap(teams, t => t.members)

    return members.filter(m => m.status === status)
  }

  static async findSubmittingAuthor(manuscriptId) {
    const authorTeam = fixtures.teams.find(
      t => t.manuscriptId === manuscriptId && t.role === 'author',
    )
    if (!authorTeam) return
    return teamMembers.find(
      tm => tm.teamId === authorTeam.id && tm.isSubmitting === true,
    )
  }

  static async findCorrespondingAuthor(manuscriptId) {
    const authorTeam = fixtures.teams.find(
      t => t.manuscriptId === manuscriptId && t.role === 'author',
    )
    if (!authorTeam) return
    return teamMembers.find(
      tm => tm.teamId === authorTeam.id && tm.isCorresponding === true,
    )
  }

  static async findCorrespondingEditorialAssistant(journalId) {
    const editorialAssistantTeam = fixtures.teams.find(
      t => t.journalId === journalId && t.role === 'editorialAssistant',
    )
    if (!editorialAssistantTeam) return
    return teamMembers.find(
      tm =>
        tm.teamId === editorialAssistantTeam.id && tm.isCorresponding === true,
    )
  }

  static async findOneByUserAndRole({ userId, role }) {
    return find(fixtures.teams, { role, members: [{ userId }] })
  }

  static async findOneByRole({ role }) {
    const teams = filter(fixtures.teams, { role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers[0]
  }

  static async findOneByJournalAndRole({ journalId, role }) {
    const teams = filter(fixtures.teams, { journalId, role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers[0]
  }

  static async findOneByManuscriptAndRole({ manuscriptId, role }) {
    const teams = filter(fixtures.teams, { manuscriptId, role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers[0]
  }

  static async findOneBySectionAndRole({ sectionId, role }) {
    const teams = filter(fixtures.teams, { sectionId, role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers[0]
  }

  static async findOneByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
  }) {
    const teams = filter(fixtures.teams, { manuscriptId, role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members).filter(
      member => member.status === status,
    )

    return teamMembers[0]
  }

  static async findOneByManuscriptAndRoleAndUser({
    role,
    userId,
    manuscriptId,
  }) {
    const teams = filter(fixtures.teams, { manuscriptId, role })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers.find(tM => tM.userId === userId)
  }

  static async findOneByManuscriptAndUser({ userId, manuscriptId }) {
    const teams = filter(fixtures.teams, { manuscriptId })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t =>
      t.members.map(m => Object.assign(m, { role: t.role })),
    )

    return teamMembers.find(tM => tM.userId === userId)
  }

  static async findOneByJournalAndUser({ userId, journalId }) {
    const teams = filter(fixtures.teams, { journalId })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers.find(tM => tM.userId === userId)
  }

  static async findOneBySectionAndUser({ userId, sectionId }) {
    const teams = filter(fixtures.teams, { sectionId })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers.find(tM => tM.userId === userId)
  }

  static async findOneBySpecialIssueAndUser({ userId, specialIssueId }) {
    const teams = filter(fixtures.teams, { specialIssueId })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers.find(tM => tM.userId === userId)
  }

  static async findOneBySpecialIssueAndUsers({ userIds, specialIssueId }) {
    const teams = filter(fixtures.teams, { specialIssueId })
    if (!teams[0]) return
    const teamMembers = flatMap(teams, t => t.members)

    return teamMembers.find(tM => userIds.includes(tM.userId))
  }

  static async isApprovalEditor({ userId, manuscriptId, models }) {
    const { Manuscript, Team } = models

    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal.peerReviewModel]',
    )
    const approvalEditorRole = await this.getApprovalEditorRole({
      manuscript,
      TeamRole: Team.Role,
      TypesWithRIPE: ArticleType.TypesWithRIPE,
    })

    let approvalEditorTeamMembers
    approvalEditorTeamMembers = await this.findAllBySpecialIssueAndRole({
      role: approvalEditorRole,
      specialIssueId: manuscript.specialIssueId,
    })

    if (!approvalEditorTeamMembers) {
      approvalEditorTeamMembers = await this.findAllByJournalAndRole({
        role: approvalEditorRole,
        journalId: manuscript.journalId,
      })
    }

    if (approvalEditorTeamMembers.length === 0) {
      return false
    }

    return approvalEditorTeamMembers.some(member => member.userId === userId)
  }

  static async getApprovalEditorRole({
    manuscript,
    TeamRole,
    TypesWithRIPE = [],
  }) {
    const { articleType } = manuscript
    if (TypesWithRIPE.includes(articleType.name)) {
      return TeamRole.researchIntegrityPublishingEditor
    }
    if (manuscript.specialIssueId) {
      return TeamRole.triageEditor
    }
    if (articleType.hasPeerReview) {
      if (
        manuscript.journal.peerReviewModel.approvalEditors.includes(
          TeamRole.academicEditor,
        )
      ) {
        return TeamRole.academicEditor
      }
      return TeamRole.triageEditor
    } else if (
      await this.findOneByManuscriptAndRoleAndStatus({
        manuscriptId: manuscript.id,
        role: TeamRole.academicEditor,
        status: 'accepted',
      })
    ) {
      return TeamRole.academicEditor
    }
    return TeamRole.triageEditor
  }

  static async findTeamMembersWorkloadByJournal({
    journalId,
    role,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => manuscriptStatuses.includes(m.status) && m.journalId === journalId,
    )
    const manuscriptIds = manuscripts.map(m => m.id)

    const teams = fixtures.teams.filter(
      t => manuscriptIds.includes(t.manuscriptId) && t.role === role,
    )
    const teamIds = teams.map(t => t.id)
    const teamMembersFiltered = teamMembers.filter(
      tm =>
        teamIds.includes(tm.teamId) && teamMemberStatuses.includes(tm.status),
    )
    const workload = countBy(teamMembersFiltered, 'userId')

    let teamMembersWorkload = []
    forEach(workload, (value, key) => {
      teamMembersWorkload = [
        ...teamMembersWorkload,
        { userId: key, workload: value },
      ]
    })

    return teamMembersWorkload
  }
  static async findTeamMembersWorkloadBySection({
    sectionId,
    role,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => manuscriptStatuses.includes(m.status) && m.sectionId === sectionId,
    )
    const manuscriptIds = manuscripts.map(m => m.id)

    const teams = fixtures.teams.filter(
      t => manuscriptIds.includes(t.manuscriptId) && t.role === role,
    )
    const teamIds = teams.map(t => t.id)
    const teamMembersFiltered = teamMembers.filter(
      tm =>
        teamIds.includes(tm.teamId) && teamMemberStatuses.includes(tm.status),
    )
    const workload = countBy(teamMembersFiltered, 'userId')

    let teamMembersWorkload = []
    forEach(workload, (value, key) => {
      teamMembersWorkload = [
        ...teamMembersWorkload,
        { userId: key, workload: value },
      ]
    })

    return teamMembersWorkload
  }
  static async findTeamMembersWorkloadBySpecialIssue({
    specialIssueId,
    role,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m =>
        manuscriptStatuses.includes(m.status) &&
        m.specialIssueId === specialIssueId,
    )
    const manuscriptIds = manuscripts.map(m => m.id)

    const teams = fixtures.teams.filter(
      t => manuscriptIds.includes(t.manuscriptId) && t.role === role,
    )
    const teamIds = teams.map(t => t.id)
    const teamMembersFiltered = teamMembers.filter(
      tm =>
        teamIds.includes(tm.teamId) && teamMemberStatuses.includes(tm.status),
    )
    const workload = countBy(teamMembersFiltered, 'userId')

    let teamMembersWorkload = []
    forEach(workload, (value, key) => {
      teamMembersWorkload = [
        ...teamMembersWorkload,
        { userId: key, workload: value },
      ]
    })

    return teamMembersWorkload
  }

  static async deleteStaffMemberByUserIdAndRole({ userId, role }) {
    const team = teams.find(
      t =>
        t.role === role &&
        t.journalId === null &&
        t.specialIssueId === null &&
        t.manuscriptId === null &&
        t.sectionId === null,
    )
    team.members = team.members.filter(member => member.userId !== userId)
  }

  async save() {
    const existingTeamMember = teamMembers.find(m => m.id === this.id)
    if (existingTeamMember) {
      assign(existingTeamMember, this)
    } else {
      if (!this.id) {
        this.id = chance.guid()
      }
      teamMembers.push(this)
    }

    return Promise.resolve(this)
  }

  static async findTriageEditor({
    TeamRole,
    sectionId,
    journalId,
    manuscriptId,
  }) {
    let triageEditor = await this.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: TeamRole.triageEditor,
      status: this.Statuses.active,
    })

    if (triageEditor) return triageEditor

    if (!journalId && !sectionId) {
      throw new Error('Journal ID or Section ID is required.')
    }

    triageEditor = await this.findOneBySectionAndRole({
      sectionId,
      role: TeamRole.triageEditor,
    })

    if (triageEditor) return triageEditor

    triageEditor = await this.findOneByJournalAndRole({
      journalId,
      role: TeamRole.triageEditor,
    })

    return triageEditor
  }

  static async handleCorrespondingAuthor({
    manuscriptId,
    editedAuthor,
    inputCorresponding,
  }) {
    const correspondingAuthor = await this.findCorrespondingAuthor(manuscriptId)
    if (inputCorresponding === true) {
      await this.updateCorrespondingMember({
        newCorrespondingMember: editedAuthor,
        oldCorrespondingMember: correspondingAuthor,
      })
    } else {
      const submittingAuthor = await this.findSubmittingAuthor(manuscriptId)
      if (editedAuthor.id === correspondingAuthor.id) {
        await this.updateCorrespondingMember({
          oldCorrespondingMember: editedAuthor,
          newCorrespondingMember: submittingAuthor,
        })
      }
      if (!correspondingAuthor) {
        submittingAuthor.updateProperties({
          isCorresponding: true,
        })
        await submittingAuthor.save()
      }
    }
  }

  static async updateCorrespondingMember({
    newCorrespondingMember,
    oldCorrespondingMember,
  }) {
    oldCorrespondingMember.isCorresponding = false
    newCorrespondingMember.isCorresponding = true
  }

  setAlias(identity) {
    this.alias = pick(identity, [
      'aff',
      'title',
      'email',
      'surname',
      'country',
      'givenNames',
    ])
  }

  getName() {
    return `${get(this, 'alias.givenNames', '')} ${get(
      this,
      'alias.surname',
      '',
    )}`
  }

  toDTO() {
    return {
      ...this,
      user: this.user ? this.user.toDTO() : undefined,
      alias: {
        ...this.alias,
        name: {
          surname: this.alias.surname,
          givenNames: this.alias.givenNames,
        },
      },
    }
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  async delete() {
    if (this.team) {
      this.team.members = this.team.members.filter(
        member => member.id !== this.id,
      )
    }
    teamMembers = teamMembers.filter(member => member.id !== this.id)
  }
  static updateMany = async ({ teamMembers, trx }) => {
    teamMembers.map(async tm => tm.save())
  }
  static insertMany = async ({ teamMembers, trx }) => {
    teamMembers.map(tm =>
      fixtures.generateTeamMember({
        properties: {
          userId: tm.userId,
          teamId: tm.teamId,
          status: tm.status,
        },
        TeamMember,
      }),
    )
  }
  static knex() {
    return this
  }
}

module.exports = TeamMember
