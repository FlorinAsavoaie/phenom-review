const bcrypt = require('bcrypt')
const { assign, remove } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { identities, users } = fixtures
const { findOneByMock, findByMock } = require('./repositoryMocks')

class Identity {
  constructor(props) {
    this.id = chance.guid()
    this.userId = props.userId
    this.type = props.type || 'local'
    this.isConfirmed =
      props.isConfirmed === undefined ? true : props.isConfirmed
    this.email = props.email || chance.email()
    this.passwordHash =
      '$2a$12$BrbCU5BzSWycO0ykn84hH.bFgk.lrmZQ6CgQiD9p6Yy1Y0DQy1maW' // the hash for 'password'
    this.surname = props.surname || chance.last()
    this.givenNames = props.givenNames || chance.first()
    this.aff = props.aff || chance.company()
    this.country = props.country || chance.country()
    this.title = props.title || 'mr'
  }

  static findOneBy = values => findOneByMock(values, 'identities', fixtures)
  static findBy = values => findByMock(values, 'identities', fixtures)
  static findOneByEmail = value => {
    const foundObj = Object.values(identities).find(
      fixtureObj => fixtureObj.email.toLowerCase() === value.toLowerCase(),
    )

    return Promise.resolve(foundObj)
  }
  static hashPassword = password => bcrypt.hash(password, 1)

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  async validPassword(password) {
    return bcrypt.compare(password, this.passwordHash)
  }

  async saveRecursively() {
    await this.save()
  }

  async save() {
    identities.push(this)
    return Promise.resolve(this)
  }
  async delete() {
    remove(identities, identity => identity.id === this.id)
    const user = users.find(user => user.id === this.userId)
    remove(user.identities, identity => identity.id === this.id)
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = Identity
