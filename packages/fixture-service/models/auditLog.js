const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { logs } = fixtures
const { findByFieldMock, findByMock } = require('./repositoryMocks')

class AuditLog {
  constructor(props) {
    this.id = chance.guid()
    this.created = props.create || Date.now()
    this.updated = props.updated || Date.now()
    this.userId = props.userId
    this.manuscriptId = props.manuscriptId
    this.action = props.action || chance.word()
    this.objectType = props.objectType
    this.objectId = props.objectId
    this.journalId = props.journalId || chance.word()
  }

  static findByField = (field, value) =>
    findByFieldMock(field, value, 'logs', fixtures)
  static findBy = values => findByMock(values, 'logs', fixtures)

  async save() {
    logs.push(this)
    return Promise.resolve(this)
  }

  toDTO() {
    return {
      ...this,
      user: this.user ? this.user.toDTO() : undefined,
    }
  }
}

module.exports = AuditLog
