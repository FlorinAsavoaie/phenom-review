const journals = []
const generateJournal = ({ properties, Journal }) => {
  const journal = new Journal(properties || {})
  journals.push(journal)

  return journal
}

module.exports = { journals, generateJournal }
