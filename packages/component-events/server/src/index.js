/* eslint-disable sonarjs/no-identical-functions */
const { pick } = require('lodash')

const PPBK4962_ENABLED =
  (process.env.FEATURE_PPBK4962_SPECIAL_ISSUE || '').toString() === 'true'

module.exports.initialize = ({
  models: { Journal, Manuscript, User, Team, SpecialIssue },
}) => ({
  async publishSubmissionEvent({
    submissionId,
    eventName,
    timestamp = new Date().toISOString(),
    messageAttributes = null,
  }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      eagerLoadRelations: [
        'files',
        `journal.[
          teams.members,
          peerReviewModel
        ]`,
        'sourceJournal',
        'section',
        'articleType',
        'reviewerSuggestions',
        `reviews.[
          comments.files,
          rejectDecisionInfo
        ]`,
        'teams.members.user.identities',
        'specialIssue.peerReviewModel',
      ],
    })
    if (manuscripts.length === 0) return

    let peerReviewModel = {}
    const manuscript = manuscripts[0]

    if (manuscript.specialIssue) {
      ;({ peerReviewModel } = manuscript.specialIssue)
    } else {
      ;({ peerReviewModel } = manuscript.journal)
    }

    const data = {
      submissionId,
      manuscripts: manuscripts.map(manuscript => {
        const properties = pick(manuscript, [
          'id',
          'title',
          'files',
          'version',
          'customId',
          'abstract',
          'journalId',
          'sectionId',
          'acceptedDate',
          'preprintValue',
          'specialIssueId',
          'dataAvailability',
          'fundingStatement',
          'conflictOfInterest',
          'linkedSubmissionCustomId',
          'qualityChecksSubmittedDate',
        ])

        properties.files.forEach(file => {
          delete file.manuscriptId
          delete file.commentId
        })

        const articleTypeName =
          manuscript.articleType && manuscript.articleType.name

        const sourceJournalName =
          manuscript.sourceJournal && manuscript.sourceJournal.name

        const pissn = manuscript.sourceJournal && manuscript.sourceJournal.pissn
        const eissn = manuscript.sourceJournal && manuscript.sourceJournal.eissn
        const publisher =
          manuscript.sourceJournal && manuscript.sourceJournal.publisher

        const sourceJournalManuscriptId =
          manuscript.sourceJournalManuscriptId || undefined

        return {
          ...properties,
          submissionCreatedDate: manuscript.created,
          editors: [
            ...getEditors({
              Team,
              peerReviewModel,
              teams: manuscript.teams.filter(
                team =>
                  team.role !== Team.Role.author &&
                  team.role !== Team.Role.reviewer &&
                  team.role !== Team.Role.submittingStaffMember,
              ),
            }),
          ],
          submittingStaffMembers: manuscript.getSubmittingStaffMembersForEventData(
            { Team },
          ),
          authors: manuscript.getAuthorsForEventData(),
          reviewers: manuscript.getReviewersForEventData({
            Team,
          }),
          reviews: manuscript.getReviewsForEventData(),
          articleType: { name: articleTypeName },
          sourceJournal: { name: sourceJournalName, pissn, eissn, publisher },
          sourceJournalManuscriptId,
        }
      }),
    }

    const messagePayload = {
      data,
      event: eventName,
      timestamp: new Date(timestamp).toISOString(),
    }

    if (messageAttributes) {
      messagePayload.messageAttributes = { type: messageAttributes }
    }

    await applicationEventBus.publishMessage(messagePayload)
  },
  async publishUserEvent({ userId, eventName }) {
    const user = await User.find(userId, 'identities')
    delete user.passwordResetTimestamp
    delete user.passwordResetToken
    delete user.confirmationToken
    delete user.invitationToken
    delete user.unsubscribeToken

    user.identities.forEach(identity => {
      delete identity.userId
      delete identity.passwordHash
    })

    await applicationEventBus.publishMessage({
      event: eventName,
      data: { ...user },
    })
  },
  async publishJournalEvent({ journalId, eventName }) {
    const journal = await Journal.find(
      journalId,
      `[
        sections.[
          teams.members.user.identities,
          specialIssues.[
            teams.members.user.identities,
            peerReviewModel
          ]
        ],
        specialIssues.[
          teams.members.user.identities,
          peerReviewModel
        ],
        teams.members.user.identities,
        peerReviewModel,
        journalArticleTypes.articleType
      ]`,
    )

    journal.sections = journal.sections.map(section => {
      section.specialIssues = section.specialIssues.map(specialIssue => ({
        ...specialIssue,
        peerReviewModel: { name: specialIssue.peerReviewModel.name },
        editors: getEditors({
          teams: specialIssue.teams,
          peerReviewModel: specialIssue.peerReviewModel,
          Team,
        }),
      }))

      return {
        ...section,
        editors: getEditors({
          teams: section.teams,
          peerReviewModel: journal.peerReviewModel,
          Team,
        }),
      }
    })

    journal.articleTypes = journal.journalArticleTypes.map(
      journalArticleType => ({
        name: journalArticleType.articleType.name,
      }),
    )

    journal.specialIssues = journal.specialIssues.map(specialIssue => ({
      ...specialIssue,
      peerReviewModel: { name: specialIssue.peerReviewModel.name },
      editors: getEditors({
        teams: specialIssue.teams,
        peerReviewModel: specialIssue.peerReviewModel,
        Team,
      }),
    }))

    const cleanSpecialIssue = specialIssue => {
      delete specialIssue.teams
      delete specialIssue.peerReviewModelId
      delete specialIssue.journalId
      delete specialIssue.sectionId
      delete specialIssue.description
      delete specialIssue.topics
      delete specialIssue.acronym
      delete specialIssue.publicationDate
      delete specialIssue.isAnnual
      delete specialIssue.conflictOfInterest
      delete specialIssue.type
    }

    const data = {
      ...journal,
      peerReviewModel: { name: journal.peerReviewModel.name },
      editors: getEditors({
        teams: journal.teams,
        peerReviewModel: journal.peerReviewModel,
        Team,
      }),
    }

    delete data.apc
    delete data.teams
    delete data.journalArticleTypes
    delete data.peerReviewModelId
    data.sections.forEach(section => {
      delete section.teams
      delete section.journalId
    })
    data.specialIssues.forEach(cleanSpecialIssue)
    data.sections.forEach(section =>
      section.specialIssues.forEach(cleanSpecialIssue),
    )
    await applicationEventBus.publishMessage({
      data,
      event: eventName,
    })
  },
  async publishSpecialIssueEvent({ specialIssueId, eventName }) {
    if (!PPBK4962_ENABLED) {
      return
    }
    const specialIssue = await SpecialIssue.find(
      specialIssueId,
      `[
        teams.members.user.identities,
        journal,
        section.[
          journal
        ],
    ]`,
    )

    const siDTO = specialIssue.toDTO()
    if (!siDTO.isPublished || !siDTO.leadGuestEditor || !siDTO.acronym) {
      return
    }

    const journal = specialIssue.section?.journal || specialIssue.journal
    const data = {
      id: specialIssueId,
      customId: specialIssue.customId,
      title: specialIssue.name,
      acronym: specialIssue.acronym,
      journalId: journal.id,
      journalTitle: journal.name,
      journalCode: journal.code,
      sectionId: specialIssue.section?.id,
      sectionTitle: specialIssue.section?.name,
      isCancelled: specialIssue.isCancelled,
      submissionUrl: specialIssue.submissionUrl,
      submissionStartDate: specialIssue.startDate,
      submissionEndDate: specialIssue.endDate,
      publicationDate: specialIssue.publicationDate,
      description: specialIssue.description,
      topics: specialIssue.topics,
      editors: specialIssue.teams.flatMap(team =>
        team.members.map(member => ({
          id: member.id,
          role:
            team.role === Team.Role.triageEditor
              ? 'lead guest editor'
              : 'guest editor',
          surname: member.alias?.surname,
          givenNames: member.alias?.givenNames,
          email: member.alias?.email,
          affiliation: member.alias?.aff,
          countryCode: member.alias?.country,
        })),
      ),
    }
    await applicationEventBus.publishMessage({
      data,
      event: eventName,
      messageAttributes: {
        EventName: eventName,
      },
    })
  },
  async publishSendEmailEvent({ usecase, eventData }) {
    const { templatePlaceholders, emailInput } = eventData
    const { sender, recipient, bcc, replyTo, updatedHtmlBodies } = emailInput
    const data = {
      usecase,
      templatePlaceholders,
      sender,
      recipient,
      bcc,
      replyTo,
      updatedHtmlBodies,
    }
    await applicationEventBus.publishMessage({
      data,
      event: 'EmailRequested',
    })
  },
  async publishFileEvent({ data, eventName }) {
    await applicationEventBus.publishMessage({
      data,
      event: eventName,
    })
  },
})

const getEditors = ({ peerReviewModel, teams, Team }) => {
  const teamReducer = (acc, team) => {
    const members = team.members.map(member =>
      member.getEventData({
        peerReviewModel,
        role: team.role,
        Team,
      }),
    )
    return [...acc, ...members]
  }

  return teams.reduce(teamReducer, [])
}
