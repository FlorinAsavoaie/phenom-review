const events = require('../src')
const models = require('@pubsweet/models')
const { logger } = require('component-logger')

const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./useCases')

const resolvers = {
  Query: {},
  Mutation: {
    async emitSubmissionEvent(_, { input }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.emitSubmissionEventUseCase
        .initialize({ eventsService, logger })
        .execute({
          userId: ctx.user,
          ...input,
        })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
