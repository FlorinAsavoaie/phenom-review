const fs = require('fs')
const path = require('path')

const events = require('./server/src')
const resolvers = require('./server/src/resolvers')
const useCases = require('./server/src/useCases')

module.exports = {
  ...events,
  resolvers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
  useCases,
}
