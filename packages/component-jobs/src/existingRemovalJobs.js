module.exports.initialize = ({
  models: { Job, Manuscript, Team, TeamMember, ReviewerSuggestion },
  logger,
  logEvent,
  eventsService,
}) => ({
  async handle(job) {
    try {
      const { invitationId, manuscriptId, action } = job.data

      if (!manuscriptId) {
        logger.warn(`Manuscript id is undefined`)
        return job.done()
      }

      if (!invitationId) {
        logger.warn(`Team Member id is undefined`)
        return job.done()
      }

      const manuscript = await Manuscript.find(manuscriptId)
      if (!manuscript) {
        logger.warn(`Manuscript ${manuscriptId} has not been found`)
        return job.done()
      }
      if (!Manuscript.InProgressStatuses.includes(manuscript.status)) {
        logger.warn(
          `Cannot remove team members on manuscript ${manuscriptId} with status ${manuscript.status}`,
        )

        return job.done()
      }

      const teamMember = await TeamMember.find(invitationId, 'team')

      if (!teamMember) {
        logger.error(`No team member found with id ${invitationId}`)
        return job.done()
      }
      const { role } = teamMember.team

      if (
        ![TeamMember.Statuses.pending, TeamMember.Statuses.accepted].includes(
          teamMember.status,
        ) ||
        ![Team.Role.reviewer, Team.Role.academicEditor].includes(role)
      ) {
        logger.warn(
          `Cannot remove team members on manuscript ${manuscriptId} with status ${manuscript.status} and team member role ${role}`,
        )

        return job.done()
      }

      let eventName
      if (role === Team.Role.reviewer) {
        if (teamMember.status === TeamMember.Statuses.accepted) {
          eventName = 'ReviewerRemoved'
        } else {
          eventName = 'ReviewerInvitationExpired'
        }
        await handleReviewer({
          teamMember,
          manuscript,
          models: { Team, Manuscript, TeamMember, ReviewerSuggestion },
        })
      } else if (teamMember.status !== TeamMember.Statuses.accepted) {
        eventName = 'AcademicEditorInvitationExpired'
        await handleAcademicEditor({
          teamMember,
          manuscript,
          models: { TeamMember, Team, Manuscript },
        })
      } else {
        logger.warn(
          `Cannot remove accepted academic editor on manuscript ${manuscriptId}`,
        )

        return job.done()
      }

      if (action) {
        logEvent({
          userId: null,
          manuscriptId,
          action,
          objectType: logEvent.objectType.user,
          objectId: teamMember.userId,
        })
      }

      logger.info(
        `Successfully removed ${role} with id ${teamMember.id} from manuscript ${manuscriptId}`,
      )

      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: `Submission${eventName}`,
      })

      return job.done()
    } catch (e) {
      logger.error('Something went wrong', e)
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'removal-*',
      jobHandler: this.handle,
    })
  },
})

const handleAcademicEditor = async ({
  manuscript,
  teamMember,
  models: { TeamMember, Team, Manuscript },
}) => {
  teamMember.updateProperties({ status: TeamMember.Statuses.expired })
  await teamMember.save()
  const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      role: Team.Role.academicEditor,
      status: TeamMember.Statuses.accepted,
      manuscriptId: manuscript.id,
    },
  )
  if (!activeAcademicEditor) {
    manuscript.updateProperties({ status: Manuscript.Statuses.submitted })
    await manuscript.save()
  }
}

const handleReviewer = async ({
  manuscript,
  teamMember,
  models: { Team, TeamMember, Manuscript, ReviewerSuggestion },
}) => {
  const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
    queryObject: {
      email: teamMember.alias.email,
      manuscriptId: manuscript.id,
    },
  })
  if (reviewerSuggestion) {
    reviewerSuggestion.isInvited = false
    await reviewerSuggestion.save()
  }

  teamMember.updateProperties({ status: TeamMember.Statuses.expired })
  await teamMember.save()

  const reviewers = await TeamMember.findAllByManuscriptAndRoleAndExcludedStatuses(
    {
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
      excludedStatuses: [
        TeamMember.Statuses.expired,
        TeamMember.Statuses.removed,
        TeamMember.Statuses.conflicting,
      ],
    },
  )

  if (reviewers.length === 0) {
    manuscript = await Manuscript.find(manuscript.id)
    if (manuscript.status !== Manuscript.Statuses.academicEditorAssigned) {
      manuscript.updateStatus(Manuscript.Statuses.academicEditorAssigned)
      await manuscript.save()
    }
  }
}
