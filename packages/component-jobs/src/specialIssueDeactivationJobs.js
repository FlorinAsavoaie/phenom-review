module.exports.initialize = ({
  models: { Job, SpecialIssue, Section, TeamMember, Team, Journal },
  Email,
  logger,
  eventsService,
  getPropsService,
}) => ({
  async handle(job) {
    try {
      const { specialIssueId, emailProps } = job.data
      const specialIssue = await SpecialIssue.find(specialIssueId)

      if (!specialIssue.isActive) return

      specialIssue.updateProperties({ isActive: false })
      await specialIssue.save()

      logger.info(`Special Issue ${specialIssueId} has been deactivated`)

      const { sectionId } = specialIssue
      let { journalId } = specialIssue
      let eventName = 'JournalSpecialIssueDeactivated'

      if (sectionId) {
        ;({ journalId } = await Section.find(sectionId))
        eventName = 'JournalSectionSpecialIssueDeactivated'
      }

      const journal = await Journal.find(journalId)
      // notify guest editors
      if (journal.isActive) {
        const guestEditors = await TeamMember.findAllBySpecialIssueAndRole({
          specialIssueId: specialIssue.id,
          role: Team.Role.academicEditor,
        })
        guestEditors.forEach(guestEditor => {
          const newEmailProps = getPropsService.getProps({
            emailProps,
            user: guestEditor,
          })
          const email = new Email(newEmailProps)
          email.sendEmail()
        })
      }

      eventsService.publishJournalEvent({
        journalId,
        eventName,
      })
      eventsService.publishSpecialIssueEvent({
        specialIssueId,
        eventName: 'SpecialIssueUpdated',
      })

      return job.done()
    } catch (e) {
      throw new Error(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'special-issue-deactivation*',
      jobHandler: this.handle,
    })
  },
})
