const {
  generateManuscript,
  generateTeamMember,
  generateJob,
} = require('component-generators')
const { initialize } = require('../src/academicEditorRemovalJobs')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  Manuscript: {
    find: jest.fn(),
    Statuses: {
      submitted: 'submitted',
    },
  },
  Team: {
    Role: {
      academicEditor: 'academicEditor',
    },
  },
  TeamMember: {
    find: jest.fn(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
    findAllBySubmissionAndRoleAndUser: jest.fn(),
    Statuses: {
      expired: 'expired',
    },
  },
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logger = {
  warn: jest.fn(),
  info: jest.fn(),
}

const logEvent = jest.fn()
logEvent.objectType = {
  user: 'user',
}

const job = generateJob()

describe('academic editor removal jobs', () => {
  let wrapper
  beforeAll(() => {
    wrapper = initialize({ models, logger, logEvent, eventsService })
  })

  it('should have the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('should warn that the invitation id is undefined', async () => {
    const job = generateJob({ invitationId: undefined })

    await wrapper.handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Invitation id is undefined when trying to cancel academic editor on manuscript ${job.data.manuscriptId}`,
    )
  })

  it('should warn that no team member was found', async () => {
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(undefined)

    await wrapper.handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `No team member has been found for id ${job.data.invitationId}`,
    )
  })

  it('should update the team member status to expired', async () => {
    const teamMember = generateTeamMember()
    const manuscript = generateManuscript()
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockResolvedValue([teamMember])
    jest.spyOn(teamMember, 'updateProperties').mockReturnValue(teamMember)
    jest.spyOn(teamMember, 'save').mockReturnValue(teamMember)

    await wrapper.handle(job)

    expect(teamMember.updateProperties).toHaveBeenCalledWith({
      status: models.TeamMember.Statuses.expired,
    })
    expect(teamMember.save).toHaveBeenCalled()
  })

  it('should update the manuscript status if no academic editor is found', async () => {
    const manuscript = generateManuscript()
    const teamMember = generateTeamMember()
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockResolvedValue([teamMember])
    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(undefined)
    jest.spyOn(teamMember, 'updateProperties').mockReturnValue(teamMember)
    jest.spyOn(teamMember, 'save').mockReturnValue(teamMember)

    await wrapper.handle(job)

    expect(teamMember.updateProperties).toHaveBeenCalledWith({
      status: models.TeamMember.Statuses.expired,
    })
    expect(teamMember.save).toHaveBeenCalled()
  })

  it('should have logged the event with correct details', async () => {
    const manuscript = generateManuscript()
    const teamMember = generateTeamMember()
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(undefined)

    await wrapper.handle(job)

    expect(logEvent).toHaveBeenCalledWith({
      userId: null,
      manuscriptId: job.data.manuscriptId,
      action: job.data.action,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  })

  it('should publish the submission event', async () => {
    const manuscript = generateManuscript()
    const teamMember = generateTeamMember()
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(undefined)
    jest
      .spyOn(models.TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockResolvedValue([teamMember])

    await wrapper.handle(job)

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorInvitationExpired',
    })
  })
  it('should update the team members statuses to expired on all versions', async () => {
    const teamMember1 = generateTeamMember()
    const teamMember2 = generateTeamMember()
    const manuscript = generateManuscript()
    const previousVersionsMembershipsOfCurrentTeamMember = [
      teamMember1,
      teamMember2,
    ]

    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember1)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findAllBySubmissionAndRoleAndUser')
      .mockReturnValue([teamMember1, teamMember2])

    previousVersionsMembershipsOfCurrentTeamMember.forEach(teamMember => {
      jest.spyOn(teamMember, 'updateProperties').mockReturnValue(teamMember)
      jest.spyOn(teamMember, 'save').mockReturnValue(teamMember)
    })
    await wrapper.handle(job)

    previousVersionsMembershipsOfCurrentTeamMember.forEach(teamMember => {
      expect(teamMember.updateProperties).toHaveBeenCalledWith({
        status: models.TeamMember.Statuses.expired,
      })
      expect(teamMember.save).toHaveBeenCalled()
    })
  })
})
