const { generateJob } = require('component-generators')
const { initialize } = require('../src/triageEditorEmailJobs')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
}
const Email = jest.fn().mockImplementation(() => ({
  async sendEmail() {
    return jest.fn()
  },
}))

const logEvent = jest.fn()
logEvent.objectType = {
  user: 'user',
}

const job = generateJob()

describe('triageEditorEmailJobs', () => {
  let wrapper
  beforeAll(() => {
    wrapper = initialize({ models, Email, logEvent })
  })

  it('should have the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('should have tried to instantiate a new email with passed email props', () => {
    wrapper.handle(job)
    expect(Email).toHaveBeenCalledWith(job.data.emailProps)
  })

  it('should have logged the event with correct details', () => {
    wrapper.handle(job)

    expect(job.data.logActivity).toBe(true)

    expect(logEvent).toHaveBeenCalledWith({
      userId: null,
      manuscriptId: job.data.manuscriptId,
      action: job.data.action,
      objectType: logEvent.objectType.user,
      objectId: job.data.userId,
    })
  })

  it('should have called job.done', () => {
    wrapper.handle(job)
    expect(job.done).toHaveBeenCalled()
  })
})
