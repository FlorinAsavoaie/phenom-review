import * as dotenv from 'dotenv'

import { BuildManifestCommand } from './commands'

dotenv.config()

const command = new BuildManifestCommand()
;(async function main(): Promise<void> {
  await command.run()
})()
