import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'

export enum App {
  appReview = 'appReview',
}

export enum Tenant {
  hindawi = 'hindawi',
  gsw = 'gsw',
}

export enum Environment {
  dev = 'dev',
  qa = 'qa',
  demo = 'demo',
  prod = 'prod',
  demoGsw = 'demoGsw',
}

type AppCfg = Record<Tenant, Record<Environment, WithAwsSecretsServiceProps>>

export type MasterConfig = Record<App, AppCfg>
