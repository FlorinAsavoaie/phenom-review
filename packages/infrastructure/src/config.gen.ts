import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as requireGlob from 'require-glob'

const serviceConfigs = requireGlob.sync(
  [
    'service-*/infrastructure/chart/**/*.ts',
    'app-*/infrastructure/chart/**/*.ts',
  ],
  {
    cwd: `${__dirname}/../..`,
  },
)

const masterConfig: {
  [tenant: string]: {
    [env: string]: { [app: string]: WithAwsSecretsServiceProps }
  }
} = createMasterConfig(serviceConfigs)

export { masterConfig }

function createMasterConfig(serviceConfigs: any) {
  return Object.entries(serviceConfigs).reduce((acc, entry: any) => {
    const [
      serviceName,
      {
        infrastructure: { chart },
      },
    ] = entry

    delete chart.default

    // eslint-disable-next-line guard-for-in,no-restricted-syntax
    for (const tenant in chart) {
      // eslint-disable-next-line guard-for-in,no-restricted-syntax
      for (const key in chart[tenant]) {
        chart[tenant][key] = chart[tenant][key].index.values
      }
    }

    return {
      ...acc,
      [serviceName]: chart,
    }
  }, {})
}
