# Infrastructure Gaia

This component gathers infrastructure needs from all services and creates the necessary kubernetes manifests, in order to be deployed to our cluster.

### Prerequisites

1. [install aws-sdk](https://aws.amazon.com/sdk-for-javascript/)
2. [setup AWS access](https://hindawi.atlassian.net/wiki/spaces/PPT/pages/2776530947/AWS+Access)
3. [install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
4. contact a team member for configuration that will be added to `~.kube/config`
5. install [lens](https://k8slens.dev/)
6. add cluster defined in `.kube/config`

### Using the service

## Important:

the below details are important only if you plan to `synth` and apply charts to the cluster manually. Usually we do this through the ci pipeline, so it might not be of concern to know the following:

---

The `yarn synth` command will generate kubernetes manifests in `./dist-k8s` for all packages that have an `./infrastructure` folder exporting the appropriate config.

Those can be used with `kubectl apply -f ./dist-k8s/` to apply the changes on the cluster

### Required Env vars

```dotenv
CLUSTER=hindawi-dev
TENANT=hindawi
NODE_ENV=qa
CI_COMMIT_SHA=latest
```

```
yarn synth --profile [AWS_PROFILE]
```

- to deploy to a specific namespace:

```
kubectl apply -n qa -f ./dist-k8s/
```
