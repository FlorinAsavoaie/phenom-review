module.exports = {
  isDateToday(inputDate) {
    if (!inputDate) {
      return false
    }

    const date = new Date(inputDate)
    const today = new Date()

    return (
      date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    )
  },
  isDateInTheFuture(inputDate) {
    if (!inputDate) {
      return false
    }

    const futureDate = new Date(inputDate)
    const today = new Date()

    return futureDate > today
  },
  isDateInThePast(inputDate) {
    if (!inputDate) {
      return false
    }

    const pastDate = new Date(inputDate)
    const today = new Date()

    return pastDate < today
  },
  areDatesEqual(previousDate, currentDate) {
    if (!previousDate || !currentDate) {
      return false
    }
    const prevDate = new Date(previousDate)
    const currDate = new Date(currentDate)

    return (
      prevDate.getDate() === currDate.getDate() &&
      prevDate.getMonth() === currDate.getMonth() &&
      prevDate.getFullYear() === currDate.getFullYear()
    )
  },
  getExpectedDate({ timestamp = Date.now(), daysExpected = 0 }) {
    const date = new Date(timestamp)
    let expectedDate = date.getDate() + daysExpected
    date.setDate(expectedDate)

    expectedDate = date.toLocaleDateString('en-US', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    })

    return expectedDate
  },
}
