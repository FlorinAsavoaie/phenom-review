module.exports = {
  generateReviewerSuggestion(props) {
    return {
      updateProperties: jest.fn(),
      save: jest.fn(),
      ...props,
    }
  },
}
