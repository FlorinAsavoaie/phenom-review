const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateReview(props) {
    return {
      id: chance.guid(),
      isValid: true,
      setSubmitted: jest.fn(),
      save: jest.fn(),
      ...props,
    }
  },
  getRecommendations() {
    return recommendations
  },
}

const recommendations = {
  responseToRevision: 'responseToRevision',
  minor: 'minor',
  major: 'major',
  reject: 'reject',
  publish: 'publish',
  returnToAcademicEditor: 'returnToAcademicEditor',
}
