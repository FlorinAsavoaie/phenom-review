const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateTeam(props) {
    return {
      id: chance.guid(),
      role: roles.author,
      delete: jest.fn(),
      addMember: jest.fn(),
      save: jest.fn(),
      ...props,
    }
  },
  getTeamRoles() {
    return roles
  },
}

const roles = {
  author: 'author',
  admin: 'admin',
  triageEditor: 'triageEditor',
  reviewer: 'reviewer',
  academicEditor: 'academicEditor',
  editorialAssistant: 'editorialAssistant',
  researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
}
