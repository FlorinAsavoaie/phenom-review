const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateJob(props) {
    return {
      done: jest.fn(),
      data: {
        manuscriptId: chance.guid(),
        userId: chance.guid(),
        invitationId: chance.guid(),
        teamMemberId: chance.guid(),
        journalId: chance.guid(),
        action: chance.word(),
        emailProps: {
          to: chance.email(),
          from: chance.email(),
        },
        logActivity: true,
        ...props,
      },
    }
  },
  getJobTypes() {
    return types
  },
}

const types = { activation: 'activation', deactivation: 'deactivation' }
