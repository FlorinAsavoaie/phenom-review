const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateUser(props) {
    return {
      id: chance.guid(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
