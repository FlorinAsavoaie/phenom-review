const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateJournal(props) {
    return {
      id: chance.guid(),
      name: chance.name(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      saveGraph: jest.fn(),
      scheduleJob: jest.fn(),
      cancelJobs: jest.fn(),
      ...props,
    }
  },
}
