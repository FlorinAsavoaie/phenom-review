const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generatePeerReviewModel(props) {
    return {
      id: chance.guid(),
      name: chance.name(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      ...props,
    }
  },
}
