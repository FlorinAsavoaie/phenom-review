CopyText

No after copy text specified:

```js
<div style={{display: 'flex'}}>
  <CopyText>
    text to copy
  </CopyText>
</div>
```

After copy specified:

```js
<div style={{display: 'flex'}}>
  <CopyText textAfterCopy="Text has been copied!">
    text to copy
  </CopyText>
</div>
```