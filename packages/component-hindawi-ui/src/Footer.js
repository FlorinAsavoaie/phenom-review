import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { ActionLink, Row, Text } from '../'

const Footer = ({
  supportEmail,
  publisher,
  websiteLink,
  privacyLink,
  termsLink,
}) => (
  <Root
    alignItems="center"
    as="footer"
    data-test-id="footer-row-id"
    justify="center"
  >
    <ActionLink data-test-id="hindawi-redirect" to={websiteLink}>
      {publisher}
    </ActionLink>
    <ActionLink data-test-id="privacy-policy" ml={3} to={privacyLink}>
      Privacy Policy
    </ActionLink>
    <ActionLink data-test-id="terms-of-service" ml={3} to={termsLink}>
      Terms of Service
    </ActionLink>
    <Text display="flex" ml={4} secondary>
      Support:
      <ActionLink
        data-test-id="email-support"
        ml={1}
        to={`mailto:${supportEmail}`}
      >
        {supportEmail}
      </ActionLink>
    </Text>
  </Root>
)

export default Footer

const Root = styled(Row)`
  background-color: ${th('colorBackground')};
  border-top: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 10);

  z-index: ${th('zIndex.appBar')};
`
