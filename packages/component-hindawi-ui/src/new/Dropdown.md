A simple dropdown

```js
const options = [
  { id: 'opt-1', value: 'value-1', message: 'Value One' },
  { id: 'opt-2', value: 'value-2', message: 'Value Two' },
  { id: 'opt-3', value: 'value-3', message: 'Value Three' },
  { id: 'opt-4', value: 'value-4', message: 'A Very Very Long Label Here' },
]

;<Dropdown
  value={options[1]}
  onChange={() => alert('Option selected')}
  optionLabel="message"
  options={options}
/>
```
