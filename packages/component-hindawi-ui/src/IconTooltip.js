import React from 'react'
import PropTypes from 'prop-types'
import Tippy from '@tippy.js/react'
import { ThemeProvider, withTheme } from 'styled-components'

import { Icon } from '../'

const IconTooltip = ({
  icon,
  theme,
  primary,
  fontSize,
  interactive,
  content,
  ...rest
}) => (
  <Tippy
    arrow
    content={
      <InfoTooltip
        content={content}
        data-test-id="icon-tooltip"
        theme={theme}
        {...rest}
      />
    }
    interactive={interactive}
    placement="bottom"
    theme="light"
    trigger="click"
  >
    <span>
      <Icon fontSize={fontSize} icon={icon} primary={primary} {...rest} />
    </span>
  </Tippy>
)

const InfoTooltip = ({ theme, content }) => (
  <ThemeProvider theme={theme}>
    {typeof content === 'function' ? content() : content}
  </ThemeProvider>
)
IconTooltip.propTypes = {
  /** What icon to be used. */
  icon: PropTypes.string,
  /** Size of the icon. */
  fontSize: PropTypes.string,
  /** What content to be used in tooltip. */
  content: PropTypes.func,
  /** If true the content can be clicked (can be interacted with). */
  interactive: PropTypes.bool,
}

IconTooltip.defaultProps = {
  icon: 'tooltip',
  fontSize: '14px',
  content: () => {},
  interactive: false,
}

export default withTheme(IconTooltip)
