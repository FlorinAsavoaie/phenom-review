import React, { Fragment, useState, useEffect, useRef } from 'react'
import Calendar from 'react-calendar'
import moment from 'moment'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Icon, Item, Text } from '../'

function useComponentAccessibility({ initialValue }) {
  const [isCalendarOpen, setIsCalendarOpen] = useState(initialValue)
  const ref = useRef(null)

  const handleKeyDown = event => {
    if (event.key === 'Escape') {
      setIsCalendarOpen(false)
    }
  }

  const handleClickOutside = event => {
    if (!isCalendarOpen) return
    if (
      ref.current &&
      !ref.current.contains(event.target) &&
      !ref.current.nextSibling.contains(event.target)
    ) {
      setIsCalendarOpen(false)
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', handleKeyDown)
    document.addEventListener('click', handleClickOutside)
    return () => {
      document.removeEventListener('keydown', handleKeyDown)
      document.removeEventListener('click', handleClickOutside)
    }
  })

  return { ref, isCalendarOpen, setIsCalendarOpen }
}

const DatePicker = ({ value, onChange, disabled, minDate = new Date() }) => {
  const { ref, isCalendarOpen, setIsCalendarOpen } = useComponentAccessibility({
    initialValue: false,
  })
  const handleChange = day => {
    onChange(new Date(moment(day).format('YYYY-MM-DD')).toISOString())
    setIsCalendarOpen(false)
  }
  return (
    <Fragment>
      <CalendarButton
        data-test-id="calendar-button"
        disabled={disabled}
        onClick={() => setIsCalendarOpen(true)}
        ref={ref}
      >
        <Item data-test-id="date-field" flex={1}>
          <StyledText
            data-test-id="current-date"
            disabled={disabled}
            fontWeight={400}
          >
            {value && moment(value).format('YYYY-MM-DD')}
          </StyledText>
        </Item>

        <StyledIcon fontSize="18px" icon="calendar" isActive={!disabled} />
      </CalendarButton>
      {isCalendarOpen && (
        <StyledCalendar
          activeStartDate={new Date()}
          className="calendar-root"
          data-test-id="month-calendar"
          maxDetail="month"
          minDate={minDate}
          minDetail="month"
          next2Label={null}
          nextLabel={<Icon color="colorSecondary" icon="caretRight" />}
          onClickDay={handleChange}
          prev2Label={null}
          prevLabel={<Icon color="colorSecondary" icon="caretLeft" />}
          showFixedNumberOfWeeks
          value={new Date(value) || new Date()}
        />
      )}
    </Fragment>
  )
}

export default DatePicker

const CalendarButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  border: ${th('borderWidth')} solid ${th('colorBorder')};

  :active,
  :focus {
    border-color: ${th('contrastGrayColor')};
  }

  :disabled {
    opacity: 1;
    cursor: default;
  }
`
const StyledIcon = styled(Icon)`
  color: ${({ isActive }) =>
    isActive ? th('actionSecondaryColor') : th('colorBorder')};
  cursor: ${({ isActive }) => (isActive ? 'pointer' : 'default')};
`
const StyledText = styled(Text)`
  color: ${th('textSecondaryColor')};
  opacity: ${({ disabled }) => (disabled ? 0.3 : 1)};
`

const StyledCalendar = styled(Calendar)`
  &.calendar-root {
    box-sizing: content-box;
    height: 250px;
    width: 250px;
    border: none;
    box-shadow: 0px 0px 2px 2px ${th('colorBorder')};
    position: absolute;
    margin-top: 55px;
    margin-left: 2px;
    z-index: ${th('zIndex.datePicker')};
  }
  /* Header */
  .react-calendar__navigation {
    background-color: ${th('colorBackgroundHue2')};
    height: 40px;
    margin: 0;
    padding: 0;
    button[disabled] {
      background-color: ${th('colorBackgroundHue2')};
    }
  }

  .react-calendar__navigation__label {
    color: ${th('colorText')};
    font-size: ${th('fontSizeBase')};
    font-weight: ${th('fontWeightSemibold')};
  }

  /* Weekdays */
  .react-calendar__month-view__weekdays {
    align-items: center;
    background-color: ${th('colorBackgroundHue2')};
    font-size: ${th('fontSizeBaseMedium')};
    font-weight: ${th('fontWeightNormal')};
    height: 20px;
    text-transform: none;
  }

  /* Days */
  .react-calendar__tile {
    color: ${th('colorText')};
    height: 31.68px;
    z-index: 1;
  }

  .react-calendar__month-view__days {
    height: 190px;
    background: ${th('colorBackgroundHue2')};
    position: relative;
  }

  .react-calendar__month-view__days::before {
    content: ' ';
    position: absolute;
    width: 249px;
    height: 190px;
    right: 0;
    left: 0;
    background: ${th('button.primary')};
  }

  .react-calendar__tile.react-calendar__month-view__days__day {
    background-color: ${th('colorBackgroundHue')};
    border-bottom: 1px solid ${th('colorBorder')};
    border-right: 1px solid ${th('colorBorder')};
    padding: 2px;

    :nth-child(-n + 7) {
      border-top: 0;
    }

    :nth-child(7n) {
      border-right: 0;
    }

    :nth-last-child(-n + 7) {
      border-bottom: 0;
    }

    :nth-child(7n + 1) {
      border-left: 0;
    }

    :disabled {
      color: ${th('colorFurnitureHue')};
    }
  }
  .react-calendar__tile.react-calendar__tile--now {
    background-clip: ${({ value }) => !value && 'content-box'};
    background-color: ${({ value }) => !value && `${th('colorBackgroundHue')}`};
  }

  .react-calendar__tile--active {
    background-clip: content-box;
    background-color: ${th('colorBackgroundHue')};
  }
  .react-calendar__tile.react-calendar__month-view__days__day:enabled {
    :focus,
    :hover,
    :active,
    :visited {
      background-clip: content-box;
      background-color: ${th('colorBackgroundHue')};
    }
  }

  /* Other */
  abbr[title] {
    text-decoration: none;
  }
`
