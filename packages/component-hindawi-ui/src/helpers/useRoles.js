const useRoles = ({
  hasTriageEditor,
  triageEditorLabel,
  academicEditorLabel,
  hasFigureheadEditor,
  figureheadEditorLabel,
}) => {
  const roles = {
    academicEditor: academicEditorLabel,
    editorialAssistant: 'Editorial Assistant',
    triageEditorSI: 'Lead Guest Editor',
    academicEditorSI: 'Guest Editor',
  }
  if (hasTriageEditor) roles.triageEditor = triageEditorLabel
  if (hasFigureheadEditor) roles.figureheadEditor = figureheadEditorLabel
  return {
    roles: Object.entries(roles).reduce(
      (acc, el) => [
        ...acc,
        {
          value: el[0],
          label: el[1],
        },
      ],
      [],
    ),
  }
}

export default useRoles
