import React, { Fragment } from 'react'
import { isNumber } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H3, Spinner } from '@pubsweet/ui'
import { compose, withState, withHandlers } from 'recompose'

import { withFetching } from './helpers'
import AuthorCardEdit from './AuthorCardEdit'
import { Row, Tag, Icon, Item, PersonInfo, withCountries } from '../'

const Author = ({
  item,
  index,
  isFetching,
  toggleEdit,
  countryLabel,
  deleteAuthor,
  isSubmitting,
  isEditingForm,
  isCorresponding,
}) => (
  <Fragment>
    <Row justify="space-between" mb={1}>
      <Item justify="flex-start">
        <H3>{isNumber(index) ? `#${index + 1} Author` : 'Author'}</H3>
        <Fragment>
          {isSubmitting && <Tag ml={1}>SUBMITTING</Tag>}
          {isCorresponding && <Tag ml={1}>CORRESPONDING</Tag>}
        </Fragment>
      </Item>
      {isFetching ? (
        <StyledSpinner>
          <Spinner />
        </StyledSpinner>
      ) : (
        !isEditingForm && (
          <Fragment>
            {!isSubmitting && (
              <Icon
                color="colorSecondary"
                data-test-id={`delete-author-${index}`}
                fontSize="16px"
                icon="delete"
                mr={3}
                onClick={deleteAuthor}
              />
            )}
            <Icon
              color="colorSecondary"
              data-test-id={`edit-author-${index}`}
              fontSize="16px"
              icon="edit"
              onClick={toggleEdit}
            />
          </Fragment>
        )
      )}
    </Row>
    <PersonInfo person={item} />
  </Fragment>
)

const AuthorCard = ({
  item,
  index,
  onEdit,
  editMode,
  isFetching,
  cancelEdit,
  saveAuthor,
  toggleEdit,
  isSubmitting,
  countryLabel,
  deleteAuthor,
  fetchingError,
  isCorresponding,
  setCorresponding,
  isEditingForm,
  dragHandle,
  authorsLength,
  isAuthorEmailEditable,
}) => (
  <Root>
    {!isEditingForm && dragHandle}
    <AuthorContainer>
      {editMode ? (
        <AuthorCardEdit
          cancelEdit={cancelEdit}
          deleteAuthor={deleteAuthor(item, index)}
          fetchingError={fetchingError}
          index={isEditingForm ? authorsLength - 1 : index}
          isAuthorEmailEditable={isAuthorEmailEditable}
          isFetching={isFetching}
          item={item}
          onEdit={onEdit}
          saveAuthor={saveAuthor}
          setCorresponding={setCorresponding}
        />
      ) : (
        <Author
          countryLabel={countryLabel}
          deleteAuthor={deleteAuthor(item, index)}
          index={isEditingForm ? index - 1 : index}
          isCorresponding={isCorresponding}
          isEditingForm={isEditingForm}
          isFetching={isFetching}
          isSubmitting={isSubmitting}
          item={item}
          toggleEdit={toggleEdit}
        />
      )}
    </AuthorContainer>
  </Root>
)

export default compose(
  withFetching,
  withCountries,
  withState(
    'editMode',
    'setEditMode',
    ({ isEditingForm = false }) => isEditingForm,
  ),
  withHandlers({
    toggleEdit: ({ index, editMode, setEditMode, onEdit }) => () => {
      setEditMode(!editMode)
      if (typeof onEdit === 'function') {
        onEdit(index)
      }
    },
    cancelEdit: ({ index, setEditMode, clearError, onCancelEdit }) => () => {
      setEditMode(false)
      clearError()
      if (typeof onCancelEdit === 'function') {
        onCancelEdit(index)
      }
    },
    deleteAuthor: ({ onDeleteAuthor, ...props }) => (author, index) => () => {
      if (typeof onDeleteAuthor === 'function') {
        onDeleteAuthor(author, { ...props, index })
      }
    },
    saveAuthor: ({ id, onSaveAuthor, ...props }) => index => values => {
      if (typeof onSaveAuthor === 'function') {
        onSaveAuthor({ id, ...values }, { ...props, index })
      }
    },
  }),
)(AuthorCard)

// #region styles
const AuthorContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4);
`

const Root = styled.div`
  align-items: center;
  background-color: ${props =>
    props.isOver ? th('colorFurniture') : th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  justify-content: flex-start;
  margin-bottom: calc(${th('gridUnit')} * 2);
  position: relative;
`

const StyledSpinner = styled.div`
  position: absolute;
  right: calc(${th('gridUnit')} * 4);
  top: ${th('gridUnit')};
`
// #endregion
