import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Button, Spinner } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'

import { Icon, Row, Text, withFetching } from '../../'

const MultiAction = ({
  title,
  content,
  onClose,
  subtitle,
  onConfirm,
  isFetching,
  cancelText,
  confirmText,
  fetchingError,
  renderContent,
}) => (
  <Root>
    <Icon
      bold
      color="colorFurnitureHue"
      fontSize="24px"
      icon="remove1"
      onClick={onClose}
      right={8}
      secondary
      top={12}
    />
    <H2>{title}</H2>
    {subtitle && (
      <Text align="center" secondary>
        {subtitle}
      </Text>
    )}
    {renderContent()}
    {fetchingError && (
      <Text align="center" error>
        {fetchingError}
      </Text>
    )}
    <Row justify={isFetching ? 'center' : 'space-between'} mt={6}>
      {isFetching ? (
        <Spinner size={6} />
      ) : (
        <RowAlignRight>
          <Button
            data-test-id="modal-confirm"
            onClick={onConfirm}
            primary
            width="auto"
            style={{ marginLeft: '12px' }}
          >
            {confirmText}
          </Button>
          <Button data-test-id="modal-cancel" onClick={onClose} width="auto">
            {cancelText}
          </Button>
        </RowAlignRight>
      )}
    </Row>
  </Root>
)

MultiAction.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** The text you want to see on the button when someone cancel the report. */
  cancelText: PropTypes.string,
  /** Callback function fired when cancel confirmation card. */
  onCancel: PropTypes.func, // eslint-disable-line
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** When is true will show a spinner.  */
  onClose: PropTypes.func,
  /** Callback function fired when you want to close the card. */
  isFetching: PropTypes.bool,
  /** The component you want to show on the card. */
  content: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object,
    PropTypes.string,
  ]),
}
MultiAction.defaultProps = {
  title: undefined,
  subtitle: undefined,
  confirmText: 'OK',
  cancelText: 'Cancel',
  onCancel: undefined,
  onConfirm: undefined,
  onClose: undefined,
  isFetching: undefined,
  content: undefined,
}

export default compose(
  withFetching,
  withHandlers({
    onConfirm: ({ onConfirm, ...props }) => () => {
      if (typeof onConfirm === 'function') {
        onConfirm(props)
      } else {
        props.hideModal()
      }
    },
    onClose: ({ onCancel, ...props }) => () => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
    renderContent: ({ content, ...props }) => () => {
      if (!content) return null
      if (typeof content === 'object') {
        return content
      } else if (typeof content === 'function') {
        return content(props)
      }
      return <Text dangerouslySetInnerHTML={{ __html: content }} mb={2} />
    },
  }),
  setDisplayName('MultiActionModal'),
)(MultiAction)

// #region styles
const Root = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 10);
  padding-bottom: calc(${th('gridUnit')} * 6);
  width: calc(${th('gridUnit')} * 130);

  ${H2} {
    margin: 0 0 calc(${th('gridUnit')} * 2) 0;
    text-align: center;
  }
`

const RowAlignRight = styled.div`
  display: flex;
  flex-direction: row-reverse;
  width: calc(${th('gridUnit')} * 130);
`

// #endregion
