Open a confirmation modal by clicking on an element

```jsx
import ActionLink from '../ActionLink'
;<OpenModal
  onConfirm={props => console.log('confirm', props)}
  onCancel={props => console.log('cancel', props)}
  title="Are you sure?"
  confirmText="Delete"
  modalKey="123"
>
  {showModal => (
    <ActionLink icon="trash" onClick={showModal}>
      Delete
    </ActionLink>
  )}
</OpenModal>
```

Pass a custon component as the modal content.

```jsx
import Tag from '../Tag'
const Custom = `<div>inside the modal</div>`

;<OpenModal
  onConfirm={props => console.log('confirm', props)}
  onCancel={props => console.log('cancel', props)}
  title="Are you sure?"
  confirmText="Delete"
  content={Custom}
  modalKey="123"
>
  {showModal => <Tag onClick={showModal}>CUSTOM</Tag>}
</OpenModal>
```

Open a single action modal.

```jsx
import ActionLink from '../ActionLink'
;<OpenModal
  onConfirm={props => console.log('confirm', props)}
  title="Are you sure?"
  confirmText="I am pretty sure"
  modalKey="1234"
  single
>
  {showModal => (
    <ActionLink icon="eye" onClick={showModal}>
      See the truth
    </ActionLink>
  )}
</OpenModal>
```
