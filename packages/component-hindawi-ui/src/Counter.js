import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Text } from '@hindawi/ui'

const Counter = ({ number }) => <CounterWrapper>{number}</CounterWrapper>

const CounterWrapper = styled(Text)`
  border: 1px solid ${th('grey50')};
  border-radius: 3px;
  font-weight: ${th('fontWeightBold')};
  font-size: ${th('counterTextSize')};
  line-height: ${th('counterTextLineHeight')};
  padding: 2px 4px;
`

export default Counter
