type getFnT = (cookieKey: string) => string | null

type CookiePublicApi = {
  get: getFnT
  set?: () => void // this will be implemented later
}

type CookieApiFn<Response> = () => Response

export const cookieManager: CookieApiFn<CookiePublicApi> = () => {
  const get: getFnT = cookieKey => {
    const cookies = document.cookie.split(';')

    if (!cookies) return null

    const regex = RegExp(cookieKey)
    const eaDashboardCookie = cookies.find(cookie => regex.test(cookie))

    if (!eaDashboardCookie) return null

    return eaDashboardCookie.split('=')[1]
  }

  return {
    get,
  }
}
