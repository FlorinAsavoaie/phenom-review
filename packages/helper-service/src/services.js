const querystring = require('querystring')

const getBaseUrl = req => `${req.protocol}://${req.get('host')}`

const createUrl = (baseUrl, slug, queryParams = null) => {
  if (
    process.env.KEYCLOAK_SERVER_URL &&
    queryParams &&
    queryParams.confirmationToken
  ) {
    const { createRegistrationURL } = require('component-sso')
    return createRegistrationURL(
      `${baseUrl}${slug}?${querystring.encode(queryParams)}`,
    )
  }
  return !queryParams
    ? `${baseUrl}${slug}`
    : `${baseUrl}${slug}?${querystring.encode(queryParams)}`
}

const generateHash = () =>
  Array.from({ length: 4 }, () =>
    Math.random()
      .toString(36)
      .slice(4),
  ).join('')

module.exports = {
  getBaseUrl,
  createUrl,
  generateHash,
}
