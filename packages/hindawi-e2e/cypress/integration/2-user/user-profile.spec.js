import { AUTHOR1 } from '../../fixtures/models/users'
import {
  navigateToUserProfile,
  changePassword,
  editProfile,
} from '../../support/user'
import { registerGQLInterceptor } from '../../support/interceptors'

describe('User Profile', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  it('user can change password', () => {
    cy.loginKeycloak(AUTHOR1.email)
    cy.visit('/')
    navigateToUserProfile()
    changePassword('Password1!', 'Password1!')
  })

  it('user can edit profile details', () => {
    cy.loginKeycloak(AUTHOR1.email)
    cy.visit('/')
    navigateToUserProfile()
    editProfile({
      firstName: 'AuthorTest changed',
      lastName: 'Author changed',
      title: 'Dr',
      country: 'Mexico',
      affiliation: 'New Affiliation',
    })
  })

  it('user sees ORCID ID on page', () => {
    cy.loginKeycloak(AUTHOR1.email)
    cy.visit('/')
    navigateToUserProfile()
    cy.get('[data-test-id="orcid-action"]').contains('Link')
  })
})
