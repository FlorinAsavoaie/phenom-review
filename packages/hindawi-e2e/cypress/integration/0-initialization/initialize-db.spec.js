import { stopCypressIfTestFailed } from '../../utils'

describe('Initialize database', () => {
  let dbConfig, publisher

  before(() => {
    dbConfig = Cypress.env('dbConfig')
    publisher = Cypress.env('publisher')
  })

  afterEach(function() {
    // if initialising database fails there is no reason to go on with the tests
    stopCypressIfTestFailed(this.currentTest)
  })

  it('clean, seed, add users, add journals', () => {
    cy.exec('yarn cypress:cleandb', {
      timeout: 500000,
      env: {
        DATABASE: dbConfig.database,
        DB_USER: dbConfig.user,
        DB_PASS: dbConfig.password,
        DB_HOST: dbConfig.host,
      },
    })

    cy.exec('yarn cypress:seeddb', {
      timeout: 500000,
      env: {
        DATABASE: dbConfig.database,
        DB_USER: dbConfig.user,
        DB_PASS: dbConfig.password,
        DB_HOST: dbConfig.host,
      },
    })

    cy.task('addUsers')
    cy.task('addJournals', publisher)
  })
})
