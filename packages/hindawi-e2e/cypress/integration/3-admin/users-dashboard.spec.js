import { ADMIN } from '../../fixtures/models/users'
import {
  addUser,
  editUser,
  navigateToUsersDashboard,
} from '../../support/admin'
import { generateRandomEmail } from '../../utils'

describe('Users Dashboard', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
  })

  it('admin can add admin, author and edit user profile', () => {
    const newAdminEmail = generateRandomEmail()
    const newAuthorEmail = generateRandomEmail()

    cy.loginKeycloak(ADMIN.email)
    navigateToUsersDashboard()

    addUser({
      firstName: 'testAdmin100',
      lastName: 'testAdmin100',
      title: 'Mr',
      email: newAdminEmail,
      country: 'Andorra',
      affiliation: 'Automatic checks',
      isAdmin: true,
    })

    addUser({
      firstName: 'AuthorTest100',
      lastName: 'Author100',
      title: 'Dr',
      email: newAuthorEmail,
      country: 'Canada',
      affiliation: 'Automatic checks',
    })

    cy.reload()

    editUser(newAuthorEmail, {
      firstName: 'AuthorTest101',
      lastName: 'Author101',
      title: 'Mr',
      country: 'Romania',
      affiliation: 'Automatic checks',
    })
  })
})
