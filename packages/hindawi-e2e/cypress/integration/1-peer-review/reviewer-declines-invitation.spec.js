import { ASTROLOGY } from '../../fixtures/models/journals'
import { EA, AE, REVIEWER1 } from '../../fixtures/models/users'
import {
  inviteReviewer,
  acceptInvitationAsAE,
  navigateToManuscript,
  inviteAcademicEditor,
  declineInvitationAsReviewer,
} from '../../support/manuscript'
import { registerGQLInterceptor } from '../../support/interceptors'
import { stopCypressIfTestFailed } from '../../utils'

describe('Reviewer declines invitation', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  afterEach(function() {
    stopCypressIfTestFailed(this.currentTest)
  })

  it('1. Create Submission via API', () => {
    cy.createSubmission({ journalName: ASTROLOGY.name })
  })

  it('2. EA invites Academic Editor', () => {
    cy.loginKeycloak(EA.email)
    navigateToManuscript()
    inviteAcademicEditor(AE.email)
  })

  it('3. AE accepts invitation and invites reviewer', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    acceptInvitationAsAE()
    inviteReviewer(REVIEWER1)
  })

  it('4. Reviewer declines invitation', () => {
    cy.loginKeycloak(REVIEWER1.email)
    navigateToManuscript()
    declineInvitationAsReviewer()
  })

  it('5. Academic Editor should see that a Reviewer Declined', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    cy.contains('DECLINED')
  })
})
