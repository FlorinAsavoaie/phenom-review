import {
  inviteReviewer,
  acceptInvitationAsAE,
  acceptInvitationAsReviewer,
  navigateToManuscript,
  submitReview,
  makeDecisionForMajorRevision,
  makeDecisionToPublish,
  inviteAcademicEditor,
} from '../../support/manuscript'
import {
  expandSubmitRevisionSection,
  updateManuscriptTitle,
  updateManuscriptAbstract,
  deleteMainManuscript,
  uploadMainManuscript,
  uploadCoverLetter,
  uploadSupplementalFile,
  addComment,
  confirmRevision,
} from '../../support/manuscript/revision'
import { registerGQLInterceptor } from '../../support/interceptors'
import { stopCypressIfTestFailed } from '../../utils'

import { ASTROLOGY } from '../../fixtures/models/journals'
import { EA, AE, AUTHOR1, REVIEWER1 } from '../../fixtures/models/users'

describe('Single Tier Main Flow', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  afterEach(function() {
    stopCypressIfTestFailed(this.currentTest)
  })

  it('1. Create Submission via API', () => {
    cy.createSubmission({ journalName: ASTROLOGY.name })
  })

  it('2. EA invites Academic Editor', () => {
    cy.loginKeycloak(EA.email)
    navigateToManuscript()
    inviteAcademicEditor(AE.email)
  })

  it('3. AE accepts invitation and invites reviewer', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    acceptInvitationAsAE()
    inviteReviewer(REVIEWER1)
  })

  it('4. Reviewer accepts invitation and submits review', () => {
    cy.loginKeycloak(REVIEWER1.email)
    navigateToManuscript()
    acceptInvitationAsReviewer()
    submitReview('This is rubbish', 'Major Revision')
  })

  it('5. AE makes a Major Revision decision', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    makeDecisionForMajorRevision('This looks bad')
  })

  it('6. Author submits revision', () => {
    cy.loginKeycloak(AUTHOR1.email)
    navigateToManuscript()
    expandSubmitRevisionSection()
    updateManuscriptTitle('[Auto] manuscript v2')
    updateManuscriptAbstract('This is version 2')
    deleteMainManuscript('manuscript.txt')
    uploadMainManuscript('manuscript.txt')
    uploadCoverLetter('cover-letter.txt')
    uploadSupplementalFile('supplemental.txt')
    addComment("Here's another revision")
    confirmRevision()
  })

  it('7. AE invites again reviewer', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    inviteReviewer(REVIEWER1)
  })

  it('8. Reviewer accepts invitation and submits review for publish', () => {
    cy.loginKeycloak(REVIEWER1.email)
    navigateToManuscript()
    acceptInvitationAsReviewer()
    submitReview('Awesome', 'Publish')
  })

  it('9. AE makes decision to publish', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    makeDecisionToPublish()
  })
})
