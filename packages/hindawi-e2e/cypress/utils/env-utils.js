export const updateEnvManuscript = () => {
  cy.url().then(url => {
    const manuscriptId = url.substring(url.lastIndexOf('/') + 1)
    Cypress.env('manuscriptId', manuscriptId)
  })
}

export const getCurrentManuscriptId = () => Cypress.env('manuscriptId')

export const setAccessToken = token => Cypress.env('access_token', token)

export const getAccessToken = () => Cypress.env('access_token')

export const clearAccessToken = () => Cypress.env('access_token', undefined)

export const getDefaultPassword = () => Cypress.env('password')

export const getKeycloakConfig = () => ({
  authBaseUrl: Cypress.env('root'),
  realm: Cypress.env('realm'),
  client_id: Cypress.env('client_id'),
})

export const getBaseUrl = () => Cypress.config('baseUrl')

export const getGatewayUrl = () => Cypress.config('gatewayUrl')
