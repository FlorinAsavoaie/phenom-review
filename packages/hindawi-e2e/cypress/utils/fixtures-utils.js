export const getRelativeFilePath = fileName => `/files/${fileName}`

export const getFullEmail = fixtureEmail =>
  `${Cypress.env('emailPrefix') + fixtureEmail}`
