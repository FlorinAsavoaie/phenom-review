import { getGatewayUrl } from '../../utils/env-utils'

// this method should be called in beforeEach of tests where we wait on some gql operations.
// see: https://docs.cypress.io/api/commands/intercept
export const registerGQLInterceptor = () => {
  const mutations = [
    'createDraftManuscript',
    'updateDraftManuscript',
    'addAuthorToManuscript',
    'submitManuscript',
    'requestRevision',
    'updateDraftRevision',
    'submitRevision',
    'makeDecisionToPublish',
    'makeDecisionToReject',
    'inviteAcademicEditor',
    'makeRecommendationToPublish',
    'assignEditorialRole',
    'inviteReviewer',
    'submitReview',
    'updateDraftReview',
    'updateUser',
    'acceptAcademicEditorInvitation',
  ]
  const queries = ['getCommunicationPackage', 'getSubmission']

  // We are intercepting explicit 'application/json' POST requests as currently cy.intercept() breaks multipart form submissions
  // see: https://github.com/cypress-io/cypress/issues/14527
  cy.intercept(
    {
      method: 'POST',
      url: getGatewayUrl(),
      headers: {
        'content-type': 'application/json',
      },
    },
    req => {
      mutations.forEach(mutation => aliasMutation(req, mutation))
      queries.forEach(query => aliasQuery(req, query))
    },
  )
}

const aliasQuery = (req, operationName) => {
  if (hasOperationName(req, operationName)) {
    req.alias = `gql-${operationName}-query`
  }
}

const aliasMutation = (req, operationName) => {
  if (hasOperationName(req, operationName)) {
    req.alias = `gql-${operationName}-mutation`
  }
}

const hasOperationName = (req, operationName) => {
  const { body } = req

  return (
    Object.prototype.hasOwnProperty.call(body, 'operationName') &&
    body.operationName === operationName
  )
}
