import { ADMIN, AUTHOR1 } from '../../fixtures/models/users'
import { DEFAULT_SUBMISSION } from '../../fixtures/models/submissions'

export const createSubmission = (options = {}) => {
  const {
    journalName,
    articleTypeName,
    author,
    abstract,
    title,
    conflictOfInterest,
    dataAvailability,
    fundingStatement,
    manuscriptFileName,
    manuscriptFileContent,
  } = { ...DEFAULT_SUBMISSION, ...options }

  cy.loginKeycloak(ADMIN.email)
    .getActiveJournals()
    .then(activeJournals => {
      const journal = activeJournals.find(j => j.name === journalName)
      const articleType = journal.articleTypes.find(
        a => a.name === articleTypeName,
      )
      Cypress.env('journal', journal)
      Cypress.env('articleType', articleType)
    })
    .logoutKeycloak()

  cy.loginKeycloak(AUTHOR1.email)
    .createDraftManuscript()
    .then(draftManuscript => {
      Cypress.env('manuscriptId', draftManuscript.id)
      Cypress.env('submissionId', draftManuscript.submissionId)
    })
    .uploadManuscriptFile({
      fileName: manuscriptFileName,
      fileContent: manuscriptFileContent,
    })
    .updateDraftManuscript({
      author,
      abstract,
      title,
      conflictOfInterest,
      dataAvailability,
      fundingStatement,
      manuscriptFileName,
    })
    .submitManuscript()
    .logoutKeycloak()

  cy.loginKeycloak(ADMIN.email)
    .approveEQS()
    .logoutKeycloak()
}
