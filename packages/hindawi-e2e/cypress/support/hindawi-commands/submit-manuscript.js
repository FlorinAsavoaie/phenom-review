import { getAccessToken } from '../../utils/env-utils'

export const submitManuscript = () => {
  const manuscriptId = Cypress.env('manuscriptId')

  const submitManuscript = `mutation{
    submitManuscript(manuscriptId:"${manuscriptId}")
  }`

  cy.request({
    method: 'POST',
    url: `/graphql`,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: { query: submitManuscript },
    failOnStatusCode: false,
  }).then(response => {
    if (response.body.errors) {
      throw new Error(`Submit Manuscript failed: ${JSON.stringify(response)}`)
    }
  })
}
