import { getAccessToken } from '../../utils/env-utils'

export const createDraftManuscript = () => {
  const journal = Cypress.env('journal')
  const createDraftManuscriptQuery = `mutation{
    createDraftManuscript (input: {journalId: "${journal.id}"})
    {
      id, 
      submissionId
    }
  }`

  cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: { query: createDraftManuscriptQuery },
    failOnStatusCode: false,
  }).then(response => {
    if (response.body.errors) {
      throw new Error(
        `Create Draft Manuscript failed: ${JSON.stringify(response)}`,
      )
    }

    return response.body.data.createDraftManuscript
  })
}
