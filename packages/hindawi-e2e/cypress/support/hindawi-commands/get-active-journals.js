import { getAccessToken } from '../../utils/env-utils'

export const getActiveJournals = () => {
  const getActiveJournalsQuery = `query{
    getActiveJournals {
      id
      name
      eissn
      pissn
      articleTypes { 
        id
        name
      }
    }
  }`

  cy.request({
    method: 'POST',
    url: '/graphql',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: { query: getActiveJournalsQuery },
    failOnStatusCode: false,
  }).then(response => {
    if (response.body.errors) {
      throw new Error(`Get Active Journals failed: ${JSON.stringify(response)}`)
    }

    return response.body.data.getActiveJournals
  })
}
