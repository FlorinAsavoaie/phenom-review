export const navigateToAdminDashboard = () => {
  cy.visit('/')
  cy.get('[data-test-id="admin-menu-button"]').click()
  cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
}

export const navigateToUsersDashboard = () => {
  navigateToAdminDashboard()
  cy.contains('Users').click()
}

export const navigateToJournalsDashboard = () => {
  navigateToAdminDashboard()
  cy.contains('Journals').click()
}

export const navigateToJournalDetailsPage = journalCode => {
  navigateToJournalsDashboard()
  cy.contains('[data-test-id="dashboard-list-items"] div', journalCode).click()
}
