export const addUser = ({
  firstName,
  lastName,
  title,
  email,
  country,
  affiliation,
  isAdmin,
  isRIPE,
}) => {
  clickAddUserBtn()
  addFirstName(firstName)
  addLastName(lastName)
  addTitle(title)
  addEmail(email)
  addCountry(country)
  addAffiliation(affiliation)
  if (isAdmin) assignUserAsAdmin()
  if (isRIPE) assignUserAsRIPE()
  confirmAddUser()
}

export const editUser = (
  email,
  { firstName, lastName, title, country, affiliation, isAdmin, isRIPE },
) => {
  startEditUser(email)
  addFirstName(firstName)
  addLastName(lastName)
  addTitle(title)
  addCountry(country)
  addAffiliation(affiliation)
  isAdmin ? assignUserAsAdmin() : unassignUserAsAdmin()
  isRIPE ? assignUserAsRIPE() : unassignUserAsRIPE()
  confirmAddUser()
}

const clickAddUserBtn = () => {
  cy.get('[data-test-id="add-user-btn"]').click()
}

const addFirstName = firstName => {
  cy.get('[data-test-id="first-name-input"]')
    .clear()
    .type(firstName)
}

const addLastName = lastName => {
  cy.get('[data-test-id="last-name-input"]')
    .clear()
    .type(lastName)
}

const addTitle = title => {
  cy.get('[data-test-id="title-input-filter"]')
    .get('[data-test-id="title-input-filter"]')
    .click()
    .get('[role="option"]')
    .contains(title)
    .click()
}

const addEmail = email => {
  cy.get('[data-test-id="email-input"]')
    .clear()
    .type(email)
}

const addCountry = country => {
  cy.get('[data-test-id="country"]')
    .get('[data-test-id="country-dropdown"]')
    .clear()

  cy.get('[data-test-id="country-dropdown"]')
    .get('[role="option"]')
    .contains(country)
    .click()
}

const addAffiliation = affiliation => {
  cy.get('[data-test-id="affiliation-author"] input')
    .clear()
    .type(affiliation)
}

const assignUserAsAdmin = () => {
  cy.get('#ps-modal-root input[type="checkbox"]')
    .eq(0)
    .check({ force: true })
}
const unassignUserAsAdmin = () => {
  cy.get('#ps-modal-root input[type="checkbox"]')
    .eq(0)
    .uncheck({ force: true })
}

const assignUserAsRIPE = () => {
  cy.get('#ps-modal-root input[type="checkbox"]')
    .eq(1)
    .check({ force: true })
}

const unassignUserAsRIPE = () => {
  cy.get('#ps-modal-root input[type="checkbox"]')
    .eq(1)
    .uncheck({ force: true })
}

const confirmAddUser = () => {
  cy.get('[data-test-id="modal-confirm"]').click()
}

const startEditUser = email => {
  cy.contains('tr', email)
    .find('.icn_icn_moreDefault')
    .click({ force: true })

  cy.contains('tr', email)
    .find('[data-test-id="dropdown-option"]')
    .contains('Edit User')
    .click()
}
