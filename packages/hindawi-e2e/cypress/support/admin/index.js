export {
  navigateToAdminDashboard,
  navigateToUsersDashboard,
  navigateToJournalsDashboard,
  navigateToJournalDetailsPage,
} from './common'

export { addUser, editUser } from './users-dashboard'
export { addJournal, editJournal } from './journals-dashboard'
export { assignEditorialRole } from './journal-details'
