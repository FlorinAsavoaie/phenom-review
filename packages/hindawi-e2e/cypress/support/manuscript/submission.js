import { getRelativeFilePath } from '../../utils'

// You will find that submitManuscript waits for different graphql mutations on purpose to ensure
// that no concurrency issues appear during a specific test run.
// This is currently an ongoing issue with:
// 1. the wizard doesn't disable the 'NEXT STEP' if there are current grapql mutations happening in the background
// 2. backend calls of graphql mutations are not properly treated to avoid concurrency issues
export const beginSubmission = () => {
  cy.get('[data-test-id="manuscript-submit"]')
    .should('be.visible')
    .click()
  cy.wait([
    '@gql-createDraftManuscript-mutation',
    '@gql-updateDraftManuscript-mutation',
  ])
}

export const selectJournal = journalName => {
  cy.get('[data-test-id="journal-select"]')
    .click()
    .type(journalName)
  cy.findByText(journalName).click()
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const clickNextBtn = () => {
  cy.get('button')
    .contains('NEXT STEP')
    .click()
}

export const agreeTermsAndConditions = () => {
  cy.get('[data-test-id="agree-checkbox"] input').click({ force: true })
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const addManuscriptTitle = title => {
  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type(title)
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const selectManuscriptType = type => {
  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains(type).click()
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const addManuscriptAbstract = text => {
  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type(text)
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const addAuthor = ({
  email,
  firstName,
  lastName,
  affiliation,
  country,
}) => {
  cy.get('[data-test-id="submission-add-author"] button').click()
  cy.wait(['@gql-updateDraftManuscript-mutation'])

  cy.get('[data-test-id="email-author"]').type(email)
  cy.get('[data-test-id="givenNames-author"]').type(firstName)
  cy.get('[data-test-id="surname-author"]').type(lastName)
  cy.get('[data-test-id="affiliation-author"]').type(affiliation)
  cy.get('[data-test-id="country-author"]').click()
  cy.contains(country).click()

  cy.get('.icn_icn_save').click({ force: true })
  cy.wait([
    '@gql-addAuthorToManuscript-mutation',
    '@gql-updateDraftManuscript-mutation',
  ])

  cy.get('[data-test-id="sortable-item-unsaved-author"]').should('not.exist')
}

export const declareConflictOfInterest = (isInCOI, reason) => {
  const coiCheckboxText = isInCOI ? 'Yes' : 'No'

  cy.findByText(coiCheckboxText).click()
  cy.wait(['@gql-updateDraftManuscript-mutation'])

  if (isInCOI) {
    cy.get('textarea[name="meta.conflictOfInterest"]').type(reason)
    cy.wait(['@gql-updateDraftManuscript-mutation'])
  }
}

export const addDataAvailabilityStatement = text => {
  cy.get('textarea[name="meta.dataAvailability"]').type(text)
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const addFundingStatement = text => {
  cy.get('textarea[name="meta.fundingStatement"]').type(text)
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const uploadMainManuscript = fileName => {
  cy.get('[data-test-id="add-file-manuscript"] input[type="file"]').attachFile({
    filePath: getRelativeFilePath(fileName),
  })
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const uploadCoverLetter = fileName => {
  cy.get('[data-test-id="add-file-coverLetter"] input[type="file"]').attachFile(
    {
      filePath: getRelativeFilePath(fileName),
    },
  )
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const uploadSupplementalFile = fileName => {
  cy.get(
    '[data-test-id="add-file-supplementary"] input[type="file"]',
  ).attachFile({
    filePath: getRelativeFilePath(fileName),
  })
  cy.wait(['@gql-updateDraftManuscript-mutation'])
}

export const confirmSubmission = () => {
  cy.contains('SUBMIT MANUSCRIPT').click()
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.wait(['@gql-submitManuscript-mutation'])

  cy.contains('Thank You for Submitting Your Manuscript').should('be.visible')
}
