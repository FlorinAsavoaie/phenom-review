export const triageEditorMakesDecision = decision => {
  switch (decision) {
    case 'Publish': {
      throw new Error('not implemented')
    }
    default: {
      cy.get('div[label="Your Editorial Decision"]')
        .should('be.visible')
        .click()

      cy.get('div[data-test-id="decision-dropdown"]')
        .should('be.visible')
        .click()

      cy.get('div[role="listbox"]')
        .parent()
        .contains(decision)
        .click()

      cy.get('[data-test-id="triage-editor-decision-message"]')
        .should('be.visible')
        .type('loreimpsum dolor sit ameloreimpsum dolores.')

      cy.get('[data-test-id="submit-triage-editor-decision"]')
        .should('be.visible')
        .click({ force: true })
    }
  }
}
