import { editTemplateAndSend } from './editTemplateAndSend'
import { checkStatus } from '../../../support/manuscript'
import { statuses } from '../../../fixtures/models/statuses'
import { allUsers } from '../../../fixtures/models/users'

const triageEditor = allUsers.CE
export const triageEditorInvitesAcademicEditor = academicEditor => {
  cy.logoutKeycloak()
  cy.loginKeycloak(triageEditor.email)
  cy.visit('/')

  cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    .first()
    .should('be.visible')
    .click()

  cy.get('[data-test-id="assign-academic-editor"]').click()

  checkStatus(statuses.submitted.triageEditor)

  // invite acaremic editor by email
  cy.get(
    '[data-test-id="manuscript-assign-academic-editor-filter"]',
  ).type(Cypress.env('email') + academicEditor.email, { force: true })
  cy.get(`[data-test-id^="manuscript-assign-academic-editor-invite"]`)
    .contains('Invite')
    .click()

  editTemplateAndSend(academicEditor)

  checkStatus(statuses.academicEditorInvited.triageEditor)
  cy.logoutKeycloak()
}
