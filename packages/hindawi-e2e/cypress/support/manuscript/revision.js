import {
  updateEnvManuscript,
  getCurrentManuscriptId,
  getRelativeFilePath,
} from '../../utils'

export const expandSubmitRevisionSection = () => {
  cy.get(`[data-test-id="submit-revision-contextual-box"]`).click()
}

export const updateManuscriptTitle = title => {
  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type(title)
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const updateManuscriptAbstract = text => {
  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type(text)
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const uploadMainManuscript = fileName => {
  cy.get('[data-test-id="add-file-manuscript"] input[type="file"]').attachFile({
    filePath: getRelativeFilePath(fileName),
  })
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const uploadCoverLetter = fileName => {
  cy.get('[data-test-id="add-file-coverLetter"] input[type="file"]').attachFile(
    {
      filePath: getRelativeFilePath(fileName),
    },
  )
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const uploadSupplementalFile = fileName => {
  cy.get(
    '[data-test-id="add-file-supplementary"] input[type="file"]',
  ).attachFile({
    filePath: getRelativeFilePath(fileName),
  })
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const deleteMainManuscript = fileName => {
  cy.get('[data-test-id="add-file-manuscript"]')
    .contains(fileName)
    .get('[data-test-id$="-delete"]')
    .click()
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const addComment = text => {
  cy.get('[name="responseToReviewers.content"]').type(text)
  cy.wait(['@gql-updateDraftRevision-mutation'])
}

export const confirmRevision = () => {
  cy.contains('Submit revision').click()
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.wait(['@gql-submitRevision-mutation'])
  cy.url().should('not.contain', getCurrentManuscriptId())
  updateEnvManuscript()
}
