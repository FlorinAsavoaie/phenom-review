const { FUZZY_SYSTEMS } = require('./journals')
const { AUTHOR1 } = require('./users')

const DEFAULT_SUBMISSION = {
  journalName: FUZZY_SYSTEMS.name,
  articleTypeName: 'Research Article',
  author: AUTHOR1,
  abstract: 'Automatic checks',
  title: '[Auto] manuscript',
  conflictOfInterest: true,
  dataAvailability: 'Wrong side of heaven',
  fundingStatement: 'The Righteous Side Of Hell',
  manuscriptFileName: 'manuscript.txt',
  manuscriptFileContent: 'some content',
}

module.exports = {
  DEFAULT_SUBMISSION,
}
