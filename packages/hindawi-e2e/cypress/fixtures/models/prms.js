const {
  ASTROLOGY,
  FUZZY_SYSTEMS,
  ASTRONOMY,
  METEOROLOGY,
  LITHOSPHERE,
} = require('./journals')

const PRM_TYPES = Object.freeze({
  SECTION_EDITOR: 'Section Editor',
  CHIEF_MINUS: 'Chief Minus',
  SINGLE_TIER: 'Single Tier Academic Editor',
  ASSOCIATE_EDITOR: 'Associate Editor',
  LITHOSPHERE: 'ithosphere Model',
})

const hindawi = {
  [PRM_TYPES.SECTION_EDITOR]: [ASTRONOMY],
  [PRM_TYPES.CHIEF_MINUS]: [FUZZY_SYSTEMS],
  [PRM_TYPES.SINGLE_TIER]: [ASTROLOGY],
  [PRM_TYPES.ASSOCIATE_EDITOR]: [METEOROLOGY],
}

const gsw = {
  [PRM_TYPES.LITHOSPHERE]: [LITHOSPHERE],
}

const prms = {
  hindawi,
  gsw,
}

module.exports = {
  prmTypes: PRM_TYPES,
  prms,
}
