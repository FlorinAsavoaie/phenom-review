const configService = require('config')
const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const events = require('component-events')

const useCases = require('./use-cases')
const s3Service = require('./services/s3Service')

const eventsService = events.initialize({ models })

const resolvers = {
  Query: {
    async getFileInfo(_, { fileId }) {
      return useCases.getFileInfoUseCase
        .initialize({ models, services: { configService, s3Service } })
        .execute({ fileId })
    },
  },
  Mutation: {
    async deleteFile(_, { fileId }, ctx) {
      return useCases.deleteFileUseCase
        .initialize({ logEvent, models, s3Service })
        .execute({ fileId, userId: ctx.user })
    },
    async getSignedUrl(_, { fileId }) {
      return useCases.getSignedUrlUseCase
        .initialize({ models, s3Service })
        .execute(fileId)
    },
    async uploadFile(_, params, ctx) {
      return useCases.uploadFileUseCase
        .initialize({
          logEvent,
          models,
          useCases,
          services: { s3Service, eventsService, configService },
        })
        .execute({ params, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
