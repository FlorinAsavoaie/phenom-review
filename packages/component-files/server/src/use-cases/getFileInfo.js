const initialize = ({
  models: { File },
  services: { configService, s3Service },
}) => {
  async function execute({ fileId }) {
    const file = await File.find(fileId)

    if (
      file.scanStatus !== File.ScanStatuses.SCANNING ||
      !file.scanningStartTime
    ) {
      return file
    }

    const antiMalwarePassedTimeout = checkIfAntiMalwareCheckPassedTimeout(file)

    if (antiMalwarePassedTimeout) {
      const { quarantineBucket, bucket: reviewBucket } = configService.get(
        'pubsweet-component-aws-s3',
      )

      await s3Service.copyObjectToAnotherBucket({
        key: file.providerKey,
        sourceBucket: quarantineBucket,
        destinationBucket: reviewBucket,
      })

      return updateFileScanStatus(file, File.ScanStatuses.TIMEOUT_PASSED)
    }

    return file
  }

  return { execute }

  function convertTimeToMilliseconds(date) {
    return date.getTime() / 1000
  }

  function checkIfAntiMalwareCheckPassedTimeout(file) {
    const antiMalwareTimeout = configService.get('antiMalwareTimeout')

    const timePassedSinceTheFileWasCreated =
      convertTimeToMilliseconds(new Date()) -
      convertTimeToMilliseconds(file.scanningStartTime)

    return timePassedSinceTheFileWasCreated >= antiMalwareTimeout
  }

  async function updateFileScanStatus(file, status) {
    await File.updateBy({
      queryObject: {
        providerKey: file.providerKey,
      },
      propertiesToUpdate: {
        scanStatus: status,
      },
    })

    return { ...file, scanStatus: File.ScanStatuses.TIMEOUT_PASSED }
  }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
