const initialize = ({ models: { File }, s3Service }) => ({
  execute: async fileId => {
    const file = await File.find(fileId)

    return s3Service.getSignedUrl(file.providerKey)
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
