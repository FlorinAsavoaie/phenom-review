const { getFileInfoUseCase } = require('../src/use-cases')

const ScanStatuses = {
  SCANNING: 'scanning',
  HEALTHY: 'healthy',
  TIMEOUT_PASSED: 'timeoutPassed',
}

const models = {
  File: {
    find: jest.fn(),
    updateBy: jest.fn(),
    ScanStatuses,
  },
}

const services = {
  configService: {
    get: jest.fn(data => {
      if (data === 'antiMalwareTimeout') return 60
      return 'anything'
    }),
  },
  s3Service: { copyObjectToAnotherBucket: jest.fn() },
}

describe('getFileInfo use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return initial file if the status is not scanning', async () => {
    const fileId = '123'

    const file = { id: fileId, scanStatus: ScanStatuses.HEALTHY }

    jest.spyOn(models.File, 'find').mockResolvedValue(file)

    const result = await getFileInfoUseCase
      .initialize({ models, services })
      .execute({ fileId })

    expect(models.File.find).toHaveBeenCalledTimes(1)
    expect(models.File.find).toHaveBeenCalledWith(fileId)
    expect(result).toBe(file)
  })

  it("should return the initial file if the anti-malware timeout didn't passed", async () => {
    const fileId = '123'

    const file = {
      id: fileId,
      scanStatus: ScanStatuses.SCANNING,
      scanningStartTime: new Date('2022-10-01T00:00:00.000Z'),
    }

    jest.spyOn(models.File, 'find').mockResolvedValue(file)

    const mockDate = new Date('2022-10-01T00:00:05.000Z')
    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate)

    const result = await getFileInfoUseCase
      .initialize({ models, services })
      .execute({ fileId })

    expect(models.File.find).toHaveBeenCalledTimes(1)
    expect(models.File.find).toHaveBeenCalledWith(fileId)

    expect(services.configService.get).toHaveBeenCalledTimes(1)
    expect(services.configService.get).toHaveBeenCalledWith(
      'antiMalwareTimeout',
    )

    expect(result).toBe(file)

    spy.mockRestore()
  })

  it('should change file status if the anti-malware timeout passed', async () => {
    const fileId = '123'
    const providerKey = '567'

    const file = {
      id: fileId,
      providerKey,
      scanStatus: ScanStatuses.SCANNING,
      scanningStartTime: new Date('2022-10-01T00:00:00.000Z'),
    }

    jest.spyOn(models.File, 'find').mockResolvedValue(file)

    const mockDate = new Date('2022-10-01T00:02:00.000Z')
    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate)

    const result = await getFileInfoUseCase
      .initialize({ models, services })
      .execute({ fileId })

    expect(models.File.find).toHaveBeenCalledTimes(1)
    expect(models.File.find).toHaveBeenCalledWith(fileId)

    expect(services.s3Service.copyObjectToAnotherBucket).toHaveBeenCalledTimes(
      1,
    )

    expect(models.File.updateBy).toHaveBeenCalledTimes(1)
    expect(models.File.updateBy).toHaveBeenCalledWith({
      queryObject: {
        providerKey: file.providerKey,
      },
      propertiesToUpdate: {
        scanStatus: ScanStatuses.TIMEOUT_PASSED,
      },
    })

    expect(result).toMatchObject({
      ...file,
      scanStatus: ScanStatuses.TIMEOUT_PASSED,
    })

    spy.mockRestore()
  })
})
