import gql from 'graphql-tag'

import { fileDetails } from './fragments'

export const uploadFile = gql`
  mutation uploadFile(
    $entityId: String!
    $fileInput: FileInput
    $file: Upload!
  ) {
    uploadFile(entityId: $entityId, fileInput: $fileInput, file: $file) {
      ...fileDetails
    }
  }
  ${fileDetails}
`

export const getSignedUrl = gql`
  mutation getSignedUrl($fileId: String!) {
    getSignedUrl(fileId: $fileId)
  }
`

export const deleteFile = gql`
  mutation deleteFile($fileId: String!) {
    deleteFile(fileId: $fileId)
  }
`
