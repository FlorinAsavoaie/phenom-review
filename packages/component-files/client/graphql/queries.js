import gql from 'graphql-tag'

export const getFileScanStatus = gql`
  query getFileInfo($fileId: String!) {
    getFileInfo(fileId: $fileId) {
      scanStatus
    }
  }
`
