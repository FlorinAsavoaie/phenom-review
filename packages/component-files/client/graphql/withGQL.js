/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'

import * as mutations from './mutations'

export const withSignedUrl = compose(
  graphql(mutations.getSignedUrl, {
    name: 'getSignedUrl',
  }),
  withHandlers({
    getSignedUrl: ({ getSignedUrl }) => fileId =>
      getSignedUrl({
        variables: { fileId },
      }),
  }),
)

export default options => {
  return compose(
    graphql(mutations.uploadFile, {
      name: 'uploadFile',
      options,
    }),
    graphql(mutations.deleteFile, {
      name: 'deleteFile',
      options,
    }),
    withHandlers({
      deleteFile: ({ deleteFile }) => fileId =>
        deleteFile({ variables: { fileId } }),
      uploadFile: ({ uploadFile }) => ({ entityId, fileInput, file }) =>
        uploadFile({
          variables: { entityId, fileInput, file },
        }).then(r => r.data.uploadFile),
    }),
  )
}
