const {
  getTeamRoles,
  generateManuscript,
  generateJournal,
  generateTeamMember,
} = require('component-generators')
const { notifySubmitterUseCase } = require('../src/useCases')

let models = {}
let Journal = {}
let Manuscript = {}
let TeamMember = {}
let Team = {}

const notificationService = { notifySubmitter: jest.fn() }
describe('notifySubmitter', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findOneByManuscriptAndRole: jest.fn(),
      },
    }
    ;({ Journal, Manuscript, TeamMember, Team } = models)
  })
  it('Sends the email to submitter', async () => {
    const manuscript = generateManuscript()
    const journal = generateJournal()
    const submittingStaffMember = generateTeamMember({
      role: Team.Role.submittingStaffMember,
    })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRole')
      .mockResolvedValue(submittingStaffMember)

    await notifySubmitterUseCase
      .initialize({
        models,
        notificationService,
      })
      .execute({ data: manuscript.submissionId })

    expect(notificationService.notifySubmitter).toHaveBeenCalledTimes(1)
    expect(notificationService.notifySubmitter).toHaveBeenCalledWith({
      journal,
      manuscript,
      submittingStaffMember,
    })
  })
})
