const models = require('@pubsweet/models')
const { repos } = require('component-model')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const { transaction } = require('objection')
const { logger } = require('component-logger')

const cookies = require('./cookies')
const useCases = require('./useCases')
const events = require('component-events')
const notificationService = require('./notifications/notification')
const {
  useCases: { getUserWithWorkloadUseCase },
} = require('component-model')

const {
  articleTypeWithPeerReview,
  shortArticleType,
  articleTypeEditorials,
} = require('./useCases/strategies')

const {
  useCases: { validateFilesUseCase },
} = require('component-files')

const { File } = models
const fileValidator = validateFilesUseCase.initialize({
  File,
})
const { femService } = require('./services')

const resolvers = {
  Mutation: {
    async createDraftManuscript(_, { input }, ctx) {
      return useCases.createDraftManuscriptUseCase
        .initialize({
          models,
          notificationService,
          transaction,
          logger,
          loaders: ctx.loaders,
        })
        .execute({ input, userId: ctx.user })
    },
    async updateDraftManuscript(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateDraftManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
    async addAuthorToManuscript(_, { manuscriptId, authorInput }, ctx) {
      const sso = process.env.KEYCLOAK_SERVER_URL
        ? require('component-sso')
        : null
      return useCases.addAuthorToManuscriptUseCase
        .initialize({ models, logEvent, sso })
        .execute({ manuscriptId, authorInput, userId: ctx.user })
    },
    async removeAuthorFromManuscript(
      _,
      { manuscriptId, authorTeamMemberId },
      ctx,
    ) {
      return useCases.removeAuthorFromManuscriptUseCase
        .initialize({ models, logEvent })
        .execute({ manuscriptId, authorTeamMemberId, userId: ctx.user })
    },
    async editAuthorFromManuscript(_, params, ctx) {
      return useCases.editAuthorFromManuscriptUseCase
        .initialize({ models, logEvent })
        .execute({ params, userId: ctx.user })
    },
    async updateManuscriptFile(_, params, ctx) {
      return useCases.updateManuscriptFileUseCase
        .initialize(models)
        .execute(params)
    },
    async submitManuscript(_, { manuscriptId }, ctx) {
      const eventsService = events.initialize({ models })
      cookies.setManuscriptId(manuscriptId, ctx)

      return useCases.submitManuscriptUseCase
        .initialize({
          models,
          logEvent,
          useCases,
          eventsService,
          fileValidator,
          notificationService,
          shortArticleType,
          articleTypeEditorials,
          articleTypeWithPeerReview,
          getUserWithWorkloadUseCase,
        })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async editManuscript(_, { manuscriptId }) {
      const eventsService = events.initialize({ models })
      return useCases.editManuscriptUseCase
        .initialize({
          models,
          eventsService,
          fileValidator,
        })
        .execute({ manuscriptId })
    },
    /**
     * Saves the SubmissionEditorialMapping for a specific submission
     * @param submissionId
     * @param submissionEditorialModel
     * @returns SubmissionEditorialMapping
     */
    async setSubmissionEditorialMapping(
      _,
      { submissionId, submissionEditorialModel },
    ) {
      return useCases.setSubmissionEditorialMappingUseCase
        .initialize({ repos, models, transaction })
        .execute({ submissionId, submissionEditorialModel })
    },
  },
  Query: {
    async getActiveJournals(_, { input }, ctx) {
      return useCases.getActiveJournalsUseCase
        .initialize(models)
        .execute({ userId: ctx.user })
    },
    async getManuscriptByCustomId(_, { input }) {
      return useCases.getManuscriptByCustomIdUseCase
        .initialize(models)
        .execute({ input })
    },
    async getPublishedManuscripts(
      _,
      { input, journalId, sectionId, specialIssueId },
    ) {
      return useCases.getPublishedManuscriptsUseCase
        .initialize(models)
        .execute({ input, journalId, sectionId, specialIssueId })
    },
    /**
     * Based on a specific combination of Journal, Issue type, Article type, section retrieve a SubmissionEditorialModel
     * @param submissionId
     * @param journalId
     * @param issueType
     * @param articleType
     * @param isOnSection
     * @returns SubmissionEditorialModel
     */
    async getSubmissionEditorialModel(
      _,
      { submissionId, journalId, issueType, articleType, isOnSection },
    ) {
      return useCases.getSubmissionEditorialModelUseCase
        .initialize({ models, femService })
        .execute({
          submissionId,
          journalId,
          issueType,
          articleType,
          isOnSection,
        })
    },

    /**
     * Get Submission Em configuration for a manuscript
     * @param submissionId
     * @returns submissionEditorialMapping
     */
    async getSubmissionEditorialMapping(_, { submissionId }) {
      return useCases.getSubmissionEditorialMappingUseCase
        .initialize(repos)
        .execute({ submissionId })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
