const config = require('config')
const { services } = require('helper-service')
const Email = require('component-sendgrid')
const { get } = require('lodash')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const resetPath = config.get('invite-reset-password.url')
const staffEmail = config.get('staffEmail')
const footerText = config.get('emailFooterText.registeredUsers')
const unconfirmedUsersFooterText = config.get(
  'emailFooterText.unregisteredUsers',
)
const bccAddress = config.get('bccAddress')
const accountsEmail = config.get('accountsEmail')
const maintenanceEmailsDestinations = config.get(
  'maintenanceEmailsDestinations',
)
const publisherName = config.get('publisherName')

const FOOTER_TEXT_PATTERN = '{recipientEmail}'
const emailPath = 'alias.email'
const MANUSCRIPT_DETAILS = 'MANUSCRIPT DETAILS'

module.exports = {
  async sendToConfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'confirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, emailPath) || staffEmail

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'LOGIN',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifySubmitter({ journal, manuscript, submittingStaffMember }) {
    const { alias, user } = submittingStaffMember
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'confirmed-authors',
      manuscript,
      journalName,
      submittingAuthor: submittingStaffMember,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'LOGIN',
        paragraph,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendToUnconfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const editorialAssistantEmail =
      get(editorialAssistant, emailPath) || staffEmail
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'unconfirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          phenomId: user.id,
          confirmationToken: user.confirmationToken,
          ...alias,
        }),
        ctaText: 'CREATE ACCOUNT',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(
          unconfirmedUsersFooterText,
          {
            pattern: FOOTER_TEXT_PATTERN,
            replacement: alias.email,
          },
          { pattern: '{journalName}', replacement: journalName },
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendSubmittingAuthorConfirmation({
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const emailType = 'submitting-author-manuscript-submitted'
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      manuscript,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, emailPath) || staffEmail
    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: `${submittingAuthor.alias.surname}`,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: MANUSCRIPT_DETAILS,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyEditorsWhenNewManuscriptSubmitted({
    journal,
    editor,
    submittingAuthor,
    editorialAssistant,
    manuscript,
  }) {
    const { name: journalName } = journal
    const editorName = editor.getName()
    const editorEmail = get(editor, emailPath)
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail =
      get(editorialAssistant, emailPath) || get(journal, 'email')

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      manuscript,
      submittingAuthor,
      emailType: 'editors-new-manuscript-submitted',
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      toUser: {
        email: editorEmail,
        name: editorName,
      },
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      content: {
        subject: `${manuscript.customId}: New manuscript submitted`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaText: MANUSCRIPT_DETAILS,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(editor, 'user.id', ''),
          token: get(editor, 'user.unsubscribeToken', ''),
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: editorEmail,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendAlertWhenRemainingCustomIdsBellowThreshold({
    remainingCustomIdsCount,
    customIdsThreshold,
  }) {
    const emailType = 'alert-when-remaining-customIds-bellow-threshold'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      remainingCustomIdsCount,
      customIdsThreshold,
    })
    maintenanceEmailsDestinations.forEach(maintenanceEmailDestination => {
      const email = new Email({
        type: 'user',
        bcc: bccAddress,
        fromEmail: `${publisherName.toUpperCase()} SUPPORT <${accountsEmail}>`,
        toUser: maintenanceEmailDestination,
        content: {
          subject: `The number of custom Ids is bellow threshold on ${publisherName} publisher!`,
          paragraph,
          footerText: '',
        },
        bodyProps,
      })
      return email.sendEmail()
    })
  },
}
