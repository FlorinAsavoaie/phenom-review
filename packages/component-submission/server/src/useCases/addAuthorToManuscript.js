const uuid = require('uuid')
const { sortBy } = require('lodash')
const { rorService } = require('component-ror')

const initialize = ({
  models: { Manuscript, Team, Identity, User },
  logEvent,
  sso,
}) => ({
  execute: async ({
    manuscriptId,
    authorInput: {
      isSubmitting = false,
      isCorresponding = false,
      ...authorIdentity
    },
    userId,
  }) => {
    await appendRORInfoIfMissing(authorIdentity)

    let user
    const options = {
      manuscriptId,
      role: Team.Role.author,
    }
    const authorTeam = await Team.findOrCreate({
      options,
      queryObject: options,
      eagerLoadRelations: 'members.user.identities',
    })

    const identity = await Identity.findOneByEmail(authorIdentity.email)

    if (identity) {
      user = await User.find(identity.userId)
      user.identities = [identity]
      if (!user.isActive) {
        throw new ConflictError('Invited user is inactive.')
      }
    } else {
      user = new User({
        agreeTc: !!sso,
        isActive: true,
        defaultIdentityType: 'local',
        confirmationToken: uuid.v4(),
        passwordResetToken: uuid.v4(),
      })

      const newIdentity = new Identity({
        type: 'local',
        isConfirmed: false,
        ...authorIdentity,
      })
      user.assignIdentity(newIdentity)
    }

    if (authorTeam.members) {
      if (isCorresponding) {
        authorTeam.members.forEach(a => (a.isCorresponding = false))
      }

      const submittingAuthorExists = authorTeam.members.some(
        a => a.isSubmitting,
      )
      if (!submittingAuthorExists) isSubmitting = true

      const correspondingAuthorExists = authorTeam.members.some(
        a => a.isCorresponding,
      )
      if (!correspondingAuthorExists) isCorresponding = true
    }

    const author = authorTeam.addMember({
      user,
      options: {
        isSubmitting,
        isCorresponding,
        alias: authorIdentity,
      },
    })

    await authorTeam.saveGraph({ insertMissing: true, relate: true })

    const manuscript = await Manuscript.find(manuscriptId, 'journal')
    if (authorTeam.members.length === 1) {
      author.updateProperties({
        isSubmitting: true,
        isCorresponding: true,
      })
      await author.save()
    }

    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_added,
        objectType: logEvent.objectType.user,
        objectId: user.id,
      })
    }

    return sortBy(authorTeam.members, 'position').map(a => a.toDTO())
  },
})

const appendRORInfoIfMissing = async authorIdentity => {
  if (authorIdentity.aff && !authorIdentity.affRorId) {
    const bestROR = await rorService.getBestROR(
      authorIdentity.aff,
      authorIdentity.country,
    )
    if (bestROR) {
      authorIdentity.affRorId = bestROR.organization.id
    }
  }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
