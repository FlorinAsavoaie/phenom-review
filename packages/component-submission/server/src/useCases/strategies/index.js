const articleTypeWithPeerReview = require('./articleTypeWithPeerReview')
const shortArticleType = require('./shortArticleType')
const articleTypeEditorials = require('./articleTypeEditorials')

module.exports = {
  articleTypeWithPeerReview,
  shortArticleType,
  articleTypeEditorials,
}
