import {
  removeTypename,
  validateWizard,
  parseFormValues,
  autosaveRequest,
  setInitialValues,
  findIdsByJournal,
  findIdsBySection,
  parseManuscriptFiles,
  findIdsBySpecialIssue,
  getSubmissionIds,
} from '../utils'

describe('Wizard utils', () => {
  describe('Form validations', () => {
    it('author is being added/edited', () => {
      const values = {
        isEditing: true,
        authors: [],
        files: {
          manuscript: [{ id: '12', size: 1000 }],
        },
      }

      const errors = validateWizard(2)(values)

      expect(errors.isEditing).toBe('An author is being edited.')
    })

    it('form has no errors', () => {
      const values = {
        isEditing: false,
        authors: [{ id: '123', name: 'author' }],
        files: {
          manuscript: [{ id: '12', size: 1000 }],
        },
        meta: {
          articleType: 'research',
        },
      }

      const errors = validateWizard(3)(values)

      expect(errors).toEqual({})
    })
  })
  // #endregion

  describe('Form initial values parser', () => {
    it('correctly parses files', () => {
      const files = [
        {
          id: '53d03c2f-7c23-41d7-874e-b9734e19662e',
          type: 'manuscript',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '6a13ce1d-12d8-4482-89ae-e9c04125d7aa',
          type: 'manuscript',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '3bd81442-3b77-4116-9849-d801e16dbd9e',
          type: 'manuscript',
          size: 78727,
          mimeType: 'application/pdf',
          originalName: 'interviu.pdf',
        },
        {
          id: '2cc68139-a27e-4a13-ad95-b54aa7832b18',
          type: 'manuscript',
          size: 78727,
          mimeType: 'application/pdf',
          originalName: 'interviu.pdf',
        },
        {
          id: '8dd7509f-7bb8-4944-9c13-b24164fc8467',
          type: 'coverLetter',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '2bff600b-3845-449c-bcf4-bfa47c2e1802',
          type: 'supplementary',
          size: 78727,
          mimeType: 'application/pdf',
          originalName: 'interviu.pdf',
        },
        {
          id: '2cd4dcab-9757-413a-8c03-557007e9d61a',
          type: 'coverLetter',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '010db8f0-01a1-41a5-a1ee-5bd2adf318f2',
          type: 'coverLetter',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '7d37e02e-0370-41f2-bba0-1d17411b44ae',
          type: 'coverLetter',
          size: 15879,
          mimeType: 'image/jpeg',
          originalName: 'eu.jpeg',
        },
        {
          id: '2b493c13-4863-4a65-83f5-630ea401b5bc',
          type: 'manuscript',
          size: 78727,
          mimeType: 'application/pdf',
          originalName: 'interviu.pdf',
        },
        {
          id: '4151f1b6-9702-4d3a-923b-0cce80b5e67a',
          type: 'figure',
          size: 51233,
          mimeType: 'image/jpeg',
          originalName: 'figure1.jpeg',
        },
      ]

      const expected = {
        manuscript: [
          {
            id: '53d03c2f-7c23-41d7-874e-b9734e19662e',
            type: 'manuscript',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
          {
            id: '6a13ce1d-12d8-4482-89ae-e9c04125d7aa',
            type: 'manuscript',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
          {
            id: '3bd81442-3b77-4116-9849-d801e16dbd9e',
            type: 'manuscript',
            size: 78727,
            mimeType: 'application/pdf',
            originalName: 'interviu.pdf',
          },
          {
            id: '2cc68139-a27e-4a13-ad95-b54aa7832b18',
            type: 'manuscript',
            size: 78727,
            mimeType: 'application/pdf',
            originalName: 'interviu.pdf',
          },
          {
            id: '2b493c13-4863-4a65-83f5-630ea401b5bc',
            type: 'manuscript',
            size: 78727,
            mimeType: 'application/pdf',
            originalName: 'interviu.pdf',
          },
        ],
        coverLetter: [
          {
            id: '8dd7509f-7bb8-4944-9c13-b24164fc8467',
            type: 'coverLetter',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
          {
            id: '2cd4dcab-9757-413a-8c03-557007e9d61a',
            type: 'coverLetter',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
          {
            id: '010db8f0-01a1-41a5-a1ee-5bd2adf318f2',
            type: 'coverLetter',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
          {
            id: '7d37e02e-0370-41f2-bba0-1d17411b44ae',
            type: 'coverLetter',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
          },
        ],
        supplementary: [
          {
            id: '2bff600b-3845-449c-bcf4-bfa47c2e1802',
            type: 'supplementary',
            size: 78727,
            mimeType: 'application/pdf',
            originalName: 'interviu.pdf',
          },
        ],
        figure: [
          {
            id: '4151f1b6-9702-4d3a-923b-0cce80b5e67a',
            type: 'figure',
            size: 51233,
            mimeType: 'image/jpeg',
            originalName: 'figure1.jpeg',
          },
        ],
      }

      const result = parseManuscriptFiles(files)

      expect(result).toEqual(expected)
    })

    it('sets the correct initial values', () => {
      const values = {
        id: '4b63bc21-6742-4cb5-8feb-52f74677591e',
        journalId: 'f2b32dc6-a546-41b0-b209-1df5f8b7e7ee',
        meta: {
          abstract: 'This here be the abstract.',
          articleType: 'research',
          title: 'Strong title',
          agreeTc: true,
          conflictOfInterest: null,
          dataAvailability: null,
          fundingStatement: null,
          __typename: 'ManuscriptMeta',
        },
        status: 'draft',
        authors: [
          {
            id: '914a00e3-3c71-487a-91c8-792a0710f7d3',
            isSubmitting: true,
            isCorresponding: true,
            alias: {
              aff: 'Boko Haram',
              email: 'alexandru.munteanu+a1@hindawi.com',
              country: 'RO',
              name: {
                surname: 'Author1',
                givenNames: 'AlexA1',
                __typename: 'Name',
              },
              __typename: 'Alias',
            },
            __typename: 'TeamMember',
          },
        ],
        files: [
          {
            id: '0bd315c5-aeeb-45d4-a846-fba0f53d5139',
            type: 'manuscript',
            size: 78727,
            mimeType: 'application/pdf',
            originalName: 'interviu.pdf',
            __typename: 'File',
          },
          {
            id: 'f4188d33-8b78-457d-bccc-4acd7b54e1b4',
            type: 'manuscript',
            size: 4271,
            mimeType: 'application/pdf',
            originalName: 'DMOSA_JS.pdf',
            __typename: 'File',
          },
          {
            id: 'b57d8a7f-be91-4a72-ac77-45015cf1dec6',
            type: 'supplementary',
            size: 8170042,
            mimeType: 'application/pdf',
            originalName: 'brosura-Mafia_A5-1.pdf',
            __typename: 'File',
          },
          {
            id: 'da2739ad-a511-4fa4-8feb-6ae56392bf87',
            type: 'supplementary',
            size: 15879,
            mimeType: 'image/jpeg',
            originalName: 'eu.jpeg',
            __typename: 'File',
          },
          {
            id: 'a587e6ca-e414-423d-aa7d-4d000b8aff78',
            type: 'supplementary',
            size: 49324,
            mimeType: 'image/png',
            originalName: 'Screen Shot 2019-01-02 at 15.08.03.png',
            __typename: 'File',
          },
        ],
        __typename: 'Manuscript',
      }

      const expected = {
        id: '4b63bc21-6742-4cb5-8feb-52f74677591e',
        issueType: 'regularIssue',
        journalId: 'f2b32dc6-a546-41b0-b209-1df5f8b7e7ee',
        sourceJournalId: undefined,
        sourceJournalManuscriptId: '',
        isTransfer: 'no',
        linkedSubmissionCustomId: '',
        meta: {
          abstract: 'This here be the abstract.',
          articleType: 'research',
          title: 'Strong title',
          agreeTc: true,
          conflictOfInterest: null,
          dataAvailability: null,
          fundingStatement: null,
          hasConflictOfInterest: 'no',
        },
        sectionId: undefined,
        specialIssueId: undefined,
        authors: [
          {
            id: '914a00e3-3c71-487a-91c8-792a0710f7d3',
            isSubmitting: true,
            isCorresponding: true,
            alias: {
              aff: 'Boko Haram',
              email: 'alexandru.munteanu+a1@hindawi.com',
              country: 'RO',
              name: {
                surname: 'Author1',
                givenNames: 'AlexA1',
              },
            },
          },
        ],
        files: {
          manuscript: [
            {
              id: '0bd315c5-aeeb-45d4-a846-fba0f53d5139',
              type: 'manuscript',
              size: 78727,
              mimeType: 'application/pdf',
              originalName: 'interviu.pdf',
            },
            {
              id: 'f4188d33-8b78-457d-bccc-4acd7b54e1b4',
              type: 'manuscript',
              size: 4271,
              mimeType: 'application/pdf',
              originalName: 'DMOSA_JS.pdf',
            },
          ],
          coverLetter: [],
          supplementary: [
            {
              id: 'b57d8a7f-be91-4a72-ac77-45015cf1dec6',
              type: 'supplementary',
              size: 8170042,
              mimeType: 'application/pdf',
              originalName: 'brosura-Mafia_A5-1.pdf',
            },
            {
              id: 'da2739ad-a511-4fa4-8feb-6ae56392bf87',
              type: 'supplementary',
              size: 15879,
              mimeType: 'image/jpeg',
              originalName: 'eu.jpeg',
            },
            {
              id: 'a587e6ca-e414-423d-aa7d-4d000b8aff78',
              type: 'supplementary',
              size: 49324,
              mimeType: 'image/png',
              originalName: 'Screen Shot 2019-01-02 at 15.08.03.png',
            },
          ],
          figure: [],
        },
      }

      const result = setInitialValues(values)

      expect(result).toEqual(expected)
    })
  })

  describe('Form value parsing', () => {
    it('deeply removes __typename', () => {
      const input = {
        id: '992eb580-63d7-4e10-bccf-55255f8fa450',
        isSubmitting: true,
        isCorresponding: true,
        alias: {
          aff: 'Boko Haram',
          email: 'alexandru.munteanu+a1@hindawi.com',
          country: 'RO',
          name: {
            surname: 'Author1',
            givenNames: 'AlexA1',
            __typename: 'Name',
          },
          __typename: 'Alias',
        },
        __typename: 'TeamMember',
      }

      const expected = {
        id: '992eb580-63d7-4e10-bccf-55255f8fa450',
        isSubmitting: true,
        isCorresponding: true,
        alias: {
          aff: 'Boko Haram',
          email: 'alexandru.munteanu+a1@hindawi.com',
          country: 'RO',
          name: {
            surname: 'Author1',
            givenNames: 'AlexA1',
          },
        },
      }

      const result = removeTypename(input)

      expect(result).toEqual(expected)
    })

    it('remove __typename from the values', () => {
      const values = {
        id: 'man123',
        journalId: '1234',
        meta: {
          title: 'abc',
          conflictOfInterest: null,
          dataAvailability: null,
          fundingStatement: null,
          __typename: 'ManuscriptMetadata',
        },
      }

      const result = parseFormValues(values)

      expect(result).toEqual({
        manuscriptId: 'man123',
        autosaveInput: {
          authors: [],
          files: [],
          journalId: '1234',
          sourceJournalId: null,
          linkedSubmissionCustomId: null,
          sourceJournalManuscriptId: null,
          sectionId: null,
          specialIssueId: null,
          meta: {
            title: 'abc',
            conflictOfInterest: null,
            dataAvailability: null,
            fundingStatement: null,
          },
        },
      })
    })

    it('parses authors', () => {
      const authors = [
        {
          id: 'unsaved-author',
        },
        {
          id: '992eb580-63d7-4e10-bccf-55255f8fa450',
          isSubmitting: true,
          isCorresponding: true,
          alias: {
            aff: 'Boko Haram',
            email: 'alexandru.munteanu+a1@hindawi.com',
            country: 'RO',
            name: {
              surname: 'Author1',
              givenNames: 'AlexA1',
              __typename: 'Name',
            },
            __typename: 'Alias',
          },
          __typename: 'TeamMember',
        },
        {
          id: '3cc4c7e6-6fa8-4cda-82f8-31fe4064c7c7',
          isSubmitting: false,
          isCorresponding: false,
          alias: {
            aff: 'Tsd',
            email: 'alexandru.munteanu+a5@hindawi.com',
            country: 'RO',
            name: {
              surname: 'MuntAuthor2',
              givenNames: 'AlexAuthor2',
              __typename: 'Name',
            },
            __typename: 'Alias',
          },
          __typename: 'TeamMember',
        },
        {
          id: 'cff3dd51-50d6-415d-9739-49d829352fbf',
          isSubmitting: false,
          isCorresponding: false,
          alias: {
            aff: 'Tsd',
            email: 'alexandru.munteanu+a10@hindawi.com',
            country: 'RO',
            name: {
              surname: 'MuntAuthor2',
              givenNames: 'AlexAuthor2',
              __typename: 'Name',
            },
            __typename: 'Alias',
          },
          __typename: 'TeamMember',
        },
        {
          id: 'd312e5b2-d174-4133-b68f-bbf3475a6dee',
          isSubmitting: false,
          isCorresponding: false,
          alias: {
            aff: 'TSD',
            email: 'alexandru.munteanu+author19@hindawi.com',
            country: 'RO',
            name: {
              surname: 'Author19',
              givenNames: 'AlexA19',
              __typename: 'Name',
            },
            __typename: 'Alias',
          },
          __typename: 'TeamMember',
        },
      ]

      const values = {
        id: 'abc',
        authors,
      }

      const result = parseFormValues(values)

      const expected = {
        manuscriptId: 'abc',
        autosaveInput: {
          sectionId: null,
          specialIssueId: null,
          journalId: null,
          sourceJournalId: null,
          linkedSubmissionCustomId: null,
          sourceJournalManuscriptId: null,
          files: [],
          meta: {},
          authors: [
            {
              isSubmitting: true,
              isCorresponding: true,
              aff: 'Boko Haram',
              email: 'alexandru.munteanu+a1@hindawi.com',
              country: 'RO',
              surname: 'Author1',
              givenNames: 'AlexA1',
            },
            {
              isSubmitting: false,
              isCorresponding: false,
              aff: 'Tsd',
              email: 'alexandru.munteanu+a5@hindawi.com',
              country: 'RO',
              surname: 'MuntAuthor2',
              givenNames: 'AlexAuthor2',
            },
            {
              isSubmitting: false,
              isCorresponding: false,
              aff: 'Tsd',
              email: 'alexandru.munteanu+a10@hindawi.com',
              country: 'RO',
              surname: 'MuntAuthor2',
              givenNames: 'AlexAuthor2',
            },
            {
              isSubmitting: false,
              isCorresponding: false,
              aff: 'TSD',
              email: 'alexandru.munteanu+author19@hindawi.com',
              country: 'RO',
              surname: 'Author19',
              givenNames: 'AlexA19',
            },
          ],
        },
      }

      expect(result).toEqual(expected)
    })

    it('parses form without authors', () => {
      const values = {
        id: 'abc',
      }

      const result = parseFormValues(values)

      expect(result).toEqual({
        manuscriptId: 'abc',
        autosaveInput: {
          sectionId: null,
          specialIssueId: null,
          journalId: null,
          sourceJournalId: null,
          linkedSubmissionCustomId: null,
          sourceJournalManuscriptId: null,
          authors: [],
          files: [],
          meta: {},
        },
      })
    })

    it('parses files', () => {
      const values = {
        id: '17bc24c0-2359-4029-b039-d3036c9a82f0',
        journalId: '805df852-6b40-4ea0-a26d-a4dd6df65521',
        meta: {
          abstract: 'df',
          articleType: null,
          title: 'k',
          agreeTc: true,
          conflictOfInterest: null,
          dataAvailability: null,
          fundingStatement: null,
          __typename: 'ManuscriptMeta',
        },
        status: 'draft',
        authors: [
          {
            id: 'bfcffc28-8e38-45ea-9cdd-f16755ec7e91',
            isSubmitting: false,
            isCorresponding: false,
            alias: {
              aff: 'TSD',
              email: 'alexandru.munteanu+author7@hindawi.com',
              country: 'AX',
              name: {
                surname: 'A17',
                givenNames: 'AlexA17',
                __typename: 'Name',
              },
              __typename: 'Alias',
            },
            __typename: 'TeamMember',
          },
          {
            id: 'dad2d643-c186-4bb4-a28b-35f50ded490e',
            isSubmitting: true,
            isCorresponding: true,
            alias: {
              aff: 'Boko Haram',
              email: 'alexandru.munteanu+a1@hindawi.com',
              country: 'RO',
              name: {
                surname: 'Author1',
                givenNames: 'AlexA1',
                __typename: 'Name',
              },
              __typename: 'Alias',
            },
            __typename: 'TeamMember',
          },
        ],
        files: {
          manuscript: [
            {
              id: 'c410af06-4ec1-4d51-a88c-b7c1ddf24d8a',
              type: 'manuscript',
              size: 4271,
              mimeType: 'application/pdf',
              originalName: 'DMOSA_JS.pdf',
            },
          ],
          coverLetter: [
            {
              id: 'ee721714-08d4-4d94-932f-96d5e763fcd2',
              type: 'coverLetter',
              size: 15879,
              mimeType: 'image/jpeg',
              originalName: 'eu.jpeg',
            },
          ],
          supplementary: [
            {
              id: '8870f156-9702-4d3a-923b-0cce804f53e7',
              type: 'supplementary',
              size: 743108,
              mimeType: 'image/jpeg',
              originalName: 'IMG_20181220_182725.jpg',
            },
          ],
          figure: [
            {
              id: '4151f1b6-9702-4d3a-923b-0cce80b5e67a',
              type: 'figure',
              size: 51233,
              mimeType: 'image/jpeg',
              originalName: 'figure1.jpeg',
            },
          ],
        },
        __typename: 'Manuscript',
      }

      const expected = {
        manuscriptId: '17bc24c0-2359-4029-b039-d3036c9a82f0',
        autosaveInput: {
          journalId: '805df852-6b40-4ea0-a26d-a4dd6df65521',
          sourceJournalId: null,
          linkedSubmissionCustomId: null,
          sourceJournalManuscriptId: null,
          sectionId: null,
          specialIssueId: null,
          authors: [
            {
              isSubmitting: false,
              isCorresponding: false,
              aff: 'TSD',
              email: 'alexandru.munteanu+author7@hindawi.com',
              country: 'AX',
              surname: 'A17',
              givenNames: 'AlexA17',
            },
            {
              isSubmitting: true,
              isCorresponding: true,
              aff: 'Boko Haram',
              email: 'alexandru.munteanu+a1@hindawi.com',
              country: 'RO',
              surname: 'Author1',
              givenNames: 'AlexA1',
            },
          ],
          meta: {
            abstract: 'df',
            articleType: null,
            title: 'k',
            agreeTc: true,
            conflictOfInterest: null,
            dataAvailability: null,
            fundingStatement: null,
          },
          files: [
            {
              id: 'c410af06-4ec1-4d51-a88c-b7c1ddf24d8a',
              type: 'manuscript',
              size: 4271,
              name: 'DMOSA_JS.pdf',
            },
            {
              id: 'ee721714-08d4-4d94-932f-96d5e763fcd2',
              type: 'coverLetter',
              size: 15879,
              name: 'eu.jpeg',
            },
            {
              id: '8870f156-9702-4d3a-923b-0cce804f53e7',
              type: 'supplementary',
              size: 743108,
              name: 'IMG_20181220_182725.jpg',
            },
            {
              id: '4151f1b6-9702-4d3a-923b-0cce80b5e67a',
              type: 'figure',
              size: 51233,
              name: 'figure1.jpeg',
            },
          ],
        },
      }

      const result = parseFormValues(values)

      expect(result).toEqual(expected)
    })
  })

  describe('Form autosave', () => {
    let realDateNow
    const timestamp = 1548340892600
    beforeAll(() => {
      realDateNow = Date.now
      Date.now = jest.fn(() => timestamp)
    })

    it('sends the autosave request', async () => {
      const values = {
        id: 'man123',
        meta: {
          conflicts: {
            __typename: 'Conflicts',
          },
          __typename: 'ManuscriptMetadata',
        },
      }

      const updateDraft = jest.fn(() => Promise.resolve())
      const updateAutosave = jest.fn()

      await autosaveRequest({
        values,
        updateDraft,
        updateAutosave,
      })

      expect(updateDraft).toHaveBeenCalledWith({
        variables: parseFormValues(values),
      })

      expect(updateAutosave).toHaveBeenCalledTimes(2)
      expect(updateAutosave.mock.calls).toEqual([
        [
          {
            variables: {
              params: { error: null, inProgress: true, updatedAt: null },
            },
          },
        ],
        [
          {
            variables: {
              params: {
                error: null,
                inProgress: false,
                updatedAt: timestamp,
              },
            },
          },
        ],
      ])
    })

    afterAll(() => {
      Date.now = realDateNow
    })
  })

  describe('findIdsByJournal', () => {
    it('is case insensitive', () => {
      const journals = [
        { id: 1, code: 'BCA' },
        { id: 2, code: 'DEV' },
      ]
      const code = 'bCa'
      const res = findIdsByJournal({ journals, journal: code })

      expect(res.journalId).toEqual(1)
    })
    it('returns an empty object if no match is found', () => {
      const journals = [
        { id: 1, code: 'BCA' },
        { id: 2, code: 'DEV' },
      ]
      const code = 'HIN'
      const res = findIdsByJournal({ journals, journal: code })

      expect(res).toEqual({})
    })
  })

  describe('findIdsBySection', () => {
    it('is type insensitive', () => {
      const journals = [
        {
          id: 1,
          sections: [
            { id: 11, customId: '777' },
            { id: 12, customId: '456' },
          ],
        },
        { id: 2, code: 'DEV', sections: [{ id: 21, customId: '300' }] },
      ]
      const customId = 456
      const res = findIdsBySection({ journals, section: customId })

      expect(res.journalId).toEqual(1)
      expect(res.sectionId).toEqual(12)
    })
    it('returns an empty object if no match is found', () => {
      const journals = [
        {
          id: 1,
          sections: [
            { id: 11, customId: '777' },
            { id: 12, customId: '456' },
          ],
        },
        { id: 2, sections: [{ id: 21, customId: '300' }] },
      ]
      const customId = 616
      const res = findIdsBySection({ journals, section: customId })

      expect(res).toEqual({})
    })
    it('returns an empty object if the journal has no sections', () => {
      const journals = [
        { id: 1, sections: [] },
        { id: 2, sections: [] },
      ]
      const customId = 111
      const res = findIdsBySection({ journals, section: customId })

      expect(res).toEqual({})
    })
  })

  describe('findIdsBySpecialIssue', () => {
    it('is type insensitive', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, specialIssues: [{ id: 111, customId: '098' }] }],
          specialIssues: [{ id: 101, customId: '123' }],
        },
        {
          id: 2,
          sections: [{ id: 21, specialIssues: [{ id: 211, customId: '919' }] }],
          specialIssues: [{ id: 201, customId: '743' }],
        },
      ]
      const customId = 919
      const res = findIdsBySpecialIssue({ journals, specialIssue: customId })

      expect(res.journalId).toEqual(2)
      expect(res.sectionId).toEqual(21)
      expect(res.specialIssueId).toEqual(211)
    })
    it('returns an empty object if no match is found', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, specialIssues: [{ id: 111, customId: '098' }] }],
          specialIssues: [{ id: 101, customId: '123' }],
        },
        {
          id: 2,
          sections: [{ id: 21, specialIssues: [{ id: 211, customId: '919' }] }],
          specialIssues: [{ id: 201, customId: '743' }],
        },
      ]
      const customId = 222
      const res = findIdsBySpecialIssue({ journals, specialIssue: customId })

      expect(res).toEqual({})
    })
    it('returns an empty object if the journal or sections have no special issues', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, specialIssues: [] }],
          specialIssues: [],
        },
        {
          id: 2,
          sections: [{ id: 21, specialIssues: [] }],
          specialIssues: [],
        },
      ]
      const customId = 444
      const res = findIdsBySpecialIssue({ journals, specialIssue: customId })

      expect(res).toEqual({})
    })
    it('returns a null section id if the special issue is on the journal', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, specialIssues: [{ id: 111, customId: '098' }] }],
          specialIssues: [{ id: 101, customId: '123' }],
        },
        {
          id: 2,
          sections: [{ id: 21, specialIssues: [{ id: 211, customId: '919' }] }],
          specialIssues: [{ id: 201, customId: '743' }],
        },
      ]
      const customId = 123
      const res = findIdsBySpecialIssue({ journals, specialIssue: customId })

      expect(res.journalId).toEqual(1)
      expect(res.sectionId).toEqual(null)
      expect(res.specialIssueId).toEqual(101)
    })
  })

  describe('getSubmissionIds', () => {
    it('returns an empty object if no valid paramter is found', () => {
      const journals = [{ id: 1 }]
      const params = { a: 'lo', end: 'ava' }

      const res = getSubmissionIds({ journals, params })

      expect(res).toEqual({})
    })
    it('returns an empty object if the journal list is empty', () => {
      const journals = []
      const params = { a: 'lo', end: 'ava' }

      const res = getSubmissionIds({ journals, params })

      expect(res).toEqual({})
    })
    it('ignores journal and section if special issue provided', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, specialIssues: [{ id: 111, customId: '098' }] }],
          specialIssues: [{ id: 101, customId: '123' }],
        },
        {
          id: 2,
          code: 'covid',
          sections: [{ id: 21, specialIssues: [{ id: 211, customId: '919' }] }],
          specialIssues: [{ id: 201, customId: '743' }],
        },
      ]
      const params = { journal: 'covid', section: 19, specialIssue: '098' }

      const res = getSubmissionIds({ journals, params })

      expect(res.journalId).toEqual(1)
      expect(res.sectionId).toEqual(11)
      expect(res.specialIssueId).toEqual(111)
    })
    it('ignores journal if section provided', () => {
      const journals = [
        {
          id: 1,
          sections: [{ id: 11, customId: '1336' }],
        },
        {
          id: 2,
          code: 'covid',
          sections: [{ id: 21, customId: '1338' }],
        },
      ]
      const params = { journal: 'covid', section: 1336 }

      const res = getSubmissionIds({ journals, params })

      expect(res.journalId).toEqual(1)
      expect(res.sectionId).toEqual(11)
      expect(res.specialIssueId).toEqual(null)
    })
  })
})
