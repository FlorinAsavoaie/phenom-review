export const publisher = {
  links: {
    websiteLink: 'https://www.hindawi.com/',
    privacyLink: 'https://www.hindawi.com/privacy/',
    termsLink: 'https://www.hindawi.com/terms/',
    ethicsLink: 'https://www.hindawi.com/ethics/',
    coiLink: 'https://www.hindawi.com/ethics/#conflicts-of-interest',
    apcLink: 'https://www.hindawi.com/journals/{journalCode}/apc',
    journalAuthorsGuidelinesLink:
      'https://www.hindawi.com/journals/{journalCode}/guidelines/ ',
    authorsGuidelinesLink: 'https://www.hindawi.com/publish-research/authors/',
  },
  recommendationScreenInfoBox: {
    title:
      'A reminder that recommendations should not be made solely on novelty or perceived impact/interest',
    description: `
      <p>Research published in the journal must be:</p>
      <ul>
        <li>In scope of the journal;</li>
        <li>Scientifically valid;</li>
        <li>Technically accurate in its methods and results;</li>
        <li>Representative of a specific advance, or replication, or null/negative result, which is worthy of publication;</li>
        <li>As reproducible as possible;</li>
        <li>Ethically sound.</li>
      </ul>
      <p>We do not impose any page or article limitations, and reviews should not take these into consideration.</p>
      <p>Please provide authors with detailed and constructive feedback.</p>
      <p>For more information read our <a href="https://www.hindawi.com/publish-research/reviewers/" target="_blank" rel="noreferrer">Reviewer guidelines</a>.</p>
    `,
  },
}

export const journal = {
  statuses: [],
}
