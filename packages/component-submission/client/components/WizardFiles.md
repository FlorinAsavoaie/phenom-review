Submission wizard file section.

```js
const { Formik } = require('formik')

const files = {
  manuscripts: [
    { id: 'file1', name: 'file1.pdf', size: 123 },
    { id: 'file2', name: 'file2.pdf', size: 45677 },
  ],
  coverLetter: [{ id: 'file3', name: 'file3.pdf', size: 6542 }],
  supplementary: [],
  figure: [],
}

;<Formik>{formikProps => <WizardFiles files={files} />}</Formik>
```
