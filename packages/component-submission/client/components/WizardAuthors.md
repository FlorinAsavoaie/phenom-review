Submission wizard author adder/editor component.

```js
const { Formik } = require('formik')

const journal = {
  submission: {
    links: {
      authorGuidelines: 'https://www.hindawi.com/journals/bca/guidelines/',
      articleProcessing: 'https://www.hindawi.com/journals/bca/apc/',
    },
  },
}

const initialValues = {
  authors: [
    {
      id: '123',
      alias: {
        aff: 'Stupidly Long Affiliation Name Here',
        email: 'abcalongemailrightherebroshouldoverflowwithstyle@def.ghi',
        country: 'RO',
        name: {
          givenNames: 'Alpha',
          surname: 'Bravo',
        },
      },
    },
    {
      id: '456',
      alias: {
        aff: 'Mormon Church',
        email: 'that.nigerian.prince@niger.com',
        country: 'RO',
        name: {
          givenNames: 'Nairobi',
          surname: 'Mogadishu',
        },
      },
    },
  ],
}

;<Formik initialValues={initialValues}>
  {({ errors, setFieldValue, ...rest }) => (
    <WizardAuthors
      setWizardEditMode={value => setFieldValue('isEditing', value)}
      addAuthor={adder => () => {
        adder({
          id: 'unsaved-author',
          email: '',
          country: '',
          lastName: '',
          firstName: '',
          affiliation: '',
        })
      }}
      onSaveAuthor={(author, { index, formFns, setWizardEditMode }) => {
        setWizardEditMode(false)
        formFns.remove(index)
        formFns.push({ ...author, id: Math.floor(Math.random() * 10000) })
      }}
      wizardErrors={errors}
      journal={journal}
    />
  )}
</Formik>
```

Submission wizard author section with errors.

```js
const { Formik } = require('formik')

const journal = {
  submission: {
    links: {
      authorGuidelines: 'https://www.hindawi.com/journals/bca/guidelines/',
      articleProcessing: 'https://www.hindawi.com/journals/bca/apc/',
    },
  },
}

const initialValues = {
  authors: [
    {
      id: '123',
      alias: {
        aff: 'Stupidly Long Affiliation Name Here',
        email: 'abcalongemailrightherebroshouldoverflowwithstyle@def.ghi',
        country: 'RO',
        name: {
          givenNames: 'Alpha',
          surname: 'Bravo',
        },
      },
    },
    {
      id: '456',
      alias: {
        aff: 'Mormon Church',
        email: 'that.nigerian.prince@niger.com',
        country: 'RO',
        name: {
          givenNames: 'Nairobi',
          surname: 'Mogadishu',
        },
      },
    },
  ],
}

;<Formik initialValues={initialValues}>
  {({ errors, setFieldValue, ...rest }) => (
    <WizardAuthors
      addAuthor={adder => () => {
        adder({
          id: 'unsaved-author',
          email: '',
          country: '',
          lastName: '',
          firstName: '',
          affiliation: '',
        })
      }}
      journal={journal}
      onSaveAuthor={(author, { index, formFns, setWizardEditMode }) => {
        setWizardEditMode(false)
        formFns.remove(index)
        formFns.push({ ...author, id: Math.floor(Math.random() * 10000) })
      }}
      setWizardEditMode={value => setFieldValue('isEditing', value)}
      wizardErrors={{
        isEditing: 'You have an unsaved author...',
        authorError: 'Oops! Something is wrong with your authors...',
      }}
    />
  )}
</Formik>
```
