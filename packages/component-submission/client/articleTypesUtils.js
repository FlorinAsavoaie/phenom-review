const articleTypes = {
  editorial: 'Editorial',
  retraction: 'Retraction',
  letterToTheEditor: 'Letter to the Editor',
  expressionOfConcern: 'Expression of Concern',
  erratum: 'Erratum',
  corrigendum: 'Corrigendum',
  caseSeries: 'Case Series',
  caseReport: 'Case Report',
  reviewArticle: 'Review Article',
  researchArticle: 'Research Article',
  commentary: 'Commentary',
}
const shortArticleTypes = [
  articleTypes.editorial,
  articleTypes.retraction,
  articleTypes.letterToTheEditor,
  articleTypes.expressionOfConcern,
  articleTypes.erratum,
  articleTypes.corrigendum,
]
const articleTypesWithLinkedArticle = [
  articleTypes.retraction,
  articleTypes.letterToTheEditor,
  articleTypes.expressionOfConcern,
  articleTypes.erratum,
  articleTypes.corrigendum,
]
const articleTypesEligibleForSpecialIssue = [
  articleTypes.editorial,
  articleTypes.retraction,
  articleTypes.letterToTheEditor,
  articleTypes.expressionOfConcern,
  articleTypes.erratum,
  articleTypes.corrigendum,
  articleTypes.reviewArticle,
  articleTypes.researchArticle,
  articleTypes.commentary,
]

export const checkIfIsShortArticleType = articleTypeLabel =>
  shortArticleTypes.includes(articleTypeLabel)

export const checkIfAllowsLinkedArticle = articleTypeLabel =>
  articleTypesWithLinkedArticle.includes(articleTypeLabel)

export const checkIfCanBeSubmittedToSI = articleTypeLabel =>
  articleTypesEligibleForSpecialIssue.includes(articleTypeLabel)
