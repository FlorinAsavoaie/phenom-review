import gql from 'graphql-tag'

export const deleteManuscript = gql`
  mutation deleteManuscript($manuscriptId: String!) {
    deleteManuscript(manuscriptId: $manuscriptId)
  }
`
export const archiveManuscript = gql`
  mutation archiveManuscript($submissionId: String!) {
    archiveManuscript(submissionId: $submissionId)
  }
`
export const withdrawManuscript = gql`
  mutation withdrawManuscript($submissionId: String!) {
    withdrawManuscript(submissionId: $submissionId)
  }
`
