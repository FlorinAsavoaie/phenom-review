import React from 'react'
import { Button, IconRemove, Spinner } from '@hindawi/phenom-ui'
import {
  Header,
  Collapse,
  Title,
  CheckboxGroup,
  CheckboxGroupWrapper,
  StyledLabel,
} from './styles'
import {
  StatusCategoryT,
  StatusT,
} from '../../../store/EADashboard/filters/statusCategories'

import { FiltersManagerAPII } from '../../../store/EADashboard/filters'

const { Panel } = Collapse

export const StatusFilters: React.FC<{
  filters: FiltersManagerAPII
  statusesCount: any
}> = ({ filters, statusesCount }) => {
  const { statuses } = filters

  const {
    value: statusesCountValue,
    loading: statusesCountLoading,
  } = statusesCount

  const {
    set: updateSelectedStatuses,
    reset,
    value: statusCategories,
  } = statuses

  interface AddCountI {
    status: StatusT
    loading: boolean
    count: number
  }

  interface ResultingStatusI {
    label: string | JSX.Element
    value: string
  }

  const addCount = ({
    status,
    loading,
    count,
  }: AddCountI): ResultingStatusI => {
    const countLabel = loading ? (
      <span className="spinner-wrapper">
        <Spinner />
      </span>
    ) : (
      `${count || 0}`
    )

    return {
      value: status.value,
      label: (
        <StyledLabel>
          <span>{status.label}</span>
          <span className="count">{countLabel}</span>
        </StyledLabel>
      ),
    }
  }

  const countManuscripts = (status: StatusT) => {
    if (status.value.includes(',')) {
      const composedStatuses = status.value.split(',')
      return composedStatuses.reduce(
        (count, currentStatus) =>
          count + (statusesCountValue[currentStatus] || 0),
        0,
      )
    }
    return statusesCountValue[status.value] || 0
  }

  const parseStatuses = (statuses: StatusT[]) =>
    statuses.map(status =>
      addCount({
        status,
        loading: statusesCountLoading,
        count: countManuscripts(status),
      }),
    )

  const handleCheckboxChange = categoryLabel => newValues => {
    updateSelectedStatuses(categoryLabel, newValues)
  }

  return (
    <div>
      <Header>
        <Title>Manuscripts status</Title>
        <Button icon={<IconRemove />} onClick={reset} type="link">
          Reset all
        </Button>
      </Header>
      <Collapse
        accordion
        defaultActiveKey={['In Review']}
        expandIconPosition="right"
      >
        {statusCategories.map((category: StatusCategoryT) => (
          <Panel header={category.label} key={category.label}>
            <CheckboxGroupWrapper direction="vertical" size="small">
              <CheckboxGroup
                onChange={handleCheckboxChange(category.label)}
                options={parseStatuses(category.statuses)}
                value={category.selectedValues}
              />
            </CheckboxGroupWrapper>
          </Panel>
        ))}
      </Collapse>
    </div>
  )
}

export default StatusFilters
