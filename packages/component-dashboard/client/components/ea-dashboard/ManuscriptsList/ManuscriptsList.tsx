import React from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { ManuscriptCard, ManuscriptCardV2 } from '../..'
import { PlaceholderList } from './PlaceholderList'

const Center = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

const renderCard = history => (m, index, arr) => {
  const onClick = () => {
    if (m.status === 'draft') {
      history.push(`/submit/${m.submissionId}/${m.id}`)
    } else {
      history.push(`/details/${m.submissionId}/${m.id}`)
    }
  }

  return (
    <ManuscriptCardV2
      academicEditor={m.academicEditor}
      data-test-id="row"
      isLast={index === arr.length - 1}
      key={m.id}
      manuscript={m}
      onClick={onClick}
      role={m.role}
    />
  )
}

interface ManuscriptsListProps {
  limit?: number
  loading: boolean
  manuscripts: Array<any>
}

export const ManuscriptsList: React.FC<ManuscriptsListProps> = ({
  limit = 10,
  loading,
  manuscripts,
}) => {
  const history = useHistory()

  if (loading) {
    return <PlaceholderList limit={limit} />
  }

  if (manuscripts.length === 0) {
    return <Center>No manuscripts found.</Center>
  }
  const cards = manuscripts.map(renderCard(history)).slice(0, limit)

  return <div>{cards}</div>
}
