import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withRouter } from 'react-router'
import { th } from '@pubsweet/ui-toolkit'
import { H3, H4, DateParser } from '@pubsweet/ui'
import {
  Row,
  Text,
  Icon,
  roles,
  StatusTag,
  TextTooltip,
  AuthorTagList,
  ReviewerBreakdown,
} from '@hindawi/ui'
import { compose, setDisplayName, withProps, withHandlers } from 'recompose'

import withAcademicEditorStatus from './withAcademicEditorStatus'

const ManuscriptCard = ({
  onClick,
  journalName,
  manuscript: {
    meta,
    status,
    created,
    version,
    customId,
    visibleStatus,
    authors = [],
    id: manuscriptId,
    reviewers = [],
    articleType = {},
    researchIntegrityPublishingEditor,
  },
  manuscript,
  isLast,
  academicEditorLabel,
  canSeeReviewerReports,
  academicEditorVisibleStatus,
  researchIntegrityPublishingEditorName,
  statusColor,
  ...rest
}) => {
  const title = meta.title || 'No title'
  const hasPeerReview = get(manuscript, 'articleType.hasPeerReview', '')
  return (
    <Root
      data-test-id={`manuscript-${manuscriptId}`}
      isLast={isLast}
      onClick={onClick}
    >
      <MainContainer>
        <Row alignItems="center" justify="space-between">
          <TextTooltip overflow="hidden" title={title}>
            <H3>{title}</H3>
          </TextTooltip>
          <StatusTag
            hasVersion={hasPeerReview}
            statusColor={statusColor}
            version={version}
          >
            {visibleStatus}
          </StatusTag>
        </Row>
        {authors && authors.length > 0 && (
          <Row alignItems="center" justify="flex-start" mb={2}>
            <AuthorTagList authors={authors} withTooltip />
          </Row>
        )}
        <Row alignItems="center" justify="flex-start">
          {customId && !!articleType && (
            <Text customId mr={2}>{`ID ${customId}`}</Text>
          )}
          {created && status !== 'draft' && (
            <DateParser humanizeThreshold={0} timestamp={created}>
              {(timestamp, timeAgo) => (
                <Text
                  mr={6}
                >{`Submitted on ${timestamp} (${timeAgo} ago)`}</Text>
              )}
            </DateParser>
          )}
          {!!articleType && (
            <Fragment>
              <Text primary>{articleType.name}</Text>
              <Text journal={!!journalName} ml={2}>
                {journalName}
              </Text>
            </Fragment>
          )}
        </Row>
        <Row alignItems="center" height={8} justify="flex-start">
          {researchIntegrityPublishingEditor ? (
            <Fragment>
              <H4>Research Integrity Publishing Editor</H4>
              <Text ml={2} mr={6}>
                {researchIntegrityPublishingEditorName}
              </Text>
            </Fragment>
          ) : (
            journalName && (
              <Fragment>
                <H4>{academicEditorLabel}</H4>
                <Text display="flex" ml={2} mr={6} whiteSpace="nowrap">
                  {academicEditorVisibleStatus}
                </Text>
              </Fragment>
            )
          )}

          {canSeeReviewerReports && (
            <Fragment>
              <H4 mr={1}>Reviewer Reports</H4>
              <ReviewerBreakdown reviewers={reviewers} />
            </Fragment>
          )}
        </Row>
      </MainContainer>
      <SideNavigation>
        <Icon color="colorSecondary" icon="caretRight" />
      </SideNavigation>
    </Root>
  )
}
export default compose(
  withRouter,
  withAcademicEditorStatus,
  withProps(
    ({
      manuscript: {
        role,
        reviewers,
        researchIntegrityPublishingEditor,
        statusColor,
      },
    }) => ({
      statusColor,
      canSeeReviewerReports:
        reviewers &&
        reviewers.length > 0 &&
        ![roles.AUTHOR, roles.REVIEWER].includes(role),
      researchIntegrityPublishingEditorName: `${get(
        researchIntegrityPublishingEditor,
        'alias.name.givenNames',
      )} ${get(researchIntegrityPublishingEditor, 'alias.name.surname')}`,
    }),
  ),
  withProps(({ manuscript }) => ({
    journalName: get(manuscript, 'journal.name', ''),
    academicEditorLabel: get(manuscript, 'peerReviewModel.academicEditorLabel'),
  })),
  withHandlers({
    onClick: ({ manuscript, history, ...rest }) => () => {
      if (manuscript.status === 'draft') {
        history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
      } else {
        history.push(`/details/${manuscript.submissionId}/${manuscript.id}`)
      }
    },
  }),
  setDisplayName('ManuscriptCard'),
)(ManuscriptCard)
// #region styles

const teamMember = PropTypes.shape({
  id: PropTypes.string,
  isSubmitting: PropTypes.bool,
  isCorresponding: PropTypes.bool,
  alias: PropTypes.shape({
    aff: PropTypes.string,
    email: PropTypes.string,
    country: PropTypes.string,
    name: PropTypes.shape({
      surname: PropTypes.string,
      givenNames: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
})

ManuscriptCard.propTypes = {
  manuscript: PropTypes.shape({
    id: PropTypes.string,
    role: PropTypes.string,
    created: PropTypes.string,
    customId: PropTypes.string,
    visibleStatus: PropTypes.string,
    meta: PropTypes.shape({
      agreeTc: PropTypes.bool,
      title: PropTypes.string,
      abstract: PropTypes.string,
      articleType: PropTypes.string,
    }),
    academicEditor: teamMember,
    authors: PropTypes.arrayOf(teamMember),
  }),
}

ManuscriptCard.defaultProps = {
  manuscript: {},
}

// #region styles
const MainContainer = styled.div`
  justify-content: flex-start;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: ${th('gridUnit')};
  width: calc(100% - (${th('gridUnit')} * 5));
  overflow: hidden;
  ${Row} {
    [data-tooltipped] {
      overflow: hidden;
    }
    ${H4} {
      white-space: nowrap;
    }
    ${H3} {
      display: block;
      margin-bottom: 0;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      padding-right: calc(${th('gridUnit')} * 2);
    }
  }
`
const SideNavigation = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue2')
    ? th('colorBackgroundHue2')
    : th('colorBackgroundHue')};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  display: flex;
  justify-content: center;
  width: calc(${th('gridUnit')} * 6);
`

const Root = styled.div`
  background-color: #fff;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  cursor: pointer;
  display: flex;
  margin: calc(${th('gridUnit')} / 2) calc(${th('gridUnit')} / 2)
    calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} / 2);
  width: 100%;
  ${H3} {
    margin: 0;
    margin-bottom: calc(${th('gridUnit')} * 2);
  }

  &:hover {
    box-shadow: ${th('dashboardCard.hoverShadow')};
  }

  margin-bottom: calc(${props => (props.isLast ? 6 : 2)} * ${th('gridUnit')});
`
// #endregion
