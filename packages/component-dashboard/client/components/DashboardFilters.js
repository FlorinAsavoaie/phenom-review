import React, { useState, Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'
import { Row, Item, Label, Menu, Icon } from '@hindawi/ui'

const DashboardFilters = ({
  values,
  options,
  changeSort,
  searchValue,
  changeStatus,
  genericRoles,
  goToFirstPage,
  setSearchValue,
  changeManuscriptProperty,
  fetchManuscripts,
}) => {
  const [inputValue, setInputValue] = useState(searchValue)
  const handleOnChange = event => setInputValue(event.target.value)
  const handleOnSearch = value => {
    setSearchValue(value)
    goToFirstPage()
    fetchManuscripts({ search: value })
  }
  const clearSearch = () => {
    setInputValue('')
    handleOnSearch('')
  }

  const searchPlaceholderText =
    values.manuscriptProperty &&
    options.manuscriptProperty.find(
      property => property.value === values.manuscriptProperty,
    ).label

  return (
    <Root>
      <Row alignItems="flex-end" justify="flex-start" pl={20} pt={4}>
        <Fragment>
          <Item
            alignItems="flex-start"
            data-test-id="dashboard-filter-manuscript-property"
            flex={0}
            vertical
          >
            <Menu
              borderBottomRightRadius={0}
              borderTopRightRadius={0}
              inline
              onChange={manuscriptProperty => {
                changeManuscriptProperty(manuscriptProperty)
              }}
              options={options.manuscriptProperty}
              placeholder="Search By"
              value={values.manuscriptProperty}
              width={33}
            />
          </Item>

          <InputWrapper>
            <Input
              disabled={!values.manuscriptProperty}
              minLength={3}
              name="searchFilter"
              onChange={handleOnChange}
              onKeyPress={e => e.key === 'Enter' && handleOnSearch(inputValue)}
              placeholder={
                values.manuscriptProperty
                  ? `Type ${searchPlaceholderText}...`
                  : `Select search option first`
              }
              value={inputValue}
            />
            {searchValue && (
              <StyledIcon fontSize="16px" icon="remove" onClick={clearSearch} />
            )}
          </InputWrapper>
        </Fragment>

        <Item
          alignItems="flex-start"
          data-test-id="dashboard-filter-status"
          flex={0}
          ml={2}
          mr={2}
          vertical
        >
          <Label>Status</Label>
          <Menu
            inline
            onChange={status => {
              changeStatus(status)
              goToFirstPage()
            }}
            options={options.status}
            placeholder="Please select"
            value={values.status}
            width={48}
          />
        </Item>

        <Item
          alignItems="flex-start"
          data-test-id="dashboard-filter-order"
          flex={0}
          vertical
        >
          <Label>Order</Label>
          <Menu
            inline
            onChange={changeSort}
            options={options.sort}
            placeholder="Please select"
            value={values.sort}
            width={40}
          />
        </Item>
        <SearchButton
          disabled={!values.manuscriptProperty}
          invert
          ml={2}
          mr={4}
          onClick={() => handleOnSearch(inputValue)}
          secondary
          xs
        >
          Search
        </SearchButton>
      </Row>
    </Root>
  )
}

export default DashboardFilters

const Root = styled.div`
  background-color: ${th('backgroundColor')};
`
const SearchButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  min-width: calc(${th('gridUnit')} * 17);
`

const InputWrapper = styled.div`
  position: relative;
`

const Input = styled.input`
  box-sizing: border-box;
  height: calc(${th('gridUnit')} * 8);
  width: calc(${th('gridUnit')} * 121);
  padding: 0;
  padding-left: calc(${th('gridUnit')} * 2);
  padding-right: calc(${th('gridUnit')} * 2);

  border-width: ${th('borderWidth')};
  border-style: ${th('borderStyle')};
  border-color: ${th('colorBorder')};
  border-radius: 0 ${th('borderRadius')} ${th('borderRadius')} 0;

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  color: th('textPrimaryColor')};
  ::placeholder {
    color: th('textPrimaryColor')};
    opacity: 1;
    font-family: ${th('defaultFont')};
  }
  :focus {
    outline: none;
    border-color: ${th('action.colorActive')};
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;

  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`
