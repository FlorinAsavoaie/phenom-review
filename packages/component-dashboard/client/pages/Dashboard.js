/* eslint-disable sonarjs/no-duplicate-string */
import React, { Fragment, useEffect, useState } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { useLazyQuery } from 'react-apollo'
import { Text, Row, Item, Pagination } from '@hindawi/ui'
import { useKeycloakRole } from 'component-sso/client'
import { Redirect } from 'react-router'
import { useCurrentUser } from 'component-authentication/client'

import { getManuscripts } from '../graphql/queries'
import {
  DashboardFilters,
  ManuscriptCard,
  usePagination,
  useFilters,
} from '../components'

const ITEMS_PER_PAGE = 10

const Dashboard = () => {
  const currentUser = useCurrentUser()
  const { hasAdminRole, hasEARole } = useKeycloakRole()
  const [
    getManuscriptsMutation,
    { data, loading, called },
  ] = useLazyQuery(getManuscripts, { fetchPolicy: 'network-only' })

  const [searchValue, setSearchValue] = useState('')
  const [isFirstFetch, setIsFirstFetch] = useState(true)
  const currentUserRole = get(currentUser, 'role')
  const manuscripts = get(data, 'getManuscripts.manuscripts', [])
  const {
    nextPage,
    page,
    prevPage,
    setPage,
    toFirst,
    toLast,
    pageSize,
  } = usePagination(ITEMS_PER_PAGE, get(data, 'getManuscripts.totalCount', 0))
  const statuses = get(data, 'getManuscripts.statuses', [])
  const [
    values,
    options,
    changeSort,
    changeStatus,
    changeManuscriptProperty,
  ] = useFilters(statuses)

  const fetchManuscripts = ({ search }) => {
    if (currentUserRole === 'admin' && isFirstFetch) {
      setIsFirstFetch(false)
      return
    }

    getManuscriptsMutation({
      variables: {
        input: {
          searchValue: search || searchValue,
          statusFilter: get(values, 'status', 'all'),
          dateOrder: get(values, 'sort', 'desc'),
          manuscriptPropertyFilter: get(values, 'manuscriptProperty', ''),
          page,
          pageSize,
        },
      },
    })
  }

  useEffect(() => {
    const t = setTimeout(() => {
      fetchManuscripts({})
    }, 0)

    return () => {
      clearTimeout(t)
    }
  }, [page])

  if (hasAdminRole()) {
    return <Redirect push to="/admin-dashboard" />
  }

  if (hasEARole()) {
    return <Redirect push to="/ea-dashboard" />
  }

  return (
    <Fragment>
      <DashboardFilters
        changeManuscriptProperty={changeManuscriptProperty}
        changeSort={changeSort}
        changeStatus={changeStatus}
        fetchManuscripts={fetchManuscripts}
        goToFirstPage={toFirst}
        options={options}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        values={values}
      />
      <Root data-test-id="dashboard-list-items">
        {renderCards({
          data,
          noOfManuscripts: manuscripts.length,
          loading,
          called,
        })}
        {get(data, 'getManuscripts.totalCount', 0) > ITEMS_PER_PAGE ? (
          <Item justify="flex-end" mb={2}>
            <Pagination
              itemsPerPage={ITEMS_PER_PAGE}
              nextPage={nextPage}
              page={page}
              prevPage={prevPage}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={get(data, 'getManuscripts.totalCount', 0)}
            />
          </Item>
        ) : null}
      </Root>
    </Fragment>
  )
}

const renderCards = ({ data, noOfManuscripts, loading, called }) => {
  if (!called) {
    return (
      <Row mt={15}>
        <StyledText>
          Please use the Search bar to search for a manuscript...
        </StyledText>
      </Row>
    )
  }
  if (noOfManuscripts === 0 && loading)
    return (
      <Row mt={15}>
        <Spinner mt={4} size={4} />
      </Row>
    )
  if (noOfManuscripts === 0 && called) {
    return (
      <Row mt={15}>
        <StyledText>No results found</StyledText>
      </Row>
    )
  }

  return get(data, 'getManuscripts.manuscripts', []).map((m, index) => (
    <ManuscriptCard
      academicEditor={m.academicEditor}
      data-test-id="row"
      isLast={index === get(data, 'getManuscripts.manuscripts', []).length - 1}
      key={m.id}
      manuscript={m}
      role={m.role}
    />
  ))
}

export default Dashboard

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 8);
`

const StyledText = styled(Text)`
  font-weight: 700;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`
// #endregion
