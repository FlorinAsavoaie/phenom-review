import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Col } from '@hindawi/phenom-ui'
import { SearchManuscriptsWidget } from '../components'

import { DashboardProvider } from '../store/EADashboard'
import { searchManuscriptInputResolverI } from '../store/EADashboard/manuscripts'
import { ManagerApiI, connect } from '../store/'

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 8);

  .ant-row {
    margin-bottom: 12px;
  }
`

// #endregion

export interface FiltersPropsI {
  manuscripts: ManagerApiI<any, searchManuscriptInputResolverI>
}

export const EaDashboard: React.FC = () => (
  <Root>
    <Row gutter={[12, 12]}>
      <Col span={24}>
        <SearchManuscriptsWidget />
      </Col>
    </Row>
  </Root>
)

export default connect(DashboardProvider, EaDashboard)
