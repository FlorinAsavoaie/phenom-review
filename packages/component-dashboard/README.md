# EA Dashboard

The new dashboard for Editorial Assistants is implemented as a separate page that can be found in this module in `client > pages > EADashboard.js` .

This page uses a new, and improved, query based on `findQuery` function from `objection-find` package. To this extent we added a new method on the `manuscript` model class called `getManuscriptsV2`.

A new use-case was also created, `getManuscriptsV2.ts` . This use case is used in the `resolvers` in the `Query: { ... }` section.

The way we designed this query to work implies that the client will send params like:

#### query specific parameters:

* academicEditorId _(optional)_
* triageEditorId _(optional)_
* reviewerId _(optional)_
* authorId _(optional)_
* editorialAssistantId _(optional, but it is needed for validation that the user requestiong data is the one the data belongs to)_
* editorialAssistantStatus _(optional)_ 
* isLatestVersion: brings only latest version of a submission _(optional)_
* customId: brings the manuscripts with this specific customId _(optional)_
* title: brings the manuscripts with this specific title _(optional)_
* journalId: brings the manuscripts belonging to a journal with this specific id _(optional)_

#### objection-find query parameters:

* count: counts by an optional column _(optional)_
* rangeStart: range starting index _(optional)_
* rangeEnd: rande ending index _(optional)_
* groupBy: self explanatory _(optional)_
* eager: relations to eager load _(optional)_
* orderBy: self explanatory _(optional)_
* orderByDesc: self explanatory _(optional)_
* join: join and fetch eagerly (when no relation) _(optional)_

more details about objection-find here: https://github.com/Vincit/objection-find

Then we'll map them to things like `academicEditor.userId` so that `objection-find` can understand what we are looking for and create the query for us.


This use case is using an authSome policiy `referrerIsSameAsOwner` that checks if the user that is making the request asks for manuscripts on his behalf.


## Query inout example:

```
  {
    "input": {
      "isLatestVersion": true,
      "editorialAssistantStatus": "active",
      "title": "whatever",
      "editorialAssistantId": "4784e168-4897-40c4-b675-4aebed6ee445",
      "rangeStart": 0,
      "rangeEnd": 10,
      "eager": "[articleType, teams.members, journal.peerReviewModel, specialIssue.peerReviewModel]"
    }
  }
```