const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  recommendation_accept: 'ManuscriptWithdrawn',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyManuscriptMembers: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(() => {}),
}
const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { withdrawManuscriptUseCase } = require('../src/use-cases')

const { Journal, Team, Manuscript, PeerReviewModel, TeamMember } = models

describe('Withdraw manuscript use case', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })
  it('Withdraws a submitted manuscript as admin', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    const admin = await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.submitted,
        journalId: journal.id,
      },
      Manuscript,
    })

    await withdrawManuscriptUseCase
      .initialize({
        models,
        logEvent,
        eventsService,
        notificationService,
        jobsService,
      })
      .execute({ submissionId: manuscript.submissionId, userId: admin.userId })

    expect(manuscript.status).toEqual(Manuscript.Statuses.withdrawn)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
  })
  it('Withdraws a manuscript as author and cancels Academic Editor and Reviewers jobs', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.submitted,
        journalId: journal.id,
      },
      Manuscript,
    })
    const author = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true, isCorresponding: false },
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    await withdrawManuscriptUseCase
      .initialize({
        models,
        logEvent,
        eventsService,
        jobsService,
        notificationService,
      })
      .execute({ submissionId: manuscript.submissionId, userId: author.userId })

    expect(jobsService.cancelJobs).toHaveBeenCalledTimes(1)
  })
  it('Should throw error if manuscript is draft', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    const admin = await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
        journalId: journal.id,
      },
      Manuscript,
    })

    const result = withdrawManuscriptUseCase
      .initialize({
        models,
        logEvent,
        eventsService,
        notificationService,
        jobsService,
      })
      .execute({ submissionId: manuscript.submissionId, userId: admin.userId })

    return expect(result).rejects.toThrow('Manuscript cannot be withdrawn')
  })
})
