const querystring = require('querystring')

module.exports = {
  getBaseUrl(req) {
    return `${req.protocol}://${req.get('host')}`
  },
  createUrl({ baseUrl, slug, queryParams = null }) {
    return !queryParams
      ? `${baseUrl}${slug}`
      : `${baseUrl}${slug}?${querystring.encode(queryParams)}`
  },
}
