const { get } = require('lodash')

const config = require('config')

const bccAddress = config.get('bccAddress')
const initialize = ({
  baseUrl,
  urlService,
  footerText,
  unsubscribeSlug,
  getModifiedText,
}) => ({
  getProps({
    toUser,
    subject,
    paragraph,
    fromEmail,
    journalName,
    signatureName,
  }) {
    return {
      fromEmail,
      type: 'user',
      bcc: bccAddress,
      templateType: 'notification',
      toUser: {
        email: get(toUser, 'alias.email', ''),
        name: get(toUser, 'alias.surname', ''),
      },
      content: {
        subject,
        paragraph,
        signatureName,
        signatureJournal: journalName,
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: toUser.alias.email,
        }),
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: toUser.userId,
            token: get(toUser, 'user.unsubscribeToken'),
          },
        }),
      },
    }
  },
})

module.exports = { initialize }
