export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getSpecialIssuesUseCase } = useCases
    return getSpecialIssuesUseCase.initialize(models).execute({ input, userId })
  },
})

export const authsomePolicies = ['isAuthenticated']
