const HindawiBaseModel = require('./server/dist/hindawiBaseModel')
const typeDefs = require('./server/typeDefs')
const resolvers = require('./server/dist/resolvers')
const useCases = require('./server/dist/useCases')
const repos = require('./server/dist/repos')

module.exports = {
  HindawiBaseModel,
  typeDefs,
  models: [
    { model: require('./server/dist/model/file').File, modelName: 'File' },
    {
      model: require('./server/dist/model/articleType').ArticleType,
      modelName: 'ArticleType',
    },
    {
      model: require('./server/dist/model/comment').Comment,
      modelName: 'Comment',
    },
    { model: require('./server/dist/model/job').Job, modelName: 'Job' },
    {
      model: require('./server/dist/model/journalArticleType')
        .JournalArticleType,
      modelName: 'JournalArticleType',
      modelLoaders: require('./server/dist/loaders/journal'),
    },
    {
      model: require('./server/dist/model/review').Review,
      modelName: 'Review',
    },
    { model: require('./server/dist/model/team').Team, modelName: 'Team' },
    {
      model: require('./server/dist/model/identity').Identity,
      modelName: 'Identity',
    },
    {
      model: require('./server/dist/model/journal').Journal,
      modelName: 'Journal',
      modelLoaders: require('./server/dist/loaders/journal'),
    },
    {
      model: require('./server/dist/model/sourceJournal').SourceJournal,
      modelName: 'SourceJournal',
    },
    {
      model: require('./server/dist/model/availableCustomIds')
        .AvailableCustomIds,
      modelName: 'AvailableCustomIds',
    },
    {
      modelName: 'Manuscript',
      model: require('./server/dist/model/manuscript').Manuscript,
      modelLoaders: require('./server/dist/loaders/manuscript'),
    },
    {
      modelName: 'TeamMember',
      model: require('./server/dist/model/teamMember').TeamMember,
      modelLoaders: require('./server/dist/loaders/teamMember'),
    },
    {
      modelName: 'User',
      model: require('./server/dist/model/user').User,
    },
    {
      model: require('./server/dist/model/reviewerSuggestion')
        .ReviewerSuggestion,
      modelName: 'ReviewerSuggestion',
    },
    {
      model: require('./server/dist/model/auditLog').AuditLog,
      modelName: 'AuditLog',
    },
    {
      model: require('./server/dist/model/peerReviewModel').PeerReviewModel,
      modelName: 'PeerReviewModel',
    },
    {
      model: require('./server/dist/model/section').Section,
      modelName: 'Section',
    },
    {
      model: require('./server/dist/model/specialIssue').SpecialIssue,
      modelName: 'SpecialIssue',
      modelLoaders: require('./server/dist/loaders/specialIssue'),
    },
    {
      model: require('./server/dist/model/editorSuggestion').EditorSuggestion,
      modelName: 'EditorSuggestion',
    },
    {
      model: require('./server/dist/model/rejectDecisionInfo')
        .RejectDecisionInfo,
      modelName: 'RejectDecisionInfo',
    },
    {
      model: require('./server/dist/model/peerReviewEditorialMapping')
        .PeerReviewEditorialMapping,
      modelName: 'PeerReviewEditorialMapping',
    },
  ],
  resolvers,
  useCases,
  repos,
}
