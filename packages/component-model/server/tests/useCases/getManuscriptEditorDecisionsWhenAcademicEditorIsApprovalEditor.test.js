const {
  getTeamRoles,
  getRecommendations,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const {
  getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Review: {
    Recommendations: getRecommendations(),
    findAllValidAndSubmittedByManuscriptAndRole: jest.fn(),
    findAllValidAndSubmitedBySubmissionAndRole: jest.fn(),
    findLatestValidAndSubmittedReviewBySubmissionAndRoles: jest.fn(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findOneByUserAndRoles: jest.fn(),
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
}

const { Review, TeamMember } = models

describe('Get manuscript editor decisions when academic editor is approval editor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should return publish, reject, minor and major when reviewer reports exist and academic editor is assigned ', async () => {
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue([{ id: 'ae-team-member-id' }])
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue()
    const manuscript = {}
    const submissionReviewerReports = [{ id: 'review-id' }]

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })
    expect(decisions).toEqual(['publish', 'minor', 'major', 'reject'])
  })
  it('should return minor, major and reject when there are reviewers reports but there is no AE', async () => {
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue()
    const manuscript = {}
    const submissionReviewerReports = [{ id: 'review-id' }]

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })
    expect(decisions).toEqual(['minor', 'major', 'reject'])
  })
  it('should return minor,major and reject when no reports exist, AE is pending and the user is neither Admin or EA', async () => {
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ id: 'ae-team-member-id' })
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue()
    const manuscript = {}
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })
    expect(decisions).toEqual(['minor', 'major', 'reject'])
  })
  it('should return reject when no reports exist, AE is pending or accepted, and the user is Admin or EA', async () => {
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findOneByUserAndRoles')
      .mockResolvedValue({ id: 'admin-ea-team-member-id' })
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ id: 'ae-team-member-id' })
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue()
    const manuscript = {}
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['reject'])
  })
  it('should return minor,major and reject when no reports exist, AE is neither pending nor accepted, and the user is Admin or EA', async () => {
    jest
      .spyOn(TeamMember, 'findOneByUserAndRoles')
      .mockResolvedValue({ id: 'admin-ea-team-member-id' })
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValue([])
    jest
      // no pending/accepted AE
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue()
    const manuscript = {}
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['minor', 'major', 'reject'])
  })
  it('should return reject when no reports exist, there is a pending or accepted reviewer, and the latest editorial recommendation type was a major revision', async () => {
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ id: 'reviewer-team-member-id' })
    jest
      .spyOn(Review, 'findLatestValidAndSubmittedReviewBySubmissionAndRoles')
      .mockResolvedValue({ id: 'review-id', recommendation: 'major' })
    const manuscript = {}
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['reject'])
  })
})
