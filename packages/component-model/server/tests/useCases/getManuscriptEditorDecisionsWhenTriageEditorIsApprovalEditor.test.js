const {
  getTeamRoles,
  getRecommendations,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const {
  getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Review: {
    Recommendations: getRecommendations(),
    findAllValidByManuscriptAndRole: jest.fn(),
    findAllValidAndSubmitedBySubmissionAndRole: jest.fn(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findOneByManuscriptAndRoleAndStatuses: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript: {
    Statuses: getManuscriptStatuses(),
  },
}

const { Review, TeamMember } = models

describe('Get manuscript editor decisions when triage editor is approval editor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('should return no options when a major revision recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.major },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return no options when a minor revision recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.minor },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual([])
  })
  it('should return publish, reject and returnToAcademicEditor when publish recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.publish },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'returnToAcademicEditor', 'reject'])
  })
  it('should return publish, reject and returnToAcademicEditor when reject recommendation exists', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([
        { id: 'review-id', recommendation: Review.Recommendations.reject },
      ])
    const manuscript = {}

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript })

    expect(decisions).toEqual(['publish', 'returnToAcademicEditor', 'reject'])
  })
  it('should return publish, reject,minor and major when no recommendations exist and reviewer reports exist', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])

    const manuscript = {}
    const submissionReviewerReports = [{ id: 'review-id' }]

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['publish', 'minor', 'major', 'reject'])
  })
  it('should return reject when no recommendations exist, no reports exist and AE is pending or accepted', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue({ id: 'academic-editor-team-member-id' })
    const manuscript = {}
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['reject'])
  })
  it('should return minor,major and reject when no recommendations exists, no reports exist and AE is neither pending nor accepted', async () => {
    jest
      .spyOn(Review, 'findAllValidByManuscriptAndRole')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatuses')
      .mockResolvedValue()
    const manuscript = { articleType: { hasPeerReview: true } }
    const submissionReviewerReports = []

    const decisions = await getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase
      .initialize({ models })
      .execute({ manuscript, submissionReviewerReports })

    expect(decisions).toEqual(['minor', 'major', 'reject'])
  })
})
