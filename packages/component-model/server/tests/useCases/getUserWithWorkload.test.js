const { getUserWithWorkloadUseCase } = require('../../dist/useCases')

const mockTeamMember = {
  findAllByManuscriptParentAndRole: jest.fn().mockReturnValue([]),
  findAllWithWorkloadByManuscriptParent: jest.fn().mockReturnValue([]),
}

const mockModels = {
  TeamMember: mockTeamMember,
}

const setupTestParams = (overrides = {}) => ({
  role: 'academicEditor',
  journalId: 1,
  sectionId: 1,
  specialIssueId: 1,
  manuscriptStatuses: ['', ''],
  teamMemberStatuses: ['', ''],
  models: mockModels,
  ...overrides,
})

describe('Get User for autoassignment based on workload', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  it('should return the user with lowest workload', async () => {
    const params = setupTestParams()
    const {
      findAllByManuscriptParentAndRole,
      findAllWithWorkloadByManuscriptParent,
    } = params.models.TeamMember

    const teamMembers = [
      {
        userId: 1,
      },
      {
        userId: 2,
      },
      {
        userId: 3,
      },
      {
        userId: 4,
      },
      {
        userId: 5,
      },
    ]

    const unorderedUsers = [
      {
        userId: 1,
        workload: 5,
      },
      {
        userId: 2,
        workload: 2,
      },
      {
        userId: 3,
        workload: 3,
      },
      {
        userId: 4,
        workload: 0,
      },
      {
        userId: 5,
        workload: 1,
      },
    ]

    findAllByManuscriptParentAndRole.mockReturnValue(teamMembers)
    findAllWithWorkloadByManuscriptParent.mockReturnValue(unorderedUsers)

    const user = await getUserWithWorkloadUseCase
      .initialize(mockModels)
      .execute(params)

    expect(user).toEqual({
      userId: 4,
      workload: 0,
    })
  })
  it('should return the user with lowest workload who is also present in the team members array', async () => {
    const params = setupTestParams()
    const {
      findAllByManuscriptParentAndRole,
      findAllWithWorkloadByManuscriptParent,
    } = params.models.TeamMember

    const teamMembers = [
      {
        userId: 1,
      },
      {
        userId: 2,
      },
      {
        userId: 3,
      },
      {
        userId: 5,
      },
    ]

    const unorderedUsers = [
      {
        userId: 1,
        workload: 5,
      },
      {
        userId: 2,
        workload: 2,
      },
      {
        userId: 3,
        workload: 3,
      },
      {
        userId: 4,
        workload: 0,
      },
      {
        userId: 5,
        workload: 1,
      },
    ]

    findAllByManuscriptParentAndRole.mockReturnValue(teamMembers)
    findAllWithWorkloadByManuscriptParent.mockReturnValue(unorderedUsers)

    const user = await getUserWithWorkloadUseCase
      .initialize(mockModels)
      .execute(params)

    expect(user).toEqual({
      userId: 5,
      workload: 1,
    })
  })
})
