import {
  generateTeamMember,
  generateTeam,
  getTeamRoles,
  generateUser,
  getStatusExpiredLabels,
  getTeamMemberStatuses,
} from 'component-generators'

const {
  getReviewerStatusLabelUseCase,
} = require('../../../src/useCases/teamMember')

const mockModels = {
  Team: {
    find: jest.fn(),
    Role: getTeamRoles(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    StatusExpiredLabels: getStatusExpiredLabels(),
  },
  User: {
    find: jest.fn(),
  },
}
const { User, Team, TeamMember } = mockModels

describe('Get reviewer status label use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns early if the team member is not a reviewer', async () => {
    const team = generateTeam({ role: Team.Role.author })
    const teamMember = generateTeamMember()
    jest.spyOn(Team, 'find').mockResolvedValue(team)

    await getReviewerStatusLabelUseCase.initialize(mockModels).execute({
      teamMember,
    })
    expect(Team.find).toHaveBeenCalledTimes(1)
    expect(User.find).not.toHaveBeenCalled()
  })
  it('returns Unsubscribed if the reviewer user is unsubscribed', async () => {
    const team = generateTeam({ role: Team.Role.reviewer })
    const teamMember = generateTeamMember()
    const user = generateUser({ isSubscribedToEmails: false })
    jest.spyOn(Team, 'find').mockResolvedValue(team)
    jest.spyOn(User, 'find').mockResolvedValue(user)

    const label = await getReviewerStatusLabelUseCase
      .initialize(mockModels)
      .execute({
        teamMember,
      })
    expect(Team.find).toHaveBeenCalledTimes(1)
    expect(User.find).toHaveBeenCalledTimes(1)
    expect(label).toEqual(TeamMember.StatusExpiredLabels.unsubscribed)
  })
  it('returns the reviewer status if the reviewer is not expired', async () => {
    const team = generateTeam({ role: Team.Role.reviewer })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.accepted,
    })
    const user = generateUser({ isSubscribedToEmails: true })
    jest.spyOn(Team, 'find').mockResolvedValue(team)
    jest.spyOn(User, 'find').mockResolvedValue(user)

    const label = await getReviewerStatusLabelUseCase
      .initialize(mockModels)
      .execute({
        teamMember,
      })
    expect(Team.find).toHaveBeenCalledTimes(1)
    expect(User.find).toHaveBeenCalledTimes(1)
    expect(label).toEqual(TeamMember.Statuses.accepted.toUpperCase())
  })
  it('returns Overdue if the reviewer has expired after he responded', async () => {
    const team = generateTeam({ role: Team.Role.reviewer })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.expired,
      responded: new Date(),
    })
    const user = generateUser({ isSubscribedToEmails: true })
    jest.spyOn(Team, 'find').mockResolvedValue(team)
    jest.spyOn(User, 'find').mockResolvedValue(user)

    const label = await getReviewerStatusLabelUseCase
      .initialize(mockModels)
      .execute({
        teamMember,
      })
    expect(Team.find).toHaveBeenCalledTimes(1)
    expect(User.find).toHaveBeenCalledTimes(1)
    expect(label).toEqual(TeamMember.StatusExpiredLabels.overdue)
  })
  it('returns Unresponsive if the reviewer has expired without responding', async () => {
    const team = generateTeam({ role: Team.Role.reviewer })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.expired,
      responded: null,
    })
    const user = generateUser({ isSubscribedToEmails: true })
    jest.spyOn(Team, 'find').mockResolvedValue(team)
    jest.spyOn(User, 'find').mockResolvedValue(user)

    const label = await getReviewerStatusLabelUseCase
      .initialize(mockModels)
      .execute({
        teamMember,
      })
    expect(Team.find).toHaveBeenCalledTimes(1)
    expect(User.find).toHaveBeenCalledTimes(1)
    expect(label).toEqual(TeamMember.StatusExpiredLabels.unresponsive)
  })
})
