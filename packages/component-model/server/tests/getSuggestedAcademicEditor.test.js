const {
  TeamMember,
  EditorSuggestion,
  Manuscript,
  Team,
} = require('@pubsweet/models')
const { getSuggestedAcademicEditorUseCase } = require('../dist/useCases')

describe('Get Suggested Academic Editor', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.mock('component-quality-check', () => ({
      services: {
        ConflictOfInterest: { getConflictsForTeamMembers: jest.fn(() => []) },
      },
    }))
  })
  it('should retrieve first available user based on score, workload and ROR conflicts', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
        alias: { affRorId: '02rt9z237' },
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
        alias: { affRorId: '02rt9z500' },
      },
    ])

    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          workload: 1,
          alias: { affRorId: '02rt9z237' },
        },
        {
          userId: 'user-id-2',
          workload: 3,
          alias: { affRorId: '02rt9z500' },
        },
      ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([
      {
        teamMemberId: 'team-member-id-1',
        score: 2,
      },
      {
        teamMemberId: 'team-member-id-2',
        score: 1,
      },
    ])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([{ id: '1', alias: { affRorId: '02rt9z237' } }])

    const user = await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(user).toEqual({
      alias: {
        affRorId: '02rt9z500',
      },
      id: 'team-member-id-2',
      userId: 'user-id-2',
      score: 1,
      workload: 3,
    })
  })
  it('should retrieve first available user based on score, workload and ROR conflicts even if affRORId is empty', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
        alias: { affRorId: '02rt9z237' },
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
        alias: { affRorId: '' },
      },
    ])

    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          workload: 2,
          alias: { affRorId: '02rt9z237' },
        },
        {
          userId: 'user-id-2',
          workload: 1,
          alias: { affRorId: '' },
        },
      ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([
      {
        teamMemberId: 'team-member-id-1',
        score: 1,
      },
      {
        teamMemberId: 'team-member-id-2',
        score: 2,
      },
    ])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([{ id: '1', alias: { affRorId: '' } }])

    const user = await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(user).toEqual({
      alias: {
        affRorId: '',
      },
      id: 'team-member-id-2',
      userId: 'user-id-2',
      score: 2,
      workload: 1,
    })
  })

  it('should retrieve the user based on score and workload', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
        alias: { affRORId: '' },
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
        alias: { affRORId: '' },
      },
      {
        id: 'team-member-id-3',
        userId: 'user-id-3',
        alias: { affRORId: '' },
      },
    ])

    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          workload: 1,
        },
        {
          userId: 'user-id-2',
          workload: 3,
        },
        {
          userId: 'user-id-3',
          workload: 4,
        },
      ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([
      {
        teamMemberId: 'team-member-id-1',
        score: 2,
      },
      {
        teamMemberId: 'team-member-id-2',
        score: 4,
      },
      {
        teamMemberId: 'team-member-id-3',
        score: 3,
      },
    ])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    const user = await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(user).toEqual({
      alias: { affRORId: '' },
      id: 'team-member-id-2',
      userId: 'user-id-2',
      score: 4,
      workload: 3,
    })
  })
  it('should process team members from specialIssue', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([
        {
          id: 'team-member-id-1',
          userId: 'user-id-1',
          alias: { affRORId: '' },
        },
        {
          id: 'team-member-id-2',
          userId: 'user-id-2',
          alias: { affRORId: '' },
        },
      ])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole')
    jest.spyOn(TeamMember, 'findAllByJournalAndRole')
    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValue([
        {
          userId: 'user-id-1',
          workload: 3,
        },
        {
          userId: 'user-id-2',
          workload: 1,
        },
      ])

    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)

    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledWith({
      userIds: ['user-id-1', 'user-id-2'],
      manuscript: {
        id: 'some-manuscript-id',
        submissionId: 'some-submission-id',
      },
      role: 'academicEditor',
      teamMemberStatuses: ['pending', 'accepted'],
    })
  })

  it('should process team members from section', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
        alias: { affRORId: '' },
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
        alias: { affRORId: '' },
      },
    ])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole')
    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValue([
        {
          userId: 'user-id-1',
          workload: 3,
        },
        {
          userId: 'user-id-2',
          workload: 1,
        },
      ])
    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledWith({
      userIds: ['user-id-1', 'user-id-2'],
      manuscript: {
        id: 'some-manuscript-id',
        submissionId: 'some-submission-id',
      },
      role: 'academicEditor',
      teamMemberStatuses: ['pending', 'accepted'],
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
  })

  it('should process team members from journal', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([
      {
        id: 'team-member-id-1',
        userId: 'user-id-1',
        alias: { affRORId: '' },
      },
      {
        id: 'team-member-id-2',
        userId: 'user-id-2',
        alias: { affRORId: '' },
      },
    ])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest
      .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
      .mockResolvedValue([
        {
          userId: 'user-id-1',
          workload: 3,
        },
        {
          userId: 'user-id-2',
          workload: 1,
        },
      ])
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    await getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
    })

    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent,
    ).toHaveBeenCalledWith({
      userIds: ['user-id-1', 'user-id-2'],
      manuscript: {
        id: 'some-manuscript-id',
        submissionId: 'some-submission-id',
      },
      role: 'academicEditor',
      teamMemberStatuses: ['pending', 'accepted'],
    })
  })

  it('should throw an error if no team members are found', async () => {
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValueOnce([])

    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValueOnce([])

    jest.spyOn(EditorSuggestion, 'findBy').mockReturnValueOnce([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest.spyOn(Manuscript, 'find').mockResolvedValue({
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    })
    jest.spyOn(TeamMember, 'findAllBySubmissionAndRole').mockResolvedValue([])

    const result = getSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          TeamMember,
          EditorSuggestion,
          Manuscript,
          Team,
        },
      })
      .execute({
        manuscriptId: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-issue-id',
      })

    await expect(result).rejects.toThrow(
      'No manuscript parent ID has been provided',
    )

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledWith({
      sectionId: 'some-section-id',
      role: Team.Role.academicEditor,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
    })
  })
})
