import { transaction, Transaction } from 'objection'
import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'

const { logger } = require('component-logger')

export interface EditorSuggestionI extends HindawiBaseModelProps {
  manuscriptId: string
  teamMemberId: string
  score: number

  editorId?: unknown
}

export interface EditorSuggestionRepository
  extends HindawiBaseModelRepository<EditorSuggestionI> {
  new (...args: any[]): EditorSuggestionI

  saveAll(entities: EditorSuggestionI[]): Promise<EditorSuggestionI[]>
}

export class EditorSuggestion extends HindawiBaseModel
  implements EditorSuggestionI {
  manuscriptId: string
  teamMemberId: string
  score: number

  editorId?: unknown

  static get tableName() {
    return 'editor_suggestion'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: {
          type: 'string',
          format: 'uuid',
        },
        teamMemberId: {
          type: 'string',
          format: 'uuid',
        },
        score: {
          type: 'real',
        },
      },
    }
  }

  static async saveAll(
    entities: EditorSuggestionI[],
  ): Promise<EditorSuggestion[]> {
    try {
      const savedEntities = await transaction(
        EditorSuggestion.knex(),
        async (trx: Transaction) =>
          EditorSuggestion.query(trx).upsertGraphAndFetch(entities),
      )
      logger.debug(
        `Saved EditorSuggestion with UUIDs ${savedEntities
          .map(savedEntity => savedEntity.id)
          .join(' ')}`,
      )

      return savedEntities
    } catch (err) {
      logger.error('Rolled back EditorSuggestion.saveAll()', err)
      throw err
    }
  }
}
