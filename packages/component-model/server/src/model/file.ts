import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Comment, CommentI } from './comment'
import { Manuscript, ManuscriptI } from './manuscript'

export type FileType =
  | 'figure'
  | 'manuscript'
  | 'supplementary'
  | 'coverLetter'
  | 'reviewComment'
  | 'responseToReviewers'

export interface FileI extends HindawiBaseModelProps {
  type: FileType
  label?: string
  providerKey?: string
  fileName: string
  url?: string
  mimeType: string
  size: number
  originalName: string
  manuscriptId?: string
  commentId?: string
  position?: number

  manuscript?: ManuscriptI
  comment?: CommentI
}

export interface FileRepository extends HindawiBaseModelRepository<FileI> {
  new (...args: any[]): FileI

  findAllByManuscriptAndType(options: {
    manuscriptId: string
    type: FileType
  }): Promise<FileI[]>
  findAllPublicReviewFilesByManuscript(options: {
    manuscriptId: string
  }): Promise<FileI[]>
}

export class File extends HindawiBaseModel implements FileI {
  type: FileType
  label?: string
  providerKey?: string
  fileName: string
  url?: string
  mimeType: string
  size: number
  originalName: string
  manuscriptId?: string
  commentId?: string
  position?: number

  manuscript?: Manuscript
  comment?: Comment

  static get tableName() {
    return 'file'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        type: { enum: Object.values(File.Types) },
        label: { type: ['string', 'null'] },
        providerKey: { type: ['string', 'null'] },
        fileName: { type: 'string' },
        url: { type: ['string', 'null'] },
        mimeType: { type: 'string' },
        size: { type: 'number' },
        originalName: { type: 'string' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        commentId: { type: ['string', 'null'], format: 'uuid' },
        position: { type: ['integer', null] },
        scanStatus: {
          enum: Object.values(this.ScanStatuses),
          default: this.ScanStatuses.SKIPPED,
        },
        scanningStartTime: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'file.manuscriptId',
          to: 'manuscript.id',
        },
      },
      comment: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Comment,
        join: {
          from: 'file.commentId',
          to: 'comment.id',
        },
      },
    }
  }

  static get ScanStatuses() {
    return {
      SKIPPED: 'skipped',
      SCANNING: 'scanning',
      HEALTHY: 'healthy',
      INFECTED: 'infected',
      TIMEOUT_PASSED: 'timeoutPassed',
      ERROR: 'error',
    }
  }

  static get Types(): Record<FileType, FileType> {
    return {
      figure: 'figure',
      manuscript: 'manuscript',
      supplementary: 'supplementary',
      coverLetter: 'coverLetter',
      reviewComment: 'reviewComment',
      responseToReviewers: 'responseToReviewers',
    }
  }

  static findAllByManuscript({ manuscriptId }: { manuscriptId: string }) {
    return this.query()
      .alias('f')
      .where('f.manuscript_id', manuscriptId)
  }

  static findAllByManuscriptAndType({
    manuscriptId,
    type,
  }: {
    manuscriptId: string
    type: FileType
  }) {
    return this.findAllByManuscript({ manuscriptId })
      .clone()
      .andWhere('f.type', type)
  }

  static findAllPublicReviewFilesByManuscript({
    manuscriptId,
  }: {
    manuscriptId: string
  }): Promise<File[]> {
    return this.findAllByManuscriptAndType({
      manuscriptId,
      type: this.Types.reviewComment,
    })
      .clone()
      .join('comment AS c', 'c.id', 'f.comment_id')
      .andWhere('c.type', Comment.Types.public)
  }

  toDTO() {
    const { fileName, ...file } = this

    return {
      filename: fileName,
      ...file,
    }
  }
}
