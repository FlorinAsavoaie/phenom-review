import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'

export interface SourceJournalI extends HindawiBaseModelProps {
  name: string
  pissn?: string
  eissn?: string
  publisher?: string

  manuscripts?: ManuscriptI[]
}

export interface SourceJournalRepository
  extends HindawiBaseModelRepository<SourceJournalI> {
  new (...args: any[]): SourceJournalI
}

export class SourceJournal extends HindawiBaseModel {
  name: string
  pissn?: string
  eissn?: string
  publisher?: string

  manuscripts?: ManuscriptI[]

  static get tableName() {
    return 'source_journal'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        pissn: { type: ['string', null] },
        eissn: { type: ['string', null] },
        publisher: { type: ['string', null] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'source_journal.id',
          to: 'manuscript.sourceJournalId',
        },
      },
    }
  }
}
