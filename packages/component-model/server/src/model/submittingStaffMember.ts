import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { User, UserI } from './user'

export interface SubmittingStaffMemberI extends HindawiBaseModelProps {
  userId: string
  email: string
  manuscriptId: string

  manuscripts?: ManuscriptI[]
  users?: UserI[]
}

export interface SubmittingStaffMemberRepository
  extends HindawiBaseModelRepository<SubmittingStaffMemberI> {
  new (...args: any[]): SubmittingStaffMemberI
}

export class SubmittingStaffMember extends HindawiBaseModel
  implements SubmittingStaffMemberI {
  userId: string
  email: string
  manuscriptId: string

  manuscripts?: ManuscriptI[]
  users?: UserI[]

  static get tableName() {
    return 'submitting_staff_member'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        userId: { type: 'string', format: 'uuid' },
        email: { type: 'string', format: 'string' },
        manuscriptId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'submitting_staff_member.manuscriptId',
          to: 'manuscript.id',
        },
      },
      users: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'submitting_staff_member.userId',
          to: 'user.id',
        },
      },
    }
  }
}
