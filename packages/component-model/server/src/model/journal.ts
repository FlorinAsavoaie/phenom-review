import { chain } from 'lodash'
import { Promise } from 'bluebird'
import { ConflictError } from '@pubsweet/errors'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Team, TeamI } from './team'
import {
  EditorialAssistant as EditorialAssistantModel,
  EditorialAssistantI,
} from './editorialAssistant'
import { Manuscript, ManuscriptI } from './manuscript'
import { JournalArticleType, JournalArticleTypeI } from './journalArticleType'
import { ArticleType, ArticleTypeI } from './articleType'
import { Job, JobI, JobRepository } from './job'
import { Section, SectionI } from './section'
import { PeerReviewModel, PeerReviewModelI } from './peerReviewModel'
import { SpecialIssue, SpecialIssueI } from './specialIssue'
import { JournalPreprint, JournalPreprintI } from './journalPreprint'
import { Preprint, PreprintI } from './preprint'
import { TeamMemberI } from './teamMember'

const { logger } = require('component-logger')

const journalIdCondition = 'journal.id'
const journalActiveCondition = 'journal.isActive'

export interface JournalI extends HindawiBaseModelProps {
  name: string
  publisherName?: string
  issn?: string
  code: string
  email: string
  apc?: number
  isActive?: boolean
  activationDate?: string
  peerReviewModelId?: string

  manuscripts?: ManuscriptI[]
  teams?: TeamI[]
  journalArticleTypes?: JournalArticleTypeI[]
  articleTypes?: ArticleTypeI[]
  jobs?: JobI[]
  sections?: SectionI[]
  peerReviewModel?: PeerReviewModelI
  specialIssues?: SpecialIssueI[]
  journalPreprints?: JournalPreprintI[]
  preprints?: PreprintI[]
  editorialAssistant?: EditorialAssistantI

  toDTO(): object
}

export interface JournalRepository
  extends HindawiBaseModelRepository<JournalI> {
  new (...args: any[]): JournalI
}

export class Journal extends HindawiBaseModel implements JournalI {
  name: string
  publisherName?: string
  issn?: string
  code: string
  email: string
  apc?: number
  isActive?: boolean
  activationDate?: string
  peerReviewModelId?: string

  manuscripts?: ManuscriptI[]
  teams?: TeamI[]
  journalArticleTypes?: JournalArticleTypeI[]
  articleTypes?: ArticleTypeI[]
  jobs?: JobI[]
  sections?: SectionI[]
  peerReviewModel?: PeerReviewModelI
  specialIssues?: SpecialIssueI[]
  journalPreprints?: JournalPreprintI[]
  preprints?: PreprintI[]
  editorialAssistant?: EditorialAssistantI

  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        publisherName: { type: ['string', null] },
        issn: { type: ['string', null] },
        code: { type: 'string' },
        email: { type: 'string' },
        apc: { type: ['integer', 'null'] },
        isActive: { type: ['boolean', false] },
        activationDate: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
        peerReviewModelId: { type: ['string', null], format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: journalIdCondition,
          to: 'manuscript.journalId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: journalIdCondition,
          to: 'team.journalId',
        },
      },
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: JournalArticleType,
        join: {
          from: journalIdCondition,
          to: 'journal_article_type.journalId',
        },
      },
      articleTypes: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: ArticleType,
        join: {
          from: journalIdCondition,
          through: {
            from: 'journal_article_type.journalId',
            to: 'journal_article_type.articleTypeId',
          },
          to: 'article_type.id',
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Job,
        join: {
          from: journalIdCondition,
          to: 'job.journalId',
        },
      },
      sections: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Section,
        join: {
          from: journalIdCondition,
          to: 'section.journalId',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: PeerReviewModel,
        join: {
          from: 'journal.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: SpecialIssue,
        join: {
          from: journalIdCondition,
          to: 'special_issue.journalId',
        },
      },
      journalPreprints: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: JournalPreprint,
        join: {
          from: journalIdCondition,
          to: 'journal_preprint.journalId',
        },
      },
      preprints: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: Preprint,
        join: {
          from: journalIdCondition,
          through: {
            from: 'journal_preprint.journal_id',
            to: 'journal_preprint.preprint_id',
          },
          to: 'preprint.id',
        },
      },
      editorialAssistant: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: EditorialAssistantModel,
        join: {
          from: journalIdCondition,
          to: 'editorial_assistant.journalId',
        },
      },
    }
  }

  static findAllByManuscriptIds(manuscriptIds: string[]): Promise<Journal[]> {
    return this.query()
      .select('j.*', 'm.id as manuscriptId')
      .from('journal as j')
      .join('manuscript as m', 'j.id', 'm.journal_id')
      .whereIn('m.id', manuscriptIds)
  }

  static async findAllActiveJournals(): Promise<Journal[]> {
    try {
      return await this.query()
        .select('j.*')
        .from('journal AS j')
        .where('j.isActive', true)
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllActiveByIds(journalIds: string[]): Promise<Journal[]> {
    return this.query()
      .alias('journal')
      .withGraphFetched(
        '[specialIssues(onlyActive), sections.specialIssues(onlyActive), preprints, journalPreprints]',
      )
      .whereIn(journalIdCondition, journalIds)
      .where(journalActiveCondition, true)
      .orderBy('name', 'asc')
  }
  static async findAllWithMultipleEAs(): Promise<Journal[]> {
    return this.query()
      .select('id')
      .from((query: any) =>
        query
          .select('j.id')
          .count('tm.id as numberOfTeamMembers')
          .from({ j: 'journal' })
          .join({ t: 'team' }, 't.journal_id', 'j.id')
          .join({ tm: 'team_member' }, 't.id', 'tm.team_id')
          .where('t.role', 'editorialAssistant')
          .groupBy('j.id')
          .as('nrOfEAs'),
      )
      .where('numberOfTeamMembers', '>', '1')
  }

  static async findJournalByTeamMember(
    teamMemberId: string,
  ): Promise<Journal | undefined> {
    try {
      const results = await this.query()
        .select('j.*')
        .from('journal AS j')
        .join('team AS t', 'j.id', 't.journal_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static getAllActiveJournals(): Promise<Journal[]> {
    return this.query()
      .alias('journal')
      .withGraphFetched(
        '[specialIssues(onlyActive), sections.specialIssues(onlyActive), preprints, journalPreprints]',
      )
      .where(journalActiveCondition, true)
      .orderBy('name', 'asc')
  }

  static getAllActiveJournalsForStaffMembers(): Promise<Journal[]> {
    return this.query()
      .alias('journal')
      .withGraphFetched(
        '[specialIssues, sections.specialIssues, preprints, journalPreprints]',
      )
      .where(journalActiveCondition, true)
      .orderBy('name', 'asc')
  }

  $formatDatabaseJson(json: any) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    if (email) {
      return { ...json, email: email.toLowerCase() }
    }

    return json
  }

  getCorrespondingEditorialAssistant(): TeamMemberI | undefined {
    if (!this.teams) {
      throw new Error('Teams are required')
    }

    return chain(this.teams)
      .find((t: TeamI) => t.role === Team.Role.editorialAssistant)
      .get('members', [])
      .value()
      .find((m: TeamMemberI) => m.isCorresponding === true)
  }

  assignTeam(team: TeamI): void {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  async saveJournalAndJournalArticleTypes(
    articleTypeIds: string[],
  ): Promise<void> {
    try {
      const journalArticleTypes = articleTypeIds.map(
        (articleTypeId: string) => ({
          articleTypeId,
        }),
      )
      this.updateProperties({ journalArticleTypes })
      return await this.saveGraph({ relate: true })
    } catch (err) {
      logger.error('Something went wrong', err)
      if (err.constraint) {
        throw new ConflictError(`Journal already exists.`)
      }
      throw new Error(err)
    }
  }

  scheduleJob(teamMemberId: string, jobsService: any) {
    return jobsService.scheduleJournalActivation({
      journal: this,
      teamMemberId,
    })
  }

  cancelJobs(JobModel: JobRepository) {
    if (!this.jobs) {
      return
    }

    return Promise.each(this.jobs, async (job: any) => {
      await JobModel.cancel(job.id)
    })
  }

  toDTO() {
    return { ...this }
  }
}
