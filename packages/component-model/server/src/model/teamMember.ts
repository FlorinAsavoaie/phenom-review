/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable sonarjs/no-identical-functions */
import { pick, get, chain } from 'lodash'
import Objection, { raw, transaction } from 'objection'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { EditorSuggestion, EditorSuggestionI } from './editorSuggestion'
import { User, UserI } from './user'
import { Team, TeamI, TeamMemberRole, TeamRepository } from './team'
import { Job, JobI } from './job'
import { PeerReviewModelRepository } from './peerReviewModel'
import { ArticleTypeRepository } from './articleType'
import { Models } from '.'
import { AuthorI } from './author'
import { AcademicEditorI } from './academicEditor'

const { logger } = require('component-logger')

export type TeamMemberStatus =
  | 'pending'
  | 'accepted'
  | 'declined'
  | 'submitted'
  | 'expired'
  | 'removed'
  | 'active'
  | 'conflicting'

export interface TeamMemberAlias {
  surname?: string
  givenNames?: string
  email?: string
  aff?: string
  affRorId?: string
  country?: string
  title?: string
}

export interface TeamMemberI extends HindawiBaseModelProps {
  userId: string
  teamId: string
  position?: number
  isSubmitting?: boolean
  isCorresponding?: boolean
  status: TeamMemberStatus
  reviewerNumber?: number
  responded?: string
  alias: TeamMemberAlias
  invitedDate?: string
  declineReason?: string

  user?: UserI
  team?: TeamI
  jobs?: JobI

  getDatesForEvents(options: {
    role: string
    Team: TeamRepository
  }):
    | {
        invitedDate: string
        declinedDate?: string
        acceptedDate?: string
        expiredDate?: string
        assignedDate?: string
        removedDate?: string
      }
    | undefined
  toDTO(): object
}

export interface TeamMemberRepository
  extends HindawiBaseModelRepository<TeamMemberI> {
  new (...args: any[]): TeamMemberI
  findAllByStatuses(options: {
    role: string
    statuses: string[]
    manuscriptId: string
    submissionId: string
  }): Promise<TeamMemberI[]>
  findAllByRoleAndStatusWhereRespondedNull(options: {
    role: string
    status: string
  }): Promise<TeamMemberI[]>
  findOneByManuscriptAndUser({
    userId,
    manuscriptId,
    eagerLoadRelations,
  }: {
    userId: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>
  findOneByUserAndRole(options: {
    userId: string
    role: string
    eagerLoadRelations: string | string[]
  }): Promise<TeamMemberI | undefined>
  findOneByUserAndRole(options: {
    userId: string
    role: string
    eagerLoadRelations: string | string[]
  }): Promise<TeamMemberI | undefined>
  findOneByUserAndRoles(options: {
    userId: string
    roles: string[]
  }): Promise<TeamMemberI | undefined>
  findOneByRole(options: { role: string }): Promise<TeamMemberI | undefined>
  findOneByEmailOnManuscriptParent(
    email: string,
  ): Promise<TeamMemberI | undefined>
  findOneByUserAndRoleAndStatusOnManuscript(options: {
    role: string
    userId: string
    status: string
    manuscriptId: string
  }): Promise<TeamMemberI | undefined>
  findOneByUserAndRoleAndStatus(options: {
    userId: string
    role: string
    status: string
  }): Promise<TeamMemberI | undefined>
  findOneBySubmissionAndUser(options: {
    submissionId: string
    userId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
  }): Promise<TeamMemberI | undefined>
  findSubmittingAuthor(manuscriptId: string): Promise<TeamMemberI | undefined>
  findCorrespondingEditorialAssistant(
    journalId: string,
  ): Promise<TeamMemberI | undefined>
  deleteStaffMemberByUserIdAndRole(options: {
    userId: string
    role: string
  }): Promise<number>
  isApprovalEditor(options: {
    userId: string
    manuscriptId: string
    models: Models
  }): Promise<boolean>
  handleCorrespondingAuthor(options: {
    manuscriptId: string
    editedAuthor: AuthorI
    inputCorresponding: boolean
  }): Promise<void>
  findAllByJournalWithWorkload(options: {
    role: string
    journalId: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<{ id: string; name: string; workload: string }>
  findTriageEditor(options: {
    manuscriptId: string
    sectionId: string
    journalId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>
  isSuggestionsListInSyncWithTeamMembers(
    editorSuggestions: EditorSuggestionI[],
  ): Promise<boolean>
  findTeamMembersWorkloadByJournal(argsObject: {
    journalId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMemberI[]>
  findTeamMembersWorkloadByJournalAndSpecialIssue(argsObject: {
    journalId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMemberI[]>
  findTeamMembersWorkloadBySection(argsObject: {
    sectionId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMember[]>
  findTeamMembersWorkloadBySpecialIssue(argsObject: {
    specialIssueId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMember[]>
  findTeamMemberWorkloadByManuscriptParent(options: {
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
    manuscriptParentId: string
    manuscriptParentType: string
  }): Promise<TeamMember[]>
  findAllByManuscriptAndRole(options: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI[]>
  findOneBySpecialIssueAndUsers({
    userIds,
    specialIssueId,
    eagerLoadRelations,
  }: {
    userIds: string[]
    specialIssueId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>
  findAllBySubmissionAndRole({
    role,
    submissionId,
    eagerLoadRelations,
  }: {
    role: string
    submissionId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI[]>
  findOneByManuscriptAndRoleAndStatus({
    trx,
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    status: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>
  findAllByManuscriptAndRoleAndStatus({
    trx,
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    status: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI[]>
  findOneByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }: {
    role: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>
  findAllBySubmissionAndRoleAndStatuses({
    role,
    statuses,
    submissionId,
    eagerLoadRelations,
  }: {
    role: string
    statuses: string[]
    submissionId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI[]>
  findOneByManuscriptAndRoleAndUser({
    role,
    userId,
    manuscriptId,
    eagerLoadRelations,
  }: {
    role: string
    userId: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined>

  Statuses: Record<TeamMemberStatus, TeamMemberStatus>
}

export class TeamMember extends HindawiBaseModel implements TeamMemberI {
  userId: string
  teamId: string
  position?: number
  isSubmitting?: boolean
  isCorresponding?: boolean
  status: TeamMemberStatus
  reviewerNumber?: number
  responded?: string
  alias: TeamMemberAlias
  invitedDate?: string
  declineReason?: string

  user?: UserI
  team?: TeamI
  jobs?: JobI

  workload: unknown

  static get tableName() {
    return 'team_member'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: 'string', format: 'uuid' },
        teamId: { type: 'string', format: 'uuid' },
        position: { type: ['integer', null] },
        isSubmitting: { type: ['boolean', null] },
        isCorresponding: { type: ['boolean', null] },
        status: {
          enum: Object.values(TeamMember.Statuses),
          default: TeamMember.Statuses.pending,
        },
        reviewerNumber: { type: ['integer', null] },
        responded: { type: ['string', 'object', 'null'], format: 'date-time' },
        alias: {
          type: 'object',
          properties: {
            surname: { type: ['string', 'null'] },
            givenNames: { type: ['string', 'null'] },
            email: { type: 'string' },
            aff: { type: ['string', 'null'] },
            affRorId: { type: ['string', 'null'] },
            country: { type: ['string', 'null'] },
            title: { type: ['string', 'null'] },
          },
        },
        invitedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        declineReason: { type: ['string', null] },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'team_member.userId',
          to: 'user.id',
        },
      },
      team: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: 'team_member.teamId',
          to: 'team.id',
          extra: ['role'],
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Job,
        join: {
          from: 'team_member.id',
          to: 'job.teamMemberId',
        },
      },
    }
  }

  $formatDatabaseJson(json: any) {
    json = super.$formatDatabaseJson(json)

    if (json.alias) {
      const alias = JSON.parse(json.alias)
      const email = get(alias, 'email', '')
      return { ...json, alias: { ...alias, email: email.toLowerCase() } }
    }

    return json
  }

  static get Statuses(): Record<TeamMemberStatus, TeamMemberStatus> {
    return {
      pending: 'pending',
      accepted: 'accepted',
      declined: 'declined',
      submitted: 'submitted',
      expired: 'expired',
      removed: 'removed',
      active: 'active',
      conflicting: 'conflicting',
    }
  }

  static get StatusExpiredLabels(): Record<string, string> {
    return {
      overdue: 'OVERDUE',
      unresponsive: 'UNRESPONSIVE',
      unsubscribed: 'UNSUBSCRIBED',
    }
  }

  static getActiveStatusForRole(role) {
    return role === Team.Role.triageEditor
      ? this.Statuses.active
      : this.Statuses.accepted
  }

  static async findAllByStatuses({
    role,
    statuses,
    manuscriptId,
    submissionId,
  }: {
    role: string
    statuses: string[]
    manuscriptId: string
    submissionId: string
  }): Promise<TeamMember[]> {
    try {
      return await this.query()
        .skipUndefined()
        .select('tm.*')
        .from('team_member AS tm')
        .leftJoin('team AS t', 'tm.team_id', 't.id')
        .leftJoin('manuscript AS m', 't.manuscript_id', '=', 'm.id')
        .whereIn('tm.status', statuses)
        .andWhere('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submission_id', submissionId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByRoleAndStatusWhereRespondedNull({
    role,
    status,
  }: {
    role: string
    status: string
  }): Promise<TeamMember[]> {
    try {
      return await this.query()
        .alias('tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.status', status)
        .andWhere('t.role', role)
        .andWhere('tm.responded', null)
    } catch (e) {
      throw new Error(e)
    }
  }

  static findOneByUserAndRole({
    userId,
    role,
    eagerLoadRelations,
  }: {
    userId: string
    role: string
    eagerLoadRelations: string | string[]
  }): Promise<TeamMember | undefined> {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .where('tm.userId', userId)
      .andWhere('t.role', role)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
      .limit(1)
      .first()
  }

  static findOneByUserAndRoles({
    userId,
    roles,
  }: {
    userId: string
    roles: string[]
  }): Promise<TeamMember | undefined> {
    return this.query()
      .alias('tm')
      .join('team as t', 'tm.team_id', 't.id')
      .whereIn('t.role', roles)
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static async findOneByRole({
    role,
  }: {
    role: string
  }): Promise<TeamMember | undefined> {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('t.role', role)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByEmailOnManuscriptParent(
    email: string,
  ): Promise<TeamMember | undefined> {
    try {
      return this.query()
        .alias('tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .whereIn('t.role', ['triageEditor', 'academicEditor'])
        .andWhereRaw(`tm.alias->>'email' = ?`, [email])
        .andWhere('t.manuscript_id', null)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndRoleAndStatusOnManuscript({
    role,
    userId,
    status,
    manuscriptId,
  }: {
    role: string
    userId: string
    status: string
    manuscriptId: string
  }): Promise<TeamMember | undefined> {
    return this.query()
      .select('tm.*')
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .where('t.role', role)
      .andWhere('tm.userId', userId)
      .andWhere('tm.status', status)
      .andWhere('m.id', manuscriptId)
      .limit(1)
      .first()
  }

  static async findOneByUserAndRoleAndStatus({
    userId,
    role,
    status,
  }: {
    userId: string
    role: string
    status: string
  }): Promise<TeamMember | undefined> {
    try {
      return this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('tm.status', status)
        .andWhere('t.role', role)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySubmissionAndUser({
    submissionId,
    userId,
    TeamRole,
  }: {
    submissionId: string
    userId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
  }): Promise<TeamMember | undefined> {
    try {
      const admin = await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('t.role', TeamRole.admin)
        .limit(1)
        .first()

      if (admin) return admin

      return await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .leftJoin('user as u', 'tm.userId', 'u.id')
        .leftJoin('role as r', 'r.name', 't.role') // TODO: change the role.name to role.id
        .leftJoin('submission_status as ss', 'm.status', 'ss.name') // TODO: change ss.name to ss.id
        .leftJoin('submission_label as sl', function(this: any) {
          this.on('r.id', 'sl.roleId')
          this.andOn('ss.id', 'sl.submissionStatusId')
        })
        .where('tm.userId', userId)
        .andWhere('m.submissionId', submissionId)
        .andWhere((query: any) =>
          query
            .whereNotIn('tm.status', ['declined', 'expired', 'removed'])
            .whereNotNull('t.manuscript_id'),
        )
        .orderByRaw('sl.priority DESC NULLS LAST')
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findSubmittingAuthor(
    manuscriptId: string,
  ): Promise<TeamMember | undefined> {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.isSubmitting', true)
        .andWhere('m.id', '=', manuscriptId)

      if (results.length > 0) return results[0]
      return undefined
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findCorrespondingAuthor(
    manuscriptId: string,
  ): Promise<TeamMember | undefined> {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', 'author')
        .andWhere('tm.isCorresponding', true)
        .andWhere('m.id', '=', manuscriptId)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findCorrespondingEditorialAssistant(
    journalId: string,
  ): Promise<TeamMember | undefined> {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('t.role', 'editorialAssistant')
        .andWhere('tm.isCorresponding', true)
        .andWhere('j.id', '=', journalId)

      if (results.length > 0) return results[0]
      return undefined
    } catch (e) {
      throw new Error(e)
    }
  }

  static async deleteStaffMemberByUserIdAndRole({
    userId,
    role,
  }: {
    userId: string
    role: string
  }): Promise<number> {
    try {
      return this.query()
        .delete()
        .from(raw('team_member tm USING team t'))
        .where(raw('t.id = tm.team_id'))
        .andWhere('tm.user_id', userId)
        .andWhere('t.role', role)
        .andWhere('t.journal_id', null)
        .andWhere('t.section_id', null)
        .andWhere('t.special_issue_id', null)
        .andWhere('t.manuscript_id', null)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async isApprovalEditor({ userId, manuscriptId, models }) {
    const { Manuscript, Team, PeerReviewEditorialMapping } = models
    const manuscript = await Manuscript.find(manuscriptId)

    const approvalEditorRole = await PeerReviewEditorialMapping.getApprovalEditorRoleForSubmission(
      manuscript.submissionId,
    )

    if (
      approvalEditorRole === Team.Role.triageEditor &&
      manuscript.hasTriageEditorConflictOfInterest
    ) {
      return false
    }

    const activeStatus = this.getActiveStatusForRole(approvalEditorRole)

    const approvalEditorTeamMembers = await this.findAllByManuscriptAndRoleAndStatus(
      {
        role: approvalEditorRole as string,
        manuscriptId,
        status: activeStatus,
      },
    )
    if (approvalEditorTeamMembers.length === 0) {
      return false
    }

    return approvalEditorTeamMembers.some(member => member.userId === userId)
  }

  static async findTriageEditor({
    TeamRole,
    sectionId,
    journalId,
    manuscriptId,
    eagerLoadRelations,
  }: {
    manuscriptId: string
    sectionId: string
    journalId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMember | undefined> {
    let triageEditor = await this.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: TeamRole.triageEditor,
      status: this.Statuses.active,
      eagerLoadRelations,
    })

    if (triageEditor) return triageEditor

    if (!journalId && !sectionId) {
      throw new Error('Journal ID or Section ID is required.')
    }

    triageEditor = await this.findOneBySectionAndRole({
      sectionId,
      role: TeamRole.triageEditor,
      eagerLoadRelations,
    })

    if (triageEditor) return triageEditor

    triageEditor = await this.findOneByJournalAndRole({
      journalId,
      role: TeamRole.triageEditor,
      eagerLoadRelations,
    })

    return triageEditor
  }

  static async orderAcademicEditorsByScoreAndWorkloadAndEmail({
    academicEditors,
    manuscript,
  }: {
    academicEditors: AcademicEditorI[]
    manuscript: ManuscriptI
  }) {
    const editorSuggestions = await EditorSuggestion.findBy(
      {
        manuscriptId: manuscript.id,
      },
      undefined,
    )

    const academicEditorsUserIds = academicEditors.map(tm => tm.userId)
    const teamMemberWithWorkload = await this.findTeamMemberWorkloadByUsersAndManuscriptParent(
      {
        userIds: academicEditorsUserIds,
        manuscript,
        role: Team.Role.academicEditor,
        teamMemberStatuses: [
          TeamMember.Statuses.pending,
          TeamMember.Statuses.accepted,
        ],
      },
    )
    return chain(academicEditors)
      .map((ae: any) => {
        const editorSubmissionScore = editorSuggestions.find(
          (editorSubmissionScore: any) =>
            editorSubmissionScore.teamMemberId === ae.id,
        )
        const editorSubmissionWorkload = teamMemberWithWorkload.find(
          tm => tm.userId === ae.userId,
        )
        return {
          ...ae,
          score: editorSubmissionScore ? editorSubmissionScore.score : 0,
          workload: editorSubmissionWorkload
            ? editorSubmissionWorkload.workload
            : 0,
        }
      })
      .orderBy(['score', 'workload', 'alias.email'], ['desc', 'asc', 'asc'])
      .value()
  }

  static async isSuggestionsListInSyncWithTeamMembers(
    editorSuggestions: EditorSuggestionI[],
  ): Promise<boolean> {
    const editorSuggestionIds = editorSuggestions.map(
      editorSuggestion => editorSuggestion.editorId,
    )
    const teamMembersLength = await this.query()
      .whereIn('id', editorSuggestionIds as string[])
      .resultSize()

    return teamMembersLength === editorSuggestionIds.length
  }

  // Transactions

  static async updateAndSaveMany(teamMembers: any, trx: any) {
    return this.query(trx).upsertGraph(teamMembers, {
      insertMissing: true,
      relate: true,
    })
  }

  static async removeEditorialAssistantTransaction({
    journalEA,
    manuscriptEAs,
  }: any) {
    try {
      await transaction(this.knex(), async (trx: any) => {
        await this.query(trx).deleteById(journalEA.id)
        await this.query(trx).upsertGraph(manuscriptEAs, {
          insertMissing: true,
          relate: true,
        })
      })
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  static async removeOneAndUpdateMany({
    memberToBeRemovedId,
    membersToBeUpdated,
  }: any) {
    return transaction(this.knex(), async (trx: any) => {
      await this.query(trx).deleteById(memberToBeRemovedId)
      return this.query(trx).upsertGraph(membersToBeUpdated, {
        insertMissing: true,
        relate: true,
      })
    })
  }

  static async updateCorrespondingMember({
    newCorrespondingMember,
    oldCorrespondingMember,
  }: any) {
    try {
      return await transaction(TeamMember.knex(), async (trx: any) => {
        await oldCorrespondingMember.$query(trx).update({
          isCorresponding: false,
          alias: oldCorrespondingMember.alias,
        })
        await newCorrespondingMember.$query(trx).update({
          isCorresponding: true,
          alias: newCorrespondingMember.alias,
        })
      })
    } catch (err) {
      logger.error(
        'Something went wrong while trying to change the corresponding team member',
        err,
      )
      throw new Error(
        'Something went wrong while trying to change the corresponding team member',
      )
    }
  }

  static async handleCorrespondingAuthor({
    manuscriptId,
    editedAuthor,
    inputCorresponding,
  }: {
    manuscriptId: string
    editedAuthor: AuthorI
    inputCorresponding: boolean
  }): Promise<void> {
    const correspondingAuthor = await this.findCorrespondingAuthor(manuscriptId)
    if (inputCorresponding === true) {
      await this.updateCorrespondingMember({
        newCorrespondingMember: editedAuthor,
        oldCorrespondingMember: correspondingAuthor,
      })
    } else {
      const submittingAuthor = await this.findSubmittingAuthor(manuscriptId)
      if (!submittingAuthor) {
        logger.warn(
          `Submitting author not found for manuscript with id: ${manuscriptId}.`,
        )
        return
      }
      if (editedAuthor.id === correspondingAuthor?.id) {
        await this.updateCorrespondingMember({
          newCorrespondingMember: submittingAuthor,
          oldCorrespondingMember: editedAuthor,
        })
      }
      if (!correspondingAuthor) {
        submittingAuthor.updateProperties({
          isCorresponding: true,
        })
        await submittingAuthor.save()
      }
    }
  }

  // Workload
  // for status draft, we have to exclude the manuscripts with version greater than 1
  // because those are manuscripts where a revision was requested and the workload was already counted using revisionRequested status
  static async findAllByJournalWithWorkload({
    role,
    journalId,
    teamMemberStatuses,
    manuscriptStatuses,
  }: {
    role: string
    journalId: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<{ id: string; name: string; workload: string }> {
    return this.query<any>()
      .select('tmj.user_id', 'tmj.alias', 'TMs.workload')
      .from({ tmj: 'team_member' })
      .join({ t: 'team' }, 't.id', 'tmj.team_id')
      .join({ j: 'journal' }, 'j.id', 't.journal_id')
      .leftJoin(
        query =>
          query
            .select('tmm.user_id', raw('count(tmm.id)::integer as workload'))
            .from({ tmm: 'team_member' })
            .join({ tm: 'team' }, 'tm.id', 'tmm.team_id')
            .join({ m: 'manuscript' }, 'm.id', 'tm.manuscript_id')
            .whereIn('m.status', manuscriptStatuses)
            .whereNotNull('m.submitted_date')
            .whereIn('tmm.status', teamMemberStatuses)
            .whereNotNull('tm.manuscript_id')
            .andWhere((query: any) =>
              query.where('m.version', 1).orWhereNot('m.status', 'draft'),
            )
            .andWhere('tm.role', role)
            .andWhere('m.journal_id', journalId)
            .groupBy('tmm.user_id')
            .as('TMs'),
        'TMs.user_id',
        'tmj.user_id',
      )
      .where('t.role', role)
      .andWhere('j.id', journalId)
      .groupBy('tmj.id', 'j.name', 'TMs.workload')
  }

  static findTeamMembersWorkloadByJournal(argsObject: {
    journalId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }) {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.journalId,
        manuscriptParentType: 'journal_id',
        ...argsObject,
      })
        .clone()
        .andWhere(`m.specialIssueId`, null)
        .andWhere(`m.sectionId`, null)
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadByJournalAndSpecialIssue(argsObject: {
    journalId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMember[]> {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.journalId,
        manuscriptParentType: 'journal_id',
        ...argsObject,
      })
        .clone()
        .andWhere(`m.sectionId`, null)
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadBySection(argsObject: {
    sectionId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMember[]> {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.sectionId,
        manuscriptParentType: 'section_id',
        ...argsObject,
      })
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMembersWorkloadBySpecialIssue(argsObject: {
    specialIssueId: string
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
  }): Promise<TeamMember[]> {
    try {
      return this.findTeamMemberWorkloadByManuscriptParent({
        manuscriptParentId: argsObject.specialIssueId,
        manuscriptParentType: 'special_issue_id',
        ...argsObject,
      })
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  static findTeamMemberWorkloadByManuscriptParent({
    role,
    teamMemberStatuses,
    manuscriptStatuses,
    manuscriptParentId,
    manuscriptParentType,
  }: {
    role: string
    teamMemberStatuses: string[]
    manuscriptStatuses: string[]
    manuscriptParentId: string
    manuscriptParentType: string
  }) {
    return this.query()
      .select('tm.user_id', raw('count(tm.id)::integer as workload'))
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', manuscriptStatuses)
      .andWhere('t.role', role)
      .andWhere(`m.${manuscriptParentType}`, manuscriptParentId)
      .groupBy('tm.user_id')
  }

  static findTeamMemberWorkloadByUsersAndManuscriptParent({
    userIds,
    role,
    teamMemberStatuses,
    manuscript,
  }: any) {
    const camelToSnakeCase = (str: any) =>
      str
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase()

    const parentPriorityMap = {
      // if the manuscript is on a section or special issue multiple ids will be present on manuscript
      journalId: 1, // and we need to know which one to consider for workload
      sectionId: 2,
      specialIssueId: 3,
    }

    const manuscriptParent = chain(manuscript)
      .pick('journalId', 'sectionId', 'specialIssueId')
      .pickBy((value: any) => !!value)
      .map((value, key) => ({
        id: value,
        type: camelToSnakeCase(key),
        priority: parentPriorityMap[key],
      }))
      .orderBy(['priority'], ['desc'])
      .head()
      .value()

    return this.query()
      .select('tm.user_id', raw('count(tm.id)::integer as workload'))
      .from('team_member as tm')
      .join('team as t', 'tm.team_id', 't.id')
      .join('manuscript as m', 't.manuscript_id', 'm.id')
      .whereIn('tm.user_id', userIds)
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', Manuscript.InProgressStatuses)
      .andWhere('t.role', role)
      .andWhere(`m.${manuscriptParent.type}`, manuscriptParent.id)
      .groupBy('tm.user_id')
  }

  static async findAllWithWorkloadByManuscriptParent({
    role,
    journalId,
    sectionId,
    specialIssueId,
    teamMemberStatuses,
    manuscriptStatuses,
  }: any) {
    try {
      if (specialIssueId) {
        return await this.findTeamMembersWorkloadBySpecialIssue({
          role,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      if (sectionId) {
        return await this.findTeamMembersWorkloadBySection({
          role,
          sectionId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      if (journalId) {
        return await this.findTeamMembersWorkloadByJournal({
          role,
          journalId,
          teamMemberStatuses,
          manuscriptStatuses,
        })
      }

      throw new Error('No manuscript parent ID has been provided.')
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  static async findAllByManuscriptParentAndRole({
    role,
    journalId,
    sectionId,
    specialIssueId,
  }: any) {
    try {
      let members: TeamMember[] = []
      if (specialIssueId) {
        members = await this.findAllBySpecialIssueAndRole({
          role,
          specialIssueId,
        })

        if (members.length) return members
      }

      if (sectionId) {
        members = await this.findAllBySectionAndRole({
          role,
          sectionId,
        })

        if (members.length) return members
      }

      if (journalId) {
        return await TeamMember.findAllByJournalAndRole({
          role,
          journalId,
        })
      }

      throw new Error('No manuscript parent ID has been provided.')
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  }

  // By Journal

  static findAllByJournal({ journalId, eagerLoadRelations }: any) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('journal AS j', 't.journal_id', 'j.id')
      .where('j.id', journalId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneByJournalAndUser({
    userId,
    journalId,
    eagerLoadRelations,
  }: any) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findOneByJournalAndRole({ role, journalId, eagerLoadRelations }: any) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findAllByJournalAndRole({ role, journalId, eagerLoadRelations }: any) {
    return this.findAllByJournal({
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static async findAllByJournalAndRoleAndSearchValue({
    role,
    journalId,
    searchValue = '',
    eagerLoadRelations,
  }: any) {
    return this.findAllByJournalAndRole({
      role,
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) =>
        builder
          .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
          .orWhereRaw(
            `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          )
          .orWhereRaw(
            `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          ),
      )
  }

  // By Section

  static findAllBySection({ sectionId, eagerLoadRelations }: any) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('section AS s', 't.section_id', 's.id')
      .andWhere('s.id', sectionId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneBySectionAndRole({ role, sectionId, eagerLoadRelations }: any) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findOneBySectionAndUser({
    userId,
    sectionId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findAllBySectionAndRole({ role, sectionId, eagerLoadRelations }: any) {
    return this.findAllBySection({ sectionId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
  }

  // By Special Issue

  static findAllBySpecialIssue({ specialIssueId, eagerLoadRelations }: any) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('special_issue AS si', 't.special_issue_id', 'si.id')
      .andWhere('si.id', specialIssueId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllBySpecialIssues({ specialIssueIds }: any) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('special_issue AS si', 't.special_issue_id', 'si.id')
      .whereIn('si.id', specialIssueIds)
  }

  static findOneBySpecialIssueAndUser({
    userId,
    specialIssueId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySpecialIssue({
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findOneBySpecialIssueAndUsers({
    userIds,
    specialIssueId,
    eagerLoadRelations,
  }: {
    userIds: string[]
    specialIssueId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllBySpecialIssue({
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) => builder.whereIn('tm.userId', userIds))
      .limit(1)
      .first()
  }

  static findAllBySpecialIssueAndUser({
    userId,
    specialIssueId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySpecialIssue({ specialIssueId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
  }

  static findAllBySpecialIssueAndUserAndRoles({
    roles,
    userId,
    specialIssueId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySpecialIssueAndUser({
      userId,
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) => builder.whereIn('t.role', roles))
  }

  static findAllBySpecialIssueAndRole({
    role,
    specialIssueId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySpecialIssue({ specialIssueId, eagerLoadRelations })
      .clone()
      .andWhere('t.role', role)
  }

  static findAllBySpecialIssueAndRoleAndSearchValue({
    role,
    searchValue = '',
    specialIssueId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySpecialIssueAndRole({
      role,
      specialIssueId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) =>
        builder
          .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
          .orWhereRaw(
            `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          )
          .orWhereRaw(
            `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
            [`${searchValue.toLowerCase()}%`],
          ),
      )
  }

  // By Submission

  static findAllBySubmission({ submissionId, eagerLoadRelations }: any) {
    return this.query()
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .andWhere('m.submission_id', submissionId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllBySubmissionAndRole({
    role,
    submissionId,
    eagerLoadRelations,
  }: {
    role: string
    submissionId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllBySubmission({
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static findAllBySubmissionAndRoleAndUser({
    role,
    userId,
    submissionId,
    eagerLoadRelations,
  }: any) {
    return this.findAllBySubmissionAndRole({
      role,
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
  }

  static findAllBySubmissionAndRoleAndStatuses({
    role,
    statuses,
    submissionId,
    eagerLoadRelations,
  }: {
    role: string
    statuses: string[]
    submissionId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllBySubmissionAndRole({
      role,
      submissionId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) => builder.whereIn('tm.status', statuses))
  }

  // By Manuscript

  static findAllByManuscriptsAndRole({ manuscriptIds, role }: any) {
    return this.query()
      .select('tm.*', 'm.id as manuscriptId')
      .from('team_member as tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .whereIn('m.id', manuscriptIds)
      .andWhere('t.role', role)
  }

  static findAllByManuscriptsAndRoles({ manuscriptIds, roles }: any) {
    return this.query()
      .select('tm.*', 'm.id as manuscriptId')
      .from('team_member as tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .whereIn('m.id', manuscriptIds)
      .whereIn('t.role', roles)
  }

  static findOneByManuscriptsAndRole({ manuscriptIds, role }: any) {
    return this.findAllByManuscriptsAndRole({ manuscriptIds, role })
      .clone()
      .limit(1)
  }

  static findAllByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role,
    status,
  }: any) {
    return this.findAllByManuscriptsAndRole({ manuscriptIds, role })
      .clone()
      .andWhere('tm.status', status)
  }

  static findOneByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role,
    status,
  }: any) {
    return this.findAllByManuscriptsAndRoleAndStatus({
      manuscriptIds,
      role,
      status,
    })
      .clone()
      .limit(1)
  }

  static findAllByManuscript({ trx, manuscriptId, eagerLoadRelations }: any) {
    return this.query(trx)
      .alias('tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .where('m.id', manuscriptId)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findOneByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }: {
    role: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllByManuscript({
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndUser({
    userId,
    manuscriptId,
    eagerLoadRelations,
  }: {
    userId: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }): Promise<TeamMemberI | undefined> {
    return this.findAllByManuscript({ manuscriptId, eagerLoadRelations })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }

  static findAllByManuscriptAndRole({
    trx,
    role,
    manuscriptId,
    eagerLoadRelations,
  }: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllByManuscript({
      trx,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('t.role', role)
  }

  static findOneByManuscriptAndRoleAndStatus({
    trx,
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    status: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllByManuscriptAndRole({
      trx,
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.status', status)
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndRoleAndStatuses({
    role,
    statuses,
    manuscriptId,
    eagerLoadRelations,
  }: any) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere((builder: any) => builder.whereIn('tm.status', statuses))
      .limit(1)
      .first()
  }

  static findOneByManuscriptAndRoleAndUser({
    role,
    userId,
    manuscriptId,
    eagerLoadRelations,
  }: {
    role: string
    userId: string
    manuscriptId: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.userId', userId)
      .limit(1)
      .first()
  }
  static findOneByManuscriptAndRoleAndUserAndStatus({
    role,
    userId,
    manuscriptId,
    status,
    eagerLoadRelations,
  }: any) {
    return this.findOneByManuscriptAndRoleAndUser({
      role,
      userId,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.status', status)
      .limit(1)
      .first()
  }
  static findAllByManuscriptAndRoleAndStatus({
    trx,
    role,
    status,
    manuscriptId,
    eagerLoadRelations,
  }: {
    trx?: Objection.Transaction
    role: string
    manuscriptId: string
    status: string
    eagerLoadRelations?: string | string[]
  }) {
    return this.findAllByManuscriptAndRole({
      trx,
      role,
      manuscriptId,
      eagerLoadRelations,
    })
      .clone()
      .andWhere('tm.status', status)
  }

  static findAllByManuscriptAndRoleAndExcludedStatuses({
    role,
    manuscriptId,
    excludedStatuses,
  }: any) {
    return this.findAllByManuscriptAndRole({
      role,
      manuscriptId,
    })
      .clone()
      .andWhere((builder: any) =>
        builder.whereNotIn('tm.status', excludedStatuses),
      )
  }

  // By Role
  static findAllByRole(role: any) {
    return this.query()
      .select('tm.*')
      .from('team_member AS tm')
      .join('team AS t', 'tm.team_id', 't.id')
      .andWhere('t.role', role)
  }

  static findAllByUserAndRole({ userId, role }: any) {
    return this.findAllByRole(role)
      .clone()
      .andWhere('tm.userId', userId)
  }

  static findAllByGlobalRole({ userId, role }: any) {
    return this.findAllByUserAndRole({ userId, role })
      .clone()
      .andWhereRaw(
        '(t.journal_id is not null or t.section_id is not null or t.special_issue_id is not null)',
      )
  }

  setAlias(identity: any) {
    this.alias = pick(identity, [
      'aff',
      'affRorId',
      'title',
      'email',
      'surname',
      'country',
      'givenNames',
    ])
  }

  getName() {
    return `${get(this, 'alias.givenNames', '')} ${get(
      this,
      'alias.surname',
      '',
    )}`
  }

  getEmail() {
    return `${get(this, 'alias.email', '')}`
  }

  getLastName() {
    return `${get(this, 'alias.surname', '')}`
  }

  getInvitedDate({ Team, role }: any) {
    if (role === Team.Role.academicEditor)
      return this.invitedDate ? this.invitedDate : this.created

    return role === Team.Role.reviewer ? this.created : ''
  }

  getDeclinedDate({ Team, role }: any) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      this.status === TeamMember.Statuses.declined
      ? this.responded
      : ''
  }

  getAcceptedDate({ Team, role }: any) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      [
        TeamMember.Statuses.accepted,
        TeamMember.Statuses.submitted,
        TeamMember.Statuses.removed,
        TeamMember.Statuses.conflicting,
      ].includes(this.status)
      ? this.responded
      : ''
  }

  getExpiredDate({ Team, role }: any) {
    return [Team.Role.academicEditor, Team.Role.reviewer].includes(role) &&
      this.status === TeamMember.Statuses.expired
      ? this.updated
      : ''
  }

  getAssignedDate({ Team, role }: any) {
    return [Team.Role.triageEditor, Team.Role.editorialAssistant].includes(role)
      ? this.created
      : ''
  }

  getRemovedDate({ Team, role }: any) {
    return [
      Team.Role.triageEditor,
      Team.Role.academicEditor,
      Team.Role.editorialAssistant,
    ].includes(role) && this.status === TeamMember.Statuses.removed
      ? this.updated
      : ''
  }

  getDatesForEvents({
    role,
    Team,
  }: {
    role: string
    Team: TeamRepository
  }):
    | {
        invitedDate: string
        declinedDate?: string
        acceptedDate?: string
        expiredDate?: string
        assignedDate?: string
        removedDate?: string
      }
    | undefined {
    const invitedDate = this.getInvitedDate({ Team, role })
    const declinedDate = this.getDeclinedDate({ Team, role })
    const acceptedDate = this.getAcceptedDate({ Team, role })
    const expiredDate = this.getExpiredDate({ Team, role })
    const assignedDate = this.getAssignedDate({ Team, role })
    const removedDate = this.getRemovedDate({ Team, role })

    if (role === Team.Role.reviewer)
      return { invitedDate, declinedDate, acceptedDate, expiredDate }
    if (
      [
        Team.Role.triageEditor,
        Team.Role.academicEditor,
        Team.Role.editorialAssistant,
      ].includes(role as TeamMemberRole)
    )
      return {
        invitedDate,
        declinedDate,
        acceptedDate,
        expiredDate,
        assignedDate,
        removedDate,
      }

    return undefined
  }

  getEventData({ peerReviewModel, Team, role }: any) {
    const getLabel = ({ prm, role }: any) => {
      if (role === Team.Role.editorialAssistant) return 'Editorial Assistant'
      if (role === Team.Role.triageEditor) return prm.triageEditorLabel
      if (role === Team.Role.academicEditor) return prm.academicEditorLabel
      if (role === Team.Role.researchIntegrityPublishingEditor)
        return 'Research Integrity Publishing Editor'
    }
    const orcidIdentity = this.user?.identities?.find(
      identity => identity.type === 'orcid',
    )
    return {
      id: this.id,
      userId: this.userId,
      status: this.status,
      isCorresponding: this.isCorresponding,
      ...this.alias,
      role: {
        type: role,
        label: getLabel({ prm: peerReviewModel, role, Team }),
      },
      orcidId: orcidIdentity ? orcidIdentity.identifier : '',
      ...this.getDatesForEvents({
        role,
        Team,
      }),
      declineReason: this.declineReason,
    }
  }

  toDTO() {
    return {
      ...this,
      invited: this.created,
      user: this.user ? this.user.toDTO() : undefined,
      alias: this.alias
        ? {
            ...this.alias,
            name: {
              surname: this.alias.surname,
              givenNames: this.alias.givenNames,
              title: this.alias.title,
            },
          }
        : undefined,
      role: this.team ? this.team.role : undefined,
    }
  }
}
