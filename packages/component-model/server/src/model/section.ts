/* eslint-disable sonarjs/no-duplicate-string */
import { chain } from 'lodash'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Journal, JournalI } from './journal'
import { Manuscript, ManuscriptI } from './manuscript'
import { SpecialIssue, SpecialIssueI } from './specialIssue'
import { Team, TeamI } from './team'

export interface SectionI extends HindawiBaseModelProps {
  name: string
  journalId: string

  journal?: JournalI
  manuscripts?: ManuscriptI[]
  teams?: TeamI[]
  specialIssues?: SpecialIssueI[]

  toDTO(): object
}

export interface SectionRepository
  extends HindawiBaseModelRepository<SectionI> {
  new (...args: any[]): SectionI
}

export class Section extends HindawiBaseModel implements SectionI {
  name: string
  journalId: string

  journal?: JournalI
  manuscripts?: ManuscriptI[]
  teams?: TeamI[]
  specialIssues?: SpecialIssueI[]

  static get tableName() {
    return 'section'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        journalId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'section.journalId',
          to: 'journal.id',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'section.id',
          to: 'manuscript.sectionId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'section.id',
          to: 'team.sectionId',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: SpecialIssue,
        join: {
          from: 'section.id',
          to: 'special_issue.sectionId',
        },
      },
    }
  }

  static async findAllByJournal({
    journalId,
  }: {
    journalId: string
  }): Promise<Section[]> {
    try {
      return await this.query()
        .from('section AS s')
        .join('journal AS j', 's.journal_id', 'j.id')
        .where('j.id', journalId)
    } catch (e) {
      throw new Error(e)
    }
  }

  toDTO() {
    const sectionEditors = chain(this.teams)
      .find(t => t.role === Team.Role.triageEditor)
      .get('members')
      .value()

    return {
      ...this,
      sectionEditors: sectionEditors
        ? sectionEditors.map((member: any) => member.toDTO())
        : [],
      specialIssues: this.specialIssues
        ? this.specialIssues.map((specialIssue: any) => specialIssue.toDTO())
        : [],
    }
  }
}
