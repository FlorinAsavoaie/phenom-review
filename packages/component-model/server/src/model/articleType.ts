import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { JournalArticleType, JournalArticleTypeI } from './journalArticleType'
import { Manuscript, ManuscriptI } from './manuscript'

export interface ArticleTypeI extends HindawiBaseModelProps {
  name: string
  hasPeerReview: boolean

  journalArticleTypes?: JournalArticleTypeI[]
  manuscript?: ManuscriptI

  toDTO(): object
}

export type ArticleTypeValue =
  | 'Commentary'
  | 'Editorial'
  | 'Retraction'
  | 'Case Series'
  | 'Case Report'
  | 'Review Article'
  | 'Research Article'
  | 'Letter to the Editor'
  | 'Expression of Concern'
  | 'Erratum'
  | 'Corrigendum'

export interface ArticleTypeRepository
  extends HindawiBaseModelRepository<ArticleTypeI> {
  new (...args: any[]): ArticleTypeI
  findArticleTypeByManuscript(
    manuscriptId: string,
  ): Promise<ArticleTypeI | undefined>
  findAllByManuscriptIds(manuscriptIds: string[]): Promise<ArticleTypeI[]>

  Types: Record<string, ArticleTypeValue>
  ShortArticleTypes: ArticleTypeValue[]
  TypesWithPeerReview: ArticleTypeValue[]
  TypesWithRIPE: ArticleTypeValue[]
  EditorialTypes: ArticleTypeValue[]
}

export class ArticleType extends HindawiBaseModel implements ArticleTypeI {
  name: string
  hasPeerReview: boolean

  journalArticleTypes?: JournalArticleType[]
  manuscript?: Manuscript

  static get tableName() {
    return 'article_type'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        hasPeerReview: { type: 'boolean' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: JournalArticleType,
        join: {
          from: 'article_type.id',
          to: 'journal_article_type.articleTypeId',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'article_type.id',
          to: 'manuscript.articleTypeId',
        },
      },
    }
  }

  static async findArticleTypeByManuscript(
    manuscriptId: string,
  ): Promise<ArticleTypeI | undefined> {
    try {
      const results = await this.query()
        .select('at.*')
        .from('article_type AS at')
        .join('manuscript AS m', 'at.id', 'm.article_type_id')
        .where('m.id', manuscriptId)

      if (results.length > 0) return results[0]
      return undefined
    } catch (e) {
      throw new Error(e)
    }
  }

  static findAllByManuscriptIds(
    manuscriptIds: string[],
  ): Promise<ArticleTypeI[]> {
    return this.query()
      .select('at.*', 'm.id as manuscriptId')
      .from('article_type as at')
      .join('manuscript as m', 'at.id', 'm.article_type_id')
      .whereIn('m.id', manuscriptIds)
  }

  static get Types(): Record<string, ArticleTypeValue> {
    return {
      commentary: 'Commentary',
      editorial: 'Editorial',
      retraction: 'Retraction',
      caseSeries: 'Case Series',
      caseReport: 'Case Report',
      reviewArticle: 'Review Article',
      researchArticle: 'Research Article',
      letterToTheEditor: 'Letter to the Editor',
      expressionOfConcern: 'Expression of Concern',
      erratum: 'Erratum',
      corrigendum: 'Corrigendum',
    }
  }

  static get ShortArticleTypes(): ArticleTypeValue[] {
    return [
      this.Types.corrigendum,
      this.Types.expressionOfConcern,
      this.Types.retraction,
      this.Types.erratum,
      this.Types.letterToTheEditor,
      this.Types.editorial,
    ]
  }

  static get TypesWithPeerReview(): ArticleTypeValue[] {
    return [
      this.Types.caseSeries,
      this.Types.caseReport,
      this.Types.reviewArticle,
      this.Types.researchArticle,
    ]
  }

  static get TypesWithRIPE(): ArticleTypeValue[] {
    return [
      this.Types.retraction,
      this.Types.expressionOfConcern,
      this.Types.erratum,
      this.Types.corrigendum,
    ]
  }

  static get EditorialTypes(): ArticleTypeValue[] {
    return [this.Types.commentary]
  }

  toDTO() {
    return {
      ...this,
    }
  }
}
