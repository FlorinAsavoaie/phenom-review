/* eslint-disable sonarjs/no-identical-functions */
/* eslint-disable sonarjs/no-duplicate-string */
import { ValidationError } from '@pubsweet/errors'
import { chain, get, groupBy, last, omit, pick, sortBy } from 'lodash'
import Objection, { OrderByDirection, raw, ref, transaction } from 'objection'
import uuid from 'uuid'

import HindawiBaseModel, {
  HindawiBaseModelRepository,
  HindawiBaseModelProps,
} from '../hindawiBaseModel'
import { Review, ReviewI } from './review'

import {
  AcademicEditor as AcademicEditorModel,
  AcademicEditorI,
} from './academicEditor'
import { ArticleType, ArticleTypeI, ArticleTypeRepository } from './articleType'
import { AuditLog, AuditLogI } from './auditLog'
import { Author as AuthorModel, AuthorI } from './author'
import { EditorialAssistant, EditorialAssistantI } from './editorialAssistant'
import { File, FileI } from './file'
import { Journal, JournalI } from './journal'
import {
  SubmissionEditorialMapping,
  SubmissionEditorialMappingI,
} from './submissionEditorialMapping'
import { ReviewerSuggestion, ReviewerSuggestionI } from './reviewerSuggestion'
import { Section, SectionI } from './section'
import { SourceJournal as SourceJournalModel } from './sourceJournal'
import { SpecialIssue, SpecialIssueI } from './specialIssue'
import {
  SubmittingStaffMember as SubmittingStaffMemberModel,
  SubmittingStaffMemberI,
} from './submittingStaffMember'
import { Team, TeamI, TeamMemberRole, TeamRepository } from './team'
import {
  TriageEditor as TriageEditorModel,
  TriageEditorI,
} from './triageEditor'
import { UserI } from './user'
import {
  TeamMemberI,
  TeamMemberRepository,
  TeamMemberStatus,
} from './teamMember'
import { PeerReviewModelRepository } from './peerReviewModel'
import {
  IPeerReviewEditorialMapping,
  PeerReviewEditorialMapping,
} from './peerReviewEditorialMapping'

const { logger } = require('component-logger')

const manuscriptIdCondition = 'manuscript.id'
const noDataError = 'Something went wrong. No data was updated.'

export type ManuscriptStatus =
  | 'draft'
  | 'technicalChecks'
  | 'submitted'
  | 'academicEditorInvited'
  | 'academicEditorAssigned'
  | 'reviewersInvited'
  | 'underReview'
  | 'reviewCompleted'
  | 'revisionRequested'
  | 'pendingApproval'
  | 'rejected'
  | 'inQA'
  | 'accepted'
  | 'withdrawn'
  | 'void'
  | 'deleted'
  | 'published'
  | 'olderVersion'
  | 'academicEditorAssignedEditorialType'
  | 'makeDecision'
  | 'qualityChecksRequested'
  | 'qualityChecksSubmitted'
  | 'refusedToConsider'

export interface ManuscriptI extends HindawiBaseModelProps {
  submissionId: string
  status: ManuscriptStatus
  customId?: string
  version: string
  title: string
  abstract: string
  technicalCheckToken?: string
  hasPassedEqa?: boolean
  hasPassedEqs?: boolean
  journalId?: string
  sectionId?: string
  agreeTc: boolean
  conflictOfInterest?: string
  dataAvailability?: string
  fundingStatement?: string
  articleTypeId?: string
  specialIssueId?: string
  isPostAcceptance: boolean
  preprintValue?: string
  submittedDate?: string
  acceptedDate?: string
  qualityChecksSubmittedDate?: string
  peerReviewPassedDate: string
  allowAcademicEditorAutomaticInvitation?: boolean
  hasTriageEditorConflictOfInterest: boolean
  isLatestVersion: boolean
  sourceJournalId?: string
  sourceJournalManuscriptId?: string
  linkedSubmissionCustomId?: string
  qualityCheckPassedDate?: string

  files?: FileI[]
  teams?: TeamI[]
  reviews?: ReviewI[]
  journal?: JournalI
  section?: SectionI
  logs?: AuditLogI[]
  reviewerSuggestions?: ReviewerSuggestionI[]
  articleType?: ArticleTypeI
  specialIssue?: SpecialIssueI
  editorialAssistant?: EditorialAssistantI
  authors?: AuthorI[]
  academicEditors?: AcademicEditorI[]
  triageEditor?: TriageEditorI
  sourceJournal?: JournalI
  submittingStaffMember?: SubmittingStaffMemberI
  submissionEditorialMapping?: SubmissionEditorialMappingI

  toDTO(): object
}

export interface ManuscriptRepository
  extends HindawiBaseModelRepository<ManuscriptI> {
  new (...args: any[]): ManuscriptI

  findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }: {
    submissionId: string
    eagerLoadRelations?: string[] | string
  }): Promise<ManuscriptI | undefined>
  findManuscriptsBySubmissionId({
    order,
    orderByField,
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }: {
    order?: string
    orderByField: string
    submissionId: string
    excludedStatus?: ManuscriptStatus
    eagerLoadRelations?: string | string[]
  }): Promise<ManuscriptI[]>

  NonActionableStatuses: ManuscriptStatus[]
  InProgressStatuses: ManuscriptStatus[]
  Statuses: Record<ManuscriptStatus, ManuscriptStatus>
}

export class Manuscript extends HindawiBaseModel implements ManuscriptI {
  submissionId: string
  status: ManuscriptStatus
  customId?: string
  version: string
  title: string
  abstract: string
  technicalCheckToken?: string
  hasPassedEqa?: boolean
  hasPassedEqs?: boolean
  journalId?: string
  sectionId?: string
  agreeTc: boolean
  conflictOfInterest?: string
  dataAvailability?: string
  fundingStatement?: string
  articleTypeId?: string
  specialIssueId?: string
  isPostAcceptance: boolean
  preprintValue?: string
  submittedDate?: string
  acceptedDate?: string
  qualityChecksSubmittedDate?: string
  peerReviewPassedDate: string
  allowAcademicEditorAutomaticInvitation?: boolean
  hasTriageEditorConflictOfInterest: boolean
  isLatestVersion: boolean
  sourceJournalId?: string
  sourceJournalManuscriptId?: string
  linkedSubmissionCustomId?: string
  qualityCheckPassedDate?: string

  files?: FileI[]
  teams?: TeamI[]
  reviews?: ReviewI[]
  journal?: JournalI
  section?: SectionI
  logs?: AuditLogI[]
  reviewerSuggestions?: ReviewerSuggestionI[]
  articleType?: ArticleTypeI
  specialIssue?: SpecialIssueI
  editorialAssistant?: EditorialAssistantI
  authors?: AuthorI[]
  academicEditors?: AcademicEditorI[]
  triageEditor?: TriageEditorI
  sourceJournal?: JournalI
  submittingStaffMember?: SubmittingStaffMemberI
  submissionEditorialMapping?: SubmissionEditorialMappingI
  peerReviewEditorialMapping?: IPeerReviewEditorialMapping

  comment: unknown
  role: unknown

  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        status: {
          enum: Object.values(Manuscript.Statuses),
          default: Manuscript.Statuses.draft,
        },
        customId: { type: ['string', 'null'] },
        version: { type: 'string', default: '1' },
        title: { type: 'string', default: '' },
        abstract: { type: 'string', default: '' },
        technicalCheckToken: { type: ['string', 'null'], format: 'uuid' },
        hasPassedEqa: { type: ['boolean', 'null'] },
        hasPassedEqs: { type: ['boolean', 'null'] },
        journalId: { type: ['string', null], format: 'uuid' },
        sectionId: { type: ['string', null], format: 'uuid' },
        agreeTc: { type: 'boolean', default: false },
        conflictOfInterest: { type: ['string', 'null'] },
        dataAvailability: { type: ['string', 'null'] },
        fundingStatement: { type: ['string', 'null'] },
        articleTypeId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        isPostAcceptance: { type: 'boolean', default: false },
        preprintValue: { type: ['string', 'null'] },
        submittedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        acceptedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        qualityChecksSubmittedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        peerReviewPassedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        allowAcademicEditorAutomaticInvitation: {
          type: ['boolean', 'null'],
          default: null,
        },
        hasTriageEditorConflictOfInterest: { type: 'boolean', default: false },
        isLatestVersion: { type: 'boolean', default: true },
        sourceJournalId: { type: ['string', 'null'], format: 'uuid' },
        sourceJournalManuscriptId: { type: ['string', 'null'] },
        linkedSubmissionCustomId: { type: ['string', 'null'] },
        qualityCheckPassedDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: File,
        join: {
          from: manuscriptIdCondition,
          to: 'file.manuscriptId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: manuscriptIdCondition,
          to: 'team.manuscriptId',
          extra: ['role'],
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Review,
        join: {
          from: manuscriptIdCondition,
          to: 'review.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Section,
        join: {
          from: 'manuscript.sectionId',
          to: 'section.id',
        },
      },
      logs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: AuditLog,
        join: {
          from: manuscriptIdCondition,
          to: 'audit_log.manuscriptId',
        },
      },
      reviewerSuggestions: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: ReviewerSuggestion,
        join: {
          from: manuscriptIdCondition,
          to: 'reviewer_suggestion.manuscriptId',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: ArticleType,
        join: {
          from: 'manuscript.articleTypeId',
          to: 'article_type.id',
        },
      },
      specialIssue: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: SpecialIssue,
        join: {
          from: 'manuscript.specialIssueId',
          to: 'special_issue.id',
        },
      },
      editorialAssistant: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: EditorialAssistant,
        join: {
          from: manuscriptIdCondition,
          to: 'editorial_assistant.manuscriptId',
        },
      },
      authors: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: AuthorModel,
        join: {
          from: manuscriptIdCondition,
          to: 'author.manuscriptId',
        },
      },
      academicEditors: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: AcademicEditorModel,
        join: {
          from: manuscriptIdCondition,
          to: 'academic_editor.manuscriptId',
        },
      },
      triageEditor: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: TriageEditorModel,
        join: {
          from: manuscriptIdCondition,
          to: 'triage_editor.manuscriptId',
        },
      },
      sourceJournal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: SourceJournalModel,
        join: {
          from: 'manuscript.sourceJournalId',
          to: 'source_journal.id',
        },
      },
      submittingStaffMember: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: SubmittingStaffMemberModel,
        join: {
          from: manuscriptIdCondition,
          to: 'submitting_staff_member.manuscriptId',
        },
      },
      submissionEditorialMapping: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: SubmissionEditorialMapping,
        join: {
          from: 'manuscript.submissionId',
          to: 'submission_editorial_mapping.submissionId',
        },
      },
      peerReviewEditorialMapping: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: PeerReviewEditorialMapping,
        join: {
          from: 'manuscript.submissionId',
          to: 'peer_review_editorial_mapping.submissionId',
        },
      },
    }
  }

  static get Statuses(): Record<ManuscriptStatus, ManuscriptStatus> {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      academicEditorInvited: 'academicEditorInvited',
      academicEditorAssigned: 'academicEditorAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawn: 'withdrawn',
      void: 'void',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
      academicEditorAssignedEditorialType:
        'academicEditorAssignedEditorialType',
      makeDecision: 'makeDecision',
      qualityChecksRequested: 'qualityChecksRequested',
      qualityChecksSubmitted: 'qualityChecksSubmitted',
      refusedToConsider: 'refusedToConsider',
    }
  }

  static get NonActionableStatuses(): ManuscriptStatus[] {
    const statuses = this.Statuses
    return [
      statuses.void,
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  }
  static get InProgressStatuses(): ManuscriptStatus[] {
    const statuses = this.Statuses
    return [
      statuses.submitted,
      statuses.underReview,
      statuses.makeDecision,
      statuses.reviewCompleted,
      statuses.pendingApproval,
      statuses.reviewersInvited,
      statuses.revisionRequested,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
    ]
  }

  static async updateMany(manuscripts: Manuscript[]): Promise<Manuscript[]> {
    try {
      return transaction(Manuscript.knex(), async (trx: any) => {
        const options = { noInsert: true, noDelete: true }
        return Manuscript.query(trx).upsertGraphAndFetch(manuscripts, options)
      })
    } catch (err) {
      logger.error(noDataError, err)
      throw new Error(noDataError)
    }
  }

  static compareVersion(m1: Manuscript, m2: Manuscript): false | 1 | -1 | 0 {
    if (typeof m1.version !== 'string' || typeof m2.version !== 'string') {
      return false
    }

    const v1 = m1.version.split('.')
    const v2 = m2.version.split('.')

    const k = Math.min(v1.length, v2.length)
    for (let i = 0; i < k; i += 1) {
      const parsedV1 = parseInt(v1[i], 10)
      const parsedV2 = parseInt(v2[i], 10)

      if (parsedV1 > parsedV2) {
        return 1
      }
      if (parsedV1 < parsedV2) {
        return -1
      }
    }

    if (v1.length === v2.length) {
      return 0
    }

    return v1.length < v2.length ? -1 : 1
  }

  static filterOlderVersions(manuscripts: Manuscript[]): Manuscript[] {
    const submissions = groupBy(manuscripts, 'submissionId')
    return Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      // @ts-expect-error TS(2571): Object is of type 'unknown'.
      const sortedVersions = versions.sort(this.compareVersion)
      const latestManuscript = last(sortedVersions) as Manuscript

      if (latestManuscript.status === this.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })
  }

  private static filterByRole(
    query: Objection.QueryBuilderType<Manuscript>,
    user: UserI,
    Team: TeamRepository,
    ArticleType: ArticleTypeRepository,
    role: TeamMemberRole,
  ): Objection.QueryBuilder<Manuscript, Manuscript[]> {
    const filterOutDeleted = (query: Objection.QueryBuilderType<Manuscript>) =>
      query.whereNot('m.status', 'deleted')

    const filterOutRevisionDrafts = (
      query: Objection.QueryBuilderType<Manuscript>,
    ) =>
      query.where(query =>
        query.where('m.version', 1).orWhereNot('m.status', 'draft'),
      )

    const filterTeamsWhereUserActive = (
      query: Objection.QueryBuilderType<Manuscript>,
    ) =>
      query
        .whereNotIn('tm.status', [
          'declined',
          'expired',
          'removed',
          'conflicting',
        ])
        .whereNotNull('t.manuscript_id')

    const filterOnlyWhereUserAssigned = (
      query: Objection.QueryBuilderType<Manuscript>,
      userId: string,
    ) => query.where('u.id', userId)

    const filterWithdrawnIfUserNotAuthor = (
      query: Objection.QueryBuilderType<Manuscript>, // composed where clause
    ) =>
      query.where(query =>
        query
          .where('t.role', Team.Role.author)
          .orWhereNot('m.status', 'withdrawn'),
      )

    const filterVoidIfUserNotAuthorOrAdmin = (
      query: Objection.QueryBuilderType<Manuscript>,
    ) =>
      query.where(query =>
        query
          .where('t.role', Team.Role.author)
          .orWhere('t.role', Team.Role.admin)
          .orWhereNot('m.status', 'void'),
      )

    const filterOutSATs = (query: Objection.QueryBuilderType<Manuscript>) =>
      query.where(query =>
        query
          .whereNotIn('type.name', ArticleType.ShortArticleTypes)
          .orWhereNull('m.article_type_id'),
      )

    switch (role) {
      case Team.Role.editorialAssistant:
        return query
          .modify(filterTeamsWhereUserActive)
          .modify(filterVoidIfUserNotAuthorOrAdmin)
          .modify(filterOutRevisionDrafts)
          .modify(filterOnlyWhereUserAssigned, user.id)
          .modify(filterOutSATs)
      case Team.Role.admin:
        return query.modify(filterOutRevisionDrafts)
      default:
        return query
          .modify(filterOutDeleted)
          .modify(filterTeamsWhereUserActive)
          .modify(filterWithdrawnIfUserNotAuthor)
          .modify(filterVoidIfUserNotAuthorOrAdmin)
          .modify(filterOutRevisionDrafts)
          .modify(filterOnlyWhereUserAssigned, user.id)
          .modify(filterOutSATs)
    }
  }

  private static getLatestVersionOfAllSubmissionsForAdmin() {
    const statuses = this.query()
      .select(
        'r.name as role_name',
        'ss.id as status_id',
        'ss.name as status_name',
        'ss.category as status_category',
        'sl.id as label_id',
        'sl.priority as label_priority',
        'sl.submission_filter_id as filter_id',
        'sl.name as label_name',
        'sf.name as filter_name',
      )
      .from('role as r')
      .where('r.name', 'admin')
      .join('submission_label as sl', 'r.id', 'sl.roleId')
      .join('submission_status as ss', 'sl.submission_status_id', 'ss.id')
      .leftJoin('submission_filter as sf', 'sl.submission_filter_id', 'sf.id')
      .as('statuses')

    return this.query()
      .alias('m')
      .select(
        raw(
          `DISTINCT ON (m.submission_id) m.*,
          statuses.role_name as role,
          statuses.filter_name as filter,
          statuses.label_name as visible_status,
          statuses.status_category as status_category,
          statuses.label_priority as label_priority,
          j.name as journal_name`,
        ),
      )
      .orderByRaw(
        `m.submission_id,
        string_to_array(m.version, '.')::int[] DESC,
        statuses.label_priority DESC,
        m.id`,
      )
      .join(statuses, 'm.status', 'statuses.statusName') // TODO: change the role.name to role.id
      .leftJoin('journal as j', 'j.id', 'm.journal_id')
  }

  private static getLatestVersionOfAllSubmissions(): Objection.QueryBuilder<
    Manuscript,
    Manuscript[]
  > {
    return this.query()
      .alias('m')
      .select(
        raw(
          `DISTINCT ON (m.submission_id) m.*,
          t.role as role,
          sf.name as filter,
          sl.name as visible_status,
          ss.category as status_category,
          sl.priority as label_priority,
          j.name as journal_name`,
        ),
      )
      .orderByRaw(
        `m.submission_id,
        string_to_array(m.version, '.')::int[] DESC,
        sl.priority DESC,
        m.id`,
      )
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .join('user as u', 'tm.userId', 'u.id')
      .join('role as r', 'r.name', 't.role') // TODO: change the role.name to role.id
      .join('submission_status as ss', 'm.status', 'ss.name') // TODO: change ss.name to ss.id
      .join('submission_label as sl', function(this: any) {
        this.on('r.id', 'sl.roleId')
        this.andOn('ss.id', 'sl.submissionStatusId')
      })
      .leftJoin('submission_filter as sf', 'sl.submission_filter_id', 'sf.id')
      .leftJoin('journal as j', 'j.id', 'm.journal_id')
      .leftJoin('article_type as type', 'type.id', 'm.article_type_id')
  }

  private static filterBySearchValue(
    query: Objection.QueryBuilderType<Manuscript>,
    searchValue: string,
    manuscriptPropertyFilter: string,
  ) {
    if (!manuscriptPropertyFilter || !manuscriptPropertyFilter.length)
      return query
    if (!searchValue || !searchValue.length) return query

    const filterByCustomId = (
      query: Objection.QueryBuilderType<Manuscript>,
      searchValue: string,
    ) => query.where('custom_id', searchValue)

    const filterByTitle = (
      query: Objection.QueryBuilderType<Manuscript>,
      searchValue: string,
    ) =>
      query.whereRaw('lower(title) LIKE ?', [`%${searchValue.toLowerCase()}%`])

    const filterByJournalName = (
      query: Objection.QueryBuilderType<Manuscript>,
      searchValue: string,
    ) =>
      query.whereRaw('lower(journal_name) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])

    switch (manuscriptPropertyFilter) {
      case 'customId':
        return query.modify(filterByCustomId, searchValue)
      case 'title':
        return query.modify(filterByTitle, searchValue)
      case 'journalName':
        return query.modify(filterByJournalName, searchValue)
      default:
        throw new Error(`The filter is not defined.`)
    }
  }

  static async getManuscripts({
    user,
    searchValue = '',
    statusFilter = 'all',
    manuscriptPropertyFilter = '',
    dateOrder = 'desc',
    page = 0,
    pageSize = 10,
    Team,
    ArticleType,
    role = '',
  }: {
    user: UserI
    searchValue?: string
    statusFilter?: string
    manuscriptPropertyFilter?: string
    dateOrder?: string
    page?: number
    pageSize?: number
    Team: TeamRepository
    ArticleType: ArticleTypeRepository
    role?: TeamMemberRole | ''
  }) {
    const filterByStatus = (query: any, statusFilter: any) => {
      if (!statusFilter || statusFilter === 'all') return query
      return query.where('status', statusFilter)
    }

    const versionQuery =
      role === Team.Role.admin
        ? this.getLatestVersionOfAllSubmissionsForAdmin.bind(this)
        : this.getLatestVersionOfAllSubmissions.bind(this)

    const versions = versionQuery()
      .clone()
      .modify(this.filterByRole, user, Team, ArticleType, role)
      .as('versions')

    return this.query()
      .select('*')
      .from(versions)
      .modify(filterByStatus, statusFilter)
      .modify(this.filterBySearchValue, searchValue, manuscriptPropertyFilter)
      .orderBy('updated', dateOrder as OrderByDirection)
      .page(page, pageSize)
      .withGraphFetched(
        '[articleType, journal.peerReviewModel, specialIssue.peerReviewModel]',
      )
  }

  static async _findAllOrderedByVersion({
    order = 'asc',
    queryObject,
    eagerLoadRelations,
  }: {
    order?: string
    queryObject: any
    eagerLoadRelations: string[] | string
  }) {
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findByCustomId(customId: string) {
    return this.query()
      .where({ customId })
      .first()
  }
  static async findLatestVersionByCustomId(customId: string) {
    return this.query()
      .where({ customId })
      .andWhere('isLatestVersion', true)
      .first()
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  }: {
    order?: string
    orderByField: string
    queryObject: any
    eagerLoadRelations: string[] | string
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findAllOrderedByVersion({
        order,
        queryObject,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderBy(orderByField, order as OrderByDirection)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllByJournalAndUserAndRole({
    role,
    limit,
    userId,
    journalId,
    eagerLoadRelations,
  }: {
    role: string
    limit: number
    userId: string
    journalId: string
    eagerLoadRelations: string | string[]
  }) {
    return this.query()
      .skipUndefined()
      .select('m.*')
      .from('manuscript AS m')
      .join('journal as j', 'j.id', 'm.journalId')
      .join('team as t', 't.manuscript_id', 'm.id')
      .join('team_member as tm', 'tm.team_id', 't.id')
      .where('t.role', role)
      .andWhere('tm.user_id', userId)
      .andWhere('j.id', journalId)
      .limit(limit)
      .withGraphJoined(this._parseEagerRelations(eagerLoadRelations))
  }

  static findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses({
    role,
    limit,
    userId,
    journalId,
    manuscriptStatuses,
    teamMemberStatuses,
  }: {
    role: string
    limit: number
    userId: string
    journalId: string
    manuscriptStatuses: ManuscriptStatus[]
    teamMemberStatuses: TeamMemberStatus[]
  }) {
    return this.query()
      .skipUndefined()
      .select(raw('DISTINCT ON (m.submission_id) m.submission_id'))
      .from('manuscript AS m')
      .join('journal as j', 'j.id', 'm.journalId')
      .join('team as t', 't.manuscript_id', 'm.id')
      .join('team_member as tm', 'tm.team_id', 't.id')
      .whereIn('tm.status', teamMemberStatuses)
      .whereIn('m.status', manuscriptStatuses)
      .whereNotNull('m.submitted_date')
      .andWhere('t.role', role)
      .andWhere('tm.user_id', userId)
      .andWhere('j.id', journalId)
      .limit(limit)
  }

  static findAllByJournalAndUserAndRoleAndTeamMemberStatuses({
    role,
    limit,
    userId,
    journalId,
    teamMemberStatuses,
    eagerLoadRelations,
  }: {
    role: string
    limit: number
    userId: string
    journalId: string
    eagerLoadRelations: string | string[]
    teamMemberStatuses: TeamMemberStatus[]
  }) {
    return this.findAllByJournalAndUserAndRole({
      role,
      limit,
      userId,
      journalId,
      eagerLoadRelations,
    })
      .clone()
      .whereIn('tm.status', teamMemberStatuses)
  }

  static findAllByJournalAndUserAndRoleAndTeamMemberStatusesAndManuscriptStatuses({
    role,
    limit,
    userId,
    journalId,
    teamMemberStatuses,
    manuscriptStatuses,
    eagerLoadRelations,
  }: {
    role: string
    limit: number
    userId: string
    journalId: string
    eagerLoadRelations: string | string[]
    manuscriptStatuses: ManuscriptStatus[]
    teamMemberStatuses: TeamMemberStatus[]
  }) {
    return this.findAllByJournalAndUserAndRoleAndTeamMemberStatuses({
      role,
      limit,
      userId,
      journalId,
      teamMemberStatuses,
      eagerLoadRelations,
    })
      .clone()
      .whereIn('m.status', manuscriptStatuses)
      .whereNotNull('m.submitted_date')
  }

  static async findManuscriptByTeamMember(teamMemberId: string) {
    try {
      return this.query()
        .select('m.*')
        .from('manuscript AS m')
        .join('team AS t', 'm.id', 't.manuscript_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)
        .limit(1)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async _findManuscriptsBySubmissionIdOrderedByVersion({
    order = 'asc',
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }: {
    order?: string
    submissionId: string
    excludedStatus: ManuscriptStatus
    eagerLoadRelations: string | string[]
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findManuscriptsBySubmissionId({
    order,
    orderByField,
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }: {
    order: string
    orderByField: string
    submissionId: string
    excludedStatus: ManuscriptStatus
    eagerLoadRelations: string | string[]
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findManuscriptsBySubmissionIdOrderedByVersion({
        order,
        submissionId,
        excludedStatus,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order as OrderByDirection)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findOneBySubmissionIdAndStatuses({
    statuses,
    submissionId,
    eagerLoadRelations,
  }: {
    statuses: ManuscriptStatus[]
    submissionId: string
    eagerLoadRelations: string | string[]
  }) {
    try {
      // added await, should check if breaks
      const results = await this.query()
        .select('m.*')
        .from('manuscript as m')
        .whereIn('m.status', statuses)
        .andWhere('m.submission_id', submissionId)
        .withGraphJoined(this._parseEagerRelations(eagerLoadRelations))

      if (results.length > 0) return results[0]
      return undefined
    } catch (e) {
      throw new Error(e)
    }
  }

  static findDisplayDetailsOfSubmissionByRole({
    submissionId,
    userRole,
  }: {
    submissionId: string
    userRole: string
  }) {
    return this.query()
      .select(
        'ss.category as category',
        'sl.name as visible_status',
        'm.version',
      )
      .from('submission_status as ss')
      .join('submission_label as sl', 'ss.id', 'sl.submissionStatusId')
      .join('manuscript as m', 'ss.name', 'm.status')
      .join('role as r', 'sl.roleId', 'r.id')
      .whereNot({ status: this.Statuses.draft })
      .andWhere('r.name', userRole)
      .andWhere('m.submission_id', submissionId)
      .orderByRaw(`string_to_array(m.version, '.')::int[] DESC`)
      .limit(1)
      .as('displayDetails')
  }

  static async findAllBySubmissionAndUserAndRole({
    userId,
    userRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }: {
    submissionId: string
    userRole: string
    userId: string
    excludedStatuses: ManuscriptStatus[]
    eagerLoadRelations: string | string[]
  }) {
    const displayDetails = this.findDisplayDetailsOfSubmissionByRole({
      submissionId,
      userRole,
    })

    const statusCategory = this.query()
      .select('category')
      .from(displayDetails)
      .as('statusCategory')

    const visible_status = this.query()
      .select('visible_status')
      .from(displayDetails)
      .as('visible_status')

    const manuscripts = this.query()
      .select(
        raw(`DISTINCT ON (m.id) m.*, t.role`),
        statusCategory,
        visible_status,
      )
      .from('manuscript AS m')
      .join('team AS t', 'm.id', 't.manuscript_id')
      .join('team_member AS tm', 't.id', 'tm.team_id')
      .join('user as u', 'tm.userId', 'u.id')
      .join('submission_status as ss', 'm.status', 'ss.name')
      .leftJoin('submission_label as sl', 'ss.id', 'sl.submissionStatusId')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('tm.user_id', userId)
      .andWhere('m.submission_id', submissionId)
      .andWhere('t.role', userRole)
      .andWhere((query: any) =>
        query
          .whereNotIn('tm.status', [
            'declined',
            'expired',
            'removed',
            'conflicting',
          ])
          .whereNotNull('t.manuscript_id'),
      )
      .orderByRaw(`m.id, string_to_array(version, '.')::int[] DESC`)
      .as('manuscripts')

    return this.query()
      .select('*')
      .from(manuscripts)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static findAllByJournalAndStatuses({
    journalId,
    statuses,
  }: {
    journalId: string
    statuses: ManuscriptStatus[]
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript as m')
      .whereIn('m.status', statuses)
      .andWhere('m.journal_id', journalId)
  }

  static countSubmissionsByJournalAndStatuses({
    journalId,
    statuses,
  }: {
    journalId: string
    statuses: ManuscriptStatus[]
  }) {
    const distinctSubmissions = this.query()
      .select(raw(`DISTINCT ON (m.submission_id) m.*`))
      .from('manuscript as m')
      .whereIn('m.status', statuses)
      .whereNotNull('m.submitted_date')
      .andWhere('m.journal_id', journalId)
      .as('distinctSubmissions')

    return this.query()
      .select(raw('count(*)::integer'))
      .from(distinctSubmissions)
      .first()
  }

  static async findAllForAdminBySubmission({
    adminRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }: {
    adminRole: string
    submissionId: string
    excludedStatuses: ManuscriptStatus[]
    eagerLoadRelations: string[] | string
  }) {
    const displayDetails = this.findDisplayDetailsOfSubmissionByRole({
      submissionId,
      userRole: adminRole,
    })

    const statusCategory = this.query()
      .select('category')
      .from(displayDetails)
      .as('statusCategory')

    const visible_status = this.query()
      .select('visible_status')
      .from(displayDetails)
      .as('visible_status')

    return this.query()
      .select('m.*', raw("'admin' as role"), statusCategory, visible_status)
      .from('manuscript AS m')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('m.submission_id', submissionId)
      .orderByRaw(`string_to_array(version, '.')::int[] DESC`)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  // Temporary query for redirect
  static async findLastManuscriptByCustomId({
    customId,
  }: {
    customId: string
  }) {
    return this.query()
      .whereNot({ status: this.Statuses.draft })
      .andWhere({ customId })
      .orderByRaw(`string_to_array(version, '.')::int[] desc`)
      .first()
  }

  static async findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }: {
    submissionId: string
    eagerLoadRelations: string[] | string
  }) {
    return this.query()
      .whereNot({ status: this.Statuses.draft })
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] desc`)
      .first()
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findAllByExcludedStatuses({
    excludedStatuses,
  }: {
    excludedStatuses: ManuscriptStatus[]
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .whereNotIn('m.status', excludedStatuses)
      .whereNot(builder =>
        builder.where('version', 1).andWhere('status', 'draft'),
      )
  }

  static async findAllByStatusFromSpecialIssue({
    status,
  }: {
    status: ManuscriptStatus
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .where('m.status', status)
      .whereNotNull('m.special_issue_id')
  }

  // This method is needed for production-package event.
  // We found some manuscripts that don't have peer_review_passed_date set for each subversion.
  // To fix this for the moment, we'll extract the date from the entry where it exists.
  static async getPeerReviewPassedDate({ submissionId }) {
    return this.query()
      .select('m.peer_review_passed_date')
      .from('manuscript AS m')
      .where('m.submissionId', submissionId)
      .whereNotNull('m.peer_review_passed_date')
      .orderBy('m.version', 'desc')
      .first()
      .then(result => (result ? result.peerReviewPassedDate : undefined))
  }

  static async getFinalRevisionDate({
    submissionId,
  }: {
    submissionId: string
  }) {
    return this.query()
      .select('r.submitted')
      .from('review AS r')
      .join('manuscript as m', 'r.manuscript_id', 'm.id')
      .where('r.recommendation', 'responseToRevision')
      .andWhere('m.submissionId', submissionId)
      .orderBy('r.submitted', 'desc')
      .first()
      .then((result: any) => (result ? result.submitted : undefined))
  }

  // to delete after running replaceManuscriptEAs script
  static async findManuscriptsWithWrongEA({
    Team,
    TeamMember,
    journalId,
    journalEditorialAssistantsUserIds,
  }: {
    Team: TeamRepository
    TeamMember: TeamMemberRepository
    journalId: string
    journalEditorialAssistantsUserIds: string[]
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .whereNotIn('tm.user_id', journalEditorialAssistantsUserIds)
      .andWhere('m.journal_id', journalId)
      .andWhere('tm.status', TeamMember.Statuses.active)
      .andWhere('t.role', Team.Role.editorialAssistant)
  }

  static async findAllWithWrongTriageEditor({
    Team,
    TeamMember,
  }: {
    Team: TeamRepository
    TeamMember: TeamMemberRepository
  }) {
    return this.query()
      .select('m.*')
      .from('manuscript AS m')
      .join('team as t', 'm.id', 't.manuscriptId')
      .join('team_member as tm', 't.id', 'tm.teamId')
      .whereNotExists((query: any) => {
        query
          .select('tmj.id')
          .from('team_member as tmj')
          .join('team as t', 'tmj.team_id', 't.id')
          .where('t.role', Team.Role.triageEditor)
          .andWhere(ref('m.section_id'), null)
          .andWhere(ref('m.special_issue_id'), null)
          .andWhere(`t.journal_id`, ref(`m.journal_id`))
          .andWhere('tmj.user_id', ref('tm.user_id'))
      })
      .andWhere((query: any) => {
        query.whereNotExists((query: any) => {
          query
            .select('tms.id')
            .from('team_member as tms')
            .join('team as t', 'tms.team_id', 't.id')
            .where('t.role', Team.Role.triageEditor)
            .andWhere(ref('m.special_issue_id'), null)
            .andWhere(`t.section_id`, ref(`m.section_id`))
            .andWhere('tms.user_id', ref('tm.user_id'))
        })
      })
      .andWhere((query: any) => {
        query.whereNotExists((query: any) => {
          query
            .select('tmsi.id')
            .from('team_member as tmsi')
            .join('team as t', 'tmsi.team_id', 't.id')
            .where('t.role', Team.Role.triageEditor)
            .andWhere(`t.special_issue_id`, ref(`m.special_issue_id`))
            .andWhere('tmsi.user_id', ref('tm.user_id'))
        })
      })
      .andWhere('tm.status', TeamMember.Statuses.active)
      .andWhere('t.role', Team.Role.triageEditor)
      .andWhere('m.status', 'in', this.InProgressStatuses)
  }

  static async generateUniqueCustomId(maxTries: number) {
    const uniqueCustomIdPrefix = process.env.UNIQUE_CUSTOM_ID_PREFIX || 55
    const id = `${uniqueCustomIdPrefix +
      Math.round(10000 + Math.random() * 89999).toString()}`
    const found = await this.findByCustomId(id)

    if (!found) {
      return id
    }

    if (maxTries <= 0) {
      return undefined
    }

    return this.generateUniqueCustomId(maxTries - 1)
  }

  async getHasSpecialIssueEditorialConflictOfInterest({
    TeamMember,
    Team,
  }: {
    Team: TeamRepository
    TeamMember: TeamMemberRepository
  }) {
    if (!this.specialIssueId) return false

    let authors: (AuthorI | TeamMemberI)[] = []
    if (this.authors && this.authors.length > 0) {
      authors = this.authors
    } else {
      authors = await TeamMember.findAllByManuscriptAndRole({
        manuscriptId: this.id,
        role: Team.Role.author,
      })
    }

    const userIds = authors.map((a: any) => a.userId)

    // authors are only manuscript specific team members
    const editor = await TeamMember.findOneBySpecialIssueAndUsers({
      specialIssueId: this.specialIssueId,
      userIds,
    })

    return !!editor
  }

  async getEditorLabel({
    PeerReviewModel,
    TeamMember,
    Team,
    role,
  }: {
    Team: TeamRepository
    TeamMember: TeamMemberRepository
    PeerReviewModel: PeerReviewModelRepository
    role: string
  }) {
    let peerReviewModel

    const hasSpecialIssueEditorialConflictOfInterest = await this.getHasSpecialIssueEditorialConflictOfInterest(
      { TeamMember, Team },
    )
    if (this.specialIssueId && !hasSpecialIssueEditorialConflictOfInterest) {
      peerReviewModel = await PeerReviewModel.findOneBySpecialIssue(
        this.specialIssueId,
      )
    } else {
      peerReviewModel = await PeerReviewModel.findOneByJournal(this.journalId!)
    }
    return peerReviewModel[`${role}Label`]
  }

  getSubmittingAuthor(): TeamMemberI | undefined {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find((t: any) => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (!authorTeam.members || authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find((tm: any) => tm.isSubmitting)
  }

  getAuthors(): TeamMemberI[] | undefined {
    if (!this.teams) {
      logger.warn('Cannot get authors when teams are not loaded.')
      return undefined
    }

    const authorTeam = this.teams.find((t: any) => t.role === 'author')

    if (!authorTeam || !authorTeam.members) {
      return []
    }

    return sortBy(authorTeam.members, 'position')
  }

  getReviewers(): TeamMemberI[] | undefined {
    if (!this.teams) {
      logger.warn('Cannot get reviewers when teams are not loaded.')
      return undefined
    }

    const reviewerTeam = this.teams.find((t: any) => t.role === 'reviewer')
    if (!reviewerTeam || !reviewerTeam.members) {
      return []
    }

    return reviewerTeam.members
  }

  getReviewersForEventData({ Team }: { Team: TeamRepository }): any[] {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    if (!this.reviewerSuggestions) {
      throw new Error('ReviewerSuggestions are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || !reviewerTeam.members) {
      return []
    }

    return reviewerTeam.members.map(reviewer => {
      const reviewerData = pick(reviewer, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'responded',
      ])

      const orcidIdentity = reviewer?.user?.identities?.find(
        identity => identity.type === 'orcid',
      )

      const reviewerSuggestions = this.reviewerSuggestions || []
      const externalReviewer = reviewerSuggestions.find(
        (rs: any) => rs.email === reviewer.alias.email,
      )

      return {
        ...reviewerData,
        ...reviewer.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        fromService: externalReviewer ? externalReviewer.type : '',
        ...reviewer.getDatesForEvents({
          Team,
          role: Team.Role.reviewer,
        }),
      }
    })
  }

  getReviewsForEventData() {
    if (!this.reviews) {
      return undefined
    }

    return this.reviews
      .filter(review => review.submitted !== null)
      .map(review => ({
        ...omit(review, [
          'manuscriptId',
          'rejectDecisionInfoId',
          'rejectDecisionInfo',
        ]),

        comments: review.comments?.map(comment => omit(comment, 'reviewId')),

        decisionInfo: review.rejectDecisionInfo
          ? review.rejectDecisionInfo.toDTO()
          : undefined,
      }))
  }

  getAuthorsForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find((t: any) => t.role === 'author')
    if (!authorTeam || !authorTeam.members) {
      return []
    }

    return authorTeam.members.map(author => {
      const authorData = pick(author, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'position',
        'isSubmitting',
        'isCorresponding',
      ])
      const orcidIdentity = author?.user?.identities?.find(
        identity => identity.type === 'orcid',
      )

      return {
        ...authorData,
        ...author.alias,
        affiliation: {
          name: author.alias.aff,
          ror: {
            id: author.alias.affRorId,
          },
        },
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        assignedDate: author.created,
      }
    })
  }
  getSubmittingStaffMembersForEventData({ Team }: any) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const submittingStaffMemberTeam = this.teams.find(
      (t: any) => t.role === Team.Role.submittingStaffMember,
    )
    if (!submittingStaffMemberTeam || !submittingStaffMemberTeam.members) {
      return []
    }
    return submittingStaffMemberTeam.members.map(
      (submittingStaffMember: any) => {
        const submittingStaffMemberData = pick(submittingStaffMember, [
          'id',
          'created',
          'updated',
          'userId',
          'status',
        ])
        const orcidIdentity = submittingStaffMember.user.identities.find(
          (identity: any) => identity.type === 'orcid',
        )

        return {
          ...submittingStaffMemberData,
          ...submittingStaffMember.alias,
          orcidId: orcidIdentity ? orcidIdentity.identifier : '',
          assignedDate: submittingStaffMember.created,
        }
      },
    )
  }

  getAcademicEditor() {
    if (!this.teams) {
      logger.warn('Cannot get academic editor when teams are not loaded.')
      return undefined
    }

    return chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()
  }

  getPendingAcademicEditor() {
    if (!this.teams) {
      logger.warn(
        'Cannot get pending academic editor when teams are not loaded.',
      )
      return undefined
    }

    return chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'pending')
      .value()
  }

  getRIPE() {
    if (!this.teams) {
      logger.warn('Cannot get RIPE when teams are not loaded.')
      return undefined
    }

    return chain(this.teams)
      .find(t => t.role === 'researchIntegrityPublishingEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()
  }

  _getTriageEditor() {
    if (!this.teams) {
      logger.warn('Cannot get triage editor when teams are not loaded.')
      return undefined
    }

    const triageEditorTeam = this.teams.find(
      team => team.role === 'triageEditor',
    )

    if (!triageEditorTeam) {
      return undefined
    }

    if (!triageEditorTeam.members) {
      logger.warn('Cannot get triage editor when team members are not loaded.')
      return undefined
    }

    return triageEditorTeam.members.find(member => member.status === 'active')
  }

  getAcademicEditorByStatus(status: any) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    return chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === status)
      .value()
  }

  assignTeam(team: any) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  assignFile(file: any) {
    this.files = this.files || []
    this.files.push(file)
  }

  assignReview(review: any) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = uuid.v4()
    this.submittedDate = new Date().toISOString()
  }

  updateStatus(status: any) {
    this.status = status
    return this
  }

  updateIsLatestVersionFlag(newStatus: any) {
    this.isLatestVersion = newStatus
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      (review: any) =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }
    if (
      responseToRevisionRequest.comments &&
      responseToRevisionRequest.comments[0]
    ) {
      this.comment = responseToRevisionRequest.comments[0]?.toDTO()
    }
  }

  getLatestEditorReview() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const editorReviews = this.reviews.filter((review: any) =>
      ['admin', 'triageEditor', 'academicEditor'].includes(
        review.member.team.role,
      ),
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getLatestAcademicEditorRecommendation() {
    if (!this.reviews) {
      return undefined
    }

    const editorReviews = this.reviews.filter(
      review => get(review, 'member.team.role') === 'academicEditor',
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getReviewersByStatus(status: any) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find((t: any) => t.role === 'reviewer')
    if (!reviewerTeam) {
      throw new Error('Reviewer team not found.')
    }
    if (!reviewerTeam.members?.length) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter((m: any) => m.status === status)
  }

  getCustomId() {
    return this.role === 'author' &&
      [Manuscript.Statuses.draft, Manuscript.Statuses.technicalChecks].includes(
        this.status,
      )
      ? undefined
      : this.customId
  }

  async updateWithFiles(files: any) {
    try {
      await transaction(Manuscript.knex(), async (trx: any) =>
        this.$relatedQuery('files', trx).upsertGraph(files, {
          noDelete: true,
          noInsert: true,
          relate: true,
        }),
      )
    } catch (err) {
      logger.error(noDataError, err)
      throw new Error(noDataError)
    }
  }

  static async updateManuscriptAndTeams({ manuscript, teams, Team }: any) {
    return transaction(Manuscript.knex(), async (trx: any) => {
      await manuscript.$query(trx).update(manuscript)
      await Team.updateAndSaveMany(teams, trx)
    })
  }
  static async updateManuscriptAndTeamMembers({
    manuscript,
    teamMembers,
    TeamMember,
  }: any) {
    return transaction(Manuscript.knex(), async (trx: any) => {
      await manuscript.$query(trx).update(manuscript)
      await TeamMember.updateAndSaveMany(teamMembers, trx)
    })
  }

  toDTO(): object {
    const meta = pick(this, [
      'title',
      'agreeTc',
      'abstract',
      'articleTypeId',
      'dataAvailability',
      'fundingStatement',
      'conflictOfInterest',
    ])

    return Object.assign(this, {
      meta,
      customId: this.getCustomId(),
      section: this.section ? this.section.toDTO() : undefined,
      articleType: this.articleType ? this.articleType.toDTO() : undefined,
      teams:
        this.teams && this.teams.length > 0
          ? this.teams.map(team => team.toDTO())
          : [],
    })
  }
}
