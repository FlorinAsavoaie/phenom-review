import pLimit from 'p-limit'
import config from 'config'
import { jobs } from 'pubsweet-server/src'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { TeamMember } from './teamMember'
import { Manuscript } from './manuscript'
import { Journal } from './journal'

const { logger } = require('component-logger')

const newJobCheckIntervalSeconds = config.get(
  'newJobCheckIntervalSeconds',
) as number
const jobScheduling = config.get('jobScheduling') === 'true'

const limit = pLimit(10)

export type JobType = 'activation' | 'deactivation'

export interface JobI extends HindawiBaseModelProps {
  teamMemberId: string
  manuscriptId?: string
  journalId?: string
  specialIssueId?: string
  jobType?: JobType

  delete(): Promise<JobI>
}

export interface JobRepository extends HindawiBaseModelRepository<JobI> {
  new (...args: any[]): JobI

  findOneBy(options: {
    queryObject: { jobType: any; specialIssueId: string }
  }): Promise<JobI | undefined>
  schedule({
    params,
    jobType,
    journalId,
    teamMemberId,
    manuscriptId,
    executionDate,
    specialIssueId,
    queueName,
  }: {
    params: Record<string, unknown>
    jobType: JobType
    journalId: string
    teamMemberId: string
    manuscriptId: string
    executionDate: string
    specialIssueId: string
    queueName: string
  }): Promise<JobI | undefined>
  cancel(id: string): Promise<void>
  subscribe({
    queueName,
    jobHandler,
  }: {
    queueName: string
    jobHandler: JobHandler
  }): Promise<void>
  findAllByTeamMember(teamMemberId: string): Promise<JobI[]>
  findAllByTeamMembers(teamMemberIds: string[]): Promise<JobI[]>
  findAllByTeamMemberAndManuscript({
    teamMemberId,
    manuscriptId,
  }: {
    teamMemberId: string
    manuscriptId: string
  }): Promise<JobI[]>
  findAllWithSpecialIssue(): Promise<JobI[]>
}

export interface JobHandler {
  (arg: JobI): Promise<void>
}

export class Job extends HindawiBaseModel implements JobI {
  teamMemberId: string
  manuscriptId?: string | undefined
  journalId?: string | undefined
  specialIssueId?: string | undefined
  jobType?: JobType | undefined

  static get tableName() {
    return 'job'
  }

  static get schema() {
    return {
      properties: {
        teamMemberId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        jobType: {
          type: ['string', 'null'],
          default: null,
          enum: [...Object.values(Job.Type), null],
        },
      },
    }
  }

  static get Type(): Record<JobType, JobType> {
    return {
      activation: 'activation',
      deactivation: 'deactivation',
    }
  }

  static get relationMappings() {
    return {
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: TeamMember,
        join: {
          from: 'job.teamMemberId',
          to: 'team_member.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'job.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'job.journalId',
          to: 'journal.id',
        },
      },
    }
  }

  static async schedule({
    params,
    jobType,
    journalId,
    teamMemberId,
    manuscriptId,
    executionDate,
    specialIssueId,
    queueName = 'review',
  }: {
    params: Record<string, unknown>
    jobType: JobType
    journalId: string
    teamMemberId: string
    manuscriptId: string
    executionDate: string
    specialIssueId: string
    queueName: string
  }): Promise<Job | undefined> {
    if (!jobScheduling) {
      logger.warn('Jobs are deactivated. No jobs have been scheduled. ')
      return undefined
    }

    const jobQueue = await jobs.connectToJobQueue()

    params = JSON.parse(JSON.stringify(params))

    try {
      const jobId = await limit(() =>
        jobQueue.publishAfter(queueName, params, {}, executionDate),
      )

      const job = new Job({
        jobType,
        id: jobId,
        journalId,
        teamMemberId,
        manuscriptId,
        specialIssueId,
      })
      await job.save()

      logger.info(
        `Successfully scheduled job ${jobId} on queue ${queueName} at: ${executionDate}`,
      )

      return job
    } catch (e) {
      throw new Error(e)
    }
  }

  static async cancel(id: string): Promise<void> {
    const jobQueue = await jobs.connectToJobQueue()

    try {
      await jobQueue.cancel(id)
      logger.info(`Successfully cancelled job ${id}`)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async subscribe({
    queueName,
    jobHandler,
  }: {
    queueName: string
    jobHandler: JobHandler
  }): Promise<void> {
    const jobQueue = await jobs.connectToJobQueue()

    jobQueue.onComplete(queueName, (job: any) => {
      logger.info('Successfully completed job', job)
    })

    return jobQueue.subscribe(
      queueName,
      {
        newJobCheckIntervalSeconds,
        teamSize: 10,
        teamConcurrency: 10,
      },
      jobHandler,
    )
  }

  static async findAllByTeamMember(teamMemberId: string): Promise<Job[]> {
    try {
      return await this.query()
        .select('j.*')
        .from('job AS j')
        .where('j.team_member_id', teamMemberId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByTeamMembers(teamMemberIds: string[]): Promise<Job[]> {
    return this.query()
      .select('j.*')
      .from('job AS j')
      .whereIn('j.team_member_id', teamMemberIds)
  }

  static async findAllByTeamMemberAndManuscript({
    teamMemberId,
    manuscriptId,
  }: {
    teamMemberId: string
    manuscriptId: string
  }): Promise<Job[]> {
    try {
      return await this.query()
        .select('job.*')
        .from('job')
        .where('job.team_member_id', teamMemberId)
        .andWhere('job.manuscriptId', manuscriptId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllWithSpecialIssue(): Promise<Job[]> {
    return this.query().whereNotNull('special_issue_id')
  }
}
