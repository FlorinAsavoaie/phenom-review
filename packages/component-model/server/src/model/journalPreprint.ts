import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Journal, JournalI } from './journal'
import { Preprint, PreprintI } from './preprint'

export interface JournalPreprintI extends HindawiBaseModelProps {
  journalId: string
  preprintId: string
  description: string

  journal?: JournalI
  preprint?: PreprintI
}

export interface JournalPreprintRepository
  extends HindawiBaseModelRepository<JournalPreprintI> {
  new (...args: any[]): JournalPreprintI
}

export class JournalPreprint extends HindawiBaseModel
  implements JournalPreprintI {
  journalId: string
  preprintId: string
  description: string

  journal?: JournalI
  preprint?: PreprintI

  static get tableName() {
    return 'journal_preprint'
  }

  static get schema() {
    return {
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        preprintId: { type: 'string', format: 'uuid' },
        description: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'journal_preprint.journalId',
          to: 'journal.id',
        },
      },
      preprint: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Preprint,
        join: {
          from: 'journal_preprint.preprintId',
          to: 'preprint.id',
        },
      },
    }
  }
}
