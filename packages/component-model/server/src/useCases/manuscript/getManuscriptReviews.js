const transformReviewsForReviewer = ({
  reviews,
  latestManuscript,
  userId,
  Team,
  Comment,
  manuscript,
}) =>
  reviews
    .map(r => {
      const isOwnReview = r.member.userId === userId
      const isLatestVersion = manuscript.id === latestManuscript.id
      const isReviewerReview = r.member.team.role === Team.Role.reviewer

      // Only show own reviewer reviews for last version
      if (isLatestVersion && isReviewerReview && !isOwnReview) return false

      /// Hide reviewer names from other reviewer reviews
      if (isReviewerReview && !isOwnReview) delete r.member.alias

      // Hide private comments from unowned reviews
      if (!isOwnReview)
        r.comments = r.comments.filter(c => c.type === Comment.Types.public)

      return r
    })
    .filter(r => r)

const transformReviewsForAuthor = ({ reviews, userId, Comment }) =>
  reviews.map(r => {
    const isOwnReview = r.member.userId === userId
    // Only show public comments from other reviews
    if (!isOwnReview)
      r.comments = r.comments.filter(c => c.type === Comment.Types.public)
    return r
  })

const removeRejectDecisionInfo = reviews =>
  reviews.map(review => {
    delete review.rejectDecisionInfo
    return review
  })

module.exports.initialize = models => ({
  execute: async ({ manuscript, userId }) => {
    const { Manuscript, Review, Comment, Team, TeamMember } = models

    let { reviews } = manuscript
    if (reviews && reviews.length) return reviews.filter(r => r.isValid)

    const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId: manuscript.submissionId,
    })
    const userTeam = await Team.findOneByUserAndManuscriptId({
      userId,
      manuscriptId: manuscript.id,
    })
    let role
    if (userTeam) {
      ;({ role } = userTeam)
    } else {
      const isAdmin = await TeamMember.findOneByUserAndRole({
        userId,
        role: Team.Role.admin,
      })
      if (isAdmin) role = Team.Role.admin
      else throw new Error('No role found on manuscript.')
    }

    reviews = await Review.findAllValidByManuscriptId({
      manuscriptId: manuscript.id,
      eagerLoadRelations: [
        'comments.files',
        'rejectDecisionInfo',
        'member.team',
      ],
    })

    // Reviewer
    if (role === Team.Role.reviewer) {
      reviews = transformReviewsForReviewer({
        reviews,
        latestManuscript,
        userId,
        Team,
        Comment,
        manuscript,
      })
    }

    // Author
    if (role === Team.Role.author) {
      reviews = transformReviewsForAuthor({ reviews, userId, Comment })
    }
    if (!Team.JournalRoles.includes(role) && !Team.StaffRoles.includes(role)) {
      reviews = removeRejectDecisionInfo(reviews)
    }

    return reviews.map(r => r.toDTO())
  },
})
