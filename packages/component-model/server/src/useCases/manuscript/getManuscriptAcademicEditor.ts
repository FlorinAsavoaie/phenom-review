export const initialize = ({ models, loaders }) => ({
  execute: async ({ manuscript, userId }) => {
    if (manuscript.academicEditor) return manuscript.academicEditor
    const { Team, User } = models

    const hideEmailIfNotAdminOrEa = async aeDTO => {
      // this should not be necessary we could just as well not ask for this info on client side
      // TODO: this is technical debt and should be adressed in the future

      // if there is no role property on manuscript it means that we are getting the manuscript with objectionFind
      // using findAllV2(). in this case we need to determine the role here for the moment. In the long run it would not make
      // sense to have this logic here, since we will have separate dashboards for each role and we will decide for each one if they
      // should see, or not, the email

      let { role } = manuscript
      if (!role) {
        const currentUser = await User.find(userId)
        role = await currentUser.getTeamMemberRoleForManuscript(manuscript)
      }
      const eligibleRoles = [
        Team.Role.admin,
        Team.Role.editorialAssistant,
        Team.Role.researchIntegrityPublishingEditor,
      ]
      const canSeeEmail = !!role && eligibleRoles.includes(role)
      if (aeDTO.alias && aeDTO.alias.email && !canSeeEmail) {
        delete aeDTO.alias.email
      }
      return aeDTO
    }

    const academicEditors = await loaders.Manuscript.academicEditorsLoader.load(
      manuscript.id,
    )

    return academicEditors?.map(ae => hideEmailIfNotAdminOrEa(ae)).shift()
  },
})
