export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.articleType) return manuscript.articleType
    const articleType = await loaders.Manuscript.articleTypeLoader.load(
      manuscript.id,
    )
    return articleType?.shift()
  },
})
