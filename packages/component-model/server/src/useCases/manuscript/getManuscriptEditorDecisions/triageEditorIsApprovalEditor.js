const initialize = ({ models: { Team, TeamMember, Review, Manuscript } }) => ({
  execute: async ({ manuscript, submissionReviewerReports }) => {
    const {
      reject,
      publish,
      minor,
      major,
      returnToAcademicEditor,
    } = Review.Recommendations

    if (manuscript.status === Manuscript.Statuses.academicEditorInvited) {
      return [reject]
    }

    const manuscriptEditorRecommendations = await Review.findAllValidByManuscriptAndRole(
      { manuscriptId: manuscript.id, role: Team.Role.academicEditor },
    )
    // As an approval editor I should not be able to take any decision if the
    // current version has a revision recommendation
    const revisionRecommendationTypes = [
      Review.Recommendations.minor,
      Review.Recommendations.major,
      Review.Recommendations.revision,
    ]
    const revisionRecommendation = manuscriptEditorRecommendations.find(r =>
      revisionRecommendationTypes.includes(r.recommendation),
    )
    if (revisionRecommendation) return []

    // As an approval editor I can publish, reject or return the manuscript
    // if I already have a publish or reject recommendation
    const finalRecommendationTypes = [
      Review.Recommendations.reject,
      Review.Recommendations.publish,
    ]
    const finalRecommendation = manuscriptEditorRecommendations.find(r =>
      finalRecommendationTypes.includes(r.recommendation),
    )
    if (finalRecommendation) return [publish, returnToAcademicEditor, reject]

    // As an approval editor, i can publish, reject or ask for a minor/major revision if I
    // have a reviewer report on any previous version, even if I don't have a
    // recommendation on the current version
    if (submissionReviewerReports.length) {
      return [publish, minor, major, reject]
    }

    const pendingOrAcceptedAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: manuscript.id,
      },
    )
    // As an approval editor I should only be able to reject the
    // manuscript if the academic editor is pending/accepted and I have no
    // previous editor recommendations or reviewer reports
    if (pendingOrAcceptedAcademicEditor) return [reject]

    return [minor, major, reject]
  },
})

module.exports = {
  initialize,
}
