export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    const hasSIandNoSICOI =
      manuscript.specialIssueId &&
      !manuscript.hasSpecialIssueEditorialConflictOfInterest

    const loader = hasSIandNoSICOI
      ? loaders.Manuscript.specialIssuePRMLoader
      : loaders.Manuscript.journalPRMLoader
    const manuscriptPeerReviewModelArray = await loader.load(manuscript.id)

    return manuscriptPeerReviewModelArray?.shift()
  },
})
