export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.pendingAcademicEditor)
      return manuscript.pendingAcademicEditor

    const pendingAcademicEditors = await loaders.Manuscript.pendingAcademicEditorsLoader.load(
      manuscript.id,
    )

    return pendingAcademicEditors?.shift()
  },
})
