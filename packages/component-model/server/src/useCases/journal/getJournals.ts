import { ObjectionParametrizer } from '../../ObjectionParametrizer'

interface GetJournalsInput {
  name: string
}

const initialize = ({ Journal }) => ({
  async execute({ input }: { input: GetJournalsInput }) {
    const parameters = new ObjectionParametrizer<GetJournalsInput>(
      input,
    ).getParams()
    const journals = await Journal.findAllV2(parameters)
    return journals
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
