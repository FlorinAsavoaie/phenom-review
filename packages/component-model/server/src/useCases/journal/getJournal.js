const initialize = ({ Journal }) => ({
  execute: async journalId =>
    Journal.find(journalId, [
      'teams.[members.[user.[identities]]]',
      'peerReviewModel',
      'sections.[teams.members.user.identities, specialIssues.[section,teams.members,manuscripts]]',
      'specialIssues.[teams.members.user.identities,manuscripts]',
      'preprints',
      'journalPreprints',
    ]),
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
