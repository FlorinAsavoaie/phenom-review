export const initialize = ({ loaders }) => ({
  execute: async ({ journal }) => {
    if (journal.peerReviewModel) return journal.peerReviewModel

    const peerReviewModel = await loaders.Journal.peerReviewModelLoader.load(
      journal.id,
    )
    return peerReviewModel?.shift()
  },
})
