module.exports.initialize = ({ TeamMember, Team, User }) => ({
  execute: async ({ teamMember }) => {
    let reviewerStatusLabel

    const team = await Team.find(teamMember.teamId)
    if (team.role !== Team.Role.reviewer) return

    const user = await User.find(teamMember.userId)
    if (!user.isSubscribedToEmails) {
      return TeamMember.StatusExpiredLabels.unsubscribed
    }

    if (teamMember.status !== TeamMember.Statuses.expired) {
      reviewerStatusLabel = teamMember.status.toUpperCase()
    } else {
      reviewerStatusLabel = teamMember.responded
        ? TeamMember.StatusExpiredLabels.overdue
        : TeamMember.StatusExpiredLabels.unresponsive
    }

    return reviewerStatusLabel
  },
})
