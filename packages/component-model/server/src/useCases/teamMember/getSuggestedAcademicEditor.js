const { isEmpty, chain } = require('lodash')
const { logger } = require('component-logger')

function initialize({
  models: { TeamMember, EditorSuggestion, Team, Manuscript },
}) {
  return {
    execute,
  }

  async function execute({
    trx,
    manuscriptId,
    journalId,
    sectionId,
    specialIssueId,
  }) {
    const { services } = require('component-quality-check')
    const { ConflictOfInterest } = services

    const role = Team.Role.academicEditor
    let teamMembers
    const manuscript = await Manuscript.find(manuscriptId)

    const teamMemberIds = await getEditorsTeamMemberIds({
      TeamMember,
      Team,
      submissionId: manuscript.submissionId,
    })
    const editorsConflicts = await ConflictOfInterest.getConflictsForTeamMembers(
      teamMemberIds,
      trx,
    )
    if (specialIssueId && !editorsConflicts.length) {
      teamMembers = await TeamMember.findAllBySpecialIssueAndRole({
        role,
        specialIssueId,
      })
    }

    if (isEmpty(teamMembers) && sectionId) {
      teamMembers = await TeamMember.findAllBySectionAndRole({
        sectionId,
        role,
      })
    }

    if (isEmpty(teamMembers) && journalId) {
      teamMembers = await TeamMember.findAllByJournalAndRole({
        journalId,
        role,
      })
    }

    if (isEmpty(teamMembers)) {
      throw new Error('No manuscript parent ID has been provided')
    }

    const editorSuggestions = await EditorSuggestion.findBy({
      manuscriptId,
    })

    const manuscriptAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })

    const manuscriptAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
      },
    )

    const teamMembersUserIds = teamMembers.map(tm => tm.userId)

    const teamMembersWithWorkload = await TeamMember.findTeamMemberWorkloadByUsersAndManuscriptParent(
      {
        userIds: teamMembersUserIds,
        manuscript,
        role: Team.Role.academicEditor,
        teamMemberStatuses: [
          TeamMember.Statuses.pending,
          TeamMember.Statuses.accepted,
        ],
      },
    )

    const users = teamMembers
      .filter(teamMember => {
        const memberIsAlreadyInvited = manuscriptAcademicEditors.find(
          academicEditor => academicEditor.userId === teamMember.userId,
        )
        const memberIsAuthor = manuscriptAuthors.find(
          author => author.userId === teamMember.userId,
        )
        return !memberIsAlreadyInvited && !memberIsAuthor
      })
      .map(teamMember => {
        const editorSuggestion = editorSuggestions.find(
          editorSubmissionScore =>
            editorSubmissionScore.teamMemberId === teamMember.id,
        )

        const editorWorkload = teamMembersWithWorkload.find(
          editorWithWorkload => editorWithWorkload.userId === teamMember.userId,
        )

        return {
          ...teamMember,
          score: editorSuggestion ? editorSuggestion.score : 0,
          workload: editorWorkload ? editorWorkload.workload : 0,
        }
      })

    return getAcademicEditorWithoutRORConflict({
      users,
      manuscriptAuthors,
    })
  }
}

const getEditorsTeamMemberIds = async ({ TeamMember, Team, submissionId }) => {
  const submissionAcademicEditors = await TeamMember.findAllBySubmissionAndRole(
    { submissionId, role: Team.Role.academicEditor },
  )

  const submissionTriageEditors = await TeamMember.findAllBySubmissionAndRole({
    submissionId,
    role: Team.Role.triageEditor,
  })

  return [
    ...submissionAcademicEditors.map(ae => ae.id),
    ...submissionTriageEditors.map(te => te.id),
  ]
}

function orderUsersByScoreAndWorkload(users) {
  const MAX_WORKLOAD_ALLOWED = 4

  const usersWithAllowedWorkload = chain(users)
    .filter(user => user.workload < MAX_WORKLOAD_ALLOWED)
    .orderBy(['score', 'workload', 'alias.email'], ['desc', 'asc', 'asc'])

  const usersWithWorkloadAlreadyAtMax = chain(users)
    .filter(user => user.workload >= MAX_WORKLOAD_ALLOWED)
    .orderBy(['workload', 'score', 'alias.email'], ['asc', 'desc', 'asc'])
  return [...usersWithAllowedWorkload, ...usersWithWorkloadAlreadyAtMax]
}

const isInRORConflict = ({ teamMember, authors }) => {
  if (!teamMember.alias.affRorId) return false
  return authors.some(
    author => author.alias.affRorId === teamMember.alias.affRorId,
  )
}
const getAcademicEditorWithoutRORConflict = async ({
  users,
  manuscriptAuthors,
}) => {
  const academicEditor = orderUsersByScoreAndWorkload(users).find(
    user => !isInRORConflict({ teamMember: user, authors: manuscriptAuthors }),
  )

  if (!academicEditor) {
    logger.info(`No academic editor without ROR conflict was found.`)
  }

  return academicEditor
}

module.exports = {
  initialize,
}
