module.exports.initialize = ({ User }) => ({
  async execute(teamMember) {
    if (teamMember.user) return teamMember.user

    const user = await User.find(teamMember.userId)

    return user.toDTO()
  },
})
