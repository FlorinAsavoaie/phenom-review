const initialize = ({ SourceJournal }) => ({
  execute: async () => SourceJournal.all(),
})

// change somehting

const authsomePolicies = ['isAuthenticated']

export default {
  initialize,
  authsomePolicies,
}
