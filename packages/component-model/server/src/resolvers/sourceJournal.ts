import models from '@pubsweet/models'
import { withAuthsomeMiddleware } from 'helper-service'
import useCases from '../useCases'

const resolvers = {
  Query: {
    async getSourceJournals() {
      const { SourceJournal } = models
      return useCases.getSourceJournalsUseCase
        .initialize({ SourceJournal })
        .execute()
    },
  },
  Mutation: {},
}

export default withAuthsomeMiddleware(resolvers, useCases)
