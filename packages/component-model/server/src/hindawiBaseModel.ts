import { Model, QueryBuilderType, transaction } from 'objection'

const { logger } = require('component-logger')
const findQuery = require('objection-find')
const db = require('@pubsweet/db-manager/src/db')
const { NotFoundError } = require('@pubsweet/errors')
const { merge } = require('lodash')
const uuid = require('uuid')

db.client.pool.acquireTimeoutMillis = 5000
db.client.pool.createTimeoutMillis = 3000
db.client.pool.destroyTimeoutMillis = 5000
db.client.pool.idleTimeoutMillis = 3000
db.client.pool.reapIntervalMillis = 1000
db.client.pool.createRetryIntervalMillis = 100
db.client.pool.propagateCreateError = false
db.client.pool.min = 0
db.client.pool.max = 10

Model.knex(db)

const notFoundError = (property: any, value: any, className: any) =>
  new NotFoundError(`Object not found: ${className} with ${property} ${value}`)

const integrityError = (property: any, value: any, message: any) =>
  new Error(
    `Data Integrity Error property ${property} set to ${value}: ${message}`,
  )

const parseEagerRelations = (relations?: string[] | string) =>
  (Array.isArray(relations) ? `[${relations.join(', ')}]` : relations) as any

interface Constructor<T> {
  new (): T
}
export interface HindawiBaseModelType<M extends HindawiBaseModel> {
  new (...args: any[]): M
  query<M extends HindawiBaseModel>(this: Constructor<M>): QueryBuilderType<M>
}

export interface HindawiBaseModelRepository<T> {
  knex(): any
  find(
    id: string,
    eagerLoadRelations?: string | string[],
  ): Promise<T | undefined>
  findBy(queryObject: any, eagerLoadRelations?: string | string[]): Promise<T[]>
  findOneBy({
    queryObject,
    eagerLoadRelations,
  }: {
    queryObject: any
    eagerLoadRelations?: string | string[]
  }): Promise<T | undefined>
  findOrCreate({
    queryObject,
    eagerLoadRelations,
    options,
  }: {
    queryObject: any
    eagerLoadRelations?: string | string[]
    options: any
  }): Promise<T>
}

export interface HindawiBaseModelProps {
  id: string
  created: string
  updated: string
  owners: unknown

  updateProperties(properties: any): this
  $query(trx: any): any
}
class HindawiBaseModel extends Model implements HindawiBaseModelProps {
  id: string
  created: string
  updated: string
  owners: unknown

  constructor(properties?: any) {
    super()
    if (properties) {
      this._updateProperties(properties)
    }
  }

  static _parseEagerRelations: (relations: string[] | string) => string

  static get jsonSchema() {
    // JSON schema validation is getting proper support for inheritance in
    // its draft 8: https://github.com/json-schema-org/json-schema-spec/issues/556
    // Until then, we're not using additionalProperties: false, and letting the
    // database handle this bit of the integrity checks.

    let schema: any

    const mergeSchema = (additionalSchema: any) => {
      if (additionalSchema) {
        schema = merge(schema, additionalSchema)
      }
    }

    // Crawls up the prototype chain to collect schema
    // information from models and extended models
    const getSchemasRecursively = (object: any) => {
      mergeSchema(object.schema)

      const proto = Object.getPrototypeOf(object)

      if (proto.name !== 'HindawiBaseModel') {
        getSchemasRecursively(proto)
      }
    }

    getSchemasRecursively(this)

    const baseSchema = {
      type: 'object',
      properties: {
        type: { type: 'string' },
        id: { type: 'string', format: 'uuid' },
        created: { type: ['string', 'object'], format: 'date-time' },
        updated: { type: ['string', 'object'], format: 'date-time' },
      },
      additionalProperties: false,
    }

    if (schema) {
      return merge(baseSchema, schema)
    }

    return baseSchema
  }

  $beforeInsert() {
    this.id = this.id || uuid.v4()
    this.created = new Date().toISOString()
    this.updated = this.created
  }

  $beforeUpdate() {
    this.updated = new Date().toISOString()
  }

  save(trx?: any) {
    const updateAndFetch = (instance: any, trx: any) =>
      // @ts-expect-error TS(2339): Property 'query' does not exist on type 'Function'... Remove this comment to see the full error message
      this.constructor.query(trx).patchAndFetchById(instance.id, instance)

    const insertAndFetch = (instance: any, trx: any) =>
      // @ts-expect-error TS(2339): Property 'query' does not exist on type 'Function'... Remove this comment to see the full error message
      this.constructor.query(trx).insertAndFetch(instance)

    return this._save(insertAndFetch, updateAndFetch, trx)
  }

  // eslint-disable-next-line sonarjs/cognitive-complexity
  async _save(insertAndFetch: any, updateAndFetch: any, trx: any) {
    const simpleSave = (trx = null) => updateAndFetch(this, trx)

    const protectedSave = async (trx: any) => {
      const current = await this.constructor
        // @ts-expect-error TS(2339): Property 'query' does not exist on type 'Function'... Remove this comment to see the full error message
        .query(trx)
        .findById(this.id)
        .forUpdate()

      const storedUpdateTime = new Date(current.updated).getTime()
      const instanceUpdateTime = new Date(this.updated).getTime()

      if (instanceUpdateTime < storedUpdateTime) {
        throw integrityError(
          'updated',
          this.updated,
          'is older than the one stored in the database!',
        )
      }

      return simpleSave(trx)
    }

    // start of save function...
    let result
    // Do the validation manually here, since inserting
    // model instances skips validation, and using toJSON() first will
    // not save certain fields ommited in $formatJSON (e.g. passwordHash)
    this.$validate()

    const saveFn = async (trx: any) => {
      let savedEntity

      // If an id is set on the model instance, find out if the instance
      // already exists in the database
      if (this.id) {
        if (!this.updated && this.created) {
          throw integrityError(
            'updated',
            this.updated,
            'must be set when created is set!',
          )
        }
        if (!this.updated && !this.created) {
          savedEntity = await simpleSave(trx)
        } else {
          // If the model instance has created/updated times set, provide
          // protection against potentially overwriting newer data in db.
          savedEntity = await protectedSave(trx)
        }
      }
      // If it doesn't exist, simply insert the instance in the database
      if (!savedEntity) {
        savedEntity = await insertAndFetch(this, trx)
      }
      return savedEntity
    }

    try {
      if (trx) {
        result = await saveFn(trx)
      } else {
        result = await transaction(Model.knex(), saveFn)
      }
      logger.info(`Saved ${this.constructor.name} with UUID ${result.id}`)
    } catch (err) {
      logger.info(`Rolled back ${this.constructor.name}`)
      logger.error(err)
      throw err
    }

    return result
  }

  // A private method that you shouldn't override
  _updateProperties(properties: any) {
    Object.keys(properties).forEach(prop => {
      this[prop] = properties[prop]
    })
    return this
  }

  updateProperties(properties: any) {
    return this._updateProperties(properties)
  }

  setOwners(owners: unknown) {
    this.owners = owners
  }

  // `field` is a string, `value` is a primitive or
  // `field` is an object of field, value pairs
  static findByField(field: any, value: any) {
    logger.debug('Finding', field, value)

    if (value === undefined) {
      return this.query().where(field)
    }

    return this.query().where(field, value)
  }

  static async findOneByField(field: any, value: any) {
    const results = await this.query()
      .where(field, value)
      .limit(1)
    if (!results.length) {
      throw notFoundError(field, value, this.name)
    }

    return results[0]
  }

  static async all() {
    return this.query()
  }

  static async find<T extends HindawiBaseModel>(
    this: HindawiBaseModelType<T>,
    id: string,
    eagerLoadRelations?: string | string[],
  ): Promise<any> {
    if (!id) throw new NotFoundError(`Cannot run query with undefined id.`)
    const object = await this.query()
      .findById(id)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations) as any)
    if (!object)
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)

    return object
  }

  async delete({ trx }: any = {}) {
    const deleted = await this.$query(trx).delete()
    logger.info(`Deleted ${deleted} ${this.constructor.name} records.`)
    return this
  }

  async saveRecursively() {
    return (
      this.constructor
        // @ts-expect-error TS(2339): Property 'query' does not exist on type 'Function'... Remove this comment to see the full error message
        .query()
        .upsertGraph(this, { insertMissing: true, relate: true })
    )
  }

  saveGraph(opts = {}, trx = null) {
    return (
      this.constructor
        // @ts-expect-error TS(2339): Property 'query' does not exist on type 'Function'... Remove this comment to see the full error message
        .query(trx)
        .upsertGraphAndFetch(
          this,
          Object.assign(opts, { insertMissing: true, noDelete: true }),
        )
    )
  }

  static findAllV2(params: any) {
    return findQuery(this).build(params)
  }

  static async findOneBy<T extends HindawiBaseModel>(
    this: HindawiBaseModelType<T>,
    {
      queryObject,
      eagerLoadRelations,
    }: { queryObject: any; eagerLoadRelations?: string | string[] },
  ): Promise<any> {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async findBy<T extends HindawiBaseModel>(
    this: HindawiBaseModelType<T>,
    queryObject: any,
    eagerLoadRelations?: string | string[],
  ): Promise<any[]> {
    try {
      return this.query()
        .where(queryObject)
        .withGraphFetched(parseEagerRelations(eagerLoadRelations)) as any
    } catch (e) {
      throw new Error(e)
    }
  }

  static findIn(field: any, options: any, eagerLoadRelations: any) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))
  }

  static async findOrCreate({
    queryObject,
    eagerLoadRelations,
    options,
  }: {
    queryObject: any
    eagerLoadRelations?: string | string[]
    options: any
  }) {
    let instance = await this.findOneBy({
      queryObject,
      eagerLoadRelations,
    })

    if (!instance) {
      instance = new this(options)
      await instance.save()
    }

    return instance
  }

  static async findOneByEmail(email: any, eagerLoadRelations: any) {
    const object = await this.query()
      .where('email', email.toLowerCase())
      .limit(1)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return undefined
    }

    return object[0]
  }

  static async findUniqueJournal({ name, code, email }: any) {
    const objects = await this.query()
      .whereRaw(
        'LOWER(name) LIKE ? OR LOWER(code) LIKE ? OR LOWER(email) LIKE ?',
        [name.toLowerCase(), code.toLowerCase(), email.toLowerCase()],
      )
      .limit(1)

    if (!objects.length) {
      return undefined
    }

    return objects[0]
  }

  static async findUniqueSection({ name }: any) {
    const objects = await this.query()
      .whereRaw('LOWER(name) LIKE ?', [name.toLowerCase()])
      .limit(1)

    if (!objects.length) {
      return undefined
    }

    return objects[0]
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  }: any = {}) {
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(parseEagerRelations(eagerLoadRelations))
  }

  static async updateBy({ queryObject, propertiesToUpdate }) {
    return this.query()
      .patch(propertiesToUpdate)
      .where(queryObject)
  }
}

HindawiBaseModel._parseEagerRelations = parseEagerRelations
HindawiBaseModel.pickJsonSchemaProperties = false

export default HindawiBaseModel
