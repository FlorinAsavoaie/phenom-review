const {
  getTeamRoles,
  generateArticleType,
  getArticleTypesTypes,
  getShortArticleTypes,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const { submissionQualityCheckPassedUseCase } = require('../src/useCases')

let models = {}
let Manuscript = {}
let TeamMember = {}
let ArticleType = {}

const notificationService = {
  notifyAuthorWhenMaterialChecksIsCompleted: jest.fn(),
  notifySubmitterWhenManuscriptHasPassedQC: jest.fn(),
}
describe('Submission Quality Check Passed use-case', () => {
  beforeEach(() => {
    models = {
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findOneByManuscriptAndRole: jest.fn(),
        findSubmittingAuthor: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      ArticleType: {
        ShortArticleTypes: getShortArticleTypes(),
        Types: getArticleTypesTypes(),
      },
      User: {
        find: jest.fn(),
      },
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
    }
    ;({ Manuscript, TeamMember, ArticleType } = models)
  })

  it('Should notify author when material checks is completed', async () => {
    const manuscript = generateManuscript()
    const submittingAuthor = generateTeamMember()

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findSubmittingAuthor')
      .mockResolvedValue(submittingAuthor)

    await submissionQualityCheckPassedUseCase
      .initialize({
        models,
        notificationService,
      })
      .execute({ data: { submissionId: manuscript.submissionId } })

    expect(manuscript.save).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyAuthorWhenMaterialChecksIsCompleted,
    ).toHaveBeenCalledTimes(1)
  })
  it('Should notify submitter when manuscript has passed QC if is submitted on a short article type', async () => {
    const articleType = generateArticleType({
      name: ArticleType.Types.corrigendum,
    })
    const manuscript = generateManuscript({ articleType })
    const teamMember = generateTeamMember()

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRole')
      .mockResolvedValue(teamMember)

    await submissionQualityCheckPassedUseCase
      .initialize({
        models,
        notificationService,
      })
      .execute({ data: { submissionId: manuscript.submissionId } })

    expect(manuscript.save).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifySubmitterWhenManuscriptHasPassedQC,
    ).toHaveBeenCalledTimes(1)
  })
})
