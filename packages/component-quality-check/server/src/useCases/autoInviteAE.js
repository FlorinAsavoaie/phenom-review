const initialize = ({
  models,
  getSuggestedAcademicEditorUseCase,
  inviteSuggestedAcademicEditorUseCase,
  inviteAcademicEditorUseCase,
  initializeInviteAcademicEditorUseCase,
}) => ({
  async execute({ manuscriptId }) {
    const manuscript = await models.Manuscript.find(manuscriptId)

    if (!manuscript.allowAcademicEditorAutomaticInvitation) {
      manuscript.allowAcademicEditorAutomaticInvitation = true
      await manuscript.save()
    }

    const getSuggestedAcademicEditor = getSuggestedAcademicEditorUseCase.initialize(
      {
        models,
      },
    )
    return inviteSuggestedAcademicEditorUseCase
      .initialize({
        models,
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor: initializeInviteAcademicEditorUseCase(
            inviteAcademicEditorUseCase,
          ),
        },
      })
      .execute(manuscript.submissionId)
  },
})

module.exports = {
  initialize,
}
