const initialize = ({
  models: { Manuscript, TeamMember, Team, User },
  notificationService,
}) => ({
  async execute({ data }) {
    const { submissionId } = data
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: ['articleType', 'journal'],
    })

    if (!lastManuscript) return

    lastManuscript.updateProperties({
      peerReviewPassedDate: new Date().toISOString(),
    })
    await lastManuscript.save()

    const author = await TeamMember.findSubmittingAuthor(lastManuscript.id)
    author.user = await User.find(author.userId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: lastManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      manuscriptId: lastManuscript.id,
      journalId: lastManuscript.journalId,
      sectionId: lastManuscript.sectionId,
    })

    notificationService.notifyAuthorWhenPRCCIsCompleted({
      author,
      manuscript: lastManuscript,
      editorialAssistant,
      triageEditor,
    })
  },
})

module.exports = {
  initialize,
}
