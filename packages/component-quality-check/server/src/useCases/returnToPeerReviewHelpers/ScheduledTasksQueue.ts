import * as Bluebird from 'bluebird'
import models, { Job } from '@pubsweet/models'
import Email from 'component-sendgrid'

export enum TaskType {
  event = 'event',
  notification = 'notification',
  activityLog = 'activityLog',
  invitation = 'invitation',
  emailJob = 'emailJob',
  removeJob = 'removeJob',
  jobs = 'jobs',
}

type ActivityLogPayload = {
  userId: string | null
  manuscriptId: string
  action: string
  objectType: string
  objectId: string
}

type EventServicePayload = {
  submissionId: string
  eventName: string
}

type NotificationServicePayload = {
  journal?: any // journal type
  journalName?: string // ?
  manuscript?: any // manuscript type
  academicEditor?: any // team member type
  triageEditor?: any // team member type
  editorialAssistant?: any // team member type
  approvalEditor?: any // team member type one of the above basically
  reviewers?: any // team member type
  author?: any
  specialIssue?: any
}

export type Task = {
  type: TaskType
  svMethod?: string
  payload: ActivityLogPayload | EventServicePayload | NotificationServicePayload
}

export interface ScheduledTasksQueueI {
  tasks: Task[]
  push: (tasks: Task[]) => void
  run: () => Promise<void>
  eventsService: any // not an ideal world
  notificationService: any
  activityLogService: any // not an ideal world
  invitationsService: any
  emailJobsService: any
  removeJobService: any
  jobsService: any
}

export class ScheduledTasksQueue implements ScheduledTasksQueueI {
  constructor({
    eventsService,
    notificationService,
    activityLogService,
    invitationsService,
    emailJobsService,
    emailPropsService,
    getInvitationPropsService,
    removeJobService,
    jobsService,
  }) {
    this.eventsService = eventsService.initialize({ models })
    this.notificationService = notificationService
    this.activityLogService = activityLogService
    this.invitationsService = invitationsService.initialize({
      Email,
      getPropsService: getInvitationPropsService,
    })
    this.emailJobsService = emailJobsService.initialize({
      Job,
      Email,
      logEvent: activityLogService,
      getPropsService: emailPropsService,
    })
    this.removeJobService = removeJobService.initialize({
      Job,
      logEvent: activityLogService,
    })
    this.jobsService = jobsService.initialize({ models })
  }

  eventsService = null
  notificationService = null
  activityLogService = null
  invitationsService = null
  emailJobsService = null
  removeJobService = null
  jobsService = null
  tasks = []

  push(tasks) {
    this.tasks = this.tasks.concat(tasks)
  }

  async run() {
    await Bluebird.each(this.tasks, task => {
      switch (task.type) {
        case TaskType.event:
          return this.eventsService[task.svMethod](task.payload)
        case TaskType.notification:
          return this.notificationService[task.svMethod](task.payload)
        case TaskType.activityLog:
          return this.activityLogService(task.payload)
        case TaskType.invitation:
          return this.invitationsService[task.svMethod](task.payload)
        case TaskType.emailJob:
          return this.emailJobsService[task.svMethod](task.payload)
        case TaskType.removeJob:
          return this.removeJobService[task.svMethod](task.payload)
        case TaskType.jobs:
          return this.jobsService[task.svMethod](task.payload)
        default:
          return new Error(`Unknown task type: ${task.type}`)
      }
    })
  }
}
