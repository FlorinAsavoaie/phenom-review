import models from '@pubsweet/models'
import getSuggestedAcademicEditorUseCase from 'component-model/server/src/useCases/teamMember/getSuggestedAcademicEditor'
import { inviteAcademicEditorUseCase } from './inviteAcademicEditorWithTrx'

export const autoInviteAE = async ({
  trx,
  manuscript,
  scheduledTasksQueue,
}) => {
  if (!manuscript.allowAcademicEditorAutomaticInvitation) {
    await manuscript.$query(trx).patch({
      allowAcademicEditorAutomaticInvitation: true,
    })
  }

  const getSuggestedAcademicEditor = getSuggestedAcademicEditorUseCase.initialize(
    {
      models,
    },
  )

  if (!manuscript.allowAcademicEditorAutomaticInvitation) {
    return
  }

  const academicEditor = await getSuggestedAcademicEditor.execute({
    trx,
    manuscriptId: manuscript.id,
    journalId: manuscript.journalId,
    sectionId: manuscript.sectionId,
    specialIssueId: manuscript.specialIssueId,
  })

  if (!academicEditor) {
    return
  }

  return inviteAcademicEditorUseCase({
    trx,
    scheduledTasksQueue,
    manuscript,
    userId: academicEditor.userId,
  })
}
