import models from '@pubsweet/models'
import { TaskType } from '../ScheduledTasksQueue'
import { TaskPayloadWithTrx, ReturnToPRReasons } from '../queue.utils'

export const notifyTEOfAEConflict = async ({
  submissionId,
  scheduledTasksQueue,
  submittedReasonsToReturnToPR,
}: TaskPayloadWithTrx) => {
  const {
    TeamMember,
    Team,
    Journal,
    Manuscript,
    SpecialIssue,
    PeerReviewModel,
  } = models

  const teConflictAvailable = submittedReasonsToReturnToPR.some(
    reason => reason.type === ReturnToPRReasons.triageEditor,
  )

  if (teConflictAvailable) return

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })
  const prm = await PeerReviewModel.findOneByManuscriptParent(latestManuscript)

  const isSingleTier =
    prm.name === PeerReviewModel.Types.singleTierAcademicEditor

  if (isSingleTier) return

  const triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.triageEditor,
    status: TeamMember.Statuses.active,
    eagerLoadRelations: 'user',
  })

  if (!triageEditor) return

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    eagerLoadRelations: 'user',
  })
  const triageEditorIsAlsoAcademicEditor =
    triageEditor.userId === academicEditor.userId
  if (triageEditorIsAlsoAcademicEditor) return

  const specialIssue = latestManuscript.specialIssueId
    ? await SpecialIssue.find(latestManuscript.specialIssueId)
    : undefined
  const journal = await Journal.find(latestManuscript.journalId)

  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: latestManuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    },
  )

  scheduledTasksQueue.push([
    {
      type: TaskType.notification,
      svMethod: 'notifyTriageEditorOfAEConflict',
      payload: {
        journal,
        specialIssue,
        manuscript: latestManuscript,
        triageEditor,
        academicEditor,
        editorialAssistant,
      },
    },
  ])
}
