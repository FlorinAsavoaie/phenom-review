import * as Bluebird from 'bluebird'
import models from '@pubsweet/models'
import { TaskPayloadWithTrx } from '../queue.utils'
import { TaskType } from '../ScheduledTasksQueue'

export const setReviewerStatusToAccepted = async ({
  conflictedTeamMembers,
  scheduledTasksQueue,
  submissionId,
  isAERemoved,
  trx,
}: TaskPayloadWithTrx) => {
  const { TeamMember, Team, Journal, Manuscript, SpecialIssue } = models

  await Bluebird.each(conflictedTeamMembers, async teamMember => {
    const reviewer = await TeamMember.find(teamMember.id)

    await reviewer.$query(trx).patch({ status: TeamMember.Statuses.accepted })

    // TODO: we shoud also send some e-mails to the reviewers to notify them
    // about their reviews being flagged as improper
  })
  if (!isAERemoved) {
    const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })
    const specialIssue = latestManuscript.specialIssueId
      ? await SpecialIssue.find(latestManuscript.specialIssueId)
      : undefined
    const journal = await Journal.find(latestManuscript.journalId)
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user',
      },
    )
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      latestManuscript.id,
    )

    scheduledTasksQueue.push([
      {
        type: TaskType.notification,
        svMethod: 'notifyAcademicEditorAboutImproperReviews',
        payload: {
          journal,
          specialIssue,
          manuscript: latestManuscript,
          academicEditor,
          editorialAssistant,
          author: submittingAuthor,
        },
      },
    ])
  }
}
