import models from '@pubsweet/models'
import { TaskType } from '../ScheduledTasksQueue'
import { TaskPayloadWithTrx } from '../queue.utils'

export const notifyAEOfOwnConflict = async ({
  submissionId,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const { TeamMember, Team, Journal, Manuscript, SpecialIssue } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })
  const specialIssue = latestManuscript.specialIssueId
    ? await SpecialIssue.find(latestManuscript.specialIssueId)
    : undefined
  const journal = await Journal.find(latestManuscript.journalId)
  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    eagerLoadRelations: 'user',
  })
  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: latestManuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    },
  )

  scheduledTasksQueue.push([
    {
      type: TaskType.notification,
      svMethod: 'notifyAcademicEditorOfOwnConflict',
      payload: {
        journal,
        specialIssue,
        manuscript: latestManuscript,
        academicEditor,
        editorialAssistant,
      },
    },
  ])
}
