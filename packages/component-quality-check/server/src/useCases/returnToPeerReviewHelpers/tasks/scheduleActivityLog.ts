import models from '@pubsweet/models'
import { TaskPayloadWithTrx, ReturnToPRReasons } from '../queue.utils'
import { TaskType } from '../ScheduledTasksQueue'

export const scheduleActivityLog = async ({
  submissionId,
  reason,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const { Manuscript } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const mapReasonToMessage = {
    [ReturnToPRReasons.triageEditor]:
      scheduledTasksQueue.activityLogService.actions
        .quality_checks_coi_author_triage_editor,
    [ReturnToPRReasons.academicEditor]:
      scheduledTasksQueue.activityLogService.actions
        .quality_checks_coi_author_academic_editor,
    [ReturnToPRReasons.reviewers]:
      scheduledTasksQueue.activityLogService.actions
        .quality_checks_coi_author_reviewer,
    [ReturnToPRReasons.improperReview]:
      scheduledTasksQueue.activityLogService.actions
        .quality_checks_issue_improper_review,
    [ReturnToPRReasons.decisionMadeInError]:
      scheduledTasksQueue.activityLogService.actions
        .quality_checks_issue_decision_made_in_error,
  }

  scheduledTasksQueue.push([
    {
      type: TaskType.activityLog,
      payload: {
        userId: null,
        manuscriptId: latestManuscript.id,
        action: mapReasonToMessage[reason],
        objectType:
          scheduledTasksQueue.activityLogService.objectType.manuscript,
        objectId: latestManuscript.id,
      },
    },
  ])
}
