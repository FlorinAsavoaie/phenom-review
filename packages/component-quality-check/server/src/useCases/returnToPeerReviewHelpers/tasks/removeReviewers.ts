import * as Bluebird from 'bluebird'
import models from '@pubsweet/models'
import { TaskPayloadWithTrx } from '../queue.utils'
import { TaskType, Task } from '../ScheduledTasksQueue'
import { findValidReviews } from './utils'

const getAcademicEditorNotificationPayload = async ({
  submissionId,
  conflictedTeamMembers,
  improperReviewAndCOIReviewers,
}) => {
  const { TeamMember, Team, Journal, Manuscript, Review, SpecialIssue } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const journal = await Journal.find(latestManuscript.journalId)

  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: latestManuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    },
  )

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    eagerLoadRelations: 'user',
  })

  const reviewers = await Bluebird.map(conflictedTeamMembers, async ({ id }) =>
    TeamMember.find(id),
  )

  const reviews = await findValidReviews({
    submissionId,
    improperReviewAndCOIReviewers,
    models: { Manuscript, Review, Team },
  })

  const author = await TeamMember.findSubmittingAuthor(latestManuscript.id)

  const isSpecialIssue = !!latestManuscript.specialIssueId
  const hasReviews = !!reviews.length

  let specialIssue = null

  if (isSpecialIssue) {
    specialIssue = await SpecialIssue.find(latestManuscript.specialIssueId)
  }

  return {
    journal,
    editorialAssistant,
    academicEditor,
    manuscript: latestManuscript,
    reviewers,
    hasReviews,
    author,
    specialIssue,
  }
}

export const removeReviewers = async ({
  submissionId,
  conflictedTeamMembers,
  trx,
  scheduledTasksQueue,
  isAERemoved,
  improperReviewAndCOIReviewers,
}: TaskPayloadWithTrx) => {
  const { TeamMember } = models

  await Bluebird.each(conflictedTeamMembers, async teamMember => {
    const reviewer = await TeamMember.find(teamMember.id)

    await reviewer.$query(trx).patch({
      status: TeamMember.Statuses.conflicting,
    })
  })

  const tasksToSchedule: Task[] = [
    {
      type: TaskType.event,
      svMethod: 'publishSubmissionEvent',
      payload: {
        submissionId,
        eventName: 'SubmissionReviewerRemoved',
      },
    },
  ]
  if (!isAERemoved) {
    // we notify the ae about the conflict with the conflicting reviewers

    const aeNotificationPayload = await getAcademicEditorNotificationPayload({
      submissionId,
      conflictedTeamMembers,
      improperReviewAndCOIReviewers,
    })

    tasksToSchedule.push({
      type: TaskType.notification,
      svMethod: 'notifyAcademicEditorAboutReviewerConflicts',
      payload: aeNotificationPayload,
    })
  }
  scheduledTasksQueue.push(tasksToSchedule)
}
