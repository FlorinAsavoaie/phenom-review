const initialize = ({
  models: { Manuscript },
  logEvent,
  fileValidator,
  eventsService,
}) => ({
  async execute({ submissionId, userId }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      order: 'desc',
      orderByField: 'version',
      submissionId,
    })
    if (!manuscripts.length)
      throw new ValidationError(`No manuscript was found`)

    const lastManuscript = manuscripts[1]

    if (lastManuscript.status !== Manuscript.Statuses.qualityChecksRequested) {
      throw new ValidationError(
        `Cannot submit Quality Checks in the current status`,
      )
    }

    await fileValidator.validateSubmissionFiles({
      manuscriptId: manuscripts[0].id,
    })

    manuscripts[0].updateProperties({
      status: Manuscript.Statuses.qualityChecksSubmitted,
      qualityChecksSubmittedDate: new Date().toISOString(),
      isLatestVersion: true,
    })
    await manuscripts[0].save()

    lastManuscript.updateStatus(Manuscript.Statuses.olderVersion)
    lastManuscript.updateIsLatestVersionFlag(false)
    await lastManuscript.save()

    await logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    await eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionQualityChecksSubmitted',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: ['hasAccessToSubmission', 'admin'],
}
