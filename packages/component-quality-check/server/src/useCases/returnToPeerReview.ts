import { transaction } from 'objection'

import { Manuscript } from '@pubsweet/models'
import eventsService from 'component-events'
import { dependencies as componentPeerReviewDependencies } from 'component-peer-review'
import { logEvent as activityLogService } from 'component-activity-log/server'
import notificationService from '../notifications/notification'

import { prepareReasonsList } from './returnToPeerReviewHelpers/prepareReasonsList'
import {
  createTaskQueue,
  runTaskQueue,
} from './returnToPeerReviewHelpers/queue'
import { ScheduledTasksQueue } from './returnToPeerReviewHelpers/ScheduledTasksQueue'
import { createConflictOfInterests } from './returnToPeerReviewHelpers/createConflicts'

const { logger } = require('component-logger')
const urlService = require('../urlService/urlService')
const { getModifiedText } = require('component-transform-text')

const config = require('config')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const {
  invitations: invitationsService,
  emailJobs: {
    academicEditor: { emailJobs: emailJobsService },
  },
  emailPropsServiceWrapper: emailPropsService,
  emailPropsService: invitationEmailPropsService,
  removalJobs: {
    academicEditor: { academicEditorRemovalJobs: removeJobService },
  },
  jobsService,
} = componentPeerReviewDependencies

const getInvitationPropsService = invitationEmailPropsService.initialize({
  baseUrl,
  urlService,
  footerText,
  unsubscribeSlug,
  getModifiedText,
})

const initialize = () => ({
  async execute({
    submissionId,
    conflictOfInterest,
    decisionMadeInError,
    improperReview,
  }) {
    const scheduledTasksQueue = new ScheduledTasksQueue({
      eventsService,
      notificationService,
      activityLogService,
      invitationsService,
      emailJobsService,
      emailPropsService,
      getInvitationPropsService,
      removeJobService,
      jobsService,
    })
    const reasonsList = prepareReasonsList(
      conflictOfInterest,
      improperReview,
      decisionMadeInError,
    )

    const taskQueue = createTaskQueue(reasonsList, {
      submissionId,
    })

    const processReturnToPeerReviewRequest = async () => {
      const trx = await transaction.start(Manuscript.knex())
      try {
        await createConflictOfInterests(conflictOfInterest, submissionId, trx)
        // queue for tasks to be ran. see queue.utils for tasks
        await runTaskQueue(taskQueue, scheduledTasksQueue, trx)

        await trx.commit()
      } catch (err) {
        await trx.rollback()
        throw err
      }
    }

    const runPostReturnToPeerReviewActions = async () => {
      // if every task in the task queue completes, then emit necesarry event, notifications, activityLogs
      await scheduledTasksQueue.run()
    }

    try {
      await processReturnToPeerReviewRequest()
      await runPostReturnToPeerReviewActions()
    } catch (e) {
      logger.info('Return To Peer Review error:', e)
    }
  },
})

module.exports = {
  initialize,
}
