const config = require('config')
const { services } = require('helper-service')
const { get } = require('lodash')
const Email = require('component-sendgrid')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')
const urlService = require('../urlService/urlService')

const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')
const baseUrl = config.get('pubsweet-client.baseUrl')
const bccAddress = config.get('bccAddress')
const invoicingUrl = config.get('publisherConfig.links.invoicingLink')
const servicesUrl = config.get('publisherConfig.links.authorServiceLink')
const staffEmail = config.get('staffEmail')

const getEmail = obj => get(obj, 'alias.email', '')
const getSurname = obj => get(obj, 'alias.surname', '')

const FOOTER_TEXT_PATTERN = '{recipientEmail}'
const CONTENT_CTA_TEXT = 'MANUSCRIPT DETAILS'

const computeCallForPapersLink = ({ journalCode, specialIssueCustomId }) =>
  `https://www.hindawi.com/journals/${journalCode}/si/${specialIssueCustomId}/`

module.exports = {
  async notifyAuthor({ journalName, editorialAssistant, author, manuscript }) {
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      emailType: 'author-manuscript-rejected-eqa',
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: getEmail(author),
        name: getSurname(author),
      },
      content: {
        subject: `${manuscript.customId}: Manuscript rejected`,
        paragraph,
        signatureName: editorialAssistantName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: author.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyTriageEditorAboutEditorReplacement({
    manuscript,
    journalName,
    academicEditor,
    approvalEditor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      customId,
      title,
      academicEditorName: academicEditor.getName(),
      emailType: 'author-academic-editor-coi',
    })
    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(approvalEditor, 'alias.email', ''),
        name: get(approvalEditor, 'alias.surname', ''),
      },
      content: {
        subject: 'Reassessment required due to potential Conflict of Interest',
        paragraph,
        signatureName: editorialAssistantName,
        ctaText: CONTENT_CTA_TEXT,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: approvalEditor.user.id,
          token: approvalEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: approvalEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAcademicEditor({
    journalName,
    editorialAssistant,
    academicEditor,
    manuscript,
    reviewers,
  }) {
    const reviewerNames = reviewers.map(r => r.getName()).join(', ')

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      emailType: 'academic-editor-manuscript-conflict',
      customId: manuscript.customId,
      title: manuscript.title,
      reviewerNames,
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: getEmail(academicEditor),
        name: getSurname(academicEditor),
      },
      content: {
        subject: `Reassessment required due to Conflict of Interest`,
        ctaText: CONTENT_CTA_TEXT,
        paragraph,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: academicEditor.user.id,
          token: academicEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: academicEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },

  async notifyAcademicEditorAboutReviewerConflicts({
    journal,
    editorialAssistant,
    academicEditor,
    manuscript,
    reviewers,
    hasReviews,
    specialIssue,
    author,
  }) {
    const reviewerNames = reviewers.map(r => r.getName()).join(', ')

    const emailType = [
      'author',
      'review',
      'coi',
      !!specialIssue && 'si',
      hasReviews ? 'with' : 'without',
      'reviews',
    ]
      .filter(Boolean)
      .join('-')

    let callForPapersLink
    if (specialIssue) {
      callForPapersLink = computeCallForPapersLink({
        journalCode: journal.code,
        specialIssueCustomId: specialIssue.customId,
      })
    }

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      reviewerNames,
      specialIssueName: specialIssue && specialIssue.name,
      authorName: author.getName(),
      callForPapersLink,
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: academicEditor.getEmail(),
        name: getSurname(academicEditor),
      },
      content: {
        subject: `Reassessment required due to Conflict of Interest`,
        ctaText: CONTENT_CTA_TEXT,
        paragraph,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: academicEditor.user.id,
          token: academicEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: academicEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAuthorWhenMaterialChecksIsCompleted({
    editorialAssistant,
    submittingAuthor,
    manuscript,
  }) {
    const { customId, title, journal } = manuscript

    const editorialAssistantEmail = editorialAssistant.getEmail()
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType: 'author-materialChecks-completed',
      title,
      customId,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistantEmail}>`,
      toUser: {
        email: getEmail(submittingAuthor),
        name: getSurname(submittingAuthor),
      },
      content: {
        subject: `Your manuscript is moving into production`,
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.userId,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAuthorWhenPRCCIsCompleted({
    manuscript,
    editorialAssistant,
    author,
    triageEditor,
  }) {
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const { customId, title, journal, articleType } = manuscript
    const signatureName = triageEditor ? triageEditor.getName() : journal.name
    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType: 'author-prcc-completed',
      title,
      customId,
      articleName: articleType.name,
      invoicingUrl,
      servicesUrl,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistantEmail}>`,
      toUser: {
        email: getEmail(author),
        name: getSurname(author),
      },
      content: {
        subject: `Your manuscript has been accepted for publication`,
        paragraph,
        signatureName,
        ctaText: CONTENT_CTA_TEXT,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: author.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyTriageEditorAboutHisConflictOfInterest({
    journal,
    specialIssue,
    manuscript,
    triageEditor,
    editorialAssistant,
  }) {
    const emailType = specialIssue
      ? 'lead-guest-editor-own-conflict'
      : 'triage-editor-own-conflict'

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      specialIssueName: (specialIssue && specialIssue.name) || '',
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: getEmail(triageEditor),
        name: getSurname(triageEditor),
      },
      content: {
        subject: `Reassessment required due to potential Conflict of Interest`,
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: triageEditor.user.id,
          token: triageEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: triageEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyTriageEditorOfAEConflict({
    journal,
    specialIssue,
    manuscript,
    triageEditor,
    academicEditor,
    editorialAssistant,
  }) {
    const emailType = specialIssue
      ? 'lead-guest-editor-of-ae-conflict'
      : 'triage-editor-of-ae-conflict'

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      specialIssueName: (specialIssue && specialIssue.name) || '',
      academicEditorName: academicEditor.getName(),
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: getEmail(triageEditor),
        name: getSurname(triageEditor),
      },
      content: {
        subject: `Reassessment required due to potential Conflict of Interest`,
        ctaText: CONTENT_CTA_TEXT,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: triageEditor.user.id,
          token: triageEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: triageEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAcademicEditorOfOwnConflict({
    journal,
    specialIssue,
    manuscript,
    academicEditor,
    editorialAssistant,
  }) {
    const emailType = specialIssue
      ? 'guest-editor-own-conflict'
      : 'academic-editor-own-conflict'

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName: journal.name,
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      specialIssueName: (specialIssue && specialIssue.name) || '',
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: getEmail(academicEditor),
        name: getSurname(academicEditor),
      },
      content: {
        subject: `Reassessment required due to potential Conflict of Interest`,
        paragraph,
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: academicEditor.user.id,
          token: academicEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: academicEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },

  async notifyAuthorOfManuscriptReturnToPeerReview({
    author,
    editorialAssistant,
    manuscript,
    specialIssue,
    journal,
  }) {
    const journalName = (journal && journal.name) || ''

    const emailType = specialIssue
      ? 'author-manuscript-returns-to-peer-review-for-special-issue'
      : 'author-manuscript-returns-to-peer-review-for-regular-issue'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      specialIssueName: (specialIssue && specialIssue.name) || '',
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: author.getEmail(),
        name: getSurname(author),
      },
      content: {
        subject: `Reassessment of manuscript ${manuscript.customId}`,
        paragraph,
        signatureName: editorialAssistant.getName(),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: author.getEmail(),
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },

  async notifyEditorialAssistantOfManuscriptReturnToPeerReview({
    editorialAssistant,
    manuscript,
    journal,
  }) {
    const journalName = (journal && journal.name) || ''

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'editorial-assistant-manuscript-returns-to-peer-review',
      customId: manuscript.customId,
      title: manuscript.title,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journalName} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: editorialAssistant.getEmail(),
        name: getSurname(editorialAssistant),
      },
      content: {
        subject: `Re-review process initiated for ${manuscript.customId}`,
        paragraph,
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: editorialAssistant.user.id,
          token: editorialAssistant.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: editorialAssistant.getEmail(),
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyAcademicEditorAboutImproperReviews({
    journal,
    specialIssue,
    manuscript,
    academicEditor,
    editorialAssistant,
    author,
  }) {
    const emailType = manuscript.specialIssueId
      ? 'academic-editor-improper-review-special-issue'
      : 'academic-editor-improper-review-regular-issue'

    let callForPapersLink
    if (specialIssue) {
      callForPapersLink = computeCallForPapersLink({
        journalCode: journal.code,
        specialIssueCustomId: specialIssue.customId,
      })
    }

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      customId: manuscript.customId,
      title: manuscript.title,
      authorName: author.getName(),
      callForPapersLink,
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: academicEditor.getEmail(),
        name: getSurname(academicEditor),
      },
      content: {
        subject: `${manuscript.customId}: Further review reports required`,
        ctaText: CONTENT_CTA_TEXT,
        paragraph,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: academicEditor.user.id,
          token: academicEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: academicEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifyApprovalEditorAboutTheDecisonMadeInError({
    editorialAssistant,
    approvalEditor,
    manuscript,
    author,
    journal,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'approval-editor-decision-made-in-error',
      customId: manuscript.customId,
      title: manuscript.title,
      authorName: author.getName(),
    })

    const email = new Email({
      type: 'user',
      bbc: bccAddress,
      fromEmail: `${journal.name} <${editorialAssistant.getEmail()}>`,
      toUser: {
        email: approvalEditor.getEmail(),
        name: getSurname(approvalEditor),
      },
      content: {
        subject: `${manuscript.customId}: Editorial decision reset`,
        ctaText: CONTENT_CTA_TEXT,
        paragraph,
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        signatureName: journal.name,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: approvalEditor.user.id,
          token: approvalEditor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: approvalEditor.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
  async notifySubmitterWhenManuscriptHasPassedQC({
    manuscript,
    submittingStaffMember,
  }) {
    const { alias, user } = submittingStaffMember
    const { customId, title, journal } = manuscript

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'submitter-manuscript-passed-qc',
      customId,
      title,
    })

    const email = new Email({
      type: 'user',
      bcc: bccAddress,
      fromEmail: `${journal.name} <${staffEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Your manuscript is moving into production`,
        paragraph,
        signatureJournal: `Quality Checking Team`,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: FOOTER_TEXT_PATTERN,
          replacement: alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
