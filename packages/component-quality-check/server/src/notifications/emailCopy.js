const getEmailCopy = ({
  title,
  customId,
  emailType,
  authorName,
  journalName,
  reviewerNames,
  academicEditorName,
  articleName,
  specialIssueName,
  invoicingUrl,
  servicesUrl,
  callForPapersLink,
}) => {
  let upperContent, subText, lowerContent, paragraph
  let hasLink = true
  let hasIntro = true
  const hasSignature = true

  switch (emailType) {
    case 'author-manuscript-rejected-eqa':
      hasLink = false
      paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName}.
        Thank you for your submission, and please do consider submitting again in the future.`
      break
    case 'author-academic-editor-coi':
      paragraph = `During the final checks of manuscript ${customId}: ${title}, which was recently recommended for publication, we found a potential conflict of interest between at least one of the authors and the Academic Editor, ${academicEditorName}.<br/><br/>
      To ensure a fully objective assessment, we would therefore like to ask a new Academic Editor to take a look at the manuscript, and reviewer reports, and decide how best to proceed. They will then be able either confirm the original decision, request revisions, or reject the manuscript, depending on their interpretation.<br/><br/>
      We’d therefore be grateful if you could visit the link below, and select a new Academic Editor to provide their assessment.<br/><br/>
      Many thanks for your continued contribution.<br/><br/>`
      break
    case 'author-review-coi-with-reviews':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication.
        Unfortunately, during final checks, we found a potential conflict of interest (COI) between at least one of the authors and reviewers ${reviewerNames}.<br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript and reviewer reports, and decide how best to proceed.
        A number of options are available to you:
        <ul>
          <li>If you have other reviewers, without a potential COI, who supported publication, and the conflicted review has not adversely affected the review process, you may continue with your original decision to accept the manuscript.</li>
          <li>If you have other supportive reviewers, without a potential COI, but the conflicted reviewer has adversely affected the revision of the manuscript, you may explain the situation to the author and ask for minor revisions to undo these.</li>
          <li>You may invite further reviewers if you feel more reports are needed.</li>
        </ul><br /><br />
        You can take the required action via the following link:
      `
      break
    case 'author-review-coi-without-reviews':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication.
        Unfortunately, during final checks, we found a potential conflict of interest (COI) between one or more authors and reviewers ${reviewerNames}.<br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript. As all reviewers have a potential COI, we must ask that you seek further advice from a new reviewer before making a decision. <br /> <br />
        You can take the required action via the following link:
      `
      break
    case 'author-review-coi-si-with-reviews':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication in Special Issue ${specialIssueName}.
        Unfortunately, during final checks, we found a potential conflict of interest (COI) between the author ${authorName} and reviewer ${reviewerNames}.<br/><br/>
        A link to the Special Issue Call for Papers can be found <a href="${callForPapersLink}">here</a>.<br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript and reviewer reports, and decide how best to proceed.
        A number of options are available to you:
        <ul>
          <li>If you have other reviewers, without a potential COI, who supported publication, and the conflicted review has not adversely affected the review process, you may continue with your original decision to accept the manuscript.</li>
          <li>If you have other supportive reviewers, without a potential COI, but the conflicted reviewer has adversely affected the revision of the manuscript, you may explain the situation to the author and ask for minor revisions to undo these.</li>
          <li>You may invite further reviewers if you feel more reports are needed.</li>
        </ul><br /><br />
        You can take the required action via the following link:
      `
      break
    case 'author-review-coi-si-without-reviews':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication in Special Issue ${specialIssueName}.
        Unfortunately, during final checks, we found a potential conflict of interest (COI) between the author ${authorName} and reviewer ${reviewerNames}.<br/><br/>
        A link to the Special Issue Call for Papers can be found <a href="${callForPapersLink}">here</a>. <br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript. As all reviewers have a potential COI, we must ask that you seek further advice from a new reviewer before making a decision. <br /><br />
        You can take the required action via the following link:
      `
      break
    case 'author-materialChecks-completed':
      hasLink = false
      paragraph = `
      Now that all the checks of your files have been completed, we are pleased to inform you that your manuscript is moving into production to ready it for publication online.<br/><br/>
      Our Production team will contact you in the near future with proofs of the final version of your manuscript for your feedback and comments.<br/><br/>
        Thank you again for choosing to publish with ${journalName}.<br/><br/>`
      break
    case 'submitter-manuscript-passed-qc':
      hasLink = false
      hasIntro = false
      paragraph = `
       The Quality Checking Team has confirmed that the manuscript ${title} with ID No. ${customId} has passed all quality checks and is ready for production.`
      break
    case 'author-prcc-completed':
      hasLink = true
      paragraph = `I am delighted to inform you that the review of your ${articleName} ${customId} titled ${title} has been completed and your article has been accepted for publication in ${journalName}.<br/><br/>
      Please visit the manuscript details page to review the editorial notes and any comments from external reviewers. If you have deposited your manuscript on a preprint server, now would be a good time to update it with the accepted version. If you have not deposited your manuscript on a preprint server, you are free to do so.<br/><br/>
      We will now check that all of your files are complete before passing them over to our production team for processing. We will let you know soon should we require any further information.<br/><br/>
      As an open access journal, publication of articles in ${journalName} are associated with Article Processing Charges. If applicable, you will receive a separate communication from our Editorial office in relation to this shortly. In regards to payments, we will:<br/><br/>
      <ul>
        <li>
          <strong>Only ever contact you from @hindawi.com email addresses.</strong> If you receive communications that claim to be from us, or one of our journals, but do not come from an @hindawi.com email address, please contact us directly at help@hindawi.com
        </li>
        <li>
          <strong>Only ever request payment through our own invoicing system.</strong>Any email requesting payment will always be from an @hindawi.com email address and will always direct you to our invoicing system with a link beginning ${invoicingUrl}
        </li>
      </ul> <br/>
      If you receive payment requests or information in ways other than this, or have any questions about Article Processing Charges, please contact us at help@hindawi.com.<br/><br/>
      Finally, we have partnered with leading author service providers to offer our authors discounts on a wide range of post-publication services (including videos, posters and more) to help you enhance the visibility and impact of your academic paper. Please <a href="${servicesUrl}">visit our author services page to learn more.</a><br/><br/>
       Thank you for choosing to publish with ${journalName}.<br/><br/>
      `
      break
    case 'triage-editor-own-conflict':
      hasLink = false
      paragraph = `Thank you for assessing manuscript ${customId}:${title}, which you recently approved for publication in ${journalName}. During our final checks, we found a potential conflict of interest between yourself and at least one of the authors. Typically such conflicts arise through co-publication or a shared institutional affiliation.<br/><br/>
      In light of this, and to ensure a fully objective assessment, we would therefore like to ask a second senior member of the Editorial Board to take a look at the manuscript and reviewer reports, before proceeding.<br/><br/>
      Please note that we are in no way questioning your personal judgement or impartiality. Rather, we wish to avoid any perceived conflict of interest (real or otherwise) to maintain the integrity of the review process. Having a second Editor approve the decision ensures we can achieve this goal without unduly delaying publication for the authors.<br/><br/>
      In case you find you hold a conflict of interest when assessing any future manuscripts, you can inform me or our team and we can have the manuscript reassigned. For more information about conflicts of interest, please see our Publication Ethics page.<br/><br/>
      No further action is required from you at this stage. Many thanks for your understanding and continuing contribution to ${journalName}.<br/><br/>
      `
      break
    case 'lead-guest-editor-own-conflict':
      hasLink = false
      paragraph = `Thank you for handling manuscript ${customId}:${title}, which was recently recommended for publication in Special Issue ${specialIssueName}. Unfortunately, during final checks, we found a potential conflict of interest between yourself and an author of the paper.<br/><br/>
      To ensure a fully objective assessment, we have sent the manuscript to a member of the journal’s Editorial Board for review. Please note that we are in no way questioning your personal judgement or impartiality. Rather, we wish to avoid any perceived conflict of interest (real or otherwise) to maintain the integrity of the review process. Having a second Editor approve the decision ensures we can achieve this goal without unduly delaying publication for the authors.<br/><br/>
      We greatly value your contribution as an Editor, and thank you for your understanding as we manage this process.<br/><br/>
        `
      break
    case 'lead-guest-editor-of-ae-conflict':
      hasLink = false
      paragraph = `I am contacting you regarding the manuscript ${customId}:${title}, which was recently recommended for publication in Special Issue ${specialIssueName}.<br/><br/>
      During our post-acceptance checks, our quality assurance team had identified a potential conflict of interest between one of the authors  and the Guest Editor ${academicEditorName} that would require the manuscript to be sent for re-review.<br/><br/>
      The authors and Guest Editor have also been informed of this.<br/><br/>
      We greatly value your contribution as an Editor, and thank you for your understanding as we manage this process.<br/><br/>
       `
      break
    case 'triage-editor-of-ae-conflict':
      paragraph = `During the final checks of manuscript ${customId}: ${title}, which was recently recommended for publication, we found a potential conflict of interest between at least one of the authors and the Academic Editor, ${academicEditorName}.<br/><br/>
        To ensure a fully objective assessment, we would therefore like to ask a new Academic Editor to take a look at the manuscript, and reviewer reports, and decide how best to proceed. They will then be able either confirm the original decision, request revisions, or reject the manuscript, depending on their interpretation.<br/><br/>
        We’d therefore be grateful if you could visit the link below, and select a new Academic Editor to provide their assessment.<br/><br/>
        Many thanks for your continued contribution.<br/><br/>`
      break
    case 'academic-editor-own-conflict':
      hasLink = false
      paragraph = `Thank you for handling manuscript ${customId}:${title}, which you recently recommended for publication. Unfortunately, during final checks, we found a potential conflict of interest between yourself and an author of the paper.<br/><br/>
      To ensure a fully objective assessment, we have sent the manuscript to a member of the journal’s Editorial Board for review. Please note that we are in no way questioning your personal judgement or impartiality. Rather, we wish to avoid any perceived conflict of interest (real or otherwise) to maintain the integrity of the review process. Having a second Editor approve the decision ensures we can achieve this goal without unduly delaying publication for the authors.<br/><br/>
      We greatly value your contribution as an Editor, and thank you for your understanding as we manage this process.<br/><br/>
          `
      break
    case 'guest-editor-own-conflict':
      hasLink = false
      paragraph = `Thank you for handling manuscript ${customId}:${title}, which you recently recommended for publication in Special Issue ${specialIssueName}. Unfortunately, during final checks, we found a potential conflict of interest between yourself and an author of the paper.<br/><br/>
      To ensure a fully objective assessment, we have sent the manuscript to a member of the journal’s Editorial Board for review. Please note that we are in no way questioning your personal judgement or impartiality. Rather, we wish to avoid any perceived conflict of interest (real or otherwise) to maintain the integrity of the review process. Having a second Editor approve the decision ensures we can achieve this goal without unduly delaying publication for the authors.<br/><br/>
      We greatly value your contribution as an Editor, and thank you for your understanding as we manage this process.<br/><br/>
        `
      break
    case 'author-manuscript-returns-to-peer-review-for-regular-issue':
      hasLink = false
      paragraph = `I am writing in regard to your manuscript ${customId}, entitled ${title}. During our post-acceptance checks we have unfortunately identified a potential issue with the handling of your paper during the peer review process.<br/><br/>
      In order to progress your paper, therefore, we have invited another member of the Editorial Board to reassess your manuscript and its peer review thus far.<br/><br/>
      The new Editor will either invite additional reviewers, if necessary, or you will be notified of a decision as soon as possible. You will still be able to see the status of your manuscript on Phenom Review.<br/><br/>
      Please be assured that this is a standard part of our manuscript checks to ensure the rigour of our peer review process.<br/><br/>
      Please accept our apologies for any inconvenience or delay this may cause.<br></br>`
      break
    case 'author-manuscript-returns-to-peer-review-for-special-issue':
      hasLink = false
      paragraph = `I am writing in regard to your manuscript ${customId}, entitled ${title}, submitted to Special Issue ${specialIssueName}. During our post-acceptance checks we have unfortunately identified a potential issue with the handling of your paper during the peer review process.<br/><br/>
      In order to progress your paper, therefore, we have invited another member of the Editorial Board to reassess your manuscript and its peer review thus far.<br/><br/>
      The new Editor will either invite additional reviewers, if necessary, or you will be notified of a decision as soon as possible. You will still be able to see the status of your manuscript on Phenom Review.<br/><br/>
      Please be assured that this is a standard part of our manuscript checks to ensure the rigour of our peer review process.<br/><br/>
      Please accept our apologies for any inconvenience or delay this may cause.<br></br>`
      break
    case 'editorial-assistant-manuscript-returns-to-peer-review':
      hasLink = false
      paragraph = `This email is confirmation that this manuscript ${customId}, titled "${title}" has been sent back for reassessment on the system.<br/><br/>Please ensure that this manuscript progresses through the re-review process.<br/><br/>`
      break
    case 'academic-editor-improper-review-regular-issue':
      hasLink = true
      paragraph = `Many thanks for your recent decision on manuscript ${customId} titled ${title} by ${authorName} et al.<br/><br/>
          During our post-acceptance checks, we noticed that the reviewer reports were extremely brief. We therefore request that you secure further reports from at least one additional reviewer so that you may make a more informed editorial decision, and to ensure that all aspects of the manuscript have been sufficiently reviewed and to further justify your decision to the authors.<br/><br/>
          We have reset the decision and you are now able to invite further potential reviewers via the link below:
          `
      break
    case 'academic-editor-improper-review-special-issue':
      hasLink = true
      paragraph = `Many thanks for your recent decision on manuscript ${customId} titled ${title} by ${authorName} et al.<br/><br/>
        A link to the Special Issue Call for Papers can be found <a href="${callForPapersLink}">here</a>.<br/><br/>
        During our post-acceptance checks, we noticed that the reviewer reports were extremely brief. We therefore request that you secure further reports from at least one additional reviewer so that you may make a more informed editorial decision, and to ensure that all aspects of the manuscript have been sufficiently reviewed and to further justify your decision to the authors.<br/><br/>
        We have reset the decision and you are now able to invite further potential reviewers via the link below:
        `
      break
    case 'approval-editor-decision-made-in-error':
      hasLink = true
      paragraph = `We have reset the editorial decision on manuscript ${customId} titled ${title} by ${authorName} et al. You may now access the manuscript via the link below and take the necessary actions:
          `
      break
    case 'academic-editor-manuscript-conflict':
      paragraph = `Thank you for handling manuscript ${customId}: ${title}, which you recently recommended for publication. 
          Unfortunately, during final checks, we found a potential conflict of interest between at least one of the authors and the following reviewers: ${reviewerNames}.<br/><br/>
          To ensure a fully objective assessment, we would therefore like to ask you to take a second look at the manuscript and reviewer reports, and decide how best to proceed.
          A number of options are available to you:
          <ul>
            <li>If you have other reviewers who supported publication, and the conflicted review has not adversely affected the review process, you may stick with your original decision to accept the manuscript.</li>
            <li>If you have other supportive reviewers, but the conflicted reviewer has adversely affected the revision of the manuscript, you may explain the situation to the author and ask for minor revisions to undo these.</li>
            <li>If you have no other supportive reviewers, we must ask that you seek further advice from a new reviewer before making a decision.</li>
          </ul>
          Many thanks for your continued contribution. You can take the required action(s) via the following link:`
      hasLink = true
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }
  return {
    hasLink,
    subText,
    hasIntro,
    paragraph,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
