import { repos } from 'component-model' // import with path so we can bypas index JS and use TS
import { ConflictI } from 'component-model/server/src/model/conflict' // import with path so we can bypas index JS and use TS
import { Transaction } from 'objection'

const { logger } = require('component-logger')

export type CreateConflictDTO = {
  firstConflictingTeamMemberId: string
  secondConflictingTeamMemberId: string
}

const { ConflictRepository } = repos
interface ConflictOfInterestServiceI {
  createConflict: (
    conflict: CreateConflictDTO,
    trx?: Transaction,
  ) => Promise<ConflictI>
  // getConflictsOnManuscript: (manuscriptId: string) => Promise<Conflict[]>
  // getConflictsForTeamMember: (teamMemberId: string) => Promise<Conflict[]>
}

class ConflictOfInterestService implements ConflictOfInterestServiceI {
  protected conflictRepo = new ConflictRepository()
  async createConflict(conflict: CreateConflictDTO, trx?: Transaction) {
    const existingConflictsForTeamMember = await this.getConflictsForTeamMembers(
      [conflict.firstConflictingTeamMemberId],
      trx,
    )
    const theresAlreadyAConflictForFirstActor =
      existingConflictsForTeamMember.length
    if (theresAlreadyAConflictForFirstActor)
      return logger.warn(
        `There already exists a conflict for the first actor in the conflict of interest. Team member id: ${conflict.firstConflictingTeamMemberId}`,
      )
    return this.conflictRepo.create(conflict, trx)
  }
  public async getConflictsForTeamMembers(
    teamMemberIds: string[],
    trx?: Transaction,
  ) {
    return this.conflictRepo.getConflictsForTeamMembers(teamMemberIds, trx)
  }

  // public async getConflictsOnManuscript(manuscriptId) {}
}

export const ConflictOfInterest = new ConflictOfInterestService()
