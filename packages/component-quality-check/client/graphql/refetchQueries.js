import { queries } from 'component-peer-review/client/graphql'

export const refetchGetSubmission = ({ params: { submissionId = '' } }) => ({
  query: queries.getSubmission,
  variables: { submissionId },
})
