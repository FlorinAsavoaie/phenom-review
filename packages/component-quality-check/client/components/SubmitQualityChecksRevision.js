import React, { useState, useEffect } from 'react'
import { get, chain } from 'lodash'
import { Formik } from 'formik'
import { space } from 'styled-system'
import { withRouter } from 'react-router-dom'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'
import { useLazyQuery } from 'react-apollo'
import { withGQL as withFilesGQL } from 'component-files/client'
import { visibleDeclarations } from 'component-submission/client'
import { queries as submissionQueries } from 'component-submission/client/graphql'
import { Row, MultiAction, ContextualBox } from '@hindawi/ui'
import {
  ManuscriptFiles,
  DetailsAndAuthors,
} from 'component-peer-review/client'
import { parseError } from 'component-submission/client/utils'

import { refetchGetSubmission } from '../graphql/refetchQueries'
import { AuthorDeclarations } from './'
import {
  autosaveForm,
  setQualityChecksInitialValues,
  validateQualityChecksRevision,
} from '../utils'

const SubmitQualityChecksRevision = ({
  onSubmit,
  highlight,
  editAuthor,
  journalCode,
  removeAuthor,
  handleUpload,
  handleDelete,
  revisionDraft,
  updateAutosave,
  manuscriptStatus,
  updateDraftRevision,
  updateManuscriptFile,
  addAuthorToManuscript,
}) => {
  const [
    getSubmissionEditorialMapping,
    { data: submissionEditorialMapping },
  ] = useLazyQuery(submissionQueries.getSubmissionEditorialMapping, {
    fetchPolicy: 'network-only',
    variables: { submissionId: revisionDraft.submissionId },
  })

  useEffect(() => {
    if (!revisionDraft.submissionId) return
    getSubmissionEditorialMapping({
      variables: { submissionId: revisionDraft.submissionId },
    })
  }, [revisionDraft.id])
  const submissionEditorialModel = get(
    submissionEditorialMapping,
    'getSubmissionEditorialMapping.submissionEditorialModel',
  )
  const [isFileUploading, setIsFileUploading] = useState(false)
  if (!revisionDraft) return null

  const { declarations, questions } = visibleDeclarations

  const initialValues = setQualityChecksInitialValues(revisionDraft)
  const revisionManuscriptId = get(revisionDraft, 'id')
  const manuscriptType = get(revisionDraft, 'articleType')
  const articleTypeName = chain(manuscriptType)
    .get('name')
    .camelCase()
    .value()
  const isConflictsVisible =
    declarations[articleTypeName].conflictOfInterest.isVisible
  const isDataAvailabilityVisible =
    declarations[articleTypeName].dataAvailability.isVisible
  const isFundingStatementVisible =
    declarations[articleTypeName].fundingStatement.isVisible
  const isDataAvailabilityRequired =
    declarations[articleTypeName].dataAvailability.isRequired
  const autosave = values =>
    autosaveForm({
      values,
      updateDraftRevision,
      updateAutosave,
    })

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validateQualityChecksRevision}
    >
      {({ values, errors, handleSubmit, setFieldValue }) => (
        <ContextualBox
          data-test-id="submit-qc-updates-contextual-box"
          highlight={highlight}
          label="Submit Quality Check Updates"
          mt={4}
        >
          <Root pr={4} pt={2}>
            {autosave(values)}
            <DetailsAndAuthors
              addAuthorToManuscript={addAuthorToManuscript}
              editAuthor={editAuthor}
              formErrors={errors}
              formValues={values}
              journalCode={journalCode}
              manuscriptType={manuscriptType}
              removeAuthor={removeAuthor}
              revisionManuscriptId={revisionManuscriptId}
              setFieldValue={setFieldValue}
              startExpanded
              submissionEditorialModel={submissionEditorialModel}
            />
            <ManuscriptFiles
              formErrors={errors}
              formValues={values}
              handleDelete={handleDelete}
              handleUpload={handleUpload}
              manuscriptStatus={manuscriptStatus}
              revisionManuscriptId={revisionManuscriptId}
              setIsFileUploading={setIsFileUploading}
              startExpanded
              updateManuscriptFile={updateManuscriptFile}
            />
            <AuthorDeclarations
              formValues={values}
              isConflictsVisible={isConflictsVisible}
              isDataAvailabilityRequired={isDataAvailabilityRequired}
              isDataAvailabilityVisible={isDataAvailabilityVisible}
              isFundingStatementVisible={isFundingStatementVisible}
              questions={questions}
              setFieldValue={setFieldValue}
            />
            <Row justify="flex-end" mb={4}>
              <Button
                data-test-id="submit-qc-updates"
                disabled={isFileUploading}
                onClick={handleSubmit}
                primary
                width={48}
              >
                Submit Quality Checks
              </Button>
            </Row>
          </Root>
        </ContextualBox>
      )}
    </Formik>
  )
}

export default compose(
  withRouter,
  withFilesGQL(({ match }) => ({
    refetchQueries: [refetchGetSubmission(match)],
  })),
  withModal({
    component: MultiAction,
    modalKey: 'submitQCRevision',
  }),
  withHandlers({
    onSubmit: ({ submitRevision, showModal }) => (values, formikBag) =>
      showModal({
        cancelText: 'Not yet',
        confirmText: 'Submit',
        onConfirm: modalProps => submitRevision(values, modalProps, formikBag),
        subtitle: 'Once submitted, no further changes can be made.',
        title: 'Ready to Submit the Quality Check updates?',
      }),
    handleUpload: ({ uploadFile }) => (
      file,
      { type, push, entityId, setFetching, setError, clearError, setFileField },
    ) => {
      const fileInput = {
        type,
        size: file.size,
      }

      clearError && clearError()
      setFetching(true)

      uploadFile({ entityId, fileInput, file })
        .then(uploadedFile => {
          setFetching(false)
          push && push(uploadedFile)
          setFileField && setFileField(uploadedFile)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    handleDelete: ({ deleteFile }) => (
      file,
      { index, remove, setError, setFetching, setFileField },
    ) => {
      setFetching(true)
      deleteFile(file.id)
        .then(() => {
          setFetching(false)
          remove && remove(index)
          setFileField && setFileField()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(SubmitQualityChecksRevision)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  ${space};
`
