import React from 'react'
import { Route } from 'react-router-dom'
import track from 'react-tracking'

export const TrackedRoute = ({ pageName, component, children, ...props }) => {
  window.initGTM && !window.isGTMActive && window.initGTM()
  const whatToRender = {}

  if (!component) {
    whatToRender.children = children
  } else {
    whatToRender.component = track({ page: pageName || props.path })(component)
  }

  return <Route {...whatToRender} {...props} />
}
