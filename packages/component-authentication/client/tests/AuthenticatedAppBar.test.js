import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import { AuthenticatedAppBarContent } from '../components'

jest.mock('component-submission/client', () => ({
  AutosaveIndicator: () => <span>autosave</span>,
  SubmitDraft: () => <span>submit</span>,
}))

const confirmedUser = {
  isActive: true,
  role: 'admin',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: true,
      email: 'alexandru.munteanu@hindawi.com',
      __typename: 'Local',
    },
  ],
}

const academicEditor = {
  isActive: true,
  role: 'academicEditor',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: true,
      email: 'alexandru.munteanu@hindawi.com',
      __typename: 'Local',
    },
  ],
}

const unconfirmedUser = {
  isActive: true,
  role: 'admin',
  identities: [
    {
      name: {
        givenNames: 'Aurel',
        surname: 'Becali',
        title: 'mrs',
      },
      isConfirmed: false,
      email: 'alexandru.munteanu@hindawi.com',
      __typename: 'Local',
    },
  ],
}

describe('AuthenticatedAppBarContent', () => {
  afterEach(cleanup)

  it('should show username and submit button', () => {
    const { getByText, queryByText } = render(
      <AuthenticatedAppBarContent
        currentUser={confirmedUser}
        showSubmitButton
      />,
    )

    expect(getByText(/Aurel/i)).toBeInTheDocument()
    expect(getByText(/submit/i)).toBeInTheDocument()
    expect(queryByText(/autosave/i)).toBeNull()
  })

  it('should not show submit button while on wizard route', () => {
    const cfg = {
      // set the router on the submission wizard route
      initialEntries: ['/submit/1234567-12345/5512321-12311'],
    }

    const { getByText, queryByText } = render(
      <AuthenticatedAppBarContent currentUser={confirmedUser} />,
      cfg,
    )

    expect(getByText(/Aurel/i)).toBeInTheDocument()
    expect(queryByText(/submit/i)).toBeNull()
    expect(getByText(/autosave/i)).toBeInTheDocument()
  })

  it('should show notification for unconfirmed users', () => {
    const { getByText } = render(
      <AuthenticatedAppBarContent currentUser={unconfirmedUser} />,
    )

    expect(
      getByText('Your account is not confirmed. Please check your email.'),
    ).toBeInTheDocument()
  })

  it('should be able to click logo and go to dashboard', () => {
    const goToDashboardMock = jest.fn()
    const goToMock = jest.fn(() => goToDashboardMock)
    const { container } = render(
      <AuthenticatedAppBarContent
        currentUser={confirmedUser}
        goToDashboard={goToMock()}
      />,
    )

    fireEvent.click(container.querySelector('svg'))

    expect(goToDashboardMock).toHaveBeenCalledTimes(1)
  })

  it('should interact with the menu', () => {
    const logoutMock = jest.fn()
    const goToMock = jest.fn()

    const { getByText } = render(
      <AuthenticatedAppBarContent
        currentUser={academicEditor}
        goTo={goToMock}
        logout={logoutMock}
      />,
    )

    fireEvent.click(getByText(/aurel/i))
    fireEvent.click(getByText(/my profile/i))

    expect(goToMock).toHaveBeenCalledTimes(1)
    expect(goToMock).toHaveBeenCalledWith('/profile')

    fireEvent.click(getByText(/aurel/i))
    fireEvent.click(getByText(/logout/i))

    expect(logoutMock).toHaveBeenCalledTimes(1)
  })
})
