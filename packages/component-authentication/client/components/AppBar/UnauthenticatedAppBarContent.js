import React from 'react'
import { NewSVGLogo } from '@hindawi/ui'

import { LogoContainer, Root } from '../sharedStyledComponents'

const UnauthenticatedAppBarContent = ({ publisher, logo, goTo }) => (
  <Root>
    <LogoContainer>
      <NewSVGLogo ml={5} onClick={() => goTo('/')} publisher={publisher} />
    </LogoContainer>
  </Root>
)

export default UnauthenticatedAppBarContent
