import React, { Fragment } from 'react'
import { chain } from 'lodash'
import { Route } from 'react-router-dom'
import { NewSVGLogo, UserDropDown, ActionLink, Banner } from '@hindawi/ui'
import { useJournal } from 'component-journal-info'
import {
  AutosaveIndicator,
  SubmitDraft,
  TransferManuscript,
} from 'component-submission/client'

import { Root, LogoContainer, RightContainer } from '../sharedStyledComponents'

// to do: this needs to be refactored. rendering routes here leads to bugs
const autosaveIndicatorPaths = [
  '/submit/:submissionId/:manuscriptId',
  '/details/:submissionId/:manuscriptId',
]

const AuthenticatedAppBarContent = ({
  goTo,
  logo,
  logout,
  publisher,
  currentUser = {},
  goToDashboard,
  showTransferButton,
  showSubmitButton,
}) => {
  const { name, supportEmail } = useJournal()
  const isConfirmed = chain(currentUser)
    .get('identities', [])
    .find(i => i.__typename === 'Local')
    .get('isConfirmed')
    .value()
  const SHOULD_DISPLAY_BANNER = name === 'GeoScienceWorld'

  return (
    <Fragment>
      <Root>
        <LogoContainer>
          <NewSVGLogo
            ml={5}
            onClick={goToDashboard}
            publisher={publisher}
            src={logo}
          />
        </LogoContainer>
        <RightContainer>
          {autosaveIndicatorPaths.map(path => (
            <Route component={AutosaveIndicator} exact key={path} path={path} />
          ))}
          {showTransferButton && (
            <TransferManuscript currentUser={currentUser} />
          )}
          {showSubmitButton && <SubmitDraft currentUser={currentUser} />}
          <UserDropDown currentUser={currentUser} goTo={goTo} logout={logout} />
        </RightContainer>
      </Root>
      {!isConfirmed && (
        <Banner name="confirmation">
          Your account is not confirmed. Please check your email.
        </Banner>
      )}
      {SHOULD_DISPLAY_BANNER && (
        <Banner name="GSWSubmissions">
          <i>Lithosphere</i> is moving to a new editorial service provider and
          submission system. As a result, there will be a temporary pause on
          article submissions. New manuscript submissions will resume on January
          18, 2023. We appreciate your patience during the transition period.
          There will be no disruptions in manuscripts submitted prior to
          December 30, 2022. Please direct any questions to
          editorial@geoscienceworld.org.
        </Banner>
      )}
    </Fragment>
  )
}

export default AuthenticatedAppBarContent
