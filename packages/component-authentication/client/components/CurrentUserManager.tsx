import React, { useState, useContext, createContext } from 'react'

export const CurrentUserContext = createContext({ currentUser: {} })

const CurrentUserManager = ({ children }) => {
  const [currentUser, setCurrentUser] = useState({})

  const context = {
    currentUser,
    setCurrentUser,
  }

  return (
    <CurrentUserContext.Provider value={context}>
      {children}
    </CurrentUserContext.Provider>
  )
}

export const useCurrentUser = () => {
  const { currentUser } = useContext(CurrentUserContext)

  return currentUser
}

export default CurrentUserManager
