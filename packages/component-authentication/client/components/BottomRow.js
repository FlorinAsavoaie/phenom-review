import styled from 'styled-components'
import { Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'

export default styled(Row)`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: calc(${th('gridUnit')} * 2);
  border-bottom-right-radius: calc(${th('gridUnit')} * 2);
`
