import { compose, withHandlers, withProps } from 'recompose'
import { withFetching } from '@hindawi/ui'

import { parseSearchParams } from '../utils'
import withAuthenticationGQL from '../graphql'
import { SetNewPasswordForm } from '../components'

export default compose(
  withFetching,
  withAuthenticationGQL,
  withProps(({ location }) => ({
    params: parseSearchParams(location.search),
  })),
  withHandlers({
    setPassword: ({
      history,
      setError,
      setPassword,
      setFetching,
      params: { email, token },
    }) => ({ password }) => {
      setFetching(true)
      setPassword({
        variables: {
          input: {
            email,
            token,
            password,
          },
        },
      })
        .then(r => {
          window.localStorage.removeItem('token')
          setFetching(false)
          history.push('/')
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(SetNewPasswordForm)
