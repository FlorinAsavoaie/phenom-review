import React from 'react'
import { get } from 'lodash'
import { Redirect } from 'react-router'
import { Button, H2 } from '@pubsweet/ui'
import { Row, ShadowedBox, Text } from '@hindawi/ui'

const InfoPage = ({ location, history }) => {
  const pathState = get(location, 'state', {})
  if (!pathState.title) {
    return <Redirect to="/" />
  }
  return (
    <ShadowedBox center mt={10} pb={6} pt={6}>
      <H2>{pathState.title}</H2>
      <Row mt={2}>
        <Text>{pathState.content}</Text>
      </Row>

      {pathState.path && (
        <Row mt={4}>
          <Button onClick={() => history.push(pathState.path)} primary>
            {pathState.buttonText}
          </Button>
        </Row>
      )}
    </ShadowedBox>
  )
}

export default InfoPage
