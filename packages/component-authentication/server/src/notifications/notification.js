const config = require('config')
const Email = require('component-sendgrid')

const { services } = require('helper-service')
const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const accountsEmail = config.get('accountsEmail')
const publisherName = config.get('publisherName')
const unsubscribeSlug = config.get('unsubscribe.url')
const baseUrl = config.get('pubsweet-client.baseUrl')
const footerText = config.get('emailFooterText.registeredUsers')

module.exports = {
  async notifyUserOnSignUp({ user, identity }) {
    if (process.env.KEYCLOAK_SERVER_URL) {
      return
    }
    const confirmSignUp = config.get('confirm-signup.url')

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-signup',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `Hindawi Team <${accountsEmail}>`,
      toUser: {
        email: identity.email,
        name: `${identity.surname}`,
      },
      content: {
        subject: 'Confirm your email address',
        ctaLink: services.createUrl(baseUrl, confirmSignUp, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: publisherName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: identity.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendForgotPasswordEmail({ user, identity }) {
    if (process.env.KEYCLOAK_SERVER_URL) {
      return
    }
    const forgotPath = config.get('forgot-password.url')

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-forgot-password',
    })

    const email = new Email({
      type: 'system',
      toUser: {
        email: identity.email,
      },
      fromEmail: `Hindawi Team <${accountsEmail}>`,
      content: {
        subject: 'Forgot Password',
        ctaLink: services.createUrl(baseUrl, forgotPath, {
          email: identity.email,
          token: user.passwordResetToken,
        }),
        ctaText: 'RESET PASSWORD',
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: identity.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
