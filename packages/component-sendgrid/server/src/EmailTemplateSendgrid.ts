const config = require('config')
const { logger } = require('component-logger')
const EmailTemplate = require('../../../component-email-templating')
const nodemailer = require('nodemailer')

class EmailTemplateSendgrid extends EmailTemplate {
  // eslint-disable-next-line class-methods-use-this
  send(mailData) {
    const mailerConfig = require(config.get('mailer'))
    if (!mailerConfig || !mailerConfig.transport) {
      throw new Error('Mailer: The configuration is either invalid or missing.')
    }

    const transporter = nodemailer.createTransport(mailerConfig.transport)
    transporter
      .sendMail(mailData)
      .then(info => {
        if (process.env.NODE_ENV === 'development') {
          try {
            logger.info(
              `Email sent. Preview available at: ${nodemailer.getTestMessageUrl(
                info,
              )}`,
            )
          } catch (err) {
            logger.info(`Email sent.`)
          }
        }
        return info
      })
      .catch(err => {
        logger.error('Failed to send email', err)
      })
  }

  async sendEmail() {
    const { html, text } = this._getEmailTemplate()

    const { fromEmail: from, cc, bcc } = this
    const { email: to } = this.toUser
    const { subject } = this.content

    const mailData = {
      to,
      cc,
      bcc,
      text,
      html,
      from,
      subject,
    }

    try {
      const email = await this.send(mailData)
      logger.debug(
        `Sent email from: ${from} to: ${to} with subject: "${subject}"`,
      )
      return email
    } catch (e) {
      logger.error('Something went wrong', e)
      throw new Error(e)
    }
  }
}

module.exports = EmailTemplateSendgrid
