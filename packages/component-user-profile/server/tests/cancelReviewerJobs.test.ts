import {
  getTeamRoles,
  generateUser,
  generateTeamMember,
} from 'component-generators'
import { cancelReviewerJobsUseCase } from '../src/useCases'

const models = {
  Job: {
    findAllByTeamMembers: jest.fn(),
    cancel: jest.fn(),
  },
  TeamMember: {
    findAllByUserAndRole: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
}
const { Job, TeamMember } = models
describe('cancel reviewer jobs use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns early if no reviewers are found', async () => {
    const user = generateUser()

    jest.spyOn(TeamMember, 'findAllByUserAndRole').mockResolvedValue([])

    await cancelReviewerJobsUseCase.initialize(models).execute(user.id)

    expect(TeamMember.findAllByUserAndRole).toHaveBeenCalledTimes(1)
    expect(Job.findAllByTeamMembers).toHaveBeenCalledTimes(0)
  })
  it('returns if no jobs are found', async () => {
    const user = generateUser()
    const teamMember = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findAllByUserAndRole')
      .mockResolvedValue([teamMember])
    jest.spyOn(Job, 'findAllByTeamMembers').mockResolvedValue([])

    await cancelReviewerJobsUseCase.initialize(models).execute(user.id)

    expect(TeamMember.findAllByUserAndRole).toHaveBeenCalledTimes(1)
    expect(Job.findAllByTeamMembers).toHaveBeenCalledTimes(1)
    expect(Job.cancel).toHaveBeenCalledTimes(0)
  })
  it('cancels jobs if reviewer jobs are found', async () => {
    const user = generateUser()
    const teamMember = generateTeamMember()
    const job = {
      id: 'job-id',
      delete: jest.fn(),
    }
    jest
      .spyOn(TeamMember, 'findAllByUserAndRole')
      .mockResolvedValue([teamMember])
    jest.spyOn(Job, 'findAllByTeamMembers').mockResolvedValue([job])

    await cancelReviewerJobsUseCase.initialize(models).execute(user.id)

    expect(TeamMember.findAllByUserAndRole).toHaveBeenCalledTimes(1)
    expect(Job.findAllByTeamMembers).toHaveBeenCalledTimes(1)
    expect(Job.cancel).toHaveBeenCalledTimes(1)
    expect(job.delete).toHaveBeenCalledTimes(1)
  })
})
