const { rorService } = require('component-ror')

const initialize = ({ User }) => ({
  execute: async ({ userId, input }) => {
    const user = await User.find(userId, 'identities')
    const localIdentity = user.defaultIdentity
    localIdentity.updateProperties(input)
    await appendRORInfoIfMissing(localIdentity)

    await localIdentity.save()
  },
})

const appendRORInfoIfMissing = async identity => {
  if (identity.aff && !identity.affRorId) {
    const bestROR = await rorService.getBestROR(identity.aff, identity.country)
    if (bestROR) {
      identity.affRorId = bestROR.organization.id
    }
  }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
