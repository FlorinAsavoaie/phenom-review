export type Term = {
  id: string
  parentId: string | null
  name: string
  path: string
  depth: number
  isLeaf: boolean
  isAssigned: boolean
}
