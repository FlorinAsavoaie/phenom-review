import { useState, useCallback, useEffect } from 'react'
import { TermDataNode } from '../../components/edit-expertise/tree'

import { Term } from './Term'
import { useRootAndAssignedTerms } from './useRootAndAssignedTerms'
import { useTermsByParentId } from './useTermsByParentId'

type TermTree = Term & {
  children: TermTree[]
}

export function updateTreeData<T extends TermTree>(
  tree: T[],
  key: string | number,
  children: T[],
) {
  return tree.map(node => {
    if (node.id === key) {
      return { ...node, children }
    }

    if (node.children) {
      return { ...node, children: updateTreeData(node.children, key, children) }
    }

    return node
  })
}

function toggleAssignment<T extends TermTree>(tree: T[], targetNode: T): T[] {
  return tree.map(node => {
    if (node.id === targetNode.id) {
      return {
        ...node,
        isAssigned: !node.isAssigned,
      }
    }

    if (node.children) {
      return {
        ...node,
        children: toggleAssignment(node.children, targetNode),
      }
    }

    return node
  })
}

type HookInput<T> = {
  toTreeData: (terms: Term[]) => T[]
  customSetTreeData: (value: React.SetStateAction<T[]>) => void
  expertiseTermIds: string[]
}

type HookResult<T> = {
  treeData: T[]
  assignedTerms: Term[]
  loading: boolean
  error: string | null
  loadData: (id: string, children: TermDataNode[]) => Promise<void>
  toggleNodeAssignment: (node: T) => void
}

export function useTerms<T extends TermTree>({
  toTreeData,
  customSetTreeData,
  expertiseTermIds,
}: HookInput<T>): HookResult<T> {
  const { rootTerms, assignedTerms, loading, error } = useRootAndAssignedTerms()

  const [treeData, setTreeData] = useState<T[]>(toTreeData(rootTerms))
  useEffect(() => {
    if (!loading && !error) {
      setTreeData(toTreeData(rootTerms))
    }
  }, [rootTerms, loading, error, toTreeData])

  const getTermsByParentId = useTermsByParentId()
  const loadData = useCallback(
    (id: string, children: TermDataNode[]): Promise<void> =>
      new Promise(async resolve => {
        if (children.length > 0) {
          resolve()
          return
        }

        const childrenTerms = (await getTermsByParentId(id)).map(term => ({
          ...term,
          isAssigned: expertiseTermIds.includes(term.id),
        }))

        const setterToBeCalled =
          customSetTreeData === null ? setTreeData : customSetTreeData

        setterToBeCalled(currentTreeData =>
          updateTreeData(currentTreeData, id, toTreeData(childrenTerms)),
        )

        resolve()
      }),
    [
      getTermsByParentId,
      toTreeData,
      customSetTreeData,
      setTreeData,
      expertiseTermIds,
    ],
  )
  const toggleNodeAssignment = (node: T) => {
    setTreeData(currentTreeData => toggleAssignment(currentTreeData, node))
  }

  return {
    treeData,
    assignedTerms,
    loading,
    error,
    loadData,
    toggleNodeAssignment,
  }
}
