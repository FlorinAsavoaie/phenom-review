User profile component.

```js
const journal = {
  titles: [
    {
      label: 'Mr',
      value: 'mr',
    },
    {
      label: 'Mrs',
      value: 'mrs',
    },
    {
      label: 'Miss',
      value: 'miss',
    },
    {
      label: 'Ms',
      value: 'ms',
    },
    {
      label: 'Dr',
      value: 'dr',
    },
    {
      label: 'Professor',
      value: 'prof',
    },
  ],
}

const user = {
  id: 'd784cd6e-6afa-4d63-b2a7-b9600f037507',
  type: 'user',
  admin: false,
  email: 'alexandru.munteanu+author12@hindawi.com',
  teams: [],
  title: 'dr',
  agreeTC: true,
  country: 'BW',
  isActive: true,
  lastName: 'Munteanu',
  username: 'alexandru.munteanu+author12@hindawi.com',
  firstName: 'Alexandru',
  fragments: [],
  affiliation: 'TSD',
  collections: [],
  isConfirmed: true,
  triageEditor: false,
  notifications: {
    email: {
      user: true,
      system: true,
    },
  },
  academicEditor: false,
  token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFsZXhhbmRydS5tdW50ZWFudSthdXRob3IxMkB0aGluc2xpY2VzLmNvbSIsImlkIjoiZDc4NGNkNmUtNmFmYS00ZDYzLWIyYTctYjk2MDBmMDM3NTA3IiwiaWF0IjoxNTM2MzIyMDE1LCJleHAiOjE1MzY0MDg0MTV9.y0vgd2h0Y3pUsZeWauufWg2IsB2ncVog1TiPpLiGBsM',
}

;<UserProfile
  onSave={(value, props) => {
    console.log('User', value)
  }}
  user={user}
  journal={journal}
/>
```
