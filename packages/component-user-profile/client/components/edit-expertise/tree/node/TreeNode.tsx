import React from 'react'

import { TermDataNode } from './../types'
import { NodeInfo } from './NodeInfo'
import { AssignButton } from './AssignButton'

export const TreeNode = ({
  node,
  onAssign,
  searchValue,
}: {
  node: TermDataNode
  onAssign: (node: TermDataNode) => void
  searchValue: string
}): JSX.Element => (
  <div
    className="nodeWrapper"
    style={{ height: node.depth === 1 ? '30px' : '20px' }}
    title=""
  >
    <NodeInfo node={node} searchValue={searchValue} />
    <AssignButton node={node} onAssign={onAssign} />
  </div>
)
