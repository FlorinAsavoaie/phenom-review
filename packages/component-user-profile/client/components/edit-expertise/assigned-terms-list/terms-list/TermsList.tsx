import React from 'react'
import { Popover, Tag } from '@hindawi/phenom-ui'

import { TermsContainerRoot, TermsContainer, TagWrapper } from './styles'
import { Term } from '../../../../graphql/hooks/Term'

import { EmptyListMessage } from './EmptyListMessage'

type TermsListProps = {
  terms: Term[]
  onRemove: (term: Term) => void
}

export const TermsList = ({ terms, onRemove }: TermsListProps): JSX.Element => (
  <TermsContainerRoot>
    <TermsContainer>
      {terms.length === 0 ? (
        <EmptyListMessage />
      ) : (
        terms.map(term => (
          <TagWrapper key={term.id}>
            <Popover
              content={term.path}
              mouseEnterDelay={1}
              style={{ width: 'max-content' }}
            >
              <Tag
                closable
                label={term.name}
                onClose={() => {
                  onRemove(term)
                }}
              />
            </Popover>
          </TagWrapper>
        ))
      )}
    </TermsContainer>
  </TermsContainerRoot>
)
