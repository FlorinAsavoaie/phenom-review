import React from 'react'
import { Drawer } from '@hindawi/phenom-ui'

import { DrawerTitle } from './title'
import { DrawerContent } from './content'
import { DrawerFooter } from './footer'
import { ExpertiseContextProvider } from './expertise-context'

type EditExpertiseDrawerProps = {
  visible: boolean
  onClose: () => void
}

export const EditExpertiseDrawer = ({
  visible,
  onClose,
}: EditExpertiseDrawerProps): JSX.Element => (
  <ExpertiseContextProvider>
    <Drawer
      bodyStyle={{ paddingTop: '14px' }}
      closable={false}
      destroyOnClose
      footer={<DrawerFooter onClose={onClose} />}
      footerStyle={{ paddingTop: '56px' }}
      headerStyle={{
        padding: '40px',
        paddingBottom: 0,
        borderBottom: 0,
      }}
      keyboard={false}
      maskClosable={false}
      onClose={onClose}
      placement="right"
      title={<DrawerTitle />}
      visible={visible}
      width={600}
    >
      <DrawerContent />
    </Drawer>
  </ExpertiseContextProvider>
)
