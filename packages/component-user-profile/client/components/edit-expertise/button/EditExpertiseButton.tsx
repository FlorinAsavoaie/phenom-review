import React from 'react'

import { Button } from './styles'
import { EditExpertiseDrawer } from './../drawer'
import { useDrawerVisibility } from './useDrawerVisibility'

const BUTTON_LABEL = 'Edit'

export const EditExpertiseButton = (): JSX.Element => {
  const { isVisible, showDrawer, closeDrawer } = useDrawerVisibility()

  return (
    <>
      <Button onClick={showDrawer} type="link">
        {BUTTON_LABEL}
      </Button>
      <EditExpertiseDrawer onClose={closeDrawer} visible={isVisible} />
    </>
  )
}
