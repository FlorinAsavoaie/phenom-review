import { Button as BaseButton } from '@hindawi/phenom-ui'
import styled from 'styled-components'

export const Button = styled(BaseButton)`
  font-weight: normal;
`
