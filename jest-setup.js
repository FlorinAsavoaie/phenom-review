import '@testing-library/jest-dom/extend-expect'

/* eslint-disable @typescript-eslint/no-unused-vars */
const { configure } = require('@testing-library/react')

console.error = _ => undefined
console.warn = _ => undefined

configure({
  testIdAttribute: 'data-test-id',
})

jest.setTimeout(10000) // in milliseconds

Object.assign(global, require('@pubsweet/errors'))

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn(),
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})
