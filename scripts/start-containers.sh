#!/bin/bash

set -e

images=("service-editor-suggestion" "app-gql-gateway")
missingImages=()

for image in "${images[@]}"
do
  if ! docker image ls | grep -q "$image"; then
    missingImages+=("$image")
  fi
done

missingImagesCount=${#missingImages[@]}

if (( missingImagesCount > 0 )); then
  echo "The following images are missing: ${missingImages[*]} from your machine"
  echo "Please login with HindawiThinslices & HindawiDevelopment AWS accounts to be able to fetch them"
  aws-azure-login --mode gui --profile HindawiThinslices
  account=$(aws sts get-caller-identity --profile HindawiThinslices --query "Account" --output text) 
  echo account $account
  ./scripts/docker-login.sh HindawiThinslices eu-west-1 $account  

  aws-azure-login --mode gui --profile HindawiDevelopment
  account=$(aws sts get-caller-identity --profile HindawiDevelopment --query "Account" --output text) 
  echo account $account
  ./scripts/docker-login.sh HindawiDevelopment eu-west-1 $account 
fi

docker-compose up --wait localstack postgres gql-schema-registry app-gql-gateway service-editor-suggestion
