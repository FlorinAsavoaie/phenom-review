#!/bin/bash
set -e

echo "REVIEW SETUP"

echo "1. Environment files setup"
./scripts/generate-env-files.sh

echo "2. Retrieve dependencies"
yarn

echo "3. Build"
yarn build

echo "4. Database setup"
docker-compose up -d postgres
docker-compose exec -T postgres createdb -U ${USER} review
docker-compose exec -T postgres createdb -U ${USER} editor_suggestion
yarn migrate
yarn seeddb
yarn setup-pgboss
docker-compose stop postgres

echo "5. Map hosts to localhost"
echo "We need to map some endpoints to localhost. For this we are appending these associations to /etc/hosts"
echo "If needed please provide credentials to be able to update /etc/hosts"
sudo -S ./scripts/map-to-localhost.sh xpub-review-localstack