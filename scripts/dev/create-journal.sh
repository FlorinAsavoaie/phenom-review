#!/bin/sh

# Creates a journal in the localhost/review database and maps all article types to it

psql -h localhost -p 5432 -d review -c "insert into journal(created, updated, name, publisher_name, code, email, issn, apc, is_active, activation_date, peer_review_model_id) values(now(), now(), 'Local Journal', 'DevelopmentTeam', 'LCJ', (select email from identity where surname = 'Admin' limit 1), 'localissn', 99999, true, now(), (select id from peer_review_model where name = 'Single Tier Academic Editor' limit 1));"
psql -h localhost -p 5432 -d review -c "\
    insert into journal_article_type(created, updated, journal_id, article_type_id)\
     select now(), now(), (select id from journal where name = 'Local Journal' order by limit 1), id from article_type"

