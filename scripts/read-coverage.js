/* eslint-disable no-console */

const fs = require('fs')
const { parse } = require('node-html-parser')

// * Read coverage index file

fs.readFile('coverage/lcov-report/index.html', 'utf8', (err, data) => {
  if (err) {
    console.error(err)
    return
  }

  const root = parse(data)
  console.log('Coverage overall:', root.querySelectorAll('span.strong')[3].text)
})
