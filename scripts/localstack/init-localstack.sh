#!/bin/bash
set -e

echo "########### Setting up localstack profile ###########"

aws configure set aws_access_key_id access_key --profile=localstack
aws configure set aws_secret_access_key secret_key --profile=localstack

export AWS_DEFAULT_REGION=eu-west-1
export AWS_DEFAULT_PROFILE=localstack

echo "########### Setting SQS names as env variables ###########"

PHENOM_TOPIC=test1

REVIEW_QUEUE=review-queue
EDITOR_SUGGESTION_QUEUE=editor-suggestion-queue
MALWARE_QUEUE=phenom-event-queue-sqs

REVIEW_DLQ=review-dlq
EDITOR_SUGGESTION_DLQ=editor-suggestion-dlq
MALWARE_QUEUE_DLQ=phenom-event-queue-sqs-dlq


echo "########### Creating DLQ ###########"

awslocal sqs create-queue --queue-name $REVIEW_DLQ
awslocal sqs create-queue --queue-name $EDITOR_SUGGESTION_DLQ
awslocal sqs create-queue --queue-name $MALWARE_QUEUE_DLQ

echo "########### ARN for DLQ ###########"

REVIEW_DLQ_ARN=$(awslocal sqs get-queue-attributes\
                  --attribute-name QueueArn --queue-url=http://localhost:4566/000000000000/"$REVIEW_DLQ"\
                  |  sed 's/"QueueArn"/\n"QueueArn"/g' | grep '"QueueArn"' | awk -F '"QueueArn":' '{print $2}' | tr -d '"' | xargs)

EDITOR_SUGGESTION_DLQ_ARN=$(awslocal sqs get-queue-attributes\
                  --attribute-name QueueArn --queue-url=http://localhost:4566/000000000000/"$EDITOR_SUGGESTION_DLQ"\
                  |  sed 's/"QueueArn"/\n"QueueArn"/g' | grep '"QueueArn"' | awk -F '"QueueArn":' '{print $2}' | tr -d '"' | xargs)

MALWARE_DLQ_ARN=$(awslocal sqs get-queue-attributes\
                  --attribute-name QueueArn --queue-url=http://localhost:4566/000000000000/"$MALWARE_QUEUE_DLQ"\
                  |  sed 's/"QueueArn"/\n"QueueArn"/g' | grep '"QueueArn"' | awk -F '"QueueArn":' '{print $2}' | tr -d '"' | xargs)

echo "########### Creating Source queue ###########"

awslocal sqs create-queue --queue-name $REVIEW_QUEUE \
     --attributes '{
                   "RedrivePolicy": "{\"deadLetterTargetArn\":\"'"$REVIEW_DLQ_ARN"'\",\"maxReceiveCount\":\"3\"}",
                   "VisibilityTimeout": "10"
                   }'
awslocal sqs create-queue --queue-name $EDITOR_SUGGESTION_QUEUE \
     --attributes '{
                   "RedrivePolicy": "{\"deadLetterTargetArn\":\"'"$EDITOR_SUGGESTION_DLQ_ARN"'\",\"maxReceiveCount\":\"3\"}",
                   "VisibilityTimeout": "10"
                   }'
awslocal sqs create-queue --queue-name $MALWARE_QUEUE \
     --attributes '{
                   "RedrivePolicy": "{\"deadLetterTargetArn\":\"'"$MALWARE_DLQ_ARN"'\",\"maxReceiveCount\":\"3\"}",
                   "VisibilityTimeout": "10"
                   }'

echo "########### Listing queues ###########"
awslocal sqs list-queues

echo "########### Listing Source SQS Attributes ###########"

awslocal sqs get-queue-attributes\
                  --attribute-name All --queue-url=http://localhost:4566/000000000000/"$REVIEW_QUEUE"
awslocal sqs get-queue-attributes\
                  --attribute-name All --queue-url=http://localhost:4566/000000000000/"$EDITOR_SUGGESTION_QUEUE"
awslocal sqs get-queue-attributes\
                  --attribute-name All --queue-url=http://localhost:4566/000000000000/"$MALWARE_QUEUE"


echo "########### Creating topic ###########"
awslocal sns create-topic --name=${PHENOM_TOPIC}

echo "########### Subscribing to topic ###########"

awslocal sns subscribe \
    --topic-arn arn:aws:sns:${AWS_DEFAULT_REGION}:000000000000:${PHENOM_TOPIC} \
    --protocol sqs \
    --notification-endpoint arn:aws:sqs:${AWS_DEFAULT_REGION}:000000000000:${REVIEW_QUEUE}
awslocal sns subscribe \
    --topic-arn arn:aws:sns:${AWS_DEFAULT_REGION}:000000000000:${PHENOM_TOPIC} \
    --protocol sqs \
    --notification-endpoint arn:aws:sqs:${AWS_DEFAULT_REGION}:000000000000:${EDITOR_SUGGESTION_QUEUE}
awslocal sns subscribe \
    --topic-arn arn:aws:sns:${AWS_DEFAULT_REGION}:000000000000:${PHENOM_TOPIC} \
    --protocol sqs \
    --notification-endpoint arn:aws:sqs:${AWS_DEFAULT_REGION}:000000000000:${MALWARE_QUEUE}

echo "########### Creating buckets ###########"
# Keep this bucket creation at the end of the initialization, we use it to determine the healthiness on localstack container
awslocal s3 mb s3://large-events

echo "LocalStack initialized"


