## LocalStack

### Understanding LocalStack

https://docs.localstack.cloud/localstack/

### Persistence Mechanism

When starting the Community version of LocalStack the persistence mechanism is based on a simplistic record-and-replay approach. That is, API calls are recorded while the application is running, which then are replayed once the application reboots.

Example:
`2021-09-27T14:38:07:INFO:localstack.utils.persistence: Restored 1 API calls from persistent file: /tmp/localstack/data/recorded_api_calls.json`

More details here:
https://docs.localstack.cloud/localstack/persistence-mechanism/#persistence-mechanism---community-version

The default behaviour for our configuration is NO PERSISTANCE. To enable persistance go to ./docker-compose.yml file and set `LEGACY_PERSISTENCE` to true. Recorded API calls will be at: `../tmp/localstack/data/`

### Useful API calls:

- Create Queue Locally:
  `awslocal sqs create-queue --queue-name [YOUR_QUEUE]`

- List SQS Queues:
  `awslocal sqs list-queues`

- Read SQS Queue Messages:
  `awslocal sqs receive-message --queue-url http://localhost:4566/000000000000/[YOUR_QUEUE]`

- Create SNS Topic:
  `awslocal sns create-topic --name [YOUR_TOPIC]`

- Subscribe SQS to SNS Topic:
  `awslocal sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:[YOUR_TOPIC] --protocol sqs --notification-endpoint http://localhost:4566/000000000000/[YOUR_QUEUE]`

- List SNS Subscriptions:
  `awslocal sns list-subscriptions`

- SNS Publish Message To Topic:
  `awslocal sns publish --topic-arn arn:aws:sns:eu-west-1:000000000000:[YOUR_TOPIC] --message '[YOUR_MESSAGE]'`

- S3 Create Bucket:
  `awslocal s3 mb s3://[YOUR_BUCKET]`

- S3 List Buckets:
  `awslocal s3 ls`

- List All Prefixes And Objects In A Bucket
  `awslocal s3 ls s3://[YOUR_BUCKET]`
